#include "EngineSettings.h"

namespace tezcat
{

	bool EngineSettings::FULL_SCREEN = false;
	unsigned int EngineSettings::SCREEN_WIDTH = 960;
	unsigned int EngineSettings::SCREEN_HEIGHT = 640;
	unsigned int EngineSettings::MAX_FBO_COLOR_ATTACHMENT = 8;

}

