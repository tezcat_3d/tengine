#pragma once

namespace tezcat
{
	class EngineSettings
	{
	public:

		static bool FULL_SCREEN;
		static unsigned int SCREEN_WIDTH;
		static unsigned int SCREEN_HEIGHT;
		static unsigned int MAX_FBO_COLOR_ATTACHMENT;
	};
}

