#include "GLCamera.h"
#include "GLConfig.h"
#include "GLSystemEvent.h"
#include "GLMessager.h"
#include "GLSLDataIndex.h"


namespace tezcat
{
	GLCamera::GLCamera(float fovy, float window_width, float window_height, float near, float far) :
		m_HeadUp(0.0f, 1.0f, 0.0f), m_Yaw(0), m_Pitch(0.0f), m_Front(0.0f, 0.0f, -1.0f),
		m_Speed(0), m_MouseSensitivity(0.25f),
		m_Fovy(fovy), m_WindowHeight(window_height), m_WindowWidth(window_width), m_Near(near), m_Far(far),
		m_Aspect(window_width / window_height),
		m_Destroyed(false), m_Mask(GLConfig::Masks::Layer1)
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fovy), m_Aspect, m_Near, m_Far);
//		this->updataCamera();
	}

	GLCamera::~GLCamera()
	{

	}

	void GLCamera::render()
	{
		m_ViewMatrix = glm::lookAt(m_Position, m_Position + m_Front, m_Up);
	}



	void GLCamera::reset(float fovy, float window_width, float window_height, float near, float far)
	{
		m_Fovy = fovy;
		m_WindowHeight = window_height;
		m_WindowWidth = window_width;
		m_Near = near;
		m_Far = far;
		m_Aspect = window_width / window_height;
		m_Mask = GLConfig::Masks::Disabled;

		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fovy), m_Aspect, m_Near, m_Far);
	}

	void GLCamera::reset()
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fovy), m_Aspect, m_Near, m_Far);
		this->updataCamera();
	}

	//
	//
	//
	GLCameraManager::GLCameraManager() :
		m_Index(0), m_CurrentScene(nullptr), m_CurrentCamera(nullptr)
	{
		XEventSystem::getInstance()->subscribe<Event_Scene_Replace>(this, [this](XBaseEvent *base)
		{
			Event_Scene_Replace *event = static_cast<Event_Scene_Replace *>(base);
			m_CurrentScene = new Scene();
			m_CurrentScene->name = event->name;

			auto oldScene = m_Scenes.back();
			m_Scenes.pop_back();
			m_ScenesByName.erase(oldScene->name);

			m_Scenes.push_back(m_CurrentScene);
			m_ScenesByName.insert(std::make_pair(event->name, m_CurrentScene));
		});

		XEventSystem::getInstance()->subscribe<Event_Scene_Push>(this, [this](XBaseEvent *base)
		{
			Event_Scene_Push *event = static_cast<Event_Scene_Push *>(base);
			m_CurrentScene = new Scene();
			m_CurrentScene->name = event->name;
			m_Scenes.push_back(m_CurrentScene);
			m_ScenesByName.insert(std::make_pair(event->name, m_CurrentScene));
		});

		XEventSystem::getInstance()->subscribe<Event_Scene_Pop>(this, [this](XBaseEvent *base)
		{
			Event_Scene_Pop *event = static_cast<Event_Scene_Pop *>(base);
			auto oldScene = m_Scenes.back();
			m_Scenes.pop_back();
			m_ScenesByName.erase(oldScene->name);

			if (m_Scenes.empty())
			{
				//退出引擎
			}
			else
			{
				m_CurrentScene = m_Scenes.back();
				m_CurrentCamera = m_CurrentScene->cameras.front();
			}
		});
	}

	GLCameraManager::~GLCameraManager()
	{
		XEventSystem::getInstance()->unSubscribe<Event_Scene_Replace>(this);
		XEventSystem::getInstance()->unSubscribe<Event_Scene_Push>(this);
		XEventSystem::getInstance()->unSubscribe<Event_Scene_Pop>(this);
	}

	TEntity * GLCameraManager::create(const std::string &scene_name, const std::string &name)
	{
		return this->create(scene_name, name, 16/9, 960, 640, 0.1f, 1000.0f);
	}

	TEntity * GLCameraManager::create(const std::string &scene_name, const std::string &name, float fovy, float window_width, float window_height, float near, float far)
	{
		auto e = Singleton<TEntityFactory>::getInstance()->create();

		auto tag = new TEntityTag();
		tag->setIntTag(TEntityType::Camera);
		tag->addStringTag(name);
		e->addStatic(tag);

		auto *memory = new std::aligned_storage<sizeof(GLCamera), std::alignment_of<GLCamera>::value>::type();
		GLCamera *camera = new(memory) GLCamera(fovy, window_width, window_height, near, far);
		camera->m_Index = m_CurrentScene->index++;
		m_CurrentScene->cameras.push_back(camera);
		m_CurrentCamera = camera;
		e->addStatic(camera);

		//配置messager
		auto messager = new GLMessager();
		e->addStatic(messager);
		//获得当前场景所用的shader,寻找需要使用主相机的pass
		auto &sceneConfig = Singleton<SimpleDataManager>::getInstance()->getSceneFile(scene_name);
		for (auto shader : *sceneConfig["shader"].getArray())
		{
			auto &shaderFile = Singleton<SimpleDataManager>::getInstance()->getShaderFile((*shader)().asSTDString());
			if (shaderFile["base"]["needMainCamera"]().asBool())
			{
				/*
				auto group = Singleton<ShaderEngine>::getInstance()->getProgram<true>((*shader)().asSTDString());
				GLSLCamera slcamera(
					group->file->findUniform("myMat4V")->getID(),
					group->file->findUniform("myMat4P")->getID(),
					group->file->findUniform("myVec3VPosition")->getID());
				messager->addRenderCMD(group->list, [=]()
				{
					glUniformMatrix4fv(slcamera.v, 1, GL_FALSE, &camera->m_ViewMatrix[0][0]);
					glUniformMatrix4fv(slcamera.p, 1, GL_FALSE, &camera->m_ProjectionMatrix[0][0]);
					glUniform3fv(slcamera.position, 1, &camera->m_Position[0]);
				});
				*/
			}
		}

		return e;
	}

}