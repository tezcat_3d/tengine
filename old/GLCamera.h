#pragma once


#include <bitset>
#include <string>
#include <unordered_map>
#include "GLConfig.h"
#include "XComponent.h"
#include "Singleton.h"
#include "TEntityConfig.h"

namespace tezcat
{
	class GLCamera : public XComponent<GLCamera>
	{
	public:
		friend class GLCameraManager;

	public:
		GLCamera() {}
		GLCamera(float fovy, float window_width, float window_height, float near, float far);
		~GLCamera();

	public:

		void render();

		void reset(float fovy, float window_width, float window_height, float near, float far);
		void reset();

		void setHeadUp(const glm::vec3 &head_up)
		{
//			m_HeadUp = head_up;
			m_Up = head_up;
		}

		void setHeadUp(const float &x, const float &y, const float &z)
		{
// 			m_HeadUp.x = x;
// 			m_HeadUp.y = y;
// 			m_HeadUp.z = z;
			m_Up.x = x;
			m_Up.y = y;
			m_Up.z = z;
		}

		glm::mat4 &getProjectionMatrix() { return m_ProjectionMatrix; }
		glm::mat4 &getViewMatrix() { return m_ViewMatrix; }


	public:
		void forward(float dt)
		{
			m_Position += m_Front * m_Speed * dt;
		}

		void back(float dt)
		{
			m_Position -= m_Front * m_Speed * dt;
		}

		void left(float dt)
		{
			m_Position -= m_Right * m_Speed * dt;
		}

		void right(float dt)
		{
			m_Position += m_Right * m_Speed * dt;
		}

		void up(float dt)
		{
			m_Position += m_Up * m_Speed * dt;
		}

		void down(float dt)
		{
			m_Position -= m_Up * m_Speed * dt;
		}

	public:
		void onMouse(float x_offset, float y_offset, bool constrain_pitch = true)
		{
			x_offset *= m_MouseSensitivity;
			y_offset *= m_MouseSensitivity;

			m_Yaw += x_offset;
			m_Pitch += y_offset;

			if (constrain_pitch)
			{
				if (m_Pitch > 89.0f)
				{
					m_Pitch = 89.0f;
				}
				if (m_Pitch < -89.0f)
				{
					m_Pitch = -89.0f;
				}
			}

			this->updataCamera();
		}

		void setCameraPostion(float x, float y, float z)
		{
			m_Position.x = x;
			m_Position.y = y;
			m_Position.z = z;
		}

		void setHeadUp(glm::vec3 val) { m_Up = val; }
		void setYaw(float val) { m_Yaw = val; }
		void setPitch(float val) { m_Pitch = val; }
		void setSpeed(float val) { m_Speed = val; }

	private:
		//************************************
		// 函数名:  updataCamera
		// 返回值:  void
		// 功能:    旋转相机
		//************************************
		void updataCamera()
		{
			glm::vec3 fornt;
			fornt.x = glm::cos(glm::radians(m_Yaw)) * glm::cos(glm::radians(m_Pitch));
			fornt.y = glm::sin(glm::radians(m_Pitch));
			fornt.z = glm::sin(glm::radians(m_Yaw)) * glm::cos(glm::radians(m_Pitch));

			m_Front = glm::normalize(fornt);

			m_Right = glm::normalize(glm::cross(m_Front, m_Up));
//			m_Up = glm::normalize(glm::cross(m_Right, m_Front));
		}

	private:

		glm::mat4 m_ViewMatrix;
		glm::mat4 m_ProjectionMatrix;

		glm::vec3 m_Position;
		glm::vec3 m_Front;
		glm::vec3 m_Up;
		glm::vec3 m_Right;
		glm::vec3 m_HeadUp;
		float m_WindowWidth, m_WindowHeight;
		float m_Near, m_Far;
		float m_Fovy;
		float m_Aspect;
		float m_Speed;
		float m_MouseSensitivity;
		float m_Pitch, m_Yaw;
		bool m_Destroyed;
		unsigned int m_Index;

		GLConfig::Masks m_Mask;
	};

	//
	//
	//
	class GLCameraManager
	{
		struct Scene
		{
			Scene() : index(0) {}

			size_t index;
			std::string name;
			std::vector<GLCamera *> cameras;
		};

	public:
		GLCameraManager();
		~GLCameraManager();

		TEntity *create(const std::string &scene_name, const std::string &name);
		TEntity *create(const std::string &scene_name, const std::string &name, float fovy, float window_width, float window_height, float near, float far);

		GLCamera * getCurrentCamera() const { return m_CurrentCamera; }

		void recycleCamera(const std::string &name);

	private:
//		GLCamera *create(const std::string &name, float fovy, float window_width, float window_height, float near, float far);

	private:
		size_t m_Index;
		size_t m_SceneIndex;

		std::vector<Scene *> m_Scenes;
		std::unordered_map<std::string, Scene *> m_ScenesByName;

		Scene *m_CurrentScene;
		GLCamera *m_CurrentCamera;
	};
}