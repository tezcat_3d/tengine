#include "GLDeferredRendering.h"

namespace tezcat
{
	GLDeferredRendering::GLDeferredRendering()
	{

	}

	GLDeferredRendering::~GLDeferredRendering()
	{

	}

	void GLDeferredRendering::init()
	{
// 		m_FBO.bind();
//
// 		//位置buffer
// 		glGenTextures(1, &m_Position);
// 		glBindTexture(GL_TEXTURE_2D, m_Position);
// 		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_BufferWidth, m_BufferHeight, 0, GL_RGB, GL_FLOAT, 0);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
// 		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_Position, 0);
//
// 		//法线buffer
// 		glGenTextures(1, &m_Normal);
// 		glBindTexture(GL_TEXTURE_2D, m_Normal);
// 		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_BufferWidth, m_BufferHeight, 0, GL_RGB, GL_FLOAT, 0);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
// 		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_Normal, 0);
//
// 		//颜色buffer
// 		glGenTextures(1, &m_AlbedoSpec);
// 		glBindTexture(GL_TEXTURE_2D, m_AlbedoSpec);
// 		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_BufferWidth, m_BufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
// 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
// 		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_AlbedoSpec, 0);
//
// 		//连接节点
// 		GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
// 		glDrawBuffers(3, attachments);
//
// 		//深度buffer
// 		glGenRenderbuffers(1, &m_Depth);
// 		glBindRenderbuffer(GL_RENDERBUFFER, m_Depth);
// 		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_BufferWidth, m_BufferHeight);
// 		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_Depth);
//
// 		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void GLDeferredRendering::render(GLProgram *program)
	{

	}
}