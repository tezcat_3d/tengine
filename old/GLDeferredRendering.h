#pragma once

#ifdef WIN32
#include "glew.h"
#endif // WIN32

#include "GLFrameBuffer.h"

namespace tezcat
{
	class GLProgram;
	class GLDeferredRendering
	{
	public:
		GLDeferredRendering();
		~GLDeferredRendering();

		void init();

		void bind() { m_FBO.bind(); }
		void unBind() { m_FBO.unbind(); }

		void render(GLProgram *program);

	private:
		GLFramebuffer m_FBO;

		size_t m_BufferWidth;
		size_t m_BufferHeight;

	private:

	};
}
