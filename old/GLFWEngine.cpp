#include "GLFWEngine.h"
#include <iostream>
#include "GLConfig.h"
#include "GLError.h"
#include <cassert>

#include "GLRenderEngine.h"
#include "MainEngine.h"


namespace tezcat
{
//	float GLFWEngine::DeltaTime = 0;

	GLFWEngine::GLFWEngine() :
		m_CloseEngine(true),
		m_RunningWindow(nullptr),
		m_isInit(false)
	{
		Singleton<tezcat::MainEngine>::init();
		Singleton<tezcat::GLRenderEngine>::init();
	}

	GLFWEngine::~GLFWEngine()
	{
		Singleton<tezcat::GLRenderEngine>::deleteInstance();
		Singleton<tezcat::MainEngine>::deleteInstance();
		glfwTerminate();
	}

	void GLFWEngine::oneFrame()
	{
		static double lastTime = glfwGetTime();

		double currentTime = glfwGetTime();
		GLRenderEngine::DeltaTime = float(currentTime - lastTime);

// 		if (GLRenderEngine::DeltaTime >= 1.0f / 60.0f)
// 		{
			Singleton<MainEngine>::getInstance()->logic(m_RenderEngine);

			m_RenderEngine->render();

			glfwSwapBuffers(m_RunningWindow);
			glfwPollEvents();
			lastTime = currentTime;
//		}
	}

	void GLFWEngine::loop()
	{
		while (!m_CloseEngine)
		{
			this->oneFrame();
		}
	}

	GLFWwindow * GLFWEngine::createWindow(const std::string &name, int width, int height)
	{
		assert(m_isInit == true);

		auto window = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);
		if (window != nullptr)
		{
			auto it = m_Windows.find(name);
			if (it != m_Windows.end())
			{
				glfwDestroyWindow(it->second);
				it->second = window;
			}
			else
			{
				m_Windows.insert(std::make_pair(name, window));
			}

			if (m_RunningWindow == nullptr)
			{
				m_RunningWindow = window;
			}
		}

		return window;
	}

	void GLFWEngine::initEngine()
	{
		glfwSetErrorCallback(errorCallBack);

		if (glfwInit() == GL_FALSE)
		{
			std::cout << "GL Error : glfw init error" << std::endl;
			throw std::logic_error("glfw init error!!!");
		}

		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		m_CloseEngine = false;
		m_isInit = true;
	}

	bool GLFWEngine::setKeyHandler(const std::string &window_name)
	{
		auto window = m_Windows.find(window_name);
		if (window != m_Windows.end())
		{
			glfwSetKeyCallback(window->second, keyCallBack);
			return true;
		}

		return false;
	}

	bool GLFWEngine::setMouseHandler(const std::string &window_name)
	{
		auto window = m_Windows.find(window_name);
		if (window != m_Windows.end())
		{
			glfwSetCursorPosCallback(window->second, mouseCallBack);
			glfwSetInputMode(window->second, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			return true;
		}

		return false;
	}

	bool GLFWEngine::setContextCurrent(const std::string &window_name)
	{
		auto window = m_Windows.find(window_name);
		if (window != m_Windows.end())
		{
			glfwMakeContextCurrent(window->second);
			glfwSetWindowSizeCallback(window->second, windowSizeCallBack);
			m_WindowSize = [](GLFWwindow *window, int width, int height)
			{
				glViewport(0, 0, width, height);
			};
			this->initExtra();
			return true;
		}

		return false;
	}


	void GLFWEngine::initExtra()
	{
		glewExperimental = GL_TRUE;
		auto err = glewInit();
		if (err != GLEW_OK)
		{
			std::cout << "GL Error : glew init error : " << err << std::endl;
		}
		else
		{
// 			glEnable(GL_BLEND);
// 			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
// 			tezcat::error("GL_CULL_FACE");
//			glEnable(GL_DEPTH_TEST);
//			tezcat::error("GL_DEPTH_TEST");
//			glDepthFunc(GL_LESS);
//			tezcat::error("GL_LESS");
//			glEnable(GL_CULL_FACE);
//			glClearDepthf(1.0);
			Singleton<tezcat::GLRenderEngine>::getInstance()->init();
			
			glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		}
	}

	auto GLFWEngine::getFrameSize(const std::string &window_name) -> std::tuple<int, int>
	{
		return std::tuple<int, int>(0, 0);
	}

	void GLFWEngine::switchRunningWindow(const std::string &window_name)
	{

	}

    GLFWwindow * GLFWEngine::findWindow(const std::string &window_name)
	{
		auto window = m_Windows.find(window_name);
		if (window != m_Windows.end())
		{
			return window->second;
		}

		return nullptr;
	}


//
	//
	//		
	void GLFWEngine::errorCallBack(int error, const char *description)
	{
		std::cout << "GL Error : Code<" << error << ">::" << description << std::endl;
	}

	void GLFWEngine::keyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		tezcat::Singleton<GLFWEngine>::getInstance()->m_KeyBoard(window, key, scancode, action, mods);
	}

	void GLFWEngine::windowSizeCallBack(GLFWwindow* window, int width, int height)
	{
		tezcat::Singleton<GLFWEngine>::getInstance()->m_WindowSize(window, width, height);
	}

	void GLFWEngine::mouseCallBack(GLFWwindow* window, double xops, double ypos)
	{
		tezcat::Singleton<GLFWEngine>::getInstance()->m_MouseMove(window, xops, ypos);
	}

	void GLFWEngine::onWindowSizeChange()
	{
		int width, height;
		glfwGetFramebufferSize(m_RunningWindow, &width, &height);
		glViewport(0, 0, width, height);
	}
}