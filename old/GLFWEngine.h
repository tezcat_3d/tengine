#pragma once

//#define GLEW_STATIC

#include "glew.h"
#include "GLFW/glfw3.h"
#include "Singleton.h"
#include "RenderEngine.h"

#include <string>
#include <unordered_map>
#include <functional>
#include <vector>

namespace tezcat
{
// 	class GLScene;
// 	class GLCameraManager;
	class GLFWEngine
	{
	public:
		//************************************
		// Method:    GLEngine
		// FullName:  tezcat::GLEngine::GLEngine
		// Access:    private 
		// Returns:   
		// Qualifier:
		//************************************
		GLFWEngine();

		//************************************
		// Method:    ~GLEngine
		// FullName:  tezcat::GLEngine::~GLEngine
		// Access:    public 
		// Returns:   
		// Qualifier:
		//************************************
		~GLFWEngine();

		//************************************
		// Method:    initEngine
		// FullName:  tezcat::GLEngine::initEngine
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		void initEngine();

		//************************************
		// Method:    onWindowSizeChange
		// FullName:  tezcat::GL::GLFWEngine::onWindowSizeChange
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		void onWindowSizeChange();

		//************************************
		// Method:    initExtra
		// FullName:  tezcat::GLEngine::initExtra
		// Access:    virtual public 
		// Returns:   void
		// Qualifier:
		//************************************
		virtual void initExtra();

		//************************************
		// Method:    setContextCurrent
		// FullName:  tezcat::GLEngine::setContextCurrent
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: GLFWwindow * window
		//************************************
		void setContextCurrent(GLFWwindow *window) { glfwMakeContextCurrent(window); }

		//************************************
		// Method:    setContextCurrent
		// FullName:  tezcat::GLEngine::setContextCurrent
		// Access:    public 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		bool setContextCurrent(const std::string &window_name);

		//************************************
		// Method:    createWindow
		// FullName:  tezcat::GLEngine::createWindow
		// Access:    public 
		// Returns:   GLFWwindow *
		// Qualifier:
		// Parameter: const std::string & name
		// Parameter: int width
		// Parameter: int height
		//************************************
		GLFWwindow *createWindow(const std::string &name, int width, int height);

		//************************************
		// Method:    oneFrame
		// FullName:  tezcat::GLEngine::oneFrame
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		void oneFrame();

		//************************************
		// Method:    engineShoulClosed
		// FullName:  tezcat::GLEngine::engineShoulClosed
		// Access:    public 
		// Returns:   int
		// Qualifier:
		//************************************
		bool engineShoulClosed()
		{
			return m_CloseEngine;
		}

		//************************************
		// Method:    closeWindow
		// FullName:  tezcat::GLEngine::closeWindow
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		void closeWindow(const std::string &window_name)
		{
			auto window = m_Windows.find(window_name);
			if (window != m_Windows.end())
			{
				this->closeWindow(window->second);
			}
		}

		//************************************
		// Method:    closeWindow
		// FullName:  tezcat::GLEngine::closeWindow
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: GLFWwindow * window
		//************************************
		void closeWindow(GLFWwindow *window)
		{
			glfwDestroyWindow(window);
		}

		//************************************
		// Method:    closeEngine
		// FullName:  tezcat::GLEngine::closeEngine
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		void closeEngine()
		{
			m_CloseEngine = true;
// 			for (auto window : m_Windows)
// 			{
// 				glfwDestroyWindow(window.second);
// 			}
// 			glfwTerminate();
		}

		//************************************
		// Method:    switchRunningWindow
		// FullName:  tezcat::GLEngine::switchRunningWindow
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		void switchRunningWindow(const std::string &window_name);

		void loop();

// 		void pushScene(GLScene *scene);
// 
// 		void popScene();

		void setRenderEngine(RenderEngine *engine)
		{
			m_RenderEngine = engine;
		}

	public:
		auto getFrameSize(const std::string &window_name)->std::tuple<int, int>;

	public:
		//************************************
		// Method:    setKeyHandler
		// FullName:  tezcat::GLEngine::setKeyHandler
		// Access:    private 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & window_name
		// Parameter: const std::function<void
		// Parameter: GLFWwindow *
		// Parameter: int
		// Parameter: int
		// Parameter: int
		// Parameter: int
		// Parameter: > & function
		// Copy This: GLFWwindow * window, int key, int scancode, int action, int mods
		//************************************
		bool setKeyHandler(const std::string &window_name, const std::function<void(GLFWwindow *, int, int, int, int)> &function)
		{
			if (setKeyHandler(window_name))
			{
				m_KeyBoard = function;
				return true;
			}
			return false;
		}

		bool setMouseHandler(const std::string &window_name, const std::function<void(GLFWwindow *, double, double)> &function)
		{
			if (setMouseHandler(window_name))
			{
				m_MouseMove = function;
				return true;
			}
			return false;
		}

	public:
		//************************************
		// Method:    openVsync
		// FullName:  tezcat::GLEngine::openVsync
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: bool open
		//************************************
		void openVsync(bool open)
		{
			glfwSwapInterval(open);
		}

	private:
		//************************************
		// Method:    mouseCallBack
		// FullName:  tezcat::GLFWEngine::mouseCallBack
		// Access:    private static 
		// Returns:   void
		// Qualifier:
		// Parameter: GLFWwindow * window
		// Parameter: double xops
		// Parameter: double ypos
		//************************************
		static void mouseCallBack(GLFWwindow* window, double xops, double ypos);

		//************************************
		// Method:    errorCallBack
		// FullName:  tezcat::GLEngine::errorCallBack
		// Access:    private static 
		// Returns:   void
		// Qualifier:
		// Parameter: int error
		// Parameter: const char * description
		//************************************
		static void errorCallBack(int error, const char *description);

		//************************************
		// Method:    keyCallBack
		// FullName:  tezcat::GLEngine::keyCallBack
		// Access:    private static 
		// Returns:   void
		// Qualifier:
		// Parameter: GLFWwindow * window
		// Parameter: int key
		// Parameter: int scancode
		// Parameter: int action
		// Parameter: int mods
		//************************************
		static void keyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods);

		//************************************
		// Method:    windowSizeCallBack
		// FullName:  tezcat::GL::GLFWEngine::windowSizeCallBack
		// Access:    private static 
		// Returns:   void
		// Qualifier:
		// Parameter: GLFWwindow * window
		// Parameter: int width
		// Parameter: int height
		//************************************
		static void windowSizeCallBack(GLFWwindow* window, int width, int height);

	private:


		//************************************
		// Method:    setKeyHandler
		// FullName:  tezcat::GLEngine::setKeyHandler
		// Access:    private 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		bool setKeyHandler(const std::string &window_name);

		//************************************
		// Method:    setMouseHandler
		// FullName:  tezcat::GLFWEngine::setMouseHandler
		// Access:    private 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		bool setMouseHandler(const std::string &window_name);

		//************************************
		// Method:    findWindow
		// FullName:  tezcat::GLEngine::findWindow
		// Access:    private 
		// Returns:   GLFWwindow *
		// Qualifier:
		// Parameter: const std::string & window_name
		//************************************
		GLFWwindow *findWindow(const std::string &window_name);

	private:
		bool m_CloseEngine;
		GLFWwindow *m_RunningWindow;
		std::unordered_map<std::string, GLFWwindow *> m_Windows;
		RenderEngine *m_RenderEngine;
// 		std::vector<GLScene *> m_Scenes;
// 		std::vector<GLCameraManager *> m_Cameras;

		std::function<void(GLFWwindow *, int, int, int, int)> m_KeyBoard;
		std::function<void(GLFWwindow *, int, int)> m_WindowSize;
		std::function<void(GLFWwindow *, double, double)> m_MouseMove;

	public:
		std::function<void()> OneFrame;
//		static float DeltaTime;

	private:
		bool m_isInit;
	};
}
