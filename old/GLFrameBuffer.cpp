#include "GLFrameBuffer.h"
#include "EngineSettings.h"

namespace tezcat
{
	GLFramebuffer::GLFramebuffer():
		m_DepthTexture(nullptr), m_StencilTexture(nullptr), m_DepthAndStencilTexture(nullptr)
	{
		glGenFramebuffers(1, &m_FBOID);
		glBindFramebuffer(GL_FRAMEBUFFER, m_FBOID);
		m_ColorTexture.resize(EngineSettings::MAX_FBO_COLOR_ATTACHMENT);
	}

	GLFramebuffer::~GLFramebuffer()
	{
		glDeleteFramebuffers(1, &m_FBOID);
	}
}

