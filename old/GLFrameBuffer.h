#pragma once

#include "glew.h"
#include "GLTexture.h"

#include <vector>

namespace tezcat
{
	class GLFramebuffer
	{
	public:
		GLFramebuffer();
		~GLFramebuffer();

		void unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
		void bind() { glBindFramebuffer(GL_FRAMEBUFFER, m_FBOID); }

		void bind2Draw()
		{
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBOID);
		}

		void bind2Read()
		{
			glBindFramebuffer(GL_READ_FRAMEBUFFER, m_FBOID);
		}

		void bindTextrue2D(GLTexture *tex, unsigned int &index)
		{
			m_AttachmentPoint.push_back(GL_COLOR_ATTACHMENT0 + index);
			m_ColorTexture[index] = tex;
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, GL_TEXTURE_2D, tex->getTexID(), 0);
		}

		void bindDepth(GLTexture *tex)
		{
			if (m_DepthTexture)
			{
				return;
			}
			m_DepthTexture = tex;
			m_AttachmentPoint.push_back(GL_DEPTH_ATTACHMENT);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex->getTexID(), 0);
		}

		void bindStencil(GLTexture *tex)
		{
			if (m_StencilTexture)
			{
				return;
			}
			m_StencilTexture = tex;
			m_AttachmentPoint.push_back(GL_STENCIL_ATTACHMENT);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, tex->getTexID(), 0);
		}

		void bindDepthAndStencil(GLTexture *tex)
		{
			if (m_DepthAndStencilTexture)
			{
				return;
			}
			m_DepthAndStencilTexture = tex;
			m_AttachmentPoint.push_back(GL_DEPTH_STENCIL_ATTACHMENT);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, tex->getTexID(), 0);
		}

		void bindPoint()
		{
			glDrawBuffers(m_AttachmentPoint.size(), m_AttachmentPoint.data());
		}

		void checkFBO()
		{
			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			switch (status)
			{
			case GL_FRAMEBUFFER_COMPLETE: break;
			case GL_FRAMEBUFFER_UNDEFINED: break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: break;
			case GL_FRAMEBUFFER_UNSUPPORTED: break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: break;
			}
		}

		const GLuint &getFBOID() const { return m_FBOID; }

	private:
		GLuint m_FBOID;
		std::vector<GLenum> m_AttachmentPoint;
		std::vector<GLTexture *> m_ColorTexture;
		GLTexture *m_DepthTexture;
		GLTexture *m_StencilTexture;
		GLTexture *m_DepthAndStencilTexture;
	};



}
