#include "GLLight.h"
#include "GLTransform.h"
#include "GLMessager.h"
#include "ShaderEngine.h"
#include "SimpleDataManager.h"

namespace tezcat
{
	GLLightManager::GLLightManager()
	{

	}

	GLLightManager::~GLLightManager()
	{

	}

	TEntity * GLLightManager::create(const std::string &name)
	{
		auto e = Singleton<TEntityFactory>::getInstance()->create();
		auto tag = new TEntityTag();
		tag->setIntTag(TEntityType::Light);
		e->addStatic(tag);
		e->addStatic(new GLTransform());
		e->addStatic(new GLMessager());
		e->addStatic(new GLLight());
		return e;
	}

	TEntity * GLLightManager::createDirectionalLight(const std::string &shader, const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular, const glm::vec3 &direction)
	{
		Directional *ads = new Directional();
		ads->ambient = ambient;
		ads->diffuse = diffuse;
		ads->specular = specular;
		ads->direction = direction;

		auto e = Singleton<TEntityFactory>::getInstance()->create();
		auto tag = new TEntityTag();
		tag->setIntTag(TEntityType::Light);
		e->addStatic(tag);
		e->addStatic(new GLTransform());
		e->addStatic(new GLMessager());
		e->addStatic(new GLLight(ads));

		auto messager = e->getStatic<GLMessager>();
//		messager->addRenderCMD()


		return e;
	}

	void GLLight::setDirectionalLight(const GLSLDirectionalLight &light, const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular, const glm::vec3 &direction)
	{
		auto dlight = new Directional();
		
	}

}