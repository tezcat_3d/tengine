#pragma once

#include <list>
#include "glm/glm.hpp"
#include "XComponent.h"
#include "TEntityConfig.h"
#include "GLConfig.h"
#include "GLSLDataIndex.h"

namespace tezcat
{
	struct BaseLight
	{
		virtual ~BaseLight() {}
		virtual void onRender() = 0;
	};

	struct Directional : public BaseLight
	{
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		glm::vec3 direction;

		virtual void onRender()
		{
// 			program->setUniform3fv(SL_DLight_Ambient, &ambient[0]);
// 			program->setUniform3fv(SL_DLight_Diffuse, &diffuse[0]);
// 			program->setUniform3fv(SL_DLight_Specular, &specular[0]);
// 			program->setUniform3fv(SL_DLight_Specular, &direction[0]);
		};
	};


	class GLLight : public XComponent<GLLight>
	{
	public:
		GLLight() : m_Light(nullptr) {}
		GLLight(BaseLight *light) : m_Light(light) {}
		~GLLight() { delete m_Light; }

		void setDirectionalLight(const GLSLDirectionalLight &light, const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular, const glm::vec3 &direction);

	private:
		BaseLight *m_Light;
	};

	class GLLightManager
	{
		enum class LightType : int
		{
			Directional,
			Point,
		};
	public:
		GLLightManager();
		~GLLightManager();

		TEntity *create(const std::string &name);

		TEntity *createDirectionalLight(const std::string &shader, const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular, const glm::vec3 &direction);

		TEntity *createPointLight(glm::vec3 color, float ambient, float diffuse, glm::vec3 position, float constant, float liner, float exp);

		TEntity *createSpotLight(glm::vec3 color, float ambient, float diffuse, glm::vec3 position, float constant, float liner, float exp, glm::vec3 direction, float cutoff);
	};
}