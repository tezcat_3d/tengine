#include "GLMaterial.h"
#include "glew.h"
#include "GLProgram.h"

namespace tezcat
{

	GLMaterial::GLMaterial(
		float emission_r, float emission_g, float emission_b,
		float ambient_r, float ambient_g, float ambient_b,
		float diffuse_r, float diffuse_g, float diffuse_b,
		float specular_r, float specular_g, float specular_b,
		float shininess, float opacity):
		EmissionR(emission_r), EmissionG(emission_g), EmissionB(emission_b),
		AmbientR(ambient_r), AmbientG(ambient_g), AmbientB(ambient_b),
		DiffuseR(diffuse_r), DiffuseG(diffuse_g), DiffuseB(diffuse_b),
		SpecularR(specular_r), SpecularG(specular_g), SpecularB(specular_b),
		Shininess(shininess), Opacity(opacity),
		m_TextureFlags(0)
	{

	}

	GLMaterial::GLMaterial():
		EmissionR(0), EmissionG(0), EmissionB(0),
		AmbientR(0), AmbientG(0), AmbientB(0),
		DiffuseR(0), DiffuseG(0), DiffuseB(0),
		SpecularR(0), SpecularG(0), SpecularB(0),
		Shininess(0), Opacity(0),
		m_TextureFlags(0)
	{

	}

	void GLMaterial::addTexture(GLTexture *tex, const unsigned int &index)
	{
		((unsigned char *)&m_TextureFlags)[index] = 1;
		TextureInfo *t = new TextureInfo(tex, index);
		m_Textures.push_back(t);
	}

	void GLMaterial::removeTexture(const unsigned int &index)
	{
		auto it = m_Textures.resetIterator();
		while (it->hasNext())
		{
			auto c = it->value();
			if (c->index == index)
			{
				m_Textures.remove(c->listNode);
				((unsigned char *)&m_TextureFlags)[index] = 0;
				delete c;
				return;
			}

			it->next();
		}
	}

	void GLMaterial::bind(GLProgram *program)
	{
		if (m_TextureFlags != 0)
		{
			auto it = m_Textures.resetIterator();
			while (it->hasNext())
			{
				auto *info = it->value();
				auto &index = info->index;
				glActiveTexture(GL_TEXTURE0 + index);
				info->texture->bind();
				glUniform1i(program->getTextureIndex(index), index);
			}
		}
	}
}

