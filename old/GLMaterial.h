#pragma once


#include "XComponent.h"
#include "XList.h"

#include "GLTexture.h"

namespace tezcat
{
	class GLProgram;
	class GLMaterial : public XComponent<GLMaterial>
	{
		class TextureInfo
		{
		public:
			TextureInfo(GLTexture *tex, const unsigned int &t):
				index(t), texture(tex), listNode(XList<TextureInfo *>::createNode(this)) {}
			~TextureInfo() {}

			unsigned int index;
			GLTexture *texture;

			XList<TextureInfo *>::Node *listNode;
		};

	public:
		GLMaterial();

		GLMaterial(
			float emission_r, float emission_g, float emission_b,
			float ambient_r, float ambient_g, float ambient_b,
			float diffuse_r, float diffuse_g, float diffuse_b,
			float specular_r, float specular_g, float specular_b,
			float shininess, float opacity);

		bool hasTexture(const unsigned int &texture_index)
		{
			return ((unsigned char *)&m_TextureFlags)[texture_index] == 1;
		}

		void addTexture(GLTexture *tex, const unsigned int &index);

		void removeTexture(const unsigned int &index);

		void bind(GLProgram *program);

	public:
		//自身发光属性
		float EmissionR;
		float EmissionG;
		float EmissionB;

		//环境光属性
		float AmbientR;
		float AmbientG;
		float AmbientB;

		//漫反射属性
		float DiffuseR;
		float DiffuseG;
		float DiffuseB;

		//高光属性
		float SpecularR;
		float SpecularG;
		float SpecularB;

		//高光系数
		float Shininess;
		//不透明度1完全不透明 0透明
		float Opacity;

	private:
		XList<TextureInfo *> m_Textures;
		unsigned int m_TextureFlags;
	};
}

