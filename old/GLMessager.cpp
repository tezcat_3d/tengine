#include "GLMessager.h"
#include "GLConfig.h"
#include "GLRenderQueue.h"

namespace tezcat
{
	int i = 0;

	GLMessager::GLMessager():
		m_VAOID(0),m_VBOID(0),m_EBOID(0),m_DrawMode(GL_TRIANGLES), m_IndexType(GL_UNSIGNED_INT)
	{

	}

	GLMessager::~GLMessager()
	{

	}

	void GLMessager::sendMesh()
	{
		glGenVertexArrays(1, &m_VAOID);
		glBindVertexArray(m_VAOID);

		m_VertexSize = m_ModelData->Vertices.size();
		glGenBuffers(1, &m_VBOID);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBOID);
		glBufferData(GL_ARRAY_BUFFER, m_VertexSize * sizeof(Vertex_PNUC), m_ModelData->Vertices.data(), GL_STATIC_DRAW);
		m_DrawType = DrawType::Vertex;

		GLBuffers::bindVAO_PNUC();

		if (!m_ModelData->Indices.empty())
		{
			m_IndexSize = m_ModelData->Indices.size();
			glGenBuffers(1, &m_EBOID);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOID);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndexSize * sizeof(Index_uint), m_ModelData->Indices.data(), GL_STATIC_DRAW);
			m_DrawType = DrawType::Index;
		}

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void GLMessager::sendSkyBox()
	{
		float p = 1.0f;
		float points[] = {
			-p,  p, -p,
			-p, -p, -p,
			p, -p, -p,
			p, -p, -p,
			p,  p, -p,
			-p,  p, -p,

			-p, -p,  p,
			-p, -p, -p,
			-p,  p, -p,
			-p,  p, -p,
			-p,  p,  p,
			-p, -p,  p,

			p, -p, -p,
			p, -p,  p,
			p,  p,  p,
			p,  p,  p,
			p,  p, -p,
			p, -p, -p,

			-p, -p,  p,
			-p,  p,  p,
			p,  p,  p,
			p,  p,  p,
			p, -p,  p,
			-p, -p,  p,

			-p,  p, -p,
			p,  p, -p,
			p,  p,  p,
			p,  p,  p,
			-p,  p,  p,
			-p,  p, -p,

			-p, -p, -p,
			-p, -p,  p,
			p, -p, -p,
			p, -p, -p,
			-p, -p,  p,
			p, -p,  p
		};


		glGenVertexArrays(1, &m_VAOID);
		glBindVertexArray(m_VAOID);

		m_VertexSize = sizeof(points) / sizeof(float) / 3;
		glGenBuffers(1, &m_VBOID);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBOID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);

		glEnableVertexAttribArray(GLAttributeID(AttributeID::Position));
		glVertexAttribPointer(GLAttributeID(AttributeID::Position), 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

		m_DrawType = DrawType::Vertex;
	}

	void GLMessager::send2Draw()
	{
		switch (m_DrawType)
		{
		case GLMessager::DrawType::Vertex:
			glDrawArrays(m_DrawMode, 0, m_VertexSize);
			break;
		case GLMessager::DrawType::Index:
			glDrawElements(m_DrawMode, m_IndexSize, m_IndexType, 0);
			break;
		}
	}

	void GLMessager::send2RenderPassBack()
	{
		for (auto &cmd : m_RenderPassWithFunction)
		{
//			cmd.pass->pushBackCMD(cmd.render);
		}
	}

	void GLMessager::send2RenderPassFront()
	{
		for (auto &cmd : m_RenderPassWithFunction)
		{
//			cmd.pass->pushFrontCMD(cmd.render);
		}
	}
}

