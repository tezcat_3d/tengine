#pragma once

#include "glew.h"
#include "XComponent.h"
#include "GLConfig.h"

#include <unordered_set>
#include <functional>
#include "ShaderEngine.h"

namespace tezcat
{
	class GLRenderPass;

	struct GLRenderPassWapper
	{
		GLRenderPass *pass;
		std::function<void()> render;
	};

	class GLMessager : public XComponent<GLMessager>
	{
	public:
		enum class DrawType
		{
			Vertex,
			Index,
		};

	public:
		GLMessager();
		~GLMessager();

		//投递数据区 
	public:
		
		//************************************
		// 函数名:  sendMesh
		// 返回值:  void
		// 功能:    
		//************************************
		void sendMesh();

		//************************************
		// 函数名:  sendSkyBox
		// 返回值:  void
		// 功能:    
		//************************************
		void sendSkyBox();

		//************************************
		// 函数名:  useVAO
		// 返回值:  void
		// 功能:    
		//************************************
		void useVAO() { glBindVertexArray(m_VAOID); }

		//************************************
		// 函数名:  send2Draw
		// 返回值:  void
		// 功能:    
		//************************************
		void send2Draw();

		//************************************
		// 函数名:  send2RenderPass
		// 返回值:  void
		// 功能:    
		//************************************
		void send2RenderPassBack();

		//************************************
		// 函数名:  send2RenderPassFront
		// 返回值:  void
		// 功能:    
		//************************************
		void send2RenderPassFront();

		//数据取得区
	public:
		//************************************
		// 函数名:  getVAOID
		// 返回值:  const GLuint &
		// 功能:    
		//************************************
		const GLuint &getVAOID() { return m_VAOID; }

		//************************************
		// 函数名:  getVertexSize
		// 返回值:  const size_t &
		// 功能:    
		//************************************
		const size_t &getVertexSize() { return m_VertexSize; }

		//************************************
		// 函数名:  getIndexSize
		// 返回值:  const size_t &
		// 功能:    
		//************************************
		const size_t &getIndexSize() { return m_IndexSize; }

		//************************************
		// 函数名:  getDrawType
		// 返回值:  const GLMessager::DrawType &
		// 功能:    
		//************************************
		const GLMessager::DrawType &getDrawType() { return m_DrawType; }

		//************************************
		// 函数名:  getDrawMode
		// 返回值:  const GLenum &
		// 功能:    
		//************************************
		const GLenum &getDrawMode() { return m_DrawMode; }

		//************************************
		// 函数名:  getIndexType
		// 返回值:  const GLenum &
		// 功能:    
		//************************************
		const GLenum &getIndexType() { return m_IndexType; }

		//************************************
		// 函数名:  setIndexType
		// 返回值:  void
		// 参数名:  GLenum val
		// 功能:    
		//************************************
		void setIndexType(GLenum val) { m_IndexType = val; }

		//************************************
		// 函数名:  setModelData
		// 返回值:  void
		// 参数名:  Data * data
		// 功能:    
		//************************************
		void setModelData(Data *data) { m_ModelData = data; }

		void addRender(GLRenderPass *list)
		{
//			m_RenderPass.insert(list);
//			m_RenderList = list;
		}

		void addRenderCMD(GLRenderPass *list, const std::function<void()> &function)
		{
//			m_Renders[list] = function;
			GLRenderPassWapper wapper;
			wapper.pass = list;
			wapper.render = function;
			m_RenderPassWithFunction.emplace_back(wapper);
		}

		std::vector<GLRenderPassWapper> &getRenderPassWithFunction() { return m_RenderPassWithFunction; }


	private:
		GLuint m_VAOID;

		GLuint m_VBOID;
		GLuint m_EBOID;

		size_t m_VertexSize;
		size_t m_IndexSize;

		DrawType m_DrawType;
		GLenum m_DrawMode;
		GLenum m_IndexType;

		Data *m_ModelData;

		std::vector<GLRenderPassWapper> m_RenderPassWithFunction;
	};
}