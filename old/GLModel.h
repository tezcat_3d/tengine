#pragma once

#include "glew.h"

#include "SimpleDataManager.h"
#include "TEntityConfig.h"
#include "glm/glm.hpp"
#include "GLConfig.h"

namespace tezcat
{
	class Mesh;
	class GLProgram;
	class GLMode : XComponent<GLMode>
	{
		enum class DrawType
		{
			Vertex,
			Index,
		};
	public:
		GLMode();
		~GLMode();

		void sendModel(Data *mesh);

		void render();

	private:
		GLuint m_VAOID;

		GLuint m_VBOID;
		GLuint m_EBOID;

		size_t m_VertexSize;
		size_t m_IndexSize;

		DrawType m_DrawType;
		GLenum m_DrawMode;
		GLenum m_IndexType;
	};

	class GLModelManager
	{
	public:
		GLModelManager();
		~GLModelManager();

		//************************************
		// 函数名:  create
		// 返回值:  GLEntityX *
		// 参数名:  const std::string & name
		// 功能:    
		//************************************
		TEntity *create(const std::string &name);


	private:
		//************************************
		// 函数名:  loadMesh2Entity
		// 返回值:  void
		// 参数名:  GLEntityX * e_parent
		// 参数名:  GLEntityX * e_child
		// 参数名:  Mesh * m_parent
		// 功能:    
		//************************************
		void loadMesh2Entity(TEntity *e_parent, TEntity *e_child, Mesh *m_parent);


	private:
		std::string m_TempName;
	};
}