#include "GLProgram.h"

namespace tezcat
{

	GLProgram::GLProgram(const GLuint &program):
		m_Program(program)
	{
		m_FixedGLSLTextureIndex.resize(GET_FIXED_GLSLTEXTURE_COUNT, 0);
		m_FixedGLSLMaterialIndex.resize(GET_FIXED_GLSLMATERIAL_COUNT, 0);
		m_FixedGLSLMatrixIndex.resize(GET_FIXED_GLSLMATRIX_COUNT, 0);
	}

	GLProgram::~GLProgram()
	{
		glDeleteProgram(m_Program);
	}
}

