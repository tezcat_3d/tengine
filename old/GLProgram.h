#pragma once

#include "glew.h"
#include "GLSL.h"

namespace tezcat
{
	class GLProgram
	{
	public:
		GLProgram(const GLuint &program);
		~GLProgram();

		void use() { glUseProgram(m_Program); }

		GLint &getTextureIndex(const unsigned int &index)
		{
			return m_FixedGLSLMaterialIndex[index];
		}

		GLint &getMaterialIndex(const unsigned int &index)
		{
			return m_FixedGLSLMaterialIndex[index];
		}

		GLint &getMatrixIndex(const unsigned int &index)
		{
			return m_FixedGLSLMatrixIndex[index];
		}

	private:
		friend class ShaderEngine;
		GLuint m_Program;
		std::vector<GLint> m_FixedGLSLTextureIndex;
		std::vector<GLint> m_FixedGLSLMaterialIndex;
		std::vector<GLint> m_FixedGLSLMatrixIndex;
	};
}