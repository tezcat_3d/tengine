#include "GLRenderCommand.h"

namespace tezcat
{

	GLRenderCommand::GLRenderCommand():
		ListNode(XList<GLRenderCommand *>::createNode(this)),
		OnRender(nullptr)
	{

	}

	GLRenderCommand::~GLRenderCommand()
	{

	}

}

