#pragma once

#include <functional>
#include "XList.h"
#include "XComponent.h"

namespace tezcat
{
	class GLProgram;
	class GLRenderCommand : public XComponent<GLRenderCommand>
	{
	public:
		GLRenderCommand();
		~GLRenderCommand();

	public:
		std::function<void(GLProgram *)> OnRender;
		XListNode(GLRenderCommand *);
	};
}