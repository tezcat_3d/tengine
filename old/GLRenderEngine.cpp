#include "GLRenderEngine.h"
#include "GLTransform.h"
#include "GLCamera.h"
#include "GLMessager.h"
#include "GLTexture.h"
#include "GLScene.h"
#include "GLRenderQueue.h"


namespace tezcat
{
	float GLRenderEngine::DeltaTime = 0;

	GLRenderEngine::GLRenderEngine()
	{

	}

	GLRenderEngine::~GLRenderEngine()
	{

	}

	void GLRenderEngine::init()
	{
 		m_Background = new GLRenderQueue();
 		m_Background->OnBegin = []()
 		{
			glDepthMask(GL_FALSE);
			glEnable(GL_DEPTH_TEST);
 		};
 
 		m_Background->OnEnd = []()
 		{
 			glDepthMask(GL_TRUE);
			glDisable(GL_DEPTH_TEST);
 		};
 
 		m_Normal = new GLRenderQueue();
 		m_Normal->OnBegin = []()
 		{
			glEnable(GL_DEPTH_TEST);
 		};
 
		m_Normal->OnEnd = []()
 		{
 			glDisable(GL_DEPTH_TEST);
 		};
 
 		m_Alpha = new GLRenderQueue();
 		m_Alpha->OnBegin = [this]()
 		{
 			glEnable(GL_DEPTH_TEST);
 			glEnable(GL_BLEND);
 			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 		};
 
 		m_Alpha->OnEnd = [this]()
 		{
 			glDisable(GL_BLEND);
 			glDisable(GL_DEPTH_TEST);
 		};
// 
// 		m_Overlay = new GLRenderQueue();
// 		m_Overlay->onBegin = []()
// 		{
// 			glDepthMask(GL_FALSE);
// 		};
// 
// 		m_Overlay->onEnd = []()
// 		{
// 			glDepthMask(GL_TRUE);
// 		};
	}

	void GLRenderEngine::collectInfo(TEntity *entity)
	{
		auto tag = entity->getStatic<TEntityTag>();
		switch (tag->getIntTag())
		{
		case TEntityType::Node:
			this->updataNode(entity);
			break;
		case TEntityType::Model:
			this->updataModel(entity);
			break;
		case TEntityType::Camera:
			this->updataCamera(entity);
			break;
		case TEntityType::Light:
			this->updataLight(entity);
			break;
		case TEntityType::SkyBox:
			this->updataSkyBox(entity);
			break;
		}
	}

	void GLRenderEngine::render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//		m_Background->render();
		m_Normal->render();
		m_Alpha->render();
//		m_Overlay->render();
	}

	void GLRenderEngine::addList2Queue(GLRenderPass *list)
	{
		switch (list->getBelong2Queue())
		{
		case GLRenderQueueType::Background:
			m_Background->addList(list);
			break;
		case GLRenderQueueType::Normal:
			m_Normal->addList(list);
			break;
		case GLRenderQueueType::Alpha:
			m_Alpha->addList(list);
			break;
		case GLRenderQueueType::Overlay:
			m_Overlay->addList(list);
			break;
		}
	}

	void GLRenderEngine::updataNode(TEntity *entity)
	{
		//node只接受矩阵计算
		//并且在此完成计算
		auto transform = entity->getStatic<GLTransform>();
		transform->updata();
	}

	void GLRenderEngine::updataModel(TEntity *entity)
	{
		//model有以下类型
		//普通
		//带透明
		//在这里分离两种类型的模型
		this->updataNode(entity);
		auto messager = entity->getStatic<GLMessager>();
		messager->send2RenderPassBack();
	}

	void GLRenderEngine::updataCamera(TEntity *entity)
	{
		//每一帧的每一个阶段都可能需要传递相机数据
		//所以相机数据不进入队列渲染
		//这里就完成计算
		entity->getStatic<GLCamera>()->render();
		auto messager = entity->getStatic<GLMessager>();
		messager->send2RenderPassFront();
	}

	void GLRenderEngine::updataLight(TEntity *entity)
	{
		//灯光暂时只计算矩阵
		this->updataNode(entity);
		auto messager = entity->getStatic<GLMessager>();
		messager->send2RenderPassBack();
	}

	void GLRenderEngine::updataSkyBox(TEntity *entity)
	{
		//skybox归于背景计算
		this->updataNode(entity);
		auto messager = entity->getStatic<GLMessager>();
		messager->send2RenderPassBack();
	}
}

