#pragma once

#include "Singleton.h"
#include "XList.h"
#include "TEntityConfig.h"
#include "RenderEngine.h"
#include "GLRenderQueue.h"
#include "GLFrameBuffer.h"

#include <vector>
#include <list>
#include <bitset>

namespace tezcat
{
	class GLRenderEngine : public RenderEngine
	{
		enum RenderListSort
		{
			SkyBox,
			Else,
		};

	public:
		GLRenderEngine();
		~GLRenderEngine();

		void init();

		void collectInfo(TEntity *entity);

		void render();

		void addList2Queue(GLRenderPass *list);

	private:
		void updataNode(TEntity *entity);
		void updataModel(TEntity *entity);
		void updataCamera(TEntity *entity);
		void updataLight(TEntity *entity);
		void updataSkyBox(TEntity *entity);

	private:
		void separateObject(TEntity *entity);

	public:
		static float DeltaTime;

		//1.背景队列
		//2.普通物体队列
		//3.透明物体队列
		//4.覆盖队列
		GLRenderQueue *m_Background;
		GLRenderQueue *m_Normal;
		GLRenderQueue *m_Alpha;
		GLRenderQueue *m_Overlay;
	};
}