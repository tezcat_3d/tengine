#pragma once

#include "glew.h"
#include <string>

namespace tezcat
{
// #define GLSL(content) #content
// 	const std::string VertexBase = GLSL
// 	(
// 	layout(location = 0) in vec3 in_Position;
// 	layout(location = 1) in vec3 in_Normal;
// 	layout(location = 2) in vec2 in_UV;
// 	layout(location = 3) in vec4 in_Color;
// 
// 	uniform mat4 myM;
// 	uniform mat3 myMNormal;
// 	uniform mat4 myMV;
// 	uniform mat4 myMVP;
// 	uniform mat4 myV;
// 	uniform mat4 myP;
// 
// 	out VsOut_Base
// 	{
// 		vec3 ModelNormal;
// 		vec3 FragPos;
// 	};
// 	);
// 
// 	const std::string VertexMain = GLSL
// 	(
// 	void main()
// 	{
// 		gl_Position = myMVP * in_Position;
// 	};
// 	);
// #undef GLSL


	class GLForwardRendering
	{
	public:
		GLForwardRendering();
		~GLForwardRendering();

		void render();

	private:
	};



}