#include "GLRenderQueue.h"
#include "GLCamera.h"
#include "GLProgram.h"


namespace tezcat
{
	GLRenderPass::GLRenderPass():
		ListNode(XList<GLRenderPass *>::createNode(this))
	{

	}

	GLRenderPass::~GLRenderPass()
	{

	}

	void GLRenderPass::render()
	{
		OnBegin();
		m_Program->use();
		auto it = m_CMDS.resetIterator();
		while (it->hasNext())
		{
			it->value()->OnRender(m_Program);
			it->next();
		}

		m_CMDS.clear();
		OnEnd();
	}

	void GLRenderQueue::render()
	{
		auto it = m_RenderPass.resetIterator();
		while (it->hasNext())
		{
			it->value()->render();
			it->next();
		}
	}
}