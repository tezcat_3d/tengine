#pragma once

#include <functional>
#include <unordered_set>
#include <unordered_map>
#include "GLRenderCommand.h"
#include "XList.h"

namespace tezcat
{
	class GLMessager;
	class GLRenderCommand;
	class GLProgram;

	enum class GLRenderQueueType
	{
		Background,			//背景层
		Normal,				//普通物体
		Alpha,				//透明物体
		Overlay				//覆盖层
	};

	//=========================================
	//
	//渲染通道
	//
	class GLRenderPass
	{
	public:
		XListNode(GLRenderPass *);
		std::function<void()> OnBegin;
		std::function<void()> OnEnd;

	public:

		GLRenderPass();
		~GLRenderPass();

		//************************************
		// 函数名:  render
		// 返回值:  void
		// 功能:    
		//************************************
		void render();

		//************************************
		// 函数名:  setProgram
		// 返回值:  void
		// 参数名:  GLProgram * program
		// 功能:    
		//************************************
		void setProgram(GLProgram *program) { m_Program = program; }

		GLRenderQueueType &getBelong2Queue() { return m_Belong2Queue; }
		void setBelong2Queue(GLRenderQueueType val) { m_Belong2Queue = val; }

		void addCMD(GLRenderCommand *cmd)
		{
			m_CMDS.push_back(cmd->ListNode);
		}

	private:
		GLProgram *m_Program;
		GLRenderQueueType m_Belong2Queue;
		XList<GLRenderCommand *> m_CMDS;
	};


	//
	//
	class GLRenderQueue
	{
	public:
		std::function<void()> OnBegin;
		std::function<void()> OnEnd;

	public:

		//************************************
		// 函数名:  render
		// 返回值:  void
		// 功能:    
		//************************************
		void render();

		//************************************
		// 函数名:  addList
		// 返回值:  void
		// 参数名:  GLRenderPass * list
		// 功能:    
		//************************************
		void addList(GLRenderPass *list)
		{
			m_RenderPass.push_back(list->ListNode);
		}

	private:
		XList<GLRenderPass *> m_RenderPass;
	};
}