#pragma once

#include <unordered_map>
#include <string>

namespace tezcat
{
	enum class FixedGLSLMatrixIndex : unsigned int
	{
		//矩阵数据
		MATRIX_MODEL = 0,			//模型矩阵
		MATRIX_VIEW,				//视图矩阵
		MATRIX_PROJECTION,			//投影矩阵
		MATRIX_MODELVIEW,			//模型视图矩阵
		MATRIX_MODELVIEWPROJECTION,	//模型视图投影矩阵
		MATRIX_MODELNORMAL,			//模型法线矩阵
		FIXED_DATA_COUNT			//总量
	};

	const std::unordered_map<std::string, FixedGLSLMatrixIndex> FixedGLSLMatrixRegister =
	{
		{ "ttMatModel",			FixedGLSLMatrixIndex::MATRIX_MODEL },
		{ "ttMatView",			FixedGLSLMatrixIndex::MATRIX_VIEW },
		{ "ttMatProjection",	FixedGLSLMatrixIndex::MATRIX_PROJECTION },
		{ "ttMatMV",			FixedGLSLMatrixIndex::MATRIX_MODELVIEW },
		{ "ttMatMVP",			FixedGLSLMatrixIndex::MATRIX_MODELVIEWPROJECTION },
		{ "ttMatMN",			FixedGLSLMatrixIndex::MATRIX_MODELNORMAL },
	};

#define GET_FIXED_GLSLMATRIX_INDEX(index) static_cast<unsigned int>(index)
#define GET_FIXED_GLSLMATRIX_COUNT static_cast<unsigned int>(FixedGLSLMatrixIndex::FIXED_DATA_COUNT)


	enum class FixedGLSLTextureIndex : unsigned int
	{
		//贴图数据
		TEXTRUE2D_DIFFUSE = 0,		//漫反射贴图
		TEXTRUE2D_NORMAL,			//法线贴图
		TEXTRUE2D_SPECULAR,			//高光贴图
		TEXTRUE2D_SHININESS,		//高光系数贴图
		TEXTRUE2D_AMBIENT,			//环境光贴图
		TEXTRUE2D_EMISSIVE,			//自发光贴图
		TEXTRUE2D_HEIGHT,			//高度图
		TEXTRUE2D_OPACITY,			//透明度贴图
		TEXTRUE2D_DISPLACEMENT,		//视差贴图
		TEXTRUE2D_LIGHTMAP,			//光照贴图
		TEXTRUE2D_REFLECTION,		//反射贴图
		FIXED_DATA_COUNT			//总量
	};

	const std::unordered_map<std::string, FixedGLSLTextureIndex> FixedScriptTextureRegister =
	{
		{ "diffuse",		FixedGLSLTextureIndex::TEXTRUE2D_DIFFUSE },
		{ "normal",			FixedGLSLTextureIndex::TEXTRUE2D_NORMAL },
		{ "specular",		FixedGLSLTextureIndex::TEXTRUE2D_SPECULAR },
		{ "shininess",		FixedGLSLTextureIndex::TEXTRUE2D_SHININESS },
		{ "ambient",		FixedGLSLTextureIndex::TEXTRUE2D_AMBIENT },
		{ "emissive",		FixedGLSLTextureIndex::TEXTRUE2D_EMISSIVE },
		{ "height",			FixedGLSLTextureIndex::TEXTRUE2D_HEIGHT },
		{ "opacity",		FixedGLSLTextureIndex::TEXTRUE2D_OPACITY },
		{ "displacement",	FixedGLSLTextureIndex::TEXTRUE2D_DISPLACEMENT },
		{ "lightmap",		FixedGLSLTextureIndex::TEXTRUE2D_LIGHTMAP },
		{ "reflection",		FixedGLSLTextureIndex::TEXTRUE2D_REFLECTION }
	};

	const std::unordered_map<std::string, FixedGLSLTextureIndex> FixedGLSLTextureRegister =
	{
		{ "ttTexture2D.diffuse",		FixedGLSLTextureIndex::TEXTRUE2D_DIFFUSE },
		{ "ttTexture2D.normals",		FixedGLSLTextureIndex::TEXTRUE2D_NORMAL },
		{ "ttTexture2D.specular",		FixedGLSLTextureIndex::TEXTRUE2D_SPECULAR },
		{ "ttTexture2D.shininess",		FixedGLSLTextureIndex::TEXTRUE2D_SHININESS },
		{ "ttTexture2D.ambient",		FixedGLSLTextureIndex::TEXTRUE2D_AMBIENT },
		{ "ttTexture2D.emissive",		FixedGLSLTextureIndex::TEXTRUE2D_EMISSIVE },
		{ "ttTexture2D.height",			FixedGLSLTextureIndex::TEXTRUE2D_HEIGHT },
		{ "ttTexture2D.opacity",		FixedGLSLTextureIndex::TEXTRUE2D_OPACITY },
		{ "ttTexture2D.displacement",	FixedGLSLTextureIndex::TEXTRUE2D_DISPLACEMENT },
		{ "ttTexture2D.lightmap",		FixedGLSLTextureIndex::TEXTRUE2D_LIGHTMAP },
		{ "ttTexture2D.reflection",		FixedGLSLTextureIndex::TEXTRUE2D_REFLECTION }
	};

#define GET_FIXED_GLSLTEXTURE_INDEX(index) static_cast<unsigned int>(index)
#define GET_FIXED_GLSLTEXTURE_COUNT static_cast<unsigned int>(FixedGLSLTextureIndex::FIXED_DATA_COUNT)

	enum class FixedGLSLMaterialIndex : unsigned int
	{
		//材质数据
		MATERIAL_EMISSION = 0,		//自身发光
		MATERIAL_AMBIENT,			//环境光
		MATERIAL_DIFFUSE,			//漫反射
		MATERIAL_SPECULAR,			//高光
		MATERIAL_SHININESS,			//高光系数
		MATERIAL_OPACITY,			//不透明度
		FIXED_DATA_COUNT			//总量
	};

	const std::unordered_map<std::string, FixedGLSLMaterialIndex> FixedGLSLMaterialRegister =
	{
		{ "ttMaterial.emission",		FixedGLSLMaterialIndex::MATERIAL_EMISSION },
		{ "ttMaterial.ambient",			FixedGLSLMaterialIndex::MATERIAL_AMBIENT },
		{ "ttMaterial.diffuse",			FixedGLSLMaterialIndex::MATERIAL_DIFFUSE },
		{ "ttMaterial.specular",		FixedGLSLMaterialIndex::MATERIAL_SPECULAR },
		{ "ttMaterial.shininess",		FixedGLSLMaterialIndex::MATERIAL_SHININESS },
		{ "ttMaterial.opacity",			FixedGLSLMaterialIndex::MATERIAL_OPACITY }
	};

#define GET_FIXED_GLSLMATERIAL_INDEX(index) static_cast<unsigned int>(index)
#define GET_FIXED_GLSLMATERIAL_COUNT static_cast<unsigned int>(FixedGLSLTextureIndex::FIXED_DATA_COUNT)

#define GLSL(content) #content

	const std::string BaseVertex = GLSL(
	layout(location = 0) in vec3 in_Position;
	layout(location = 1) in vec3 in_Normal;
	layout(location = 2) in vec2 in_UV;
	layout(location = 3) in vec4 in_Color;

	out VS_OUT
	{

	};

	void main()
	{

	};

	);

	const std::string BaseFragment = GLSL(
	struct TTTexture2D
	{
		sampler2D diffuse;
		sampler2D normal;
		sampler2D specular;
		sampler2D shininess;
		sampler2D ambient;
		sampler2D emissive;
		sampler2D height;
		sampler2D opacity;
		sampler2D displacement;
		sampler2D lightmap;
		sampler2D reflection;
	}ttTexture2D;

	struct TTMaterial
	{
		vec3 emission;
		vec3 ambient;
		vec3 diffuse;
		vec3 specular;
		vec3 shininess;
		vec3 opacity;
	}ttMaterial;

	void main()
	{

	};

	);

#undef GLSL
}

