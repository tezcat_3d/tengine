#pragma once


namespace tezcat
{
	struct GLSLModel
	{
		GLSLModel(int &_m, int &_mv, int &_mvp, int &_normal):
			m(_m), mv(_mv), mvp(_mvp), normal(_normal) {}

		int &m;
		int &mv;
		int &mvp;
		int &normal;
	};

	struct GLSLCamera
	{
		GLSLCamera(int &_v, int &_p, int &_position):
		v(_v), p(_p), position(_position) {}

		int &v;
		int &p;
		int &position;
	};

	struct GLSLTexture
	{
		GLSLTexture(int &_color, int &_diffuse, int &_specular, int &_normal, int &_environment):
			color(_color), diffuse(_diffuse), specular(_specular), normal(_normal), environment(_environment) {}

		int &color;
		int &diffuse;
		int &specular;
		int &normal;
		int &environment;
	};

	struct GLSLDirectionalLight
	{
		GLSLDirectionalLight(int &_ambient, int &_diffuse, int &_specular, int &_direction):
		ambient(_ambient), diffuse(_diffuse), specular(_specular), direction(_direction) {}

		int &ambient;
		int &diffuse;
		int &specular;
		int &direction;
	};
}

