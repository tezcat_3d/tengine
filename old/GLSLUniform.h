#pragma once

#include <string>
#include <unordered_map>

namespace tezcat
{
	class GLSLUniformInfo
	{
	public:
		enum class DataType
		{
			Mat4, Mat3, Mat2,
			Vec4, Vec3, Vec2,
			SamplerCube, Sampler2D,
			Float, Int, Double,
			Custom
		};

		std::string Name;
		std::string TypeName;
		DataType Type;
		unsigned int ID;
	};

	struct GLProgramInfo
	{
		unsigned int index;
		unsigned int program;
		std::unordered_map<std::string, GLSLUniformInfo> uniforms;
	};
}