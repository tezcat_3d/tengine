#include "GLScene.h"
#include "GLCamera.h"
#include "MainEngine.h"
#include "RenderEngine.h"
#include "SimpleDataManager.h"
#include "GLTransform.h"
#include "GLModel.h"
#include "GLSkyBox.h"
#include "XString.h"

#include <functional>
#include <unordered_set>

namespace tezcat
{
	GLScene::GLScene(const std::string &name):
		m_Name(name)
	{
		Singleton<MainEngine>::getInstance()->push(this);
	}

	GLScene::~GLScene()
	{
		Singleton<MainEngine>::getInstance()->pop();
	}

	void GLScene::init()
	{
// 		auto camera = Singleton<GLCameraManager>::getInstance()->create("MainCamera");
// 		this->addChild(camera);
	}

	void GLScene::updata(RenderEngine *engine)
	{
#if 1
		//非递归
		TEntity *child = nullptr;

		//    1      2
		//  1 2 3
		//深度优先遍历整棵场景树
		bool loop = true;
		auto it = m_Children.resetIterator();
		while (loop)
		{
			//有子节点
			while (it->hasNext())
			{
				child = it->value();
				engine->collectInfo(child);
				child->updataDynamicComponent();
				it->next();

				//如果有子节点
				if (child->hasChild())
				{
					//保存当前节点迭代信息
					m_EntityStack.push_back(it);
					//把迭代器改变为子节点初始迭代器
					it = child->getChildren().resetIterator();
				}
			}

			//如果没有节点遍历了,关闭循环
			if (m_EntityStack.empty())
			{
				loop = false;
			}
			//如果有没有遍历完的节点,获得上层迭代信息
			else
			{
				it = m_EntityStack.back();
				m_EntityStack.pop_back();
			}
		}

#else
		//递归
		auto it = m_Children.resetIterator();
		while (it->hasNext())
		{
			auto child = it->value();
			engine->collectInfo(child);
			child->updataDynamicComponent();
			it->next();
			this->foreachChild(engine, child);
		}
#endif
	}

	void GLScene::addChild(TEntity *node)
	{
		m_Children.push_back(node->getXListNode());
	}

	void GLScene::loadConfig()
	{
		std::function<void(std::pair<const std::string, LightScriptValue *> &pair, XList<TEntity *> &root)> function=
			[&](std::pair<const std::string, LightScriptValue *> &pair, XList<TEntity *> &root)
		{
			TEntity *entity = nullptr;
			auto &config = (*pair.second);
			auto &&type = config["type"]().asSTDString();
			if (type == "model")
			{
				entity = Singleton<GLModelManager>::getInstance()->create(config["config"]().asSTDString());
				auto transform = entity->getStatic<GLTransform>();
				auto &&position = String::split2Vector(config["position"]().asSTDString(), ',');
				transform->setPosition(std::stof(position[0]), std::stof(position[1]), std::stof(position[2]));
				auto &&rotation = String::split2Vector(config["rotation"]().asSTDString(), ',');
				transform->setRotation(std::stof(rotation[0]), std::stof(rotation[1]), std::stof(rotation[2]));
				auto &&scale = String::split2Vector(config["scale"]().asSTDString(), ',');
				transform->setScale(std::stof(scale[0]), std::stof(scale[1]), std::stof(scale[2]));
				root.push_back(entity->getXListNode());
			}
			else if (type == "camera")
			{
				auto screen_width = config["window_width"]().asFloat();
				auto screen_height = config["window_height"]().asFloat();
				auto fovy = config["fovy"]().asFloat();
				auto near = config["near"]().asFloat();
				auto far = config["far"]().asFloat();
				entity = Singleton<GLCameraManager>::getInstance()->create(m_Name, pair.first, fovy, screen_width, screen_height, near, far);

				//设置参数
				auto camera = entity->getStatic<GLCamera>();
				auto &&position = String::split2Vector(config["position"]().asSTDString(), ',');
				camera->setCameraPostion(std::stof(position[0]), std::stof(position[1]), std::stof(position[2]));
				camera->setSpeed(config["speed"]().asFloat());
				camera->setYaw(config["yaw"]().asFloat());
				camera->setPitch(config["pitch"]().asFloat());
				auto &&head = String::split2Vector(config["head"]().asSTDString(), ',');
				camera->setHeadUp(std::stof(head[0]), std::stof(head[1]), std::stof(head[2]));
				camera->reset();
				//添加
				root.push_back(entity->getXListNode());
			}
			else if (type == "skybox")
			{
				entity = Singleton<GLSkyBoxManager>::getInstance()->create(config["config"]().asSTDString());
				root.push_back(entity->getXListNode());
			}
			else if (type == "light")
			{

			}

			//遍历子节点
			auto _class = config.getClass();
			auto it = _class->find("sys_children");
			if (it != _class->end())
			{
				for (auto &child : *(it->second->getClass()))
				{
					function(child, entity->getChildren());
				}
			}
		};

		//取得配置文件
		auto &file = Singleton<SimpleDataManager>::getInstance()->getSceneFile(m_Name);
		for (auto &object : *file["root"].getClass())
		{
			function(object, m_Children);
		}
	}

	void GLScene::foreachChild(RenderEngine *engine, TEntity *entity)
	{
		auto it = entity->getChildren().resetIterator();
		while (it->hasNext())
		{
			auto child = it->value();
			engine->collectInfo(child);
			child->updataDynamicComponent();
			it->next();
			this->foreachChild(engine, child);
		}
	}

}
