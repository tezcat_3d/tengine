#pragma once

#include "TEntityConfig.h"
#include <unordered_map>
#include <vector>

namespace tezcat
{
	class RenderEngine;
	class GLScene
	{
	public:
		GLScene(const std::string &name);
		~GLScene();

		void init();

		void updata(RenderEngine *engine);

		//************************************
		// 函数名:  addChild
		// 返回值:  void
		// 参数名:  TEntity * node
		// 功能:    
		//************************************
		void addChild(TEntity *node);

		const std::string &getName() { return m_Name; }

		void loadConfig();

	private:
		void foreachChild(RenderEngine *engine, TEntity *entity);

	private:
		//名称
		std::string m_Name;
		//场景树
		XList<TEntity *> m_Children;
		//临时
		std::vector<XList<TEntity *>::Iterator *> m_EntityStack;
	};
}