#include "GLSkyBox.h"

#include "GLTransform.h"
#include "GLMessager.h"
#include "GLRenderQueue.h"
#include "ShaderEngine.h"
#include "GLTexture.h"
#include "GLSLDataIndex.h"


namespace tezcat
{
	GLSkyBoxManager::GLSkyBoxManager()
	{

	}

	GLSkyBoxManager::~GLSkyBoxManager()
	{

	}


	TEntity * GLSkyBoxManager::create(const std::string &name)
	{
		//获得skybox的配置文件
		auto &skyboxFile = Singleton<SimpleDataManager>::getInstance()->getSkyBoxFile(name);
		//获得skybox的shader配置文件
		auto &skyboxConfig = Singleton<SimpleDataManager>::getInstance()->getShaderFile(name);

		auto e = Singleton<TEntityFactory>::getInstance()->create();
		//配置tag
		auto tag = new TEntityTag();
		tag->setIntTag(TEntityType::SkyBox);
		tag->addStringTag(name);
		e->addStatic(tag);

		//配置messager
		auto messager = new GLMessager();
		messager->sendSkyBox();
		e->addStatic(messager);

		//配置transform
		auto transform = new GLTransform();
		transform->setName(name);
		e->addStatic(transform);

		/*
		//配置texture
		auto &texture = skyboxFile["texture"];
		auto com = new GLTextrueComponent();
		auto tex = new GLTextureCube(GLTexture::Environment);
		tex->bindBuffer(
			texture["positive_x"]().asSTDString(),
			texture["negative_x"]().asSTDString(),
			texture["positive_y"]().asSTDString(),
			texture["negative_y"]().asSTDString(),
			texture["positive_z"]().asSTDString(),
			texture["negative_z"]().asSTDString());
		com->addTexture(tex);
		e->addStatic(com);

		//配置各个通道中的数据索引
		for (auto &pair : *skyboxFile["shader"].getClass())
		{
			auto &shaderName = (*pair.second)().asSTDString();
			auto group = Singleton<ShaderEngine>::getInstance()->getProgram<false>(shaderName);
			auto &shaderConfig = Singleton<SimpleDataManager>::getInstance()->getShaderFile(shaderName);
			auto &textureConfig = shaderConfig["config"]["texture"];
			GLSLTexture skybox(
				group->file->findUniform(textureConfig["environment"]().asSTDString())->getID(),
				group->file->findUniform(textureConfig["environment"]().asSTDString())->getID(),
				group->file->findUniform(textureConfig["environment"]().asSTDString())->getID(),
				group->file->findUniform(textureConfig["environment"]().asSTDString())->getID(),
				group->file->findUniform(textureConfig["environment"]().asSTDString())->getID());

			messager->addRenderCMD(group->list, [=]()
			{
				messager->useVAO();
				com->bindTexture(skybox);
				messager->send2Draw();
			});
		}
		*/

		return e;
	}

}

