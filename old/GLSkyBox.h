#pragma once

#include "Singleton.h"
#include "TEntityConfig.h"
#include "SimpleDataManager.h"

namespace tezcat
{
	class GLSkyBoxManager
	{
	public:
		GLSkyBoxManager();
		~GLSkyBoxManager();

		//************************************
		// 函数名:  create
		// 返回值:  GLEntityX *
		// 参数名:  const std::string & name
		// 功能:    创建天空盒
		//************************************
		TEntity *create(const std::string &name);
	};
}
