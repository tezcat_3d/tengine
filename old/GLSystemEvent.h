#pragma once



#include "XEvent.h"
#include <string>

namespace tezcat
{
	//
	class Event_Scene_Push : public XEvent<Event_Scene_Push>
	{
	public:
		std::string name;
	};
	class Event_Scene_Pop : public XEvent<Event_Scene_Pop>
	{
	public:
		std::string name;
	};
	class Event_Scene_Replace : public XEvent<Event_Scene_Replace>
	{
	public:
		std::string name;
	};
}

