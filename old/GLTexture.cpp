#include "GLTexture.h"
#include "ImageManager.h"

namespace tezcat
{
	GLTexture::GLTexture(): m_TexID(0)
	{
		glGenTextures(1, &m_TexID);
	}

	GLTexture::~GLTexture()
	{
		glDeleteTextures(1, &m_TexID);
	}

	//
	//
	GLTexture2D::GLTexture2D() : m_Image(nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, m_TexID);
	}

	GLTexture2D::~GLTexture2D()
	{

	}

	void GLTexture2D::genTexture(const std::string &file_name)
	{
		m_Image = Singleton<ImageManager>::getInstance()->createImage(file_name);
		m_Internalformat = GL_RGBA;
		m_ExternalFormat = GL_RGBA;
		m_ExternalType = GL_UNSIGNED_BYTE;
		glTexImage2D(GL_TEXTURE_2D, m_Image->getMipMap(), m_Internalformat, m_Image->getWidth(), m_Image->getHeight(), 0, m_ExternalFormat, m_ExternalType, m_Image->getData());
	}

	void GLTexture2D::genTextureBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = internalformat;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::genDepthBuffer(const unsigned int &width, const unsigned int &height)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = GL_DEPTH_COMPONENT;
		m_ExternalFormat = GL_DEPTH_COMPONENT;
		m_ExternalType = GL_FLOAT;
		glTexImage2D(GL_TEXTURE_2D, 0, m_Internalformat, width, height, 0, m_ExternalFormat, m_ExternalType, 0);
	}

	void GLTexture2D::genStencilBuffer(const unsigned int &width, const unsigned int &height, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = GL_STENCIL_INDEX;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(GL_TEXTURE_2D, 0, m_Internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::genCustomBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = internalformat;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::updata(const unsigned int &width, const unsigned int &height, unsigned char *data)
	{
		glBindTexture(GL_TEXTURE_2D, m_TexID);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, m_ExternalFormat, m_ExternalType, data);
	}

	void GLTexture2D::updata(const unsigned int &width, const unsigned int &height, const unsigned int &xoffset, const unsigned int &yoffset, unsigned char *data)
	{
		glBindTexture(GL_TEXTURE_2D, m_TexID);
		glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset, yoffset, width, height, m_ExternalFormat, m_ExternalType, data);
	}

	void GLTexture2D::updata(GLTexture2D *tex)
	{
		glBindTexture(GL_TEXTURE_2D, m_TexID);
		glTexSubImage2D(GL_TEXTURE_2D, tex->m_Image->getMipMap(), 0, 0, tex->m_Image->getWidth(), tex->m_Image->getHeight(), m_ExternalFormat, m_ExternalType, tex->m_Image->getData());
	}


	//==============================================================
	//
	//	立方体贴图
	//
	GLTextureCube::GLTextureCube()
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_TexID);
		m_Images[Position::Count] = { nullptr };
	}

	GLTextureCube::~GLTextureCube()
	{
		
	}

	void GLTextureCube::genTexture(
		const std::string &positive_x, const std::string &negative_x,
		const std::string &positive_y, const std::string &negative_y,
		const std::string &positive_z, const std::string &negative_z)
	{
		//
		//
		auto *img_positive_x = Singleton<ImageManager>::getInstance()->createImage(positive_x);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			img_positive_x->getMipMap(), GL_RGBA,
			img_positive_x->getWidth(), img_positive_x->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_x->getData());
		m_Images[Position::POSITIVE_X] = img_positive_x;

		auto *img_negative_x = Singleton<ImageManager>::getInstance()->createImage(negative_x);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			img_negative_x->getMipMap(), GL_RGBA,
			img_negative_x->getWidth(), img_negative_x->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_x->getData());
		m_Images[Position::NEGATIVE_X] = img_negative_x;

		//
		//
		auto *img_positive_y = Singleton<ImageManager>::getInstance()->createImage(positive_y);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			img_positive_y->getMipMap(), GL_RGBA,
			img_positive_y->getWidth(), img_positive_y->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_y->getData());
		m_Images[Position::POSITIVE_Y] = img_positive_y;

		auto *img_negative_y = Singleton<ImageManager>::getInstance()->createImage(negative_y);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
			img_negative_y->getMipMap(), GL_RGBA,
			img_negative_y->getWidth(), img_negative_y->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_y->getData());
		m_Images[Position::NEGATIVE_Y] = img_negative_y;

		//
		//
		auto *img_positive_z = Singleton<ImageManager>::getInstance()->createImage(positive_z);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			img_positive_z->getMipMap(), GL_RGBA,
			img_positive_z->getWidth(), img_positive_z->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_z->getData());
		m_Images[Position::POSITIVE_Z] = img_positive_z;

		auto *img_negative_z = Singleton<ImageManager>::getInstance()->createImage(negative_z);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			img_negative_z->getMipMap(), GL_RGBA,
			img_negative_z->getWidth(), img_negative_z->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_z->getData());
		m_Images[Position::NEGATIVE_Z] = img_negative_z;

		//
		//
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}



	//==============================================================
	//
	//	贴图管理器
	//
	GLTexture2D * GLTextureManager::getTexture2D(const std::string &name)
	{
		auto it = m_AllTexture2D.find(name);

		if (it != m_AllTexture2D.end())
		{
			return it->second;
		}
		else
		{
			GLTexture2D *t2d = new GLTexture2D();
			t2d->genTexture(name);
			t2d->setTextureMinFilter(GL_LINEAR);
			t2d->setTextureMagFilter(GL_LINEAR);
			m_AllTexture2D.insert(std::make_pair(name, t2d));
			return t2d;
		}
	}
}