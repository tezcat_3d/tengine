#pragma once

#include "glew.h"

#include "Singleton.h"
#include "GLConfig.h"
#include <unordered_map>
#include <string>

namespace tezcat
{
	class Image;
	//===============================================
	//
	//	��ͼ
	//
	class GLTexture
	{
	public:
		GLTexture();
		virtual ~GLTexture();

		const unsigned int &getType() const { return m_TexType; }
		const GLuint &getTexID() const { return m_TexID; }
		virtual void unBind() = 0;
		virtual void bind() = 0;

	protected:
		GLuint m_TexID;
		unsigned int m_TexType;
	};


	//===============================================
	//
	//	2D��ͼ
	//
	class GLTexture2D : public GLTexture
	{
	public:
		GLTexture2D();
		~GLTexture2D();

		void setTexParameter(GLenum filer, GLuint mode)
		{
			glTexParameteri(GL_TEXTURE_2D, filer, mode);
		}

		void setTextureMinFilter(GLuint mode)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mode);
		}

		void setTextureMagFilter(GLuint mode)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mode);
		}

		void unBind() { glBindTexture(GL_TEXTURE_2D, 0); }

		void bind() { glBindTexture(GL_TEXTURE_2D, m_TexID); }

	public:
		void genMipMap() { glGenerateMipmap(GL_TEXTURE_2D); }

		void genTexture(const std::string &file_name);

		void genTextureBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision);

		void genDepthBuffer(const unsigned int &width, const unsigned int &height);

		void genStencilBuffer(const unsigned int &width, const unsigned int &height, GLenum format, GLenum precision);

		void genCustomBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision);

		bool isTextureCache() { return m_Image == nullptr; }

		void updata(const unsigned int &width, const unsigned int &height, unsigned char *data);

		void updata(const unsigned int &width, const unsigned int &height, const unsigned int &xoffset, const unsigned int &yoffset, unsigned char *data);

		void updata(GLTexture2D *tex);

	private:
		Image *m_Image;

		GLenum m_Internalformat;
		GLenum m_ExternalFormat;
		GLenum m_ExternalType;
	};

	//================================================
	//
	//	��������ͼ
	//
	class GLTextureCube : public GLTexture
	{
	public:
		enum Position
		{
			POSITIVE_X,		//x��
			NEGATIVE_X,		//x��
			POSITIVE_Y,
			NEGATIVE_Y,
			POSITIVE_Z,
			NEGATIVE_Z,
			Count,
		};

	public:
		GLTextureCube();
		~GLTextureCube();

		void unBind() { glBindTexture(GL_TEXTURE_CUBE_MAP, 0); }

		void bind() { glBindTexture(GL_TEXTURE_CUBE_MAP, m_TexID); }

		void genTexture(
			const std::string &positive_x, const std::string &negative_x,
			const std::string &positive_y, const std::string &negative_y,
			const std::string &positive_z, const std::string &negative_z);

	private:
		Image *m_Images[Position::Count];
	};


	//===============================================
	//
	//	��ͼ������
	//
	class GLTextureManager
	{
	public:

		void addTexture2D(const std::string &name, GLTexture2D *tex)
		{
			m_AllTexture2D.insert(std::make_pair(name, tex));
		}

		bool hasTexture2D(const std::string &name)
		{
			return m_AllTexture2D.find(name) != m_AllTexture2D.end();
		}

		GLTexture2D *getTexture2D(const std::string &name);

	private:
		std::unordered_map<std::string, GLTexture2D *> m_AllTexture2D;
	};
}