#include "GLTransform.h"

namespace tezcat
{

	glm::vec3 GLTransform::AxisX = glm::vec3(1, 0, 0);
	glm::vec3 GLTransform::AxisY = glm::vec3(0, 1, 0);
	glm::vec3 GLTransform::AxisZ = glm::vec3(0, 0, 1);
	glm::mat4 GLTransform::WorldMat4 = glm::mat4(1.0f);
	glm::mat4 GLTransform::IdentifyMat4 = glm::mat4(1.0f);
	glm::quat GLTransform::IdentifyQuat = glm::quat();



	float GLTransform::PID180 = 3.141592653f / 180;
	float GLTransform::PIM180 = 3.141592653f * 180;


	GLTransform * GLTransform::TheWorld = new GLTransform();

	GLTransform::GLTransform() :
		m_Name("default_transform"),
		m_UpdataWho(UpdataWho::Position | UpdataWho::Rotation | UpdataWho::Scale),
		m_HasChildren(false),
		m_SelfMatrix(1.0f),
		m_ParentMatrix(&TheWorld->m_SelfMatrix),
		m_Scale(1.0f),
		m_Position(0.0f),
		m_Mask(Masks::Layer1)
	{}

	GLTransform::~GLTransform()
	{
	}


	void GLTransform::updata()
	{
		switch (m_UpdataWho)
		{
		case UpdataWho::Position:
			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);

			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::Rotation:
			m_RotationMatrix = glm::toMat4(
				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));

			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::Scale:
			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);

			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::Position | UpdataWho::Rotation:
			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
			m_RotationMatrix = glm::toMat4(
				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));

			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::Position | UpdataWho::Scale:
			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::Rotation | UpdataWho::Scale:
			m_RotationMatrix = glm::toMat4(
				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));

			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		case UpdataWho::UpdataAll:
		case UpdataWho::Position | UpdataWho::Rotation | UpdataWho::Scale:
			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);

			m_RotationMatrix = glm::toMat4(
				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));

			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
			m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
			break;
		default:
			break;
		}


		m_UpdataWho = UpdataWho::NoUpdata;
	}

	GLTransform * GLTransform::clone()
	{
		GLTransform *node = new GLTransform();
		node->m_Name = m_Name;
		node->m_UpdataWho = UpdataWho::UpdataAll;
		node->m_HasChildren = m_HasChildren;

		node->m_Scale = m_Scale;
		node->m_Position = m_Position;
		node->m_Rotation = m_Rotation;

		node->m_TranslationMatrix = m_TranslationMatrix;
		node->m_RotationMatrix = m_RotationMatrix;
		node->m_ScaleMatrix = m_ScaleMatrix;
		node->m_ParentMatrix = m_ParentMatrix;
		node->m_SelfMatrix = m_SelfMatrix;

		return node;
	}
}

