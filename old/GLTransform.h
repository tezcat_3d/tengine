#pragma once

#include <unordered_map>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include "glm/gtc/type_ptr.hpp"

#include "XComponent.h"

namespace tezcat
{
	class GLTransform : public XComponent<GLTransform>
	{
	public:
		GLTransform();
		~GLTransform();

		enum UpdataWho : unsigned int
		{
			NoUpdata = 0,
			Position = 1,
			Rotation = 1 << 1,
			Scale = 1 << 2,
			UpdataAll = 1 << 3
		};

		//层掩码
		enum Masks : unsigned short
		{
			Disabled = 0,
			Layer1 = 1,
			Layer2 = 1 << 1,
			Layer3 = 1 << 2,
			Layer4 = 1 << 3,
			Layer5 = 1 << 4,
			Layer6 = 1 << 5,
			Layer7 = 1 << 6,
			Layer8 = 1 << 7,
			Layer9 = 1 << 8,
			Layer10 = 1 << 9,
			Layer11 = 1 << 10,
			Layer12 = 1 << 11,
			Layer13 = 1 << 12,
			Layer14 = 1 << 13,
			Layer15 = 1 << 14,
			Layer16 = 1 << 15,
		};

		GLTransform * getParent() const { return m_Parent; }
		void setParent(GLTransform * val)
		{ 
			m_Parent = val;
			m_ParentMatrix = &m_Parent->m_SelfMatrix;
		}

		void updata();

	public:
	
		unsigned short getMask() const { return m_Mask; }
		void addMask(Masks mask) { m_Mask |= mask; }
		void subMask(Masks mask) { m_Mask &= ~mask; }
		void setMask(Masks mask) { m_Mask = mask; }
		bool isInThisLayer(Masks model_mask)
		{
			return (model_mask & m_Mask) != 0;
		}
	public:
		glm::vec3 getPosition() const { return m_Position; }
		float getPositionX() const { return m_Position.x; }
		float getPositionY() const { return m_Position.y; }
		float getPositionZ() const { return m_Position.z; }

		void setPosition(const glm::vec3 &val)
		{
			m_Position = val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionX(const float &val)
		{
			m_Position.x = val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionY(const float &val)
		{
			m_Position.y = val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionZ(const float &val)
		{
			m_Position.z = val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPosition(const float &x, const float &y, const float &z)
		{
			m_Position.x = x;
			m_Position.y = y;
			m_Position.z = z;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionByOffset(const glm::vec3 &val)
		{
			m_Position += val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionByOffset(const float &x, const float &y, const float &z)
		{
			m_Position.x += x;
			m_Position.y += y;
			m_Position.z += z;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionXByOffset(const float &val)
		{
			m_Position.x += val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionYByOffset(const float &val)
		{
			m_Position.y += val;
			m_UpdataWho |= UpdataWho::Position;
		}

		void setPositionZByOffset(const float &val)
		{
			m_Position.z += val;
			m_UpdataWho |= UpdataWho::Position;
		}

		glm::mat4 &getTransformMatrix()
		{
			return m_TranslationMatrix;
		}

	public:
		glm::quat getRotation() const { return m_Rotation; }
		float getRotationX() { return m_Rotation.x; }
		float getRotationY() { return m_Rotation.y; }
		float getRotationZ() { return m_Rotation.z; }

		void setRotation(const glm::quat &val)
		{
			m_Rotation = val;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotation(const float &x, const float &y, const float &z)
		{
			m_Rotation.x = x * PID180;
			m_Rotation.y = y * PID180;
			m_Rotation.z = z * PID180;

			// 			if (m_Children != nullptr)
			// 			{
			// 				for (auto child : *m_Children)
			// 				{
			// 					//如果父节点矩阵更新了+-
			// 					child->setRotation(x * PID180, y * PID180, z * PID180);
			// 				}
			// 			}

			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationX(const float &val)
		{
			m_Rotation.x = val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationY(const float &val)
		{
			m_Rotation.y = val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationZ(const float &val)
		{
			m_Rotation.z = val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationByOffset(const glm::quat &val)
		{
			m_Rotation += val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationByOffset(const float &x, const float &y, const float &z)
		{
			m_Rotation.x += x * PID180;
			m_Rotation.y += y * PID180;
			m_Rotation.z += z * PID180;


			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationXByOffset(const float &val)
		{
			m_Rotation.x += val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationYByOffset(const float &val)
		{
			m_Rotation.y += val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		void setRotationZByOffset(const float &val)
		{
			m_Rotation.z += val * PID180;
			m_UpdataWho |= UpdataWho::Rotation;
		}

		glm::mat4 &getRotationMatrix()
		{
			return m_RotationMatrix;
		}


	public:
		glm::vec3 getScale() const { return m_Scale; }
		float getScaleX() { return m_Scale.x; }
		float getScaleY() { return m_Scale.y; }
		float getScaleZ() { return m_Scale.z; }

		void setScale(const glm::vec3 &val)
		{
			m_Scale = val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScale(const float &x, const float &y, const float &z)
		{
			m_Scale.x = x;
			m_Scale.y = y;
			m_Scale.z = z;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleX(const float &val)
		{
			m_Scale.x = val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleY(const float &val)
		{
			m_Scale.y = val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleZ(const float &val)
		{
			m_Scale.z = val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleByOffset(const glm::vec3 &val)
		{
			m_Scale += val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleByOffset(const float &x, const float &y, const float &z)
		{
			m_Scale.x += x;
			m_Scale.y += y;
			m_Scale.z += z;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleXByOffset(const float &val)
		{
			m_Scale.x += val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleYByOffset(const float &val)
		{
			m_Scale.y += val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		void setScaleZByOffset(const float &val)
		{
			m_Scale.z += val;
			m_UpdataWho |= UpdataWho::Scale;
		}

		glm::mat4 &getScaleMatrix()
		{
			return m_ScaleMatrix;
		}




	public:

		glm::mat4 &getSelfMatrix() { return m_SelfMatrix; }

		GLTransform *clone();
	
		std::string getName() const { return m_Name; }
		void setName(std::string val) { m_Name = val; }
	private:
		glm::vec3 m_Position;
		glm::vec3 m_LocalPosition;
		glm::vec3 m_Scale;
		glm::vec3 m_LocalScale;
		glm::quat m_Rotation;
		glm::quat m_LocalRotation;


		std::string m_Name;


		glm::mat4 m_TranslationMatrix;
		glm::mat4 m_ScaleMatrix;
		glm::mat4 m_RotationMatrix;
		glm::mat4 *m_ParentMatrix;
		glm::mat4 m_SelfMatrix;


		static glm::vec3 AxisX, AxisY, AxisZ;
		static glm::mat4 WorldMat4;
		static glm::mat4 IdentifyMat4;
		static glm::quat IdentifyQuat;


		bool m_HasChildren;

		unsigned int m_UpdataWho;
		unsigned short m_Mask;


		//弧度 = 角度乘以π后再除以180
		//角度 = 弧度除以π再乘以180
		static float PID180;
		static float PIM180;


		GLTransform *m_Parent;
		public:
			static GLTransform *TheWorld;

	};
}