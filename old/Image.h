#pragma once



namespace tezcat
{
	enum class ImageColorFormat
	{
		RGB,
		RGBA
	};

	class Image
	{
	public:
		Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format, unsigned char *data);
		Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format);
		~Image();


		const int &getWidth() const { return m_Width; }

		const int &getHeight() const { return m_Height; }

		unsigned char *getData() const { return m_Data; }

		const ImageColorFormat &getColorFormat() const { return m_ColorFormat; }

		const int &getMipMap() const { return m_MipMap; }

	private:
		int m_Width;
		int m_Height;
		unsigned char *m_Data;
		int m_MipMap;
		ImageColorFormat m_ColorFormat;
	};
}