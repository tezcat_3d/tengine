#include "ImageManager.h"
#include "png/png.h"
#include "png/pngpriv.h"

namespace tezcat
{
	ImageManager::ImageManager()
	{

	}

	ImageManager::~ImageManager()
	{
	}

	Image *ImageManager::loadImage(const std::string &file_name)
	{
		png_byte header[8];
		png_uint_32 pic_width = 0;
		png_uint_32 pic_height = 0;
		png_byte color_type = 0;
		png_byte bit_depth = 0;

		FILE *file = nullptr;
		fopen_s(&file, file_name.c_str(), "rb");

		fread_s(header, 8, 1, 8, file);
		auto is_png = png_sig_cmp(header, 0, 8);
		if (is_png != 0)
		{
			fclose(file);
//			assert(is_png == 0);
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
//		assert(png_ptr != nullptr);
		png_infop info_ptr = png_create_info_struct(png_ptr);
//		assert(info_ptr != nullptr);
		auto error = setjmp(png_jmpbuf(png_ptr));
//		assert(error == 0);

		png_init_io(png_ptr, file);
		png_set_sig_bytes(png_ptr, 8);
		png_read_info(png_ptr, info_ptr);

		pic_width = png_get_image_width(png_ptr, info_ptr);
		pic_height = png_get_image_height(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);
		bit_depth = png_get_bit_depth(png_ptr, info_ptr);

		if (bit_depth == 16)
		{
			png_set_strip_16(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		{
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}

		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_RGB ||
			color_type == PNG_COLOR_TYPE_GRAY ||
			color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_add_alpha(png_ptr, 0xFF, PNG_FILLER_AFTER);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		{
			png_set_gray_to_rgb(png_ptr);
		}

		png_read_update_info(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);

		png_bytep *row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * pic_height);
		for (unsigned int y = 0; y < pic_height; y++)
		{
			row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr, info_ptr));
		}

		unsigned char *content = (unsigned char *)malloc(sizeof(unsigned char) * pic_width * pic_height * 4);
		png_read_image(png_ptr, row_pointers);

		unsigned int pos = 0;
		for (unsigned int y = 0; y < pic_height; y++)
		{
			png_bytep row = row_pointers[y];
			for (unsigned int x = 0; x < pic_width; x++)
			{
				png_bytep px = &(row[x * 4]);
				// Do something awesome for each pixel here...
//				printf("%4d, %4d = RGBA(%3d, %3d, %3d, %3d)\n", x, y, px[0], px[1], px[2], px[3]);
				content[pos++] = px[0];
				content[pos++] = px[1];
				content[pos++] = px[2];
				content[pos++] = px[3];
			}
		}

		Image *image = new Image(pic_width, pic_height, ImageColorFormat::RGBA, content);
		m_Images.insert(std::make_pair(file_name, new ImageInfo()));
		m_Images[file_name]->cache = image;

		free(row_pointers);
		png_destroy_read_struct(&png_ptr, &info_ptr, 0);
		fclose(file);

		return image;
	}

	Image *ImageManager::createImage(const std::string &file_name)
	{
		auto it = m_Images.find(file_name);
		if (it == m_Images.end())
		{
			return this->loadImage(file_name);
		}
		else
		{
			return it->second->cache;
		}
	}
}