#pragma once

#include <unordered_map>
#include <vector>
#include "Image.h"

#include "Singleton.h"

namespace tezcat
{
	class ImageManager
	{
		struct ImageInfo
		{
			ImageInfo() : cache(nullptr) {}
			Image *cache;
		};

	public:
		ImageManager();
		~ImageManager();

		Image *loadImage(const std::string &file_name);

		Image *createImage(const std::string &file_name);


	private:
		std::unordered_map<std::string, ImageInfo *> m_Images;
	};
}