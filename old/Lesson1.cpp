#include "Lesson1.h"
#include <iostream>

#include "TEntityConfig.h"

#include "GLFWEngine.h"
#include "GLRenderEngine.h"

#include "GLModel.h"
#include "Mesh.h"
#include "GLMaterial.h"
#include "GLCamera.h"

#include "GLRenderQueue.h"
#include "GLTexture.h"
#include "GLSkyBox.h"
#include "GLLight.h"
#include "ShaderEngine.h"

#include "glm/gtc/type_ptr.hpp"


static void getError()
{
	GLenum error = glGetError();
	switch (error)
	{
	case GL_NO_ERROR:
		std::cout << "" << std::endl;
		break;
	case GL_INVALID_ENUM:
		std::cout << "" << std::endl;
		break;
	case GL_INVALID_VALUE:
		std::cout << "" << std::endl;
		break;
	case GL_INVALID_OPERATION:
		std::cout << "" << std::endl;
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		std::cout << "" << std::endl;
		break;
	case GL_OUT_OF_MEMORY:
		std::cout << "" << std::endl;
		break;
	default:
		std::cout << "" << std::endl;
		break;
	}
}

class Com1 : public tezcat::XComponent<Com1>
{
public:

	void test1()
	{
		i = 100;
		std::cout << i << std::endl;
	}

	int i;
};

class Com2 : public tezcat::XComponent<Com2>
{
public:
	void test2()
	{
		s = "ss2222";
		std::cout << s << std::endl;
	}

	std::string s;
};


namespace Lesson1
{

	Test::Test()
	{

	}

	Test::~Test()
	{

	}

	void Test::init()
	{
		tezcat::Singleton<tezcat::ShaderEngine>::getInstance()->preload();

		tezcat::GLScene *scene = new tezcat::GLScene("test");
		scene->loadConfig();

		auto camera = tezcat::Singleton<tezcat::GLCameraManager>::getInstance()->getCurrentCamera();

		tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->setKeyHandler("haha",
			[=](GLFWwindow *window, int key, int scancode, int action, int mods)
		{
//			std::cout << "Key : " << key << "ScanCode : " << scancode << "Action : " << action << "Mods : " << mods << std::endl;
			float speed = 30;
			speed *= tezcat::GLRenderEngine::DeltaTime;

			switch (key)
			{
			case GLFW_KEY_1:

				break;
			case GLFW_KEY_2:

				break;
			case GLFW_KEY_W:
				if (action == GLFW_REPEAT)
				{
					camera->forward(speed);
				}
				else if (action == GLFW_RELEASE)
				{
					camera->forward(speed);
				}
				break;
			case GLFW_KEY_S:
				if (action == GLFW_REPEAT)
				{
					camera->back(speed);
				}
				else if (action == GLFW_RELEASE)
				{
					camera->back(speed);
				}
				break;
			case GLFW_KEY_A:
				if (action == GLFW_REPEAT)
				{
					camera->left(speed);
				}
				else if (action == GLFW_RELEASE)
				{
					camera->left(speed);
				}
				break;
			case GLFW_KEY_D:
				if (action == GLFW_REPEAT)
				{
					camera->right(speed);
				}
				else if (action == GLFW_RELEASE)
				{
					camera->right(speed);
				}
				break;
			case GLFW_KEY_ESCAPE:
				if (action == GLFW_PRESS)
				{
					tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->closeEngine();
				}
			case GLFW_KEY_Z:
				if (action == GLFW_RELEASE)
				{

				}
				break;
			case GLFW_KEY_X:
				if (action == GLFW_RELEASE)
				{

				}
				break;
			}
		});

		tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->setMouseHandler("haha",
			[=](GLFWwindow *window, double xpos, double ypos)
		{
			static float lastX = static_cast<float>(xpos);
			static float lastY = static_cast<float>(ypos);

			float x_offset = static_cast<float>(xpos) - lastX;
			float y_offset = lastY - static_cast<float>(ypos);

			lastX = static_cast<float>(xpos);
			lastY = static_cast<float>(ypos);
			camera->onMouse(x_offset, y_offset);
		});
		
	}
}