#include "MainEngine.h"
#include "GLScene.h"
#include "GLSystemEvent.h"
#include "GLTexture.h"
#include "Mesh.h"
#include "TEntityConfig.h"
#include "GLCamera.h"
#include "GLModel.h"
#include "SimpleDataManager.h"
#include "ShaderEngine.h"
#include "ImageManager.h"

namespace tezcat
{
	MainEngine::MainEngine()
	{
		ShaderAssembler assembler;
		assembler.assembleBase("data/shader/default.sb");
		assembler.assembleCustom("data/shader/444.material");
		assembler.display();

		auto dataManager = Singleton<SimpleDataManager>::init();
		dataManager->loadIndex("data/index.list");
		dataManager->loadTEngineCongif("data/tengine.conf");

		Singleton<GLCameraManager>::init();
		Singleton<GLTextureManager>::init();
		Singleton<MeshManager>::init();
		Singleton<TEntityFactory>::init();
		Singleton<GLModelManager>::init();
		Singleton<ShaderEngine>::init();
		Singleton<ImageManager>::init();
	}

	MainEngine::~MainEngine()
	{
		Singleton<GLCameraManager>::deleteInstance();
		Singleton<GLTextureManager>::deleteInstance();
		Singleton<MeshManager>::deleteInstance();
		Singleton<TEntityFactory>::deleteInstance();
		Singleton<GLModelManager>::deleteInstance();
		Singleton<ShaderEngine>::deleteInstance();
		Singleton<ImageManager>::deleteInstance();

	}

	void MainEngine::replace(GLScene *scene)
	{
		m_CurrentScene = scene;
		m_Scene.back() = m_CurrentScene;

		auto event = new Event_Scene_Replace();
		event->name = scene->getName();
		XEventSystem::getInstance()->broadcast<Event_Scene_Replace>(event);
	}

	void MainEngine::push(GLScene *scene)
	{
		m_CurrentScene = scene;
		m_Scene.push_back(scene);

		auto event = new Event_Scene_Push();
		event->name = scene->getName();
		XEventSystem::getInstance()->broadcast<Event_Scene_Push>(event);
	}

	void MainEngine::pop()
	{
		auto oldScene = m_Scene.back();
		m_Scene.pop_back();
		m_CurrentScene = m_Scene.back();

		auto event = new Event_Scene_Pop();
		event->name = oldScene->getName();
		XEventSystem::getInstance()->broadcast<Event_Scene_Pop>(event);
	}

	void MainEngine::logic(RenderEngine *render_engine)
	{
		m_CurrentScene->updata(render_engine);
	}
}

