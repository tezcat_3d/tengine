#pragma once


#include "Singleton.h"
#include <vector>
#include <unordered_map>

namespace tezcat
{
	class GLScene;
	class RenderEngine;
	class MainEngine
	{
	public:
		MainEngine();
		~MainEngine();

		void replace(GLScene *scene);

		void push(GLScene *scene);

		void pop();

		void logic(RenderEngine *render_engine);

		GLScene *getSceneByName(const std::string &name)
		{
			return m_SceneByName[name];
		}

		const GLScene *getCurrentScene() const { return m_CurrentScene; }

	private:
		std::vector<GLScene *> m_Scene;
		std::unordered_map<std::string, GLScene *> m_SceneByName;
		GLScene *m_CurrentScene;
	};
}