#include "Mesh.h"
#include "XString.h"
#include "GLError.h"

#include <functional>

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace tezcat
{
	Mesh::Mesh(const std::string &name, Data *storage_data) :
		m_Name(name),
		m_StorageData(storage_data),
		m_HasModel(false)
	{

	}

	Mesh::Mesh(const std::string &name) :
		m_Name(name),
		m_StorageData(nullptr),
		m_HasModel(false)
	{

	}

	Mesh::~Mesh()
	{

	}



	void Mesh::setMatrix(
		float x1, float x2, float x3, float x4,
		float y1, float y2, float y3, float y4,
		float z1, float z2, float z3, float z4,
		float w1, float w2, float w3, float w4)
	{
		//=========================
		//分离矩阵

		auto clamp = [](const float &value)
		{
			//return (value <= -0.000001f) ? -0.000001f : value;
			return value;
		};

		//位置坐标
		//列矩阵第四列前3个
		m_Position.x = clamp(x4);
		m_Position.y = clamp(y4);
		m_Position.z = clamp(z4);

		//分离缩放
		//缩放等于前3*3矩阵中每列的长度
		float length1 = glm::length(glm::vec3(clamp(x1), clamp(y1), clamp(z1)));
		float length2 = glm::length(glm::vec3(clamp(x2), clamp(y2), clamp(z2)));
		float length3 = glm::length(glm::vec3(clamp(x3), clamp(y3), clamp(z3)));

		if (x1 > 0)
			m_Scale.x = length1;
		else
			m_Scale.x = -length1;

		if (y2 > 0)
			m_Scale.y = length2;
		else
			m_Scale.y = -length2;
		if (z3 > 0)
			m_Scale.z = length3;
		else
			m_Scale.z = -length3;


		//按Z轴旋转a弧度
		// cosa,   sina,   0,      0
		// -sina,  cosa,   0,      0
		// 0,      0,      1,      0
		// 1,      2,      3,      1
		//按X轴旋转a弧度
		// 1,      0,      0,      0
		// 0,      cosa,   sina,   0
		// 0,     -sina,   cosa,   0
		// 1,      2,      3,      1
		//按Y轴旋转a弧度
		// cosa,   0,      -sina,  0
		// 0,      1,      0,      0
		// sina,   0,      cosa,   0
		// 1,      2,      3,      1

		//弧度 = 角度乘以π后再除以180
		//角度 = 弧度除以π再乘以180

		glm::mat3 rotateMat3;
		rotateMat3[0][0] = x1 / length1;
		rotateMat3[0][1] = y1 / length1;
		rotateMat3[0][2] = z1 / length1;

		rotateMat3[1][0] = x2 / length2;
		rotateMat3[1][1] = y2 / length2;
		rotateMat3[1][2] = z2 / length2;

		rotateMat3[2][0] = x3 / length3;
		rotateMat3[2][1] = y3 / length3;
		rotateMat3[2][2] = z3 / length3;

		auto quat = glm::toQuat(rotateMat3);
		m_Rotation.y = glm::yaw(quat);
		m_Rotation.x = -glm::pitch(quat);
		m_Rotation.z = glm::roll(quat);

		// 		m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
		// 
		// 		m_RotationMatrix = glm::toMat4(
		// 			glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
		// 			glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
		// 			glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));
		// 
		// 		m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
		// 		m_SelfMatrix = (*m_ParentMatrix) * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
	}

	void Mesh::foreachAllChildren(const std::function<void(Mesh *)> &function)
	{
		function(this);
		for (auto child : m_Children)
		{
			child->foreachAllChildren(function);
			function(child);
		}
	}

	//
	//
	//
	MeshManager::MeshManager()
	{

	}

	MeshManager::~MeshManager()
	{
		this->clear();
	}

	void MeshManager::loadMeshFromFile(const std::string &file)
	{
		std::string tempFile = file;
		String::replaceAllByFlag(tempFile, "\\", "/");
		this->loadAssImp(file);
	}


	void MeshManager::loadAssImp(const std::string &file)
	{
		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile(
			file.c_str(),
			aiProcess_CalcTangentSpace |			//加入切线和双切线
			aiProcess_FlipUVs |						//翻转UV
			aiProcess_Triangulate |					//??
			aiProcess_JoinIdenticalVertices |		//相同顶点索引优化
			aiProcess_SortByPType |					//排序
			aiProcess_ImproveCacheLocality |		//提高缓存
			aiProcess_RemoveRedundantMaterials |	//去除无效材质
			aiProcess_FindDegenerates |				//去除变形的多边形
			aiProcess_FindInvalidData |				//检测无效的模型数据
			aiProcess_GenUVCoords |					//生成UV
			aiProcess_TransformUVCoords |			//预处理UV
			aiProcess_FindInstances |				//查找实例化的模型
			aiProcess_OptimizeMeshes |				//如果和可能,添加小网格
			aiProcess_ValidateDataStructure);		//有效数据验证

		if (!scene)
		{
			return;
		}

		auto glMesh = new Mesh("default");
		this->foreachChild(scene->mRootNode, glMesh, scene);
		m_Prefabs.insert(std::make_pair(file, glMesh));
	}

	void MeshManager::foreachChild(aiNode *node, Mesh *root, const aiScene *scene)
	{
		root->setName(node->mName.C_Str());
		root->setMatrix(
			node->mTransformation.a1, node->mTransformation.a2, node->mTransformation.a3, node->mTransformation.a4,
			node->mTransformation.b1, node->mTransformation.b2, node->mTransformation.b3, node->mTransformation.b4,
			node->mTransformation.c1, node->mTransformation.c2, node->mTransformation.c3, node->mTransformation.c4,
			node->mTransformation.d1, node->mTransformation.d2, node->mTransformation.d3, node->mTransformation.d4);

		if (node->mNumMeshes != 0)
		{
			for (unsigned int meshIndex = 0; meshIndex < node->mNumMeshes; meshIndex++)
			{
				Data *d = new Data();
				auto &indices = d->Indices;
				auto &vertices = d->Vertices;

				const aiMesh* mesh = scene->mMeshes[node->mMeshes[meshIndex]];
				vertices.resize(mesh->mNumVertices);

				bool hasPos = mesh->HasPositions();
				bool hasUV0 = mesh->HasTextureCoords(0);
				bool hasNormal = mesh->HasNormals();
				bool hasColor0 = mesh->HasVertexColors(0);
				bool has2T = mesh->HasTangentsAndBitangents();
				bool hasFace = mesh->HasFaces();

				for (unsigned int vertexIndex = 0; vertexIndex < mesh->mNumVertices; vertexIndex++)
				{
					if (hasPos)
					{
						aiVector3D &pos = mesh->mVertices[vertexIndex];
						vertices[vertexIndex].position = glm::vec3(pos.x, pos.y, pos.z);
					}

					if (hasUV0)
					{
						aiVector3D &UVW = mesh->mTextureCoords[0][vertexIndex];
						vertices[vertexIndex].uv = glm::vec2(UVW.x, UVW.y);
					}
					else
					{
						vertices[vertexIndex].uv = glm::vec2(0, 0);
					}

					if (hasNormal)
					{
						aiVector3D &n = mesh->mNormals[vertexIndex];
						vertices[vertexIndex].normal = glm::vec3(n.x, n.y, n.z);
					}

					if (hasColor0)
					{
						aiColor4D &n = *mesh->mColors[vertexIndex];
						vertices[vertexIndex].color = glm::vec4(n.r, n.g, n.b, n.a);
					}
					else
					{
						vertices[vertexIndex].color = glm::vec4(1, 0, 0, 1);
					}

					if (has2T)
					{
//						vertices[vertexIndex].tangent = glm::vec3(mesh->mTangents[vertexIndex].x, mesh->mTangents[vertexIndex].y, mesh->mTangents[vertexIndex].z);
//						vertices[vertexIndex].bitTangent = glm::vec3(mesh->mBitangents[vertexIndex].x, mesh->mBitangents[vertexIndex].y, mesh->mBitangents[vertexIndex].z);
					}
				}

				if (hasFace)
				{
					indices.reserve(3 * mesh->mNumFaces);
					for (unsigned int i = 0; i < mesh->mNumFaces; i++)
					{
						indices.push_back(mesh->mFaces[i].mIndices[0]);
						indices.push_back(mesh->mFaces[i].mIndices[1]);
						indices.push_back(mesh->mFaces[i].mIndices[2]);
					}
				}

				if (mesh->mMaterialIndex >= 0)
				{
					auto c = scene->mMaterials[mesh->mMaterialIndex]->GetTextureCount(aiTextureType_DIFFUSE);
//					root->setHasTex(true);
				}

				// 				if (mesh->mMaterialIndex >= 0)
				// 				{
				// 					aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
				// 					std::cout << material << std::endl;
				// 					loadTexture(material, aiTextureType_DIFFUSE);
				// 					loadTexture(material, aiTextureType_SPECULAR);
				// 					loadTexture(material, aiTextureType_AMBIENT);
				// 					loadTexture(material, aiTextureType_NORMALS);
				// 				}

				root->setHasModel(true);
				root->setStorageData(d);
			}
		}

		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			auto child = node->mChildren[i];
			Mesh *mesh = new Mesh(child->mName.C_Str());
			mesh->setParent(root);
			root->addChild(mesh);
			this->foreachChild(child, mesh, scene);
		}
	}

	void MeshManager::clear()
	{
		for (auto model : m_Prefabs)
		{
			delete model.second;
		}
		m_Prefabs.clear();
	}
}