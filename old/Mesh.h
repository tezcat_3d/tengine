#pragma once

#include <vector>
#include <unordered_map>
#include <cassert>
#include <string>
#include <functional>
#include "Singleton.h"
#include "GLConfig.h"


struct aiNode;
struct aiScene;

namespace tezcat
{
	class Mesh
	{
	public:
		Mesh(const std::string &name);
		Mesh(const std::string &name, Data *storage_data);
		~Mesh();

		void *operator new (size_t size)
		{
			return new std::aligned_storage<sizeof(Mesh), std::alignment_of<Mesh>::value>::type();
		}

		//************************************
		// 函数名:  getStorageData
		// 返回值:  Data *
		// 功能:    
		//************************************
		Data * getStorageData() const { return m_StorageData; }

		//************************************
		// 函数名:  setStorageData
		// 返回值:  void
		// 参数名:  Data * val
		// 功能:    
		//************************************
		void setStorageData(Data * val) { m_StorageData = val; }

		//************************************
		// 函数名:  hasModel
		// 返回值:  bool
		// 功能:    
		//************************************
		bool hasModel() { return m_HasModel; }

		//************************************
		// 函数名:  setHasModel
		// 返回值:  void
		// 参数名:  bool has
		// 功能:    
		//************************************
		void setHasModel(bool has) { m_HasModel = has; }

		//************************************
		// 函数名:  getChildren
		// 返回值:  std::list<Mesh *> &
		// 功能:    
		//************************************
		std::list<Mesh *> &getChildren() { return m_Children; }

		//************************************
		// 函数名:  getName
		// 返回值:  std::string
		// 功能:    
		//************************************
		std::string getName() const { return m_Name; }

		//************************************
		// 函数名:  setName
		// 返回值:  void
		// 参数名:  std::string val
		// 功能:    
		//************************************
		void setName(std::string val) { m_Name = val; }

		//************************************
		// 函数名:  getPosition
		// 返回值:  glm::vec3
		// 功能:    
		//************************************
		glm::vec3 getPosition() const { return m_Position; }

		//************************************
		// 函数名:  getScale
		// 返回值:  glm::vec3
		// 功能:    
		//************************************
		glm::vec3 getScale() const { return m_Scale; }

		//************************************
		// 函数名:  getRotation
		// 返回值:  glm::quat
		// 功能:    
		//************************************
		glm::quat getRotation() const { return m_Rotation; }


	private:
		//************************************
		// 函数名:  addChild
		// 返回值:  void
		// 参数名:  Mesh * child
		// 功能:    
		//************************************
		void addChild(Mesh *child) { m_Children.push_back(child); }

		//************************************
		// 函数名:  getParent
		// 返回值:  Mesh *
		// 功能:    
		//************************************
		Mesh * getParent() const { return m_Parent; }

		//************************************
		// 函数名:  setParent
		// 返回值:  void
		// 参数名:  Mesh * val
		// 功能:    
		//************************************
		void setParent(Mesh * val) { m_Parent = val; }


		//************************************
		// 函数名:  setMatrix
		// 返回值:  void
		// 参数名:  float x1
		// 参数名:  float x2
		// 参数名:  float x3
		// 参数名:  float x4
		// 参数名:  float y1
		// 参数名:  float y2
		// 参数名:  float y3
		// 参数名:  float y4
		// 参数名:  float z1
		// 参数名:  float z2
		// 参数名:  float z3
		// 参数名:  float z4
		// 参数名:  float w1
		// 参数名:  float w2
		// 参数名:  float w3
		// 参数名:  float w4
		// 功能:    
		//************************************
		void setMatrix(float x1, float x2, float x3, float x4,
			float y1, float y2, float y3, float y4,
			float z1, float z2, float z3, float z4,
			float w1, float w2, float w3, float w4);

		//************************************
		// 函数名:  copyData2Mode
		// 返回值:  void
		// 参数名:  glm::vec3 & position
		// 参数名:  glm::vec3 & scale
		// 参数名:  glm::quat & rotation
		// 功能:    
		//************************************
		void copyData2Mode(glm::vec3 &position, glm::vec3 &scale, glm::quat &rotation)
		{
			position = m_Position;
			scale = m_Scale;
			rotation = m_Rotation;
		}

		//************************************
		// 函数名:  foreachAllChildren
		// 返回值:  void
		// 参数名:  const std::function<void
		// 参数名:  Mesh * 
		// 参数名:  > & function
		// 功能:    
		//************************************
		void foreachAllChildren(const std::function<void(Mesh *)> &function);

	private:
		friend class MeshManager;
		Mesh *m_Parent;
		std::list<Mesh *> m_Children;
		bool m_HasModel;

		//缓存数据
	public:
		std::string m_Name;
		Data *m_StorageData;
		glm::mat4 m_MatrixData;
		glm::vec3 m_Position;
		glm::vec3 m_Scale;
		glm::quat m_Rotation;
	};

	
	class MeshManager
	{
	public:
		MeshManager();
		~MeshManager();

		//************************************
		// 函数名:  loadMeshFromFile
		// 返回值:  void
		// 参数名:  const std::string & file
		// 功能:	   从文件读取mesh
		//************************************
		void loadMeshFromFile(const std::string &file);

		//************************************
		// 函数名:  getMesh
		// 返回值:  GLMesh *
		// 参数名:  const std::string & file
		// 功能:    通过名称取得一个Mesh根节点,如果没有会尝试从文件加载
		//************************************
		Mesh *getMesh(const std::string &file)
		{
			if (m_Prefabs.find(file) == m_Prefabs.end())
			{
				this->loadMeshFromFile(file);
			}
			return m_Prefabs[file];
		}

	private:
		//************************************
		// 函数名:  loadAssImp
		// 返回值:  void
		// 参数名:  const std::string & file
		// 功能:    从assimp中获得模型
		//************************************
		void loadAssImp(const std::string &file);

	private:
		//************************************
		// 函数名:  foreachChild
		// 返回值:  void
		// 参数名:  aiNode * node
		// 参数名:  GLMesh * root
		// 功能:    遍历所有aiNode来生成Mesh
		//************************************
		void foreachChild(aiNode *node, Mesh *root, const aiScene *scene);

	private:
		//************************************
		// 函数名:  clear
		// 返回值:  void
		// 功能:    
		//************************************
		void clear();

	private:
		std::unordered_map<std::string, Mesh *> m_Prefabs;
	};
}