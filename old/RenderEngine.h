#pragma once

#include "TEntityConfig.h"

namespace tezcat
{
	class RenderEngine
	{
	public:
		RenderEngine();
		virtual ~RenderEngine();

		virtual void collectInfo(TEntity *entity) = 0;

		virtual void render() = 0;

	private:

	};
}