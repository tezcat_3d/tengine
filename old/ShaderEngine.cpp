// ShaderEngine.h.cpp : 定义控制台应用程序的入口点。
//

#include "ShaderEngine.h"
#include "GLConfig.h"
#include "XFileIO.h"
#include "XString.h"
#include "GLRenderEngine.h"
#include "GLProgram.h"
#include "XFileIO.h"
#include "XString.h"

namespace tezcat
{
	ShaderEngine::ShaderEngine() :
		m_ProgramID(0), m_QueueID(-1)
	{
		m_Shaders.resize(static_cast<GLuint>(ShaderType::Count), ShaderInfo());

		//注册所有基础类型变量
		m_SLDataType.insert(std::make_pair("vec2", ShaderDataType::vec2));
		m_SLDataType.insert(std::make_pair("vec3", ShaderDataType::vec3));
		m_SLDataType.insert(std::make_pair("vec4", ShaderDataType::vec4));
		m_SLDataType.insert(std::make_pair("mat2", ShaderDataType::mat2));
		m_SLDataType.insert(std::make_pair("mat3", ShaderDataType::mat3));
		m_SLDataType.insert(std::make_pair("mat4", ShaderDataType::mat4));
		m_SLDataType.insert(std::make_pair("samplerCube", ShaderDataType::samplerCube));
		m_SLDataType.insert(std::make_pair("sampler2D", ShaderDataType::sampler2D));
		m_SLDataType.insert(std::make_pair("float", ShaderDataType::slfloat));
		m_SLDataType.insert(std::make_pair("int", ShaderDataType::slint));

		auto jumpChar = [](const char &flag, std::string &shader, size_t &index)
		{
			while (shader[index] == flag)
			{
				index += 1;
			}
		};

		//注册所有关键字处理方法
		//处理layout
		m_KeyWordFunction["layout"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderLayout layout;
			std::string name;
			char flag;
			size_t id;
			flag = shader[index];

			jumpChar(' ', shader, index);

			if (flag == '(')
			{
				index += 1;
				//获取layout的属性
				//读到=标志位
				while (true)
				{
					flag = shader[index];
					if (flag == '=')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				if (name == "location")
				{
					layout.setIndexType(ShaderLayout::IndexType::location);
				}
				else if (name == "binding")
				{
					layout.setIndexType(ShaderLayout::IndexType::binding);
				}
				name.clear();

				//获取位置
				//跳过=
				index += 1;
				//读到位置标志位)
				while (true)
				{
					flag = shader[index];
					if (flag == ')')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				id = static_cast<size_t>(std::stoi(name));
				name.clear();

				//获取数据io
				index += 2;
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				if (name == "in")
				{
					layout.setDataIO(ShaderLayout::DataIO::in);
				}
				else if (name == "out")
				{
					layout.setDataIO(ShaderLayout::DataIO::out);
				}
				name.clear();

				//获取变量类型
				index += 1;
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				layout.setDataType(m_SLDataType[name]);
				name.clear();

				//获取变量名称
				index += 1;
				while (true)
				{
					flag = shader[index];
					if (flag == ';')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				layout.setName(name);
				name.clear();

				file->addLayout(id, layout);
			}
		};

		//处理uniform
		m_KeyWordFunction["uniform"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderUniform uniform;
			std::string name;
			char flag;
			flag = shader[index];

			while (flag == ' ')
			{
				index += 1;
				flag = shader[index];
			}

			//解析数据格式
			while (true)
			{
				flag = shader[index];
				if (flag == ' ')
				{
					break;
				}
				name += flag;
				index += 1;
			}
			String::removeByFlag(name, " ");
			auto slit = m_SLDataType.find(name);
			if (slit != m_SLDataType.end())
			{
				uniform.setDataType(slit->second);
			}
			else
			{
				uniform.setDataType(ShaderDataType::custom);
				uniform.setDataTypeString(name);
			}
			name.clear();

			//解析数据格式
			index += 1;
			while (true)
			{
				flag = shader[index];
				if (flag == ';')
				{
					break;
				}
				name += flag;
				index += 1;
			}
			String::removeByFlag(name, " ");
			file->addUniform(name, uniform);
		};

		//处理struct
		m_KeyWordFunction["struct"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderStruct slstruct;
			std::string name, struct_name;
			char flag;
			flag = shader[index];

			jumpChar(' ', shader, index);

			//取得struct名称
			while (true)
			{
				flag = shader[index];
				if (flag == '{')
				{
					String::removeByFlag(name, " ");
					struct_name = name;
					break;
				}
				name += flag;
				index += 1;
			}
			name.clear();
			//跳过}
			index += 1;

			//取得内部数据
			bool loopStruct = true;
			while (loopStruct)
			{
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}

					if (flag == '}')
					{
						loopStruct = false;
						break;
					}

					name += flag;
					index += 1;
				}

				if (loopStruct == false)
				{
					//跳过;
					index += 1;
					break;
				}

				//如果是自定义结构体
				String::removeByFlag(name, " ");
				if (m_SLDataType.find(name) == m_SLDataType.end())
				{
					std::string value;
					while (true)
					{
						flag = shader[index];
						if (flag == ';')
						{
							break;
						}
						value += flag;
						index += 1;
					}
					String::removeByFlag(value, " ");
					slstruct.m_Struct[value] = name;
					//跳过;
					index += 1;
				}
				//如果是内建
				else
				{
					std::string value;
					while (true)
					{
						flag = shader[index];
						if (flag == ';')
						{
							break;
						}
						value += flag;
						index += 1;
					}
					String::removeByFlag(value, " ");
					slstruct.m_Value[value] = name;
					//跳过;
					index += 1;
				}
				name.clear();
			}

			file->addStruct(struct_name, slstruct);
		};
	}

	ShaderEngine::~ShaderEngine()
	{

	}

	void ShaderEngine::preload()
	{
		Singleton<SimpleDataManager>::getInstance()->runCustomFunction([=](LightScriptSystem &sys)
		{
			for (auto &file : sys.getAllFile())
			{
				this->begin((*file.second)["base"]["name"]().asSTDString(), (*file.second)["base"]["queue"]().asInt());
				this->addShader((*file.second)["shader"]["vertex"]().asSTDString(), ShaderType::Vertex);
				this->addShader((*file.second)["shader"]["fragment"]().asSTDString(), ShaderType::Fragment);
				this->end();
			}
		}, SimpleDataManager::Type::Shader);
	}

	void ShaderEngine::begin(const std::string &name, const int &queue_id)
	{
		m_ProgramName = name;
		m_QueueID = queue_id;
		m_ProgramID = glCreateProgram();
	}

	void ShaderEngine::addShader(const std::string &file, ShaderType type)
	{
		GLuint id = 0;

		switch (type)
		{
		case ShaderType::Vertex:
			id = glCreateShader(GL_VERTEX_SHADER);
			break;
		case ShaderType::Fragment:
			id = glCreateShader(GL_FRAGMENT_SHADER);
			break;
		case ShaderType::Tess_Control:
			id = glCreateShader(GL_TESS_CONTROL_SHADER);
			break;
		case ShaderType::Tess_Evaluation:
			id = glCreateShader(GL_TESS_EVALUATION_SHADER);
			break;
		case ShaderType::Geometry:
			id = glCreateShader(GL_GEOMETRY_SHADER);
			break;
		}

		m_Shaders[static_cast<GLuint>(type)].id = id;

		std::string shaderHead, shader;
		auto componentShaderFile = io::loadBinary2String(file);

		m_Shaders[static_cast<GLuint>(type)].content = componentShaderFile;

		auto version = GLConfig::getGLVersion();
		shaderHead = "#version " + std::to_string(version) + " core\n";

		shader = shaderHead + componentShaderFile;

		const char *sf = shader.c_str();

		glShaderSource(id, 1, &sf, NULL);
	}

	void ShaderEngine::end()
	{
		//编译shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glCompileShader(shader.id);
			}
		}

		//检测shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				GLint comp;
				glGetShaderiv(shader.id, GL_COMPILE_STATUS, &comp);

				if (comp == GL_FALSE)
				{
					std::cout << "Shader Compilation FAILED" << std::endl;
					GLchar messages[256];
					glGetShaderInfoLog(shader.id, sizeof(messages), 0, &messages[0]);
					std::cout << messages;
				}
			}
		}

		//连接shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glAttachShader(m_ProgramID, shader.id);
			}
		}

		//连接program
		glLinkProgram(m_ProgramID);

		//连接检测
		GLint linkStatus, validateStatus;
		glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &linkStatus);

		if (linkStatus == GL_FALSE)
		{
			std::cout << "Shader Linking FAILED" << std::endl;
			GLchar messages[256];
			glGetProgramInfoLog(m_ProgramID, sizeof(messages), 0, &messages[0]);
			std::cout << messages;
		}

		glValidateProgram(m_ProgramID);
		glGetProgramiv(m_ProgramID, GL_VALIDATE_STATUS, &validateStatus);

		std::cout << "Link: " << linkStatus << "  Validate: " << validateStatus << std::endl;
		if (linkStatus == GL_FALSE)
		{
			std::cout << "Shader Validation FAILED" << std::endl;
			GLchar messages[256];
			glGetProgramInfoLog(m_ProgramID, sizeof(messages), 0, &messages[0]);
			std::cout << messages;
		}

		//删除shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glDeleteShader(shader.id);
			}
		}

		//格式化并清除数据
		ShaderFileGroup *file = new ShaderFileGroup();
		for (size_t index = 0; index < m_Shaders.size(); index++)
		{
			auto shader = m_Shaders[index];
			if (shader.id != 0)
			{
				file->m_Used = true;
				file->parse(shader.content, m_KeyWordFunction);
				shader.id = 0;
				shader.content.clear();
			}
		}

		//================================================================================
		//获得所有变量名称
		GLint id = -1;

		//遍历所有结构体获得名称
		std::function<void(const std::string &, const std::string &)> loadAllStruct2Base =
			[&](const std::string &data_name, const std::string &data_type)
		{
			std::string temp;
			//找到结构体信息
			auto it = file->m_StructInfo.find(data_type);
			//遍历结构体内部内置变量
			for (auto d : it->second.m_Value)
			{
				//主名字+结构体内变量名
				temp = data_name + "." + d.first;
				id = glGetUniformLocation(m_ProgramID, temp.c_str());
// 				if (id != -1)
// 				{
					file->m_UniformWithID[temp] = id;
					file->m_Uniforms[temp].setDataType(m_SLDataType[data_type]);
					file->m_Uniforms[temp].setDataTypeString(data_type);
					file->m_Uniforms[temp].setID(id);
//				}
			}

			//遍历所有自定义结构体
			for (auto d : it->second.m_Struct)
			{
				//主名字+结构体变量名
				temp = data_name + "." + d.first;
				loadAllStruct2Base(temp, d.second);
			}
		};

		//注册所有变量
		GLProgram *program = new GLProgram(m_ProgramID);
		for (auto uniform : file->m_UniformInfo)
		{
			if (uniform.second.getDataType() == ShaderDataType::custom)
			{
				loadAllStruct2Base(uniform.first, uniform.second.getDataTypeString());
			}
			else
			{
				id = glGetUniformLocation(m_ProgramID, uniform.first.c_str());
// 				if (id != -1)
// 				{
					file->m_UniformWithID[uniform.first] = id;
					file->m_Uniforms[uniform.first].setDataType(m_SLDataType[uniform.second.getDataTypeString()]);
					file->m_Uniforms[uniform.first].setDataTypeString(uniform.second.getDataTypeString());
					file->m_Uniforms[uniform.first].setID(id);
					switch (file->m_Uniforms[uniform.first].getDataType())
					{
					case ShaderDataType::sampler2D:
					case ShaderDataType::samplerCube:
						break;
					case ShaderDataType::mat2:
					case ShaderDataType::mat3:
					case ShaderDataType::mat4:
						break;

					case ShaderDataType::vec2:
					}
//				}
			}
		}

		//为每一个program创建一个渲染队列,并添加到渲染引擎中
		


		GLRenderPass *renderPass = new GLRenderPass();
		renderPass->setBelong2Queue(static_cast<GLRenderQueueType>(m_QueueID));
		renderPass->setProgram(program);


// 		ProgramAndRenderList *d = new ProgramAndRenderList();
// 		d->list = renderList;
// 		d->file = file;

		Singleton<GLRenderEngine>::getInstance()->addList2Queue(renderPass);

		//添加program
		m_RenderPass[m_ProgramName] = renderPass;
		m_ProgramName.clear();
		m_ProgramID = 0;
		m_QueueID = -1;
	}



	//
	//
	ShaderFileGroup::ShaderFileGroup() :
		m_Used(false)
	{

	}

	void ShaderFileGroup::parse(std::string &shader, KeyWordFunction &function)
	{
		String::removeByFlag(shader, "\n\t\r");
		std::string keyWord;
		char temp;
		for (size_t index = 0; index < shader.size(); index++)
		{
			temp = shader[index];
			//如果读到空格,判断空格前的关键字
			if (temp == ' ' || temp == '(' || temp == ';')
			{
				if (keyWord.empty())
				{
					continue;
				}
				//此时index处为空格
				auto it = function.find(keyWord);
				if (it != function.end())
				{
					function[keyWord](shader, index, this);
				}

				keyWord.clear();
			}
			else
			{
				keyWord += temp;
			}
		}
	}

	void ShaderAssembler::assembleBase(const std::string &file_path)
	{
		auto shader = io::loadBinary2String(file_path);
		auto area = String::split2List(shader, '$');
		area.remove_if([](std::string &str)
		{
			return str.empty();
		});

		std::function<void()> handleVS;
		std::function<void()> handleFS;
// 		std::function<void()> handleGS;
// 		std::function<void()> handleTS;


		for (auto &str : area)
		{
			auto pos = str.find_first_of('\n');
			auto token = str.substr(0, pos);
			str.erase(0, pos + 1);
			String::removeByFlag(token, " ");
			if (token == "vs")
			{
				handleVS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subToken = content.substr(0, subPos);
						String::removeByFlag(subToken, " ");
						content.erase(0, subPos + 1);
						if (subToken == "vslayout")
						{
							m_VS.layout.fixed = content;
						}
						else if (subToken == "vsdeclare")
						{
							m_VS.declare.fixed = content;
						}
						else if (subToken == "vsout")
						{
							m_VS.out.fixed = content;
						}
						else if (subToken == "vsuniform")
						{
							m_VS.uniform.fixed = content;
						}
						else if (subToken == "vscustom")
						{
							m_VS.custom.fixed = content;
						}
						else if (subToken == "vsfunction")
						{
							m_VS.function.fixed = content;
						}
						else if (subToken == "vsmain")
						{
							m_VS.main.fixed = content;
						}
					}
				};
			}
			else if (token == "fs")
			{
				handleFS= [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subToken = content.substr(0, subPos);
						String::removeByFlag(subToken, " ");
						content.erase(0, subPos + 1);
						if (subToken == "fslayout")
						{
							m_FS.layout.fixed = content;
						}
						else if (subToken == "fsdeclare")
						{
							m_FS.declare.fixed = content;
						}
						else if (subToken == "fsin")
						{
							m_FS.in.fixed = content;
						}
						else if (subToken == "fsout")
						{
							m_FS.out.fixed = content;
						}
						else if (subToken == "fsuniform")
						{
							m_FS.uniform.fixed = content;
						}
						else if (subToken == "fscustom")
						{
							m_FS.custom.fixed = content;
						}
						else if (subToken == "fsfunction")
						{
							m_FS.function.fixed = content;
						}
						else if (subToken == "fsmain")
						{
							m_FS.main.fixed = content;
						}
					}
				};
			}
		}

		handleVS();
		handleFS();
	}

	void ShaderAssembler::assembleCustom(const std::string &file_path)
	{
		auto shader = io::loadBinary2String(file_path);
		auto area = String::split2List(shader, '$');
		area.remove_if([](std::string &str)
		{
			return str.empty();
		});

		std::function<void()> handleVS;
		std::function<void()> handleFS;
		// 		std::function<void()> handleGS;
		// 		std::function<void()> handleTS;

		auto addCustom = [](const std::string &controller1, Area &area, std::vector<std::string> &tokens, const std::string &content)
		{
			if (controller1 == "add")
			{
				auto &controller2 = tokens[2];
				String::removeByFlag(controller2, " ");
				if (controller2 == "before")
				{
					area.before = content;
				}
				else if (controller2 == "after")
				{
					area.after = content;
				}
			}
			else if (controller1 == "replace")
			{
				area.fixed = content;
			}
		};

		for (auto &str : area)
		{
			auto pos = str.find_first_of('\n');
			auto token = str.substr(0, pos);
			String::removeByFlag(token, " ");
			str.erase(0, pos + 1);
			if (token == "vs")
			{
				handleVS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subTokenWithController = content.substr(0, subPos);
						auto tokens = String::split2Vector(subTokenWithController, ' ');
						content.erase(0, subPos + 1);

						auto &subToken = tokens[0];
						String::removeByFlag(subToken, " ");
						auto &controller1 = tokens[1];
						String::removeByFlag(controller1, " ");


						if (subToken == "vslayout")
						{
							addCustom(controller1, m_VS.layout, tokens, content);
						}
						else if (subToken == "vsdeclare")
						{
							addCustom(controller1, m_VS.declare, tokens, content);
						}
						else if (subToken == "vsout")
						{
							addCustom(controller1, m_VS.out, tokens, content);
						}
						else if (subToken == "vsuniform")
						{
							addCustom(controller1, m_VS.uniform, tokens, content);
						}
						else if (subToken == "vscustom")
						{
							addCustom(controller1, m_VS.custom, tokens, content);
						}
						else if (subToken == "vsfunction")
						{
							addCustom(controller1, m_VS.function, tokens, content);
						}
						else if (subToken == "vsmain")
						{
							addCustom(controller1, m_VS.main, tokens, content);
						}
					}
				};
			}
			else if (token == "fs")
			{
				handleFS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subTokenWithController = content.substr(0, subPos);
						auto tokens = String::split2Vector(subTokenWithController, ' ');
						content.erase(0, subPos + 1);

						auto &subToken = tokens[0];
						auto &controller1 = tokens[1];

						if (subToken == "fslayout")
						{
							addCustom(controller1, m_FS.layout, tokens, content);
						}
						else if (subToken == "fsdeclare")
						{
							addCustom(controller1, m_FS.declare, tokens, content);
						}
						else if (subToken == "fsin")
						{
							addCustom(controller1, m_FS.in, tokens, content);
						}
						else if (subToken == "fsout")
						{
							addCustom(controller1, m_FS.out, tokens, content);
						}
						else if (subToken == "fsuniform")
						{
							addCustom(controller1, m_FS.uniform, tokens, content);
						}
						else if (subToken == "fscustom")
						{
							addCustom(controller1, m_FS.custom, tokens, content);
						}
						else if (subToken == "fsfunction")
						{
							addCustom(controller1, m_FS.function, tokens, content);
						}
						else if (subToken == "fsmain")
						{
							addCustom(controller1, m_FS.main, tokens, content);
						}
					}
				};
			}
		}

		handleVS();
		handleFS();
	}

	void ShaderAssembler::display()
	{
		assembleVS();
		assembleFS();
	}

	void ShaderAssembler::assembleVS()
	{
		m_VS.assemble(m_VSResult);
		std::cout << m_VSResult;
	}

	void ShaderAssembler::assembleFS()
	{
		m_FS.assemble(m_FSResult);
		std::cout << m_FSResult;
	}

	void ShaderAssembler::removeSpcae(std::string &str)
	{
		String::removeByFlag(str, " ");
	}
}

