#pragma once

#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <vector>
#include <array>
#include "GLRenderQueue.h"
#include "SimpleDataManager.h"

#include "Singleton.h"
#include "glew.h"


/*	自定义shader组装器
声明符
@ 			-- 关键字声明符
$			-- 区域声明符

关键字
vslayout 	-- vs的layout区域
vsdeclare	-- vs声明区域
vsout 		-- vs的out区域
vsuniform	-- vs的uniform区域
vscustom	-- vs的自定义区域
vsfunction 	-- vs自定义函数区域
vsmain		-- vs主函数区域


fslayout	-- fs的layout区域
fsdeclare	-- fs声明区域
fsin 		-- fs的in区域
fsout 		-- fs的out区域
fsuniform	-- fs的uniform区域
fscustom	-- fs的自定义区域
fsfunction 	-- fs自定义函数区域
fsmain 		-- fs主函数区域


操作符
replace 	-- 替换当前位置的内容
add 		-- 向当前位置追加

二级操作符
after		-- 之前
before		-- 之后
*/

namespace tezcat
{
	class ShaderFileGroup;
	typedef std::unordered_map<std::string, std::function<void(std::string &, size_t &, ShaderFileGroup *)>> KeyWordFunction;


	enum class ShaderType
	{
		Vertex,
		Fragment,
		Tess_Control,
		Tess_Evaluation,
		Geometry,
		Count,
	};

	enum class ShaderDataType
	{
		vec2, vec3, vec4,
		mat2, mat3, mat4,
		sampler2D, samplerCube,
		slint, slfloat,
		custom
	};

	//================================
	//	shader layout
	class ShaderLayout
	{
	public:
		enum class IndexType
		{
			location = 0,
			binding
		};

		enum class DataIO
		{
			in = 0,
			out
		};

		//************************************
		// 函数名:  getName
		// 返回值:  std::string
		// 功能:    
		//************************************
		std::string getName() const { return m_Name; }

		//************************************
		// 函数名:  setName
		// 返回值:  void
		// 参数名:  std::string val
		// 功能:    
		//************************************
		void setName(std::string val) { m_Name = val; }

		//************************************
		// 函数名:  getIndexType
		// 返回值:  ShaderLayout::IndexType
		// 功能:    
		//************************************
		ShaderLayout::IndexType getIndexType() const { return m_IndexType; }

		//************************************
		// 函数名:  setIndexType
		// 返回值:  void
		// 参数名:  ShaderLayout::IndexType val
		// 功能:    
		//************************************
		void setIndexType(ShaderLayout::IndexType val) { m_IndexType = val; }

		//************************************
		// 函数名:  getDataIO
		// 返回值:  ShaderLayout::DataIO
		// 功能:    
		//************************************
		ShaderLayout::DataIO getDataIO() const { return m_DataIO; }

		//************************************
		// 函数名:  setDataIO
		// 返回值:  void
		// 参数名:  ShaderLayout::DataIO val
		// 功能:    
		//************************************
		void setDataIO(ShaderLayout::DataIO val) { m_DataIO = val; }

		//************************************
		// 函数名:  getDataType
		// 返回值:  tezcat::ShaderDataType
		// 功能:    
		//************************************
		ShaderDataType getDataType() const { return m_DataType; }

		//************************************
		// 函数名:  setDataType
		// 返回值:  void
		// 参数名:  ShaderDataType val
		// 功能:    
		//************************************
		void setDataType(ShaderDataType val) { m_DataType = val; }

	private:
		std::string m_Name;
		IndexType m_IndexType;
		DataIO m_DataIO;
		ShaderDataType m_DataType;
	};

	//================================
	//	shader uniform变量
	class ShaderUniform
	{
	public:
		ShaderUniform() : m_ID(-1), m_DataTypeString("base") {}

		//************************************
		// 函数名:  getID
		// 返回值:  GLuint
		// 功能:    
		//************************************
		GLint &getID() { return m_ID; }

		//************************************
		// 函数名:  setID
		// 返回值:  void
		// 参数名:  GLuint val
		// 功能:    
		//************************************
		void setID(GLint val) { m_ID = val; }

		//************************************
		// 函数名:  getDataType
		// 返回值:  tezcat::ShaderDataType
		// 功能:    
		//************************************
		ShaderDataType &getDataType() { return m_DataType; }

		//************************************
		// 函数名:  setDataType
		// 返回值:  void
		// 参数名:  ShaderDataType val
		// 功能:    
		//************************************
		void setDataType(ShaderDataType val) { m_DataType = val; }

		//************************************
		// 函数名:  getDataTypeString
		// 返回值:  std::string
		// 功能:    
		//************************************
		std::string getDataTypeString() const { return m_DataTypeString; }

		//************************************
		// 函数名:  setDataTypeString
		// 返回值:  void
		// 参数名:  std::string val
		// 功能:    
		//************************************
		void setDataTypeString(std::string val) { m_DataTypeString = val; }

	private:
		GLint m_ID;
		ShaderDataType m_DataType;
		std::string m_DataTypeString;
	};

	//================================
	//	shader 结构体
	class ShaderStruct
	{
	public:
		//名字,类型
		std::unordered_map<std::string, std::string> m_Value;
		//名字,类型
		std::unordered_map<std::string, std::string> m_Struct;
	};

	//================================
	//	shader文件组
	class ShaderFileGroup
	{
	public:
		ShaderFileGroup();

		//************************************
		// 函数名:  parse
		// 返回值:  void
		// 参数名:  std::string & shader
		// 参数名:  KeyWordFunction & function
		// 功能:    
		//************************************
		void parse(std::string &shader, KeyWordFunction &function);

		//************************************
		// 函数名:  addLayout
		// 返回值:  void
		// 参数名:  const size_t & id
		// 参数名:  const ShaderLayout & layout
		// 功能:    
		//************************************
		void addLayout(const size_t &id, const ShaderLayout &layout)
		{
			m_LayoutInfo.insert(std::make_pair(id, layout));
		}

		//************************************
		// 函数名:  addUniform
		// 返回值:  void
		// 参数名:  const std::string & name
		// 参数名:  ShaderUniform & uniform
		// 功能:    
		//************************************
		void addUniform(const std::string &name, ShaderUniform &uniform)
		{
			m_UniformInfo.insert(std::make_pair(name, uniform));
		}

		//************************************
		// 函数名:  addStruct
		// 返回值:  void
		// 参数名:  const std::string & name
		// 参数名:  ShaderStruct & slstruct
		// 功能:    
		//************************************
		void addStruct(const std::string &name, ShaderStruct &slstruct)
		{
			m_StructInfo.insert(std::make_pair(name, slstruct));
		}

		ShaderUniform *findUniform(const std::string &name)
		{
			return &m_Uniforms[name];
		}

	private:
		friend class ShaderEngine;
		bool m_Used;
		std::string m_Name;
		//位置,信息
		std::unordered_map<size_t, ShaderLayout> m_LayoutInfo;
		//名字,信息
		std::unordered_map<std::string, ShaderUniform> m_UniformInfo;
		//名字,信息
		std::unordered_map<std::string, ShaderStruct> m_StructInfo;
		//
		std::unordered_map<std::string, GLint> m_UniformWithID;
		//
		std::unordered_map<std::string, ShaderUniform> m_Uniforms;
	};

	class ShaderAssembler
	{
	private:
		struct Area
		{
			std::string before;
			std::string fixed;
			std::string after;

			void clear()
			{
				before.clear();
				before.shrink_to_fit();
				fixed.clear();
				fixed.shrink_to_fit();
				after.clear();
				after.shrink_to_fit();
			}
		};

		struct ShaderStruct
		{
			std::string version;	//版本区域
			Area layout;			//layout区域
			Area declare;			//声明区域
			Area uniform;			//uniform区域
			Area out;				//out区域
			Area in;				//in区域
			Area custom;			//自定义区域
			Area function;			//函数区域
			Area main;				//主函数区域

			void clear()
			{
				layout.clear();
				declare.clear();
				uniform.clear();
				out.clear();
				in.clear();
				custom.clear();
				function.clear();
				main.clear();
			}

			void assemble(std::string &result)
			{
				result =
					version +
					layout.before + layout.fixed + layout.after +
					declare.before + declare.fixed + declare.after +
					in.before + in.fixed + in.after +
					out.before + out.fixed + out.after +
					uniform.before + uniform.fixed + uniform.after +
					custom.before + custom.fixed + custom.after +
					function.before + function.fixed + function.after +
					"void main()\n{\n" + main.before + main.fixed + main.after + "\n}";
				clear();
			}

		};

	public:
		enum class Shader
		{
			VS,FS,GS,TS
		};

		void assembleBase(const std::string &file_path);
		void assembleCustom(const std::string &file_path);
		void display();

	private:
		void assembleVS();
		void assembleFS();

		void removeSpcae(std::string &str);

	private:
		ShaderStruct m_VS;
		ShaderStruct m_FS;
		ShaderStruct m_GS;
		ShaderStruct m_TS;

		std::string m_VSResult;
		std::string m_FSResult;
	};

	//================================
	//	引擎
	class ShaderEngine
	{
		struct ShaderInfo
		{
			GLuint id;
			std::string content;
		};

	public:
		ShaderEngine();
		~ShaderEngine();

		//************************************
		// 函数名:  preload
		// 返回值:  void
		// 功能:    
		//************************************
		void preload();

		//************************************
		// 函数名:  getProgram
		// 返回值:  GLProgram *
		// 参数名:  const std::string & name
		// 功能:    
		//************************************
		template<bool fast>
		GLRenderPass *getProgram(const std::string &name)
		{
			auto it = m_RenderPass.find(name);
			if (it != m_RenderPass.end())
			{
				return it->second;
			}
			else
			{
				auto &file = Singleton<SimpleDataManager>::getInstance()->getShaderFile(name);
				this->begin(file["base"]["name"]().asSTDString(), file["base"]["queue"]().asInt());
				this->addShader(file["shader"]["vertex"]().asSTDString(), ShaderType::Vertex);
				this->addShader(file["shader"]["fragment"]().asSTDString(), ShaderType::Fragment);
				this->end();
				return m_RenderPass[name];
			}
		}

		//************************************
		// 函数名:  getProgram
		// 返回值:  GLProgram *
		// 参数名:  const std::string & name * /
		// 功能:    
		//************************************
		template<>
		GLRenderPass *getProgram<true>(const std::string &name)
		{
			return m_RenderPass[name];
		}


	private:


		//************************************
		// 函数名:  begin
		// 返回值:  void
		// 参数名:  const std::string & name
		// 参数名:  const int & queue_id
		// 功能:    
		//************************************
		void begin(const std::string &name, const int &queue_id);

		//************************************
		// 函数名:  addShader
		// 返回值:  void
		// 参数名:  const std::string & file
		// 参数名:  ShaderType type
		// 功能:    
		//************************************
		void addShader(const std::string &file, ShaderType type);

		//************************************
		// 函数名:  end
		// 返回值:  void
		// 功能:    
		//************************************
		void end();



	private:
		GLuint m_ProgramID;
		std::string m_ProgramName;
		int m_QueueID;
		std::vector<ShaderInfo> m_Shaders;

		std::unordered_map<std::string, GLRenderPass *> m_RenderPass;
		std::unordered_map<std::string, ShaderFileGroup> m_Groups;
		std::unordered_map<std::string, ShaderDataType> m_SLDataType;

		ShaderAssembler m_Assembler;

		KeyWordFunction m_KeyWordFunction;
	};


}