#pragma once
#include "XEntity.h"
#include "XFactory.h"
#include "XTag.h"
#include "Singleton.h"

namespace tezcat
{
#define MaxComponent 16

	enum class TEntityType : size_t
	{
		SkyBox,
		Camera,
		Light,
		Node,
		Model,
		Count
	};


	typedef XEntity<MaxComponent> TEntity;
	typedef XTag<TEntityType> TEntityTag;

	class TEntityFactory : public XFactory2<TEntity>
	{
	public:
		TEntityFactory();
		~TEntityFactory();
	};
}
