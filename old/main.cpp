#include "GLFWEngine.h"
#include "GLRenderEngine.h"
#include <iostream>

#include "Lesson1.h"



int main()
{
	tezcat::Singleton<tezcat::GLFWEngine>::init();
	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->initEngine();
	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->setRenderEngine(tezcat::Singleton<tezcat::GLRenderEngine>::init());
 	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->createWindow("haha", 960, 640);
	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->setContextCurrent("haha");
	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->openVsync(true);
	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->onWindowSizeChange();

	Lesson1::Test t;
	t.init();

	tezcat::Singleton<tezcat::GLFWEngine>::getInstance()->loop();
	tezcat::Singleton<tezcat::GLFWEngine>::deleteInstance();



 	exit(EXIT_SUCCESS);

	return 0;
}