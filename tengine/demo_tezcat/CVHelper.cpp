#include "CVHelper.h"

namespace tezcat
{
	CVHelper::CVHelper()
	{

	}

	CVHelper::~CVHelper()
	{

	}

	void CVHelper::openCamera()
	{
		m_Capture = cv::VideoCapture(0);
		if (!m_Capture.isOpened())
		{
			abort();
		}
	}

	void CVHelper::getImage()
	{
		m_Capture >> m_Image;
		cv::cvtColor(m_Image, m_Image, CV_BGR2RGBA);
		cv::flip(m_Image, m_Image, 0);
		cv::resize(m_Image, m_Image, cv::Size(GlobalConfiguration::ScreenWidth, GlobalConfiguration::ScreenHegiht));
		m_CVVideo->getRenderObject()->getMaterial<UniversalMaterial>()->updataTexture2D("cvvideo", m_Image.cols, m_Image.rows, 0, 0, m_Image.data);
	}

	Entity * CVHelper::createCVVideo()
	{
		m_CVVideo = EntityManager::getInstance()->create();

		ScreenCommand *cmd = new ScreenCommand();
		cmd->setQueueType(RenderQueueType::Queue_Background);
		cmd->sendData2GL();

		UniversalMaterial *um = new UniversalMaterial();
		um->genTexture2DBuffer("cvvideo", GlobalConfiguration::ScreenWidth, GlobalConfiguration::ScreenHegiht);
		um->setPassID(0);

		RenderObject *screen_object = new RenderObject();
		screen_object->setObjectType(RenderObject::ObjectType::Type_Mesh);
		screen_object->setRenderCommand(cmd);
		screen_object->setTransform(Transform::create());
		screen_object->setMaterial(um);
		m_CVVideo->setRenderObject(screen_object);

		return m_CVVideo;
	}

}

