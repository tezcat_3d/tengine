#pragma once

#include "Tezcat.h"
#include "opencv2/opencv.hpp"

namespace tezcat
{
	class CVHelper
	{
	public:
		CVHelper();
		~CVHelper();

		void openCamera();
		void getImage();

		Entity *createCVVideo();

	private:
		tezcat::Entity *m_CVVideo;
		cv::Mat m_Image;
		cv::VideoCapture m_Capture;
	};
}