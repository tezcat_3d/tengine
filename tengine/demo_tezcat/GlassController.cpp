#include "GlassController.h"

namespace tezcat
{
	GlassController::GlassController(Transform *transform):
		m_Transform(transform)
	{

	}

	GlassController::~GlassController()
	{

	}

	void GlassController::logic()
	{
//		m_Transform->setRotationYByOffset(10 * GlobalConfiguration::DeltaTime);
		m_Transform->setMatrix(
			0, 0, 0,
			0, 0, 0, 
			0, 0, 0, 
			0, 0, 0);
	}
}