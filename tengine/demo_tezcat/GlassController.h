#pragma once


#include "Tezcat.h"

namespace tezcat
{
	class GlassController : public XComponent<GlassController, XDynamicFlag>
	{
	public:
		GlassController(Transform *transform);
		~GlassController();

		virtual void logic() override;

	private:
		Transform *m_Transform;
	};
}
