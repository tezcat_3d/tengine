
#include <iostream>
#include <thread>
#include <Windows.h>
#include <string>

#include "Tezcat.h"
#include "GLFW/glfw3.h"

tezcat::Camera *MainCamera = nullptr;
bool keys[1024];
bool firstMouse = true;

double lastX = 0;
double lastY = 0;
int CURSOR = GLFW_CURSOR_DISABLED;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;

	if (keys[GLFW_KEY_SPACE])
	{
		if (CURSOR == GLFW_CURSOR_NORMAL)
		{
			CURSOR = GLFW_CURSOR_DISABLED;
		}
		else
		{
			CURSOR = GLFW_CURSOR_NORMAL;
		}
		glfwSetInputMode(window, GLFW_CURSOR, CURSOR);
	}
}

void Do_Movement()
{
	float speed = 10;

	if (keys[GLFW_KEY_W])
		MainCamera->forward(tezcat::GlobalConfiguration::DeltaTime);
	if (keys[GLFW_KEY_S])
		MainCamera->back(tezcat::GlobalConfiguration::DeltaTime);
	if (keys[GLFW_KEY_A])
		MainCamera->left(tezcat::GlobalConfiguration::DeltaTime);
	if (keys[GLFW_KEY_D])
		MainCamera->right(tezcat::GlobalConfiguration::DeltaTime);

}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = static_cast<float>(xpos - lastX);
	GLfloat yoffset = static_cast<float>(lastY - ypos);

	lastX = xpos;
	lastY = ypos;

	MainCamera->onMouse(xoffset, yoffset);
}


int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 8);


	GLFWwindow* window = glfwCreateWindow(tezcat::GlobalConfiguration::ScreenWidth, tezcat::GlobalConfiguration::ScreenHegiht, tezcat::GlobalConfiguration::WindowName.c_str(), nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glViewport(0, 0, tezcat::GlobalConfiguration::ScreenWidth, tezcat::GlobalConfiguration::ScreenHegiht);


	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	tezcat::EngineEntry engineEntry;
	engineEntry.init("");

	glViewport(0, 0, tezcat::GlobalConfiguration::ScreenWidth, tezcat::GlobalConfiguration::ScreenHegiht);


	tezcat::SceneSystem::getInstance()->pushScene(
		"myScene",
		[](tezcat::Scene *scene)
	{
		tezcat::UniversalMaterial *mat = tezcat::ResourceSystem::getInstance()->createUniversalMaterialFromPackage("skybox.skybox");

		//����shader
		auto skybox_shader = tezcat::ResourceSystem::getInstance()->createProgramFromPackage("skybox");
		auto *skybox_pass = new tezcat::SkyboxPass();
		skybox_pass->setProgram(skybox_shader);
		skybox_pass->setBelong2Queue(tezcat::RenderQueueType::Queue_Background);
		skybox_pass->setPassID(mat->getPassID());

		tezcat::RenderSystem::getInstance()->attachRenderPass(skybox_pass);


		auto skybox = tezcat::ResourceSystem::getInstance()->createSkyBox();
		skybox->setName("skybox");
		auto skybox_render = skybox->getRenderObject();
		skybox_render->setMaterial(mat);
		scene->addChild(skybox);

		//
		//
		auto model_shader = tezcat::ResourceSystem::getInstance()->createProgramFromPackage("MyPBR");
		auto *mesh_pass = new tezcat::PBRPass();
		mesh_pass->setProgram(model_shader);
		mesh_pass->setBelong2Queue(tezcat::RenderQueueType::Queue_Geometry);
		mesh_pass->setPassID(0);
		tezcat::RenderSystem::getInstance()->attachRenderPass(mesh_pass);

// 		tezcat::UniversalMaterial *model_mat = tezcat::ResourceSystem::getInstance()->createUniversalMaterialFromPackage("PBRTest");
// 		model_mat->setPassID(1);

		auto model = tezcat::ResourceSystem::getInstance()->createModelFromPackage("teapot");
// 		model->getRenderObject()->getTransform()->setRotationX(-90);
// 		model->getRenderObject()->getTransform()->setRotationZ(-90);
		scene->addChild(model);

		auto camera = tezcat::ResourceSystem::getInstance()->createCamera("MainCamera");
		MainCamera = camera->getRenderObject()->getCamera();
		MainCamera->getPosition().z = 10;
		MainCamera->setSpeed(30);
		scene->addChild(camera);
	},
		nullptr,
		nullptr,
		nullptr);

	double lastFrame = 0;
	double currentFrame = 0;

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		Do_Movement();

		currentFrame = glfwGetTime();
		tezcat::GlobalConfiguration::DeltaTime = static_cast<float>(currentFrame - lastFrame);
		lastFrame = currentFrame;

		engineEntry.logic();
		engineEntry.render();
		glfwSwapBuffers(window);
	}

	glfwTerminate();

	return 0;
}