#include "Camera.h"

namespace tezcat
{
	Camera::Camera():
		m_Up(0.0f, 1.0f, 0.0f), m_Yaw(0), m_Pitch(0.0f), m_Front(0.0f, 0.0f, -1.0f),
		m_Speed(100),m_NeedUpdata(true) //m_XListNode(XList<Camera *>::createNode(this))
	{

	}

	Camera::Camera(const float &fov, const float &window_width, const float &window_height, const float &near_, const float &far_) :
		m_Up(0.0f, 1.0f, 0.0f), m_Yaw(0), m_Pitch(0.0f), m_Front(0.0f, 0.0f, -1.0f),
		m_Speed(100),
		m_Fov(fov), m_WindowHeight(window_height), m_WindowWidth(window_width), m_Near(near_), m_Far(far_),
		m_Aspect(window_width / window_height), m_NeedUpdata(true)
//		m_XListNode(XList<Camera *>::createNode(this))
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fov), m_Aspect, m_Near, m_Far);
	}

	void Camera::updata()
	{
		glm::vec3 fornt;
		fornt.x = glm::cos(glm::radians(m_Yaw)) * glm::cos(glm::radians(m_Pitch));
		fornt.y = glm::sin(glm::radians(m_Pitch));
		fornt.z = glm::sin(glm::radians(m_Yaw)) * glm::cos(glm::radians(m_Pitch));

		m_Front = glm::normalize(fornt);

		m_Right = glm::normalize(glm::cross(m_Front, m_Up));
	}

	Camera::~Camera()
	{

	}

	void Camera::render()
	{
// 		if (m_NeedUpdata)
// 		{
			m_ViewMatrix = m_Transform->getWorldMatrix() * glm::lookAt(m_Position, m_Position + m_Front, m_Up);
//			m_NeedUpdata = false;
//		}
	}



	void Camera::resetPerspective(const float &fov, const float &window_width, const float &window_height, const float &near_, const float &far_)
	{
		m_Fov = fov;
		m_WindowHeight = window_height;
		m_WindowWidth = window_width;
		m_Near = near_;
		m_Far = far_;
		m_Aspect = window_width / window_height;

		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fov), m_Aspect, m_Near, m_Far);
	}

	void Camera::resetPerspective()
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(m_Fov), m_Aspect, m_Near, m_Far);
		this->updata();
	}

	void Camera::onMouse(float x_offset, float y_offset, bool constrain_pitch /*= true*/)
	{
		x_offset *= 0.25;
		y_offset *= 0.25;

		m_Yaw += x_offset;
		m_Pitch += y_offset;

		if (constrain_pitch)
		{
			if (m_Pitch > 89.0f)
			{
				m_Pitch = 89.0f;
			}
			if (m_Pitch < -89.0f)
			{
				m_Pitch = -89.0f;
			}
		}
		m_NeedUpdata = true;

		this->updata();
	}

}

