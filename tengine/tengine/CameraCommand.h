#pragma once

#include "RenderCommand.h"

namespace tezcat
{
	class CameraCommond : public RenderCommand
	{
	public:
		CameraCommond();
		~CameraCommond();

		virtual void sendData2GL(VertexData *data) override {};

		virtual void render() override;

	private:

	};


}
