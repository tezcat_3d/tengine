#include "EngineEntry.h"
#include "RenderSystem.h"
#include "SceneSystem.h"
#include "ResourceSystem.h"
#include "EntityManager.h"
#include "EngineEvent.h"
#include "xsystem/XLogSystem.h"
#include "ProgramManager.h"


namespace tezcat
{
	EngineEntry::EngineEntry():
		m_EngineClose(false)
	{
		EngineEvent::initInstance();
		RenderSystem::initInstance();
		ResourceSystem::initInstance();
		SceneSystem::initInstance();
		EntityManager::initInstance();
		XLogSystem::initInstance();
		ProgramManager::initInstance();
	}

	EngineEntry::~EngineEntry()
	{
		EngineEvent::deleteInstance();
		RenderSystem::deleteInstance();
		ResourceSystem::deleteInstance();
		SceneSystem::deleteInstance();
		EntityManager::deleteInstance();
		XLogSystem::deleteInstance();
		ProgramManager::deleteInstance();
	}

	void EngineEntry::init(const std::string &res_dir)
	{
		ResourceSystem::getInstance()->loadConfiguration(res_dir, "resource.res");
		RenderSystem::getInstance()->initSystem();
	}

	void EngineEntry::render()
	{
		RenderSystem::getInstance()->render();
	}

	void EngineEntry::logic()
	{
		SceneSystem::getInstance()->logic();
	}
}