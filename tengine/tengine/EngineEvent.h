#pragma once

#include "xsystem/XEvent.h"

namespace tezcat
{
	//=================================================
	//
	//	Shader Event
	//
	class Program;
	class ShaderCreated : public XEvent<ShaderCreated>
	{
	public:
		virtual void clear() { program = nullptr; }

		Program *program;
	};


	//==================================================
	//
	//	Scene Event
	//
	class Scene;
	class SceneReplace : public XEvent<SceneReplace>
	{
	public:
		virtual void clear() { scene = nullptr; }

		Scene *scene;
	};

	class ScenePush : public XEvent<ScenePush>
	{
	public:
		virtual void clear() { scene = nullptr; }

		Scene *scene;
	};

	//==================================================
	//
	//	Events
	//
	class EngineEvent
	{
	public:
		static void initInstance()
		{
			XEventSystem::initInstance();
			XEventSystem::getInstance()->registerEvent<ShaderCreated>();
			XEventSystem::getInstance()->registerEvent<SceneReplace>();
			XEventSystem::getInstance()->registerEvent<ScenePush>();
		}

		static void deleteInstance()
		{
			XEventSystem::deleteInstance();
		}
	};
}