#include "Entity.h"
#include "Transform.h"

namespace tezcat
{


	Entity::Entity(Entity *parent) :
		m_GlobalID(XIDCreator<Entity>::give()),
		m_GroupID(0),
		m_IDInGroup(0),
		m_Parent(parent),
//		m_XListNode(XList<Entity *>::createNode(this)),
		m_RenderObject(nullptr)
	{

	}


	Entity::~Entity()
	{
//		delete m_XListNode;
	}

}