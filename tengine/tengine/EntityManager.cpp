#include "EntityManager.h"
#include "RenderObject.h"
#include "MeshCommand.h"
#include "Mesh.h"

namespace tezcat
{
	EntityManager::EntityManager()
	{

	}

	EntityManager::~EntityManager()
	{

	}

	Entity *EntityManager::create(Entity *parent)
	{
		if (m_Entities.empty())
		{
			Entity *e = new Entity(parent);
			return e;
		}
		else
		{
			auto e = m_Entities.front();
			e->setParent(parent);
			m_Entities.pop_front();
			return e;
		}
	}

	Entity *EntityManager::create()
	{
		return this->create(nullptr);
	}

	void EntityManager::recycle(Entity * entity)
	{
		m_Entities.push_back(entity);
	}

	Entity *EntityManager::createModel(const std::string & name, Entity * parent)
	{
		if (m_Entities.empty())
		{
			Entity *e = new Entity(parent);
			e->setName(name);
			e->setParent(parent);

			auto render_object = new RenderObject();
			render_object->setObjectType(RenderObject::ObjectType::Type_Mesh);
			render_object->setMesh(Mesh::create(name));
			render_object->setRenderCommand(new MeshCommand());
			render_object->setTransform(Transform::create());
			e->setRenderObject(render_object);

			return e;
		}
		else
		{
			auto e = m_Entities.front();
			m_Entities.pop_front();
			e->setName(name);
			e->setParent(parent);
			return e;
		}
	}
}


