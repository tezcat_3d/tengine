#include "FBO.h"

namespace tezcat
{
	
	FBO::FBO(
		const unsigned int &fbo_width, const unsigned int &fbo_height,
		const unsigned int &texture_count, const unsigned int &fbo_ID):
		m_Width(fbo_width), 
		m_Height(fbo_height),
		m_OffsetX(0),
		m_OffsetY(0),
		m_TextureCount(texture_count),
		m_FBOID(fbo_ID)
	{

	}

	FBO::~FBO()
	{

	}
}


