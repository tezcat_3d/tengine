#pragma once

#include <string>
#include <vector>
#include "GLTexture.h"

namespace tezcat
{
	class GlTexture2D;

// 	enum class InternalType : unsigned int
// 	{
// 		R8, R16, RG8, RG16, RGB8, RGB16,
// 		,RGBA8, RGBA8I, RGBA8UI, RGBA8F,
// 	};

	struct FBOConfigure
	{

	};

	class FBO
	{
	public:
		enum class FBOType
		{
			ColorFBO = 0,
			DepthFBO
		};

	public:
		FBO(const unsigned int &fbo_width, const unsigned int &fbo_height, const unsigned int &texture_count, const unsigned int &fbo_ID);

		~FBO();
		
		void setColorType() { m_FBOType = FBOType::ColorFBO; }

		void setDepthType() { m_FBOType = FBOType::DepthFBO; }

		void setRBOID(const unsigned int &rbo_id) { m_RBOID = rbo_id; }

		void setOffsetX(const unsigned int &val) { m_OffsetX = val; }

		void setOffsetY(const unsigned int &val) { m_OffsetY = val; }

		const unsigned int &getWidth() const { return m_Width; }

		const unsigned int &getHeight() const { return m_Height; }

		const unsigned int &getOffsetX() const { return m_OffsetX; }

		const unsigned int &getOffsetY() const { return m_OffsetY; }

		const unsigned int &getTextureCount() const { return m_TextureCount; }

		const FBOType &getFBOType() const { return m_FBOType; }
		
		const unsigned int &getFBOID() const { return m_FBOID; }

		const unsigned int &getRBOID() const { return m_RBOID; }


		void addColorTexture(GLTexture2D *tex) { m_ColorTexture.push_back(tex); }
		GLTexture2D* getColorTexture(const unsigned int &index) const { return m_ColorTexture[index]; }

		GLTexture2D* getDepthTexture() const { return m_DepthTexture; }

	private:
		unsigned int m_FBOID;
		unsigned int m_RBOID;
		unsigned int m_Width, m_Height;
		unsigned int m_OffsetX, m_OffsetY;
		unsigned int m_TextureCount;

		FBOType m_FBOType;
		GLTexture2D* m_DepthTexture;
		GLTexture2D* m_StencilTexture;
		std::vector<GLTexture2D *> m_ColorTexture;
	};
}