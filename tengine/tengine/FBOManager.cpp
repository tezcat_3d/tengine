#include "FBOManager.h"
#include "FBO.h"
#include "GlobalConfiguration.h"
#include "GLTexture.h"
#include "GLHead.h"
namespace tezcat
{
	FBOManager::FBOManager()
	{

	}

	tezcat::FBOManager::~FBOManager()
	{

	}

	void FBOManager::bindCustomFBO(FBO *fbo)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->getFBOID());
		glViewport(fbo->getOffsetX(), fbo->getOffsetY(), fbo->getWidth(), fbo->getHeight());
	}

	void FBOManager::bindSystemFBO()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, GlobalConfiguration::ScreenWidth, GlobalConfiguration::ScreenHegiht);
	}

	FBO *FBOManager::createColorFBO(const unsigned int &width, const unsigned int &height, const unsigned int &texture_count)
	{
		std::vector<GLenum> drawBuffers;
		drawBuffers.reserve(6);
		GLuint fboID;
		glGenFramebuffers(1, &fboID);
		glBindFramebuffer(GL_FRAMEBUFFER, fboID);
		glDrawBuffer(GL_ONE);
		glReadBuffer(GL_ONE);
	
		FBO *fbo = new FBO(width, height, texture_count, fboID);
		fbo->setColorType();
		
		for (unsigned int i = 0; i < texture_count; i++)
		{
			auto temp = new GLTexture2D();
			temp->genTextureBuffer(width, height, GL_RGBA, GL_RGBA32F, GL_UNSIGNED_BYTE);
			temp->setTextureMinFilter(GL_LINEAR);
			temp->setTextureMagFilter(GL_LINEAR);
			fbo->addColorTexture(temp);
			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, temp->getTexID(), 0);
			glBindTexture(GL_TEXTURE_2D, 0);
			drawBuffers.push_back(GL_COLOR_ATTACHMENT0 + i);
		}
		glDrawBuffers(texture_count, drawBuffers.data());

		GLuint rboID;
		glGenRenderbuffers(1, &rboID);
		glBindRenderbuffer(GL_RENDERBUFFER, rboID);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		m_FBOInSystem.push_back(fbo);
		return fbo;
	}

	FBO *FBOManager::createDepthFBO(const unsigned int &width, const unsigned int &height)
	{
		GLuint fboID;
		glGenFramebuffers(1, &fboID);
		FBO *fbo = new FBO(width,height,0,fboID);
		auto tempTexture = fbo->getDepthTexture();
		tempTexture = new GLTexture2D();
		tempTexture->genDepthBuffer(width, height);
		tempTexture->setTextureMinFilter(GL_NEAREST);
		tempTexture->setTextureMagFilter(GL_NEAREST);
		tempTexture->setTexParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		tempTexture->setTexParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tempTexture->getTexID(), 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		fbo->setDepthType();
		bindSystemFBO();
		m_FBOInSystem.push_back(fbo);
		return fbo;
	}
}