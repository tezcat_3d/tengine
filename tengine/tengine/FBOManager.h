#pragma once

#include <vector>
#include "xsystem/XSingleton.h"


namespace tezcat
{
	class FBO;
	class FBOManager :public XSingleton<FBOManager>
	{
	public:
		FBOManager();

		~FBOManager();

		FBO* getFBO(const unsigned int &index) { return m_FBOInSystem[index]; }

		void bindCustomFBO(FBO *fbo);

		void bindSystemFBO();

		FBO *createColorFBO(const unsigned int &width, const unsigned int &height, const unsigned int &texture_count);

		FBO *createDepthFBO(const unsigned int &width, const unsigned int &height);

	private:
		std::vector<FBO *> m_FBOInSystem;
		FBO *m_CacheFBO;
	};
}