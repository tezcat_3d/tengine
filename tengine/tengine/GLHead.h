#pragma once

#ifdef WIN32
#define GLEW_STATIC
#include "glew.h"
#elif Linux
#define GLEW_STATIC
#include "glew.h"
#elif IOS
#import <OpenGLES/ES3/gl.h>
#elif Android
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#endif

