#pragma once

#include "GLHead.h"

namespace tezcat
{
	namespace glstate
	{
		static inline void init()
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);

			glClearColor(0.5, 0.5, 0.5, 1.0);
			glClearDepthf(1.0f);
		}

		static inline void onRender()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		static inline void onBackground()
		{
			glDisable(GL_DEPTH_TEST);
		}

		static inline void onGeometry()
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
		}

		static inline void onAlphaBegin()
		{
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}

		static inline void onAlphaEnd()
		{
			glDisable(GL_BLEND);
		}

		static inline void onOverlay()
		{
			glDisable(GL_DEPTH_TEST);
		}
	}

}

