#include "GLTexture.h"
#include "ImageManager.h"

namespace tezcat
{
	GLTexture::GLTexture(): m_TexID(0)
	{
		glGenTextures(1, &m_TexID);
	}

	GLTexture::~GLTexture()
	{
		glDeleteTextures(1, &m_TexID);
	}

	//
	//
	GLTexture2D::GLTexture2D() : m_Image(nullptr)
	{
		m_TexType = GL_TEXTURE_2D;
		glBindTexture(m_TexType, m_TexID);
	}

	GLTexture2D::~GLTexture2D()
	{

	}

	void GLTexture2D::genTexture(Image *image)
	{
		m_Image = image;
		m_Internalformat = GL_RGBA;
		m_ExternalFormat = GL_RGBA;
		m_ExternalType = GL_UNSIGNED_BYTE;
		glTexImage2D(m_TexType, m_Image->getMipMap(), m_Internalformat, m_Image->getWidth(), m_Image->getHeight(), 0, m_ExternalFormat, m_ExternalType, m_Image->getData());
	}

	void GLTexture2D::genTextureBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = internalformat;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(m_TexType, 0, internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::storageTextureBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			//return;
		}

		m_Internalformat = internalformat;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexStorage2D(m_TexType, 0, internalformat, width, height);
	}

	void GLTexture2D::genDepthBuffer(const unsigned int &width, const unsigned int &height)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = GL_DEPTH_COMPONENT;
		m_ExternalFormat = GL_DEPTH_COMPONENT;
		m_ExternalType = GL_FLOAT;
		glTexImage2D(m_TexType, 0, m_Internalformat, width, height, 0, m_ExternalFormat, m_ExternalType, 0);
	}

	void GLTexture2D::genStencilBuffer(const unsigned int &width, const unsigned int &height, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = GL_STENCIL_INDEX;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(m_TexType, 0, m_Internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::genCustomBuffer(const unsigned int &width, const unsigned int &height, GLint internalformat, GLenum format, GLenum precision)
	{
		if (m_Image != nullptr)
		{
			return;
		}

		m_Internalformat = internalformat;
		m_ExternalFormat = format;
		m_ExternalType = precision;
		glTexImage2D(m_TexType, 0, internalformat, width, height, 0, format, precision, 0);
	}

	void GLTexture2D::updata(const unsigned int &width, const unsigned int &height, unsigned char *data)
	{
		glBindTexture(m_TexType, m_TexID);
		glTexSubImage2D(m_TexType, 0, 0, 0, width, height, m_ExternalFormat, m_ExternalType, data);
	}

	void GLTexture2D::updata(const unsigned int &width, const unsigned int &height, const unsigned int &xoffset, const unsigned int &yoffset, unsigned char *data)
	{
		glBindTexture(m_TexType, m_TexID);
		glTexSubImage2D(m_TexType, 0, xoffset, yoffset, width, height, m_ExternalFormat, m_ExternalType, data);
	}

	void GLTexture2D::updata(GLTexture2D *tex)
	{
		glBindTexture(m_TexType, m_TexID);
		glTexSubImage2D(m_TexType, tex->m_Image->getMipMap(), 0, 0, tex->m_Image->getWidth(), tex->m_Image->getHeight(), m_ExternalFormat, m_ExternalType, tex->m_Image->getData());
	}


	//==============================================================
	//
	//	��������ͼ
	//
	GLTextureCube::GLTextureCube()
	{
		m_TexType = GL_TEXTURE_CUBE_MAP;
		glBindTexture(m_TexType, m_TexID);
		m_Images[Position::Count] = { nullptr };
	}

	GLTextureCube::~GLTextureCube()
	{
		
	}

	void GLTextureCube::genTexture(Image *positive_x, Image *negative_x, Image *positive_y, Image *negative_y, Image *positive_z, Image *negative_z)
	{
		//
		//
		auto *img_positive_x = positive_x;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			img_positive_x->getMipMap(), GL_RGBA,
			img_positive_x->getWidth(), img_positive_x->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_x->getData());
		m_Images[Position::POSITIVE_X] = img_positive_x;

		auto *img_negative_x = negative_x;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			img_negative_x->getMipMap(), GL_RGBA,
			img_negative_x->getWidth(), img_negative_x->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_x->getData());
		m_Images[Position::NEGATIVE_X] = img_negative_x;

		//
		//
		auto *img_positive_y = positive_y;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			img_positive_y->getMipMap(), GL_RGBA,
			img_positive_y->getWidth(), img_positive_y->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_y->getData());
		m_Images[Position::POSITIVE_Y] = img_positive_y;

		auto *img_negative_y = negative_y;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
			img_negative_y->getMipMap(), GL_RGBA,
			img_negative_y->getWidth(), img_negative_y->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_y->getData());
		m_Images[Position::NEGATIVE_Y] = img_negative_y;

		//
		//
		auto *img_positive_z = positive_z;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			img_positive_z->getMipMap(), GL_RGBA,
			img_positive_z->getWidth(), img_positive_z->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_positive_z->getData());
		m_Images[Position::POSITIVE_Z] = img_positive_z;

		auto *img_negative_z = negative_z;
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			img_negative_z->getMipMap(), GL_RGBA,
			img_negative_z->getWidth(), img_negative_z->getHeight(),
			0, GL_RGBA, GL_UNSIGNED_BYTE, img_negative_z->getData());
		m_Images[Position::NEGATIVE_Z] = img_negative_z;

		//
		//
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glBindTexture(m_TexType, 0);
	}

}