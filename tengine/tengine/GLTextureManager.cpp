#include "GLTextureManager.h"
#include "GLTexture.h"
#include "ImageManager.h"

namespace tezcat
{

	GLTextureManager::GLTextureManager():m_ErrorTexture2D(nullptr)
	{

	}

	GLTexture2D * GLTextureManager::createTexture2D(const std::string &name, ImageManager *img_manager)
	{
		auto it = m_Texture2DInSystem.find(name);

		if (it != m_Texture2DInSystem.end())
		{
			return it->second;
		}
		else
		{
			GLTexture2D *t2d = new GLTexture2D();
			t2d->genTexture(img_manager->createImage(name));
			t2d->setTextureMinFilter(GL_LINEAR);
			t2d->setTextureMagFilter(GL_LINEAR);
			m_Texture2DInSystem.emplace(std::make_pair(name, t2d));
			return t2d;
		}
	}

	GLTexture2D * GLTextureManager::createTexture2DFromPackage(const std::string &pack_dot_name, ImageManager *img_manager)
	{
		auto it = m_Texture2DInEngine.find(pack_dot_name);

		if (it != m_Texture2DInEngine.end())
		{
			return it->second;
		}
		else
		{
			auto img = img_manager->createImageFromPackage(pack_dot_name);
			if (img)
			{
				GLTexture2D *t2d = new GLTexture2D();
				t2d->genTexture(img);
				t2d->setTextureMinFilter(GL_LINEAR);
				t2d->setTextureMagFilter(GL_LINEAR);
				t2d->setTexParameter(GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
				t2d->setTexParameter(GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
				m_Texture2DInSystem.emplace(std::make_pair(pack_dot_name, t2d));
				return t2d;
			}
			else
			{
				if (m_ErrorTexture2D == nullptr)
				{
					m_ErrorTexture2D = new GLTexture2D();
					m_ErrorTexture2D->genTexture(img_manager->getErrorImage());
					m_ErrorTexture2D->setTextureMinFilter(GL_LINEAR);
					m_ErrorTexture2D->setTextureMagFilter(GL_LINEAR);
					m_ErrorTexture2D->setTexParameter(GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
					m_ErrorTexture2D->setTexParameter(GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
				}
				return m_ErrorTexture2D;
			}
		}
	}

	GLTextureCube * GLTextureManager::createCubeMap(
		const std::string &name,
		const std::string &positive_x, const std::string &negative_x,
		const std::string &positive_y, const std::string &negative_y,
		const std::string &positive_z, const std::string &negative_z,
		ImageManager *img_manager)
	{
		auto it = m_TextureCubeInSystem.find(name);

		if (it != m_TextureCubeInSystem.end())
		{
			return it->second;
		}

		GLTextureCube *cube = new GLTextureCube();
		cube->genTexture(
			img_manager->createImage(positive_x), img_manager->createImage(negative_x),
			img_manager->createImage(positive_y), img_manager->createImage(negative_y),
			img_manager->createImage(positive_z), img_manager->createImage(negative_z));

		m_TextureCubeInSystem.emplace(std::make_pair(name, cube));
		return cube;
	}

	GLTextureCube * GLTextureManager::createCubeMapFromPackage(const std::string &pack_name, ImageManager *img_manager)
	{
		auto it = m_TextureCubeInEngine.find(pack_name);

		if (it != m_TextureCubeInEngine.end())
		{
			return it->second;
		}

		auto imgs = img_manager->createCubeMapFromPackage(pack_name);
		if (imgs.empty())
		{
			
			if (m_ErrorCubeMap == nullptr)
			{
				m_ErrorCubeMap = new GLTextureCube();
				m_ErrorCubeMap->genTexture(
					img_manager->getErrorImage(), img_manager->getErrorImage(),
					img_manager->getErrorImage(), img_manager->getErrorImage(),
					img_manager->getErrorImage(), img_manager->getErrorImage());
			}

			return m_ErrorCubeMap;
		}

		GLTextureCube *cube = new GLTextureCube();
		cube->genTexture(
			imgs[1], imgs[0],
			imgs[2], imgs[3],
			imgs[5], imgs[4]);

		m_TextureCubeInEngine.emplace(std::make_pair(pack_name, cube));
		return cube;
	}

}

