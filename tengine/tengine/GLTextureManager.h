#pragma once

#include <unordered_map>
#include <string>

namespace tezcat
{
	class GLTexture2D;
	class GLTextureCube;
	class ImageManager;
	class GLTextureManager
	{
	public:
		GLTextureManager();

		//************************************
		// 函数名:  addTexture2D
		// 返回值:  void
		// 参数名:  const std::string & name
		// 参数名:  GLTexture2D * tex
		// 功能:    
		//************************************
		void addTexture2D(const std::string &name, GLTexture2D *tex)
		{
			m_Texture2DInSystem.insert(std::make_pair(name, tex));
		}

		//************************************
		// 函数名:  hasTexture2D
		// 返回值:  bool
		// 参数名:  const std::string & name
		// 功能:    
		//************************************
		bool hasTexture2D(const std::string &name)
		{
			return m_Texture2DInSystem.find(name) != m_Texture2DInSystem.end();
		}

		//************************************
		// 函数名:	createTexture2D
		// 全名:		tezcat::GLTextureManager::createTexture2D
		// 权限:		public 
		// 返回值:   tezcat::GLTexture2D *
		// 限定符:	
		// 参数:		const std::string & name
		// 参数:		ImageManager * img_manager
		// 功能:		
		//************************************
		GLTexture2D *createTexture2D(const std::string &name, ImageManager *img_manager);


		//************************************
		// 函数名:	createTexture2DFromPackage
		// 全名:		tezcat::GLTextureManager::createTexture2DFromPackage
		// 权限:		public 
		// 返回值:   tezcat::GLTexture2D *
		// 限定符:	
		// 参数:		const std::string & pack_dot_name
		// 参数:		ImageManager * img_manager
		// 功能:		
		//************************************
		GLTexture2D *createTexture2DFromPackage(const std::string &pack_dot_name, ImageManager *img_manager);

		//************************************
		// 函数名:	createCubeMap
		// 全名:		tezcat::GLTextureManager::createCubeMap
		// 权限:		public 
		// 返回值:   tezcat::GLTextureCube *
		// 限定符:	
		// 参数:		const std::string & name
		// 参数:		const std::string & positive_x
		// 参数:		const std::string & negative_x
		// 参数:		const std::string & positive_y
		// 参数:		const std::string & negative_y
		// 参数:		const std::string & positive_z
		// 参数:		const std::string & negative_z
		// 参数:		ImageManager * img_manager
		// 功能:		
		//************************************
		GLTextureCube *createCubeMap(
			const std::string &name,
			const std::string &positive_x, const std::string &negative_x,
			const std::string &positive_y, const std::string &negative_y,
			const std::string &positive_z, const std::string &negative_z,
			ImageManager *img_manager);


		//************************************
		// 函数名:	createCubeMapFromPackage
		// 全名:		tezcat::GLTextureManager::createCubeMapFromPackage
		// 权限:		public 
		// 返回值:   tezcat::GLTextureCube *
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 参数:		ImageManager * img_manager
		// 功能:		
		//************************************
		GLTextureCube *createCubeMapFromPackage(const std::string &pack_name, ImageManager *img_manager);


		GLTexture2D *getErrorTexture2D() { return m_ErrorTexture2D; }
	private:
		GLTexture2D *m_ErrorTexture2D;
		GLTextureCube *m_ErrorCubeMap;
		std::unordered_map<std::string, GLTexture2D *> m_Texture2DInSystem;
		std::unordered_map<std::string, GLTextureCube *> m_TextureCubeInSystem;

		std::unordered_map<std::string, GLTexture2D *> m_Texture2DInEngine;
		std::unordered_map<std::string, GLTextureCube *> m_TextureCubeInEngine;
	};
}
