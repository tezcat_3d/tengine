#include "Image.h"
#include <iostream>

namespace tezcat
{
	Image::Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format, unsigned char *data):
		m_Width(width), m_Height(height), m_ColorFormat(format), m_Data(data), m_MipMap(0)
	{

	}

	Image::Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format):
		m_Width(width), m_Height(height), m_ColorFormat(format), m_Data(nullptr), m_MipMap(0)
	{

	}

	Image::~Image()
	{
		free(m_Data);
	}
}

