#pragma once



namespace tezcat
{
	enum class ImageColorFormat
	{
		RGB,
		RGBA
	};

	class Image
	{
	public:
		Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format, unsigned char *data);
		Image(const unsigned int &width, const unsigned int &height, const ImageColorFormat &format);
		~Image();


		//************************************
		// 函数名:	getWidth
		// 全名:		tezcat::Image::getWidth
		// 权限:		public 
		// 返回值:   const int &
		// 限定符:	const
		// 功能:		
		//************************************
		const int &getWidth() const { return m_Width; }

		//************************************
		// 函数名:	getHeight
		// 全名:		tezcat::Image::getHeight
		// 权限:		public 
		// 返回值:   const int &
		// 限定符:	const
		// 功能:		
		//************************************
		const int &getHeight() const { return m_Height; }

		//************************************
		// 函数名:	getData
		// 全名:		tezcat::Image::getData
		// 权限:		public 
		// 返回值:   unsigned char *
		// 限定符:	const
		// 功能:		
		//************************************
		unsigned char *getData() const { return m_Data; }

		//************************************
		// 函数名:	getColorFormat
		// 全名:		tezcat::Image::getColorFormat
		// 权限:		public 
		// 返回值:   const tezcat::ImageColorFormat &
		// 限定符:	const
		// 功能:		
		//************************************
		const ImageColorFormat &getColorFormat() const { return m_ColorFormat; }

		//************************************
		// 函数名:	getMipMap
		// 全名:		tezcat::Image::getMipMap
		// 权限:		public 
		// 返回值:   const int &
		// 限定符:	const
		// 功能:		
		//************************************
		const int &getMipMap() const { return m_MipMap; }

	private:
		int m_Width;
		int m_Height;
		unsigned char *m_Data;
		int m_MipMap;
		ImageColorFormat m_ColorFormat;
	};
}