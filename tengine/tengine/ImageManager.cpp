#include "ImageManager.h"
#include "png/png.h"
#include "png/pngpriv.h"
#include "xsystem/XString.h"


namespace tezcat
{
	struct PNG_READ_HELPER
	{
		unsigned int length;
		unsigned int offset;
		const char *data;
	};

	unsigned char color[16] =
	{//  R     G	 B     A
		0xff, 0x00, 0x00, 0xff,
		0x00, 0xff, 0x00, 0xff,
		0x00, 0x00, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff
	};

	static void png_read_call_back(png_structp png_ptr, png_bytep data, png_size_t length)
	{
		auto io = (PNG_READ_HELPER *)png_get_io_ptr(png_ptr);
		if (io->offset + length <= io->length)
		{
			memcpy(data, io->data + io->offset, length);
			io->offset += length;
		}
		else
		{
			png_error(png_ptr, "png_read_call_back failed");
		}
	}

	ImageManager::ImageManager():
		m_ErrorImage(new Image(2, 2, ImageColorFormat::RGBA, color))
//m_ErrorImage(new Image(2, 2, ImageColorFormat::RGBA, (unsigned char*)("ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000022ff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000022ff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000022ff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000022ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff000099ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000099ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000099ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000099ff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff0000ffff000099")))
	{

	}

	ImageManager::~ImageManager()
	{
		for (auto data : m_ImagesInSystem)
		{
			delete data.second;
		}

		for (auto data : m_ImagesInEngine)
		{
			delete data.second;
		}
	}

	Image *ImageManager::loadImage(const std::string &file_name)
	{
		png_byte header[8];
		png_uint_32 pic_width = 0;
		png_uint_32 pic_height = 0;
		png_byte color_type = 0;
		png_byte bit_depth = 0;

		FILE *file = nullptr;
		file = fopen(file_name.c_str(), "rb");

		fread(header, 8, 1, file);
		auto is_png = png_sig_cmp(header, 0, 8);
		if (is_png != 0)
		{
			fclose(file);
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
		png_infop info_ptr = png_create_info_struct(png_ptr);
		auto error = setjmp(png_jmpbuf(png_ptr));

		png_init_io(png_ptr, file);
		png_set_sig_bytes(png_ptr, 8);
		png_read_info(png_ptr, info_ptr);

		pic_width = png_get_image_width(png_ptr, info_ptr);
		pic_height = png_get_image_height(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);
		bit_depth = png_get_bit_depth(png_ptr, info_ptr);

		if (bit_depth == 16)
		{
			png_set_strip_16(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		{
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}

		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_RGB ||
			color_type == PNG_COLOR_TYPE_GRAY ||
			color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_add_alpha(png_ptr, 0xFF, PNG_FILLER_AFTER);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		{
			png_set_gray_to_rgb(png_ptr);
		}

		png_read_update_info(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);

		png_bytep *row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * pic_height);
		for (unsigned int y = 0; y < pic_height; y++)
		{
			row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr, info_ptr));
		}

		unsigned char *content = (unsigned char *)malloc(sizeof(unsigned char) * pic_width * pic_height * 4);
		png_read_image(png_ptr, row_pointers);

		unsigned int pos = 0;
		for (unsigned int y = 0; y < pic_height; y++)
		{
			png_bytep row = row_pointers[y];
			for (unsigned int x = 0; x < pic_width; x++)
			{
				png_bytep px = &(row[x * 4]);
				content[pos++] = px[0];
				content[pos++] = px[1];
				content[pos++] = px[2];
				content[pos++] = px[3];
			}
		}

		Image *image = new Image(pic_width, pic_height, ImageColorFormat::RGBA, content);
		m_ImagesInSystem.insert(std::make_pair(file_name, image));

		free(row_pointers);
		png_destroy_read_struct(&png_ptr, &info_ptr, 0);
		fclose(file);

		return image;
	}

	Image * ImageManager::loadImageFromMemory(const char *image_data, const unsigned int &length, const std::string &name, const std::string &pack_name)
	{
		auto trueName = String::getFileName(name);

		png_byte header[8];
		png_uint_32 pic_width = 0;
		png_uint_32 pic_height = 0;
		png_byte color_type = 0;
		png_byte bit_depth = 0;

		memcpy(header, image_data, 8);

		auto is_png = png_sig_cmp(header, 0, 8);
		if (is_png != 0)
		{
			return nullptr;
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
		png_infop info_ptr = png_create_info_struct(png_ptr);
		auto error = setjmp(png_jmpbuf(png_ptr));

		PNG_READ_HELPER helper;
		helper.length = length;
		helper.offset = 0;
		helper.data = image_data;

		png_set_read_fn(png_ptr, (png_voidp)&helper, png_read_call_back);
		png_set_sig_bytes(png_ptr, 0);
		png_read_info(png_ptr, info_ptr);

		pic_width = png_get_image_width(png_ptr, info_ptr);
		pic_height = png_get_image_height(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);
		bit_depth = png_get_bit_depth(png_ptr, info_ptr);

		if (bit_depth == 16)
		{
			png_set_strip_16(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		{
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}

		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(png_ptr);
		}

		if (color_type == PNG_COLOR_TYPE_RGB ||
			color_type == PNG_COLOR_TYPE_GRAY ||
			color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_add_alpha(png_ptr, 0xFF, PNG_FILLER_AFTER);
		}

		if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		{
			png_set_gray_to_rgb(png_ptr);
		}

		png_read_update_info(png_ptr, info_ptr);
		color_type = png_get_color_type(png_ptr, info_ptr);

		png_bytep *row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * pic_height);
		for (unsigned int y = 0; y < pic_height; y++)
		{
			row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr, info_ptr));
		}

		unsigned char *content = (unsigned char *)malloc(sizeof(unsigned char) * pic_width * pic_height * 4);
		png_read_image(png_ptr, row_pointers);

		unsigned int pos = 0;
		for (unsigned int y = 0; y < pic_height; y++)
		{
			png_bytep row = row_pointers[y];
			for (unsigned int x = 0; x < pic_width; x++)
			{
				png_bytep px = &(row[x * 4]);
				content[pos++] = px[0];
				content[pos++] = px[1];
				content[pos++] = px[2];
				content[pos++] = px[3];
			}
		}

		Image *image = new Image(pic_width, pic_height, ImageColorFormat::RGBA, content);
		m_ImagesInEngine.insert(std::make_pair(pack_name + "." + trueName, image));

		free(row_pointers);
		png_destroy_read_struct(&png_ptr, &info_ptr, 0);

		return image;
	}

	Image *ImageManager::createImage(const std::string &file_name)
	{
		auto it = m_ImagesInSystem.find(file_name);
		if (it == m_ImagesInSystem.end())
		{
			return this->loadImage(file_name);
		}
		else
		{
			return it->second;
		}
	}


	Image * ImageManager::createImageFromPackage(const std::string &pack_dot_name)
	{
		auto it = m_ImagesInEngine.find(pack_dot_name);
		if (it != m_ImagesInEngine.end())
		{
			return it->second;
		}

		return nullptr;
	}

	std::vector<Image *> ImageManager::createCubeMapFromPackage(const std::string &pack_name)
	{
		auto it = m_CubeMapsInEngine.find(pack_name);
		if (it != m_CubeMapsInEngine.end())
		{
			auto index = it->second;
			std::vector<Image *> cubeMap{ index->left, index->right, index->top, index->bottom, index->front, index->back };
			return cubeMap;
		}
		else
		{
			std::vector<Image *> cubeMap;
			return cubeMap;
		}
	}

	void ImageManager::preLoadImagePackage(const std::string &pack_name, XFilePackager::DataPackage &data_package)
	{
		for (auto data : data_package)
		{
			this->loadImageFromMemory(data.data, data.length, String::getNameByFirstDot(data.file_name), pack_name);
		}
	}

	void ImageManager::preLoadCubeMapFromPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package)
	{
		if (!data_package.empty())
		{
			std::vector<Image *> cube;
			for (auto data : data_package)
			{
				cube.push_back(this->loadImageFromMemory(data.data, data.length, String::getNameByFirstDot(data.file_name), pack_name));
			}

			CubeMap *cubeMap = new CubeMap();
			cubeMap->left = cube[0];
			cubeMap->right = cube[1];
			cubeMap->top = cube[2];
			cubeMap->bottom = cube[3];
			cubeMap->front = cube[4];
			cubeMap->back = cube[5];

			m_CubeMapsInEngine.emplace(std::make_pair(pack_name, cubeMap));
		}
	}
}