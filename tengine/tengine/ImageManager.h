#pragma once

#include <unordered_map>
#include <list>
#include <vector>
#include "Image.h"
#include "xsystem/XFilePackager.h"


namespace tezcat
{
	class ImageManager
	{
		struct CubeMap
		{
			Image *left;
			Image *right;
			Image *top;
			Image *bottom;
			Image *front;
			Image *back;
		};

	public:
		ImageManager();
		~ImageManager();

		//************************************
		// 函数名:  createImage
		// 返回值:  Image *
		// 参数名:  const std::string & file_name
		// 功能:    
		//************************************
		Image *createImage(const std::string &file_name);

		//************************************
		// Method:    createImageFromPackage
		// FullName:  tezcat::ImageManager::createImageFromPackage
		// Access:    public 
		// Returns:   tezcat::Image *
		// Qualifier:
		// Parameter: const std::string & pack_dot_name
		//************************************
		Image *createImageFromPackage(const std::string &pack_dot_name);

		//************************************
		// Method:    createCubeMapFromPackage
		// FullName:  tezcat::ImageManager::createCubeMapFromPackage
		// Access:    public 
		// Returns:   std::vector<Image *>
		// Qualifier:
		// Parameter: const std::string & pack_name
		//************************************
		std::vector<Image *> createCubeMapFromPackage(const std::string &pack_name);

		//************************************
		// Method:    preLoadImagePackage
		// FullName:  tezcat::ImageManager::preLoadImagePackage
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & oack_name
		// Parameter: XFilePackager::DataPackage & data_package
		//************************************
		void preLoadImagePackage(const std::string &oack_name, XFilePackager::DataPackage &data_package);

		//************************************
		// Method:    preLoadCubeMapFromPackage
		// FullName:  tezcat::ImageManager::preLoadCubeMapFromPackage
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & pack_name
		// Parameter: XFilePackager::DataPackage & data_package
		//************************************
		void preLoadCubeMapFromPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package);

		Image *getErrorImage() { return m_ErrorImage; }

	private:


		//************************************
		// 函数名:	loadImage
		// 全名:		tezcat::ImageManager::loadImage
		// 权限:		private 
		// 返回值:   tezcat::Image *
		// 限定符:	
		// 参数:		const std::string & file_name
		// 功能:		
		//************************************
		Image *loadImage(const std::string &file_name);


		//************************************
		// 函数名:	loadImageFromMemory
		// 全名:		tezcat::ImageManager::loadImageFromMemory
		// 权限:		private 
		// 返回值:   tezcat::Image *
		// 限定符:	
		// 参数:		const char * image_data
		// 参数:		const unsigned int & length
		// 参数:		const std::string & file_name
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		Image *loadImageFromMemory(const char *image_data, const unsigned int &length, const std::string &file_name, const std::string &pack_name);


	private:
		Image *m_ErrorImage;

		std::unordered_map<std::string, Image *> m_ImagesInEngine;
		std::unordered_map<std::string, CubeMap *> m_CubeMapsInEngine;

		std::unordered_map<std::string, Image *> m_ImagesInSystem;
		std::unordered_map<std::string, CubeMap *> m_CubeMapsInSystem;
	};
}