#pragma once


namespace tezcat
{
	//
	//
	class Light
	{
	public:
		Light();
		virtual ~Light();


	};

	//
	//
	class LightDirectional : public Light
	{
	public:
		LightDirectional();
		~LightDirectional();
	};
}