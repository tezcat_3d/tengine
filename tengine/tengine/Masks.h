#pragma once


namespace tezcat
{
	class Masks
	{
	public:
		enum Camera : unsigned short
		{
			Null = 0,
			Location0 = 1,
			Location1 = 1 << 1,
			Location2 = 1 << 2,
			Location3 = 1 << 3,
			Location4 = 1 << 4,
			Location5 = 1 << 5,
			Location6 = 1 << 6,
			Location7 = 1 << 7,
			Location8 = 1 << 8
		};

		enum Layer : unsigned short
		{
			Null = 0,
			Location0 = 1,
			Location1 = 1 << 1,
			Location2 = 1 << 2,
			Location3 = 1 << 3,
			Location4 = 1 << 4,
			Location5 = 1 << 5,
			Location6 = 1 << 6,
			Location7 = 1 << 7,
			Location8 = 1 << 8
		};
	};
}