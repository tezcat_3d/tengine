#include "MaterialManager.h"

#include "EngineConfiguration.h"
#include "xsystem/XString.h"
#include "xsystem/XLightScriptSystem.h"
#include "xsystem/XLogSystem.h"

#include "ResourceSystem.h"
#include "Program.h"

namespace tezcat
{
	MaterialManager::MaterialManager():
		m_DefaultMaterial(new MaterialGroup())
	{
		m_MaterialInEngineGroup.emplace(std::make_pair(TG_DEFAULT_MATERIAL, m_DefaultMaterial));
	}

	MaterialManager::~MaterialManager()
	{
	}

	UniversalMaterial *MaterialManager::createUniversalMaterialFromPackage(const std::string &pack_dot_name)
	{
		auto it = m_DefaultMaterial->Group.find(TG_DEFAULT_MATERIAL + pack_dot_name);
		if (it != m_DefaultMaterial->Group.end())
		{
			return it->second;
		}
		else
		{
			XLOG_WARNING(MaterialManager, "not found <" + pack_dot_name + "> use the default");
			return UniversalMaterial::DefaultMaterial;
		}
	}

	std::unordered_map<std::string, UniversalMaterial *> *MaterialManager::createUniversalMaterialGroupFromPackage(const std::string &pack_name)
	{
		return &(m_MaterialInEngineGroup[pack_name]->Group);
	}

	UniversalMaterial *MaterialManager::createDefaultMaterial(const std::string &name)
	{
		auto it = m_MaterialsInSystem.find(name);
		if (it != m_MaterialsInSystem.end())
		{
			return static_cast<UniversalMaterial *>(it->second);
		}
		else
		{
			UniversalMaterial *um = new UniversalMaterial();
			um->setName(name);
			m_MaterialsInSystem.emplace(std::make_pair(name, um));
			return um;
		}
	}

	void MaterialManager::preLoadUniversalMaterialFromPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package)
	{
		XLightScriptSystem system;

		bool muilt = data_package.size() != 1;
		if (muilt)
		{
			std::string shaderName;
			MaterialGroup *group = new MaterialGroup();
			for (auto data : data_package)
			{
				//获得材质名
				auto trueName = String::getNameByFirstDot(data.file_name);

				//从包名查找
				auto hasIt = m_MaterialInEngineGroup.find(pack_name);
				if (hasIt == m_MaterialInEngineGroup.end())
				{
					auto &file = system.loadOneFileWithMemory(data.data, pack_name);
					shaderName = file["init"]["shader"]().asSTDString();
					auto &uniformValue = file["uniform"];
					UniversalMaterial *um = new UniversalMaterial();
					um->setName(trueName);
					um->setShaderName(shaderName);
					um->setQueueID(file["init"]["queue_id"]().asInt());
					um->setPassID(file["init"]["pass_id"]().asUInt());

					//加载bool
					for (auto ubool : uniformValue["bool"].getClass())
					{
						auto array = ubool.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setBool1(ubool.first, (*array)[0]().asBool());
							break;
						case 2:
							um->setBool2(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool());
							break;
						case 3:
							um->setBool3(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool(), (*array)[2]().asBool());
							break;
						case 4:
							um->setBool4(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool(), (*array)[2]().asBool(), (*array)[3]().asBool());
							break;
						}
					}

					//加载int
					for (auto uint : uniformValue["int"].getClass())
					{
						auto array = uint.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setInt1(uint.first, (*array)[0]().asInt());
							break;
						case 2:
							um->setInt2(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt());
							break;
						case 3:
							um->setInt3(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt(), (*array)[2]().asInt());
							break;
						case 4:
							um->setInt4(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt(), (*array)[2]().asInt(), (*array)[3]().asInt());
							break;
						}
					}

					//加载float
					for (auto ufloat : uniformValue["float"].getClass())
					{
						auto array = ufloat.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setFloat1(ufloat.first, (*array)[0]().asFloat());
							break;
						case 2:
							um->setFloat2(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat());
							break;
						case 3:
							um->setFloat3(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat(), (*array)[2]().asFloat());
							break;
						case 4:
							um->setFloat4(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat(), (*array)[2]().asFloat(), (*array)[3]().asFloat());
							break;
						}
					}

					//加载texture 2d
					for (auto tex2d : uniformValue["tex2d"].getClass())
					{
						auto array = tex2d.second;
						um->setTexture2DName(tex2d.first, (*array)().asSTDString());
						um->cacheTexture2D(tex2d.first, (*array)().asSTDString());
					}

					//加载cubemap
					for (auto tex2d : uniformValue["cubemap"].getClass())
					{
						auto array = tex2d.second;
						um->setCubeMapName(tex2d.first, (*array)().asSTDString());
						um->cacheCubeMap(tex2d.first, (*array)().asSTDString());
					}

					group->Group.emplace(std::make_pair(trueName, um));
					m_DefaultMaterial->Group.emplace(std::make_pair(TG_DEFAULT_MATERIAL + pack_name + "." + trueName, um));
				}
			}

			m_MaterialInEngineGroup.emplace(std::make_pair(pack_name, group));
		}
		else
		{
			for (auto data : data_package)
			{
				//材质名
				auto trueName = TG_DEFAULT_MATERIAL + pack_name + std::string(".") + String::getNameByFirstDot(data.file_name);

				auto hasIt = m_DefaultMaterial->Group.find(trueName);
				if (hasIt == m_DefaultMaterial->Group.end())
				{
					auto &file = system.loadOneFileWithMemory(data.data, trueName);
					std::string shaderName = file["init"]["shader"]().asSTDString();
					UniversalMaterial *um = new UniversalMaterial();
					um->setName(trueName);
					um->setShaderName(shaderName);
					um->setQueueID(file["init"]["queue_id"]().asInt());
					um->setPassID(file["init"]["pass_id"]().asUInt());

					auto &uniformValue = file["uniform"];
					//加载bool
					for (auto ubool : uniformValue["bool"].getClass())
					{
						auto array = ubool.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setBool1(ubool.first, (*array)[0]().asBool());
							break;
						case 2:
							um->setBool2(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool());
							break;
						case 3:
							um->setBool3(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool(), (*array)[2]().asBool());
							break;
						case 4:
							um->setBool4(ubool.first, (*array)[0]().asBool(), (*array)[1]().asBool(), (*array)[2]().asBool(), (*array)[3]().asBool());
							break;
						}
					}

					//加载int
					for (auto uint : uniformValue["int"].getClass())
					{
						auto array = uint.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setInt1(uint.first, (*array)[0]().asInt());
							break;
						case 2:
							um->setInt2(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt());
							break;
						case 3:
							um->setInt3(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt(), (*array)[2]().asInt());
							break;
						case 4:
							um->setInt4(uint.first, (*array)[0]().asInt(), (*array)[1]().asInt(), (*array)[2]().asInt(), (*array)[3]().asInt());
							break;
						}
					}

					//加载float
					for (auto ufloat : uniformValue["float"].getClass())
					{
						auto array = ufloat.second;
						switch (array->getArray().size())
						{
						case 1:
							um->setFloat1(ufloat.first, (*array)[0]().asFloat());
							break;
						case 2:
							um->setFloat2(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat());
							break;
						case 3:
							um->setFloat3(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat(), (*array)[2]().asFloat());
							break;
						case 4:
							um->setFloat4(ufloat.first, (*array)[0]().asFloat(), (*array)[1]().asFloat(), (*array)[2]().asFloat(), (*array)[3]().asFloat());
							break;
						}
					}

					//加载texture 2d
					unsigned int index = 0;
					for (auto tex2d : uniformValue["tex2d"].getClass())
					{
						auto array = tex2d.second;
						um->setTexture2DName(tex2d.first, (*array)().asSTDString());
						um->cacheTexture2D(tex2d.first, (*array)().asSTDString());
					}

					//加载cubemap
					for (auto tex2d : uniformValue["cubemap"].getClass())
					{
						auto array = tex2d.second;
						um->setCubeMapName(tex2d.first, (*array)().asSTDString());
						um->cacheCubeMap(tex2d.first, (*array)().asSTDString());
					}

					m_DefaultMaterial->Group.emplace(std::make_pair(trueName, um));
				}
			}
		}
	}

	void MaterialManager::loadMaterialDataFromPackage(const std::string &pack_name)
	{
		auto hasIt = m_DefaultMaterial->Group.find(TG_DEFAULT_MATERIAL + std::string(".") + pack_name);
		if (hasIt != m_DefaultMaterial->Group.end())
		{
			UniversalMaterial *um = static_cast<UniversalMaterial *>(hasIt->second);

			unsigned int index = 0;
			for (auto uniform : um->getUniforms())
			{
				auto temp = uniform.second;
				if (temp->getValueType() == UniformBaseValue::ValueType::TEXTURE)
				{
					auto tex = static_cast<UniformValueWithTexture *>(temp);
					switch (tex->getValueCount())
					{
					case UniformBaseValue::ValueCount::C1:
						break;
					case UniformBaseValue::ValueCount::C2:
						tex->setTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(tex->getTextureName()), index++);
						break;
					case UniformBaseValue::ValueCount::C3:
						tex->setTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(tex->getTextureName()), index++);
						break;
					case UniformBaseValue::ValueCount::C4:
						break;
					}
				}
			}
		}
	}

	void MaterialManager::loadMaterialDataGroupFromPackage(const std::string &pack_name)
	{
		auto group = m_MaterialInEngineGroup.find(pack_name);
		if (group != m_MaterialInEngineGroup.end())
		{
			for (auto it : group->second->Group)
			{
				auto um = static_cast<UniversalMaterial *>(it.second);
				unsigned int index = 0;
				for (auto uniform : um->getUniforms())
				{
					auto temp = uniform.second;
					if (temp->getValueType() == UniformBaseValue::ValueType::TEXTURE)
					{
						auto tex = static_cast<UniformValueWithTexture *>(temp);
						switch (tex->getValueCount())
						{
						case UniformBaseValue::ValueCount::C1:
							break;
						case UniformBaseValue::ValueCount::C2:
							tex->setTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(tex->getTextureName()), index++);
							break;
						case UniformBaseValue::ValueCount::C3:
							tex->setTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(tex->getTextureName()), index++);
							break;
						case UniformBaseValue::ValueCount::C4:
							break;
						}
					}
				}
			}
		}
	}

	void MaterialManager::addUniversalMaterial(UniversalMaterial *um)
	{
		if (um->getName().find(TG_DEFAULT_MATERIAL) != std::string::npos)
		{
			m_DefaultMaterial->Group.emplace(std::make_pair(um->getName(), um));
		}
		else
		{
			auto it = m_MaterialInEngineGroup.find(um->getName());
			if (it == m_MaterialInEngineGroup.end())
			{
				auto g = it->second;
				if (g)
				{
					g->Group.emplace(std::make_pair(um->getName(), um));
				}
// 				else
// 				{
// 					MaterialGroup *group = new MaterialGroup();
// 					group->Group.emplace(std::make_pair(um->getName(), um));
// 					m_MaterialInEngineGroup.emplace(std::make_pair())
// 				}
			}
		}
	}

	std::list<UniversalMaterial *> MaterialManager::getAllUniversalMaterialByShaderName(const std::string &shader_name)
	{
		std::list<UniversalMaterial *> list;
		for (auto group : m_MaterialInEngineGroup)
		{
			for (auto data : group.second->Group)
			{
				if (data.second->getShaderName() == shader_name)
				{
					list.emplace_back(static_cast<UniversalMaterial*>(data.second));
				}
			}
		}

		return list;
	}
}