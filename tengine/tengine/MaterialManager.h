#pragma once

#include <unordered_map>
#include <list>
#include "UniversalMaterial.h"
#include "xsystem/XFilePackager.h"

namespace tezcat
{
	class MaterialManager
	{
	public:
		struct MaterialGroup
		{
			std::unordered_map<std::string, UniversalMaterial *> Group;
		};

	public:
		MaterialManager();
		~MaterialManager();


		//************************************
		// 函数名:	createUniversalMaterialFromPackage
		// 全名:		tezcat::MaterialManager::createUniversalMaterialFromPackage
		// 权限:		public 
		// 返回值:   tezcat::UniversalMaterial *
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		UniversalMaterial *createUniversalMaterialFromPackage(const std::string &pack_dot_name);

		//************************************
		// 函数名:	createUniversalMaterialGroupFromPackage
		// 全名:		tezcat::MaterialManager::createUniversalMaterialGroupFromPackage
		// 权限:		public 
		// 返回值:   std::unordered_map<std::string, UniversalMaterial *> *
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		std::unordered_map<std::string, UniversalMaterial *> *createUniversalMaterialGroupFromPackage(const std::string &pack_name);

		//************************************
		// 函数名:	createDefaultMaterial
		// 全名:		tezcat::MaterialManager::createDefaultMaterial
		// 权限:		public 
		// 返回值:   tezcat::UniversalMaterial *
		// 限定符:	
		// 参数:		const std::string & name
		// 功能:		
		//************************************
		UniversalMaterial *createDefaultMaterial(const std::string &name);

		//************************************
		// 函数名:	preLoadUniversalMaterialFromPackage
		// 全名:		tezcat::MaterialManager::preLoadUniversalMaterialFromPackage
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 参数:		XFilePackager::DataPackage & data_package
		// 功能:		
		//************************************
		void preLoadUniversalMaterialFromPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package);


		//************************************
		// 函数名:	loadMaterialDataFromPackage
		// 全名:		tezcat::MaterialManager::loadMaterialDataFromPackage
		// 权限:		public 
		// 返回值:   tezcat::UniversalMaterial *
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		void loadMaterialDataFromPackage(const std::string &pack_name);

		//************************************
		// 函数名:	loadMaterialDataGroupFromPackage
		// 全名:		tezcat::MaterialManager::loadMaterialDataGroupFromPackage
		// 权限:		public 
		// 返回值:   std::unordered_map<std::string, UniversalMaterial *>
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		void loadMaterialDataGroupFromPackage(const std::string &pack_name);

		//************************************
		// 函数名:	addUniversalMaterial
		// 全名:		tezcat::MaterialManager::addUniversalMaterial
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		UniversalMaterial * um
		// 功能:		
		//************************************
		void addUniversalMaterial(UniversalMaterial *um);

		//************************************
		// 函数名:	getAllUniversalMaterialByShaderName
		// 全名:		tezcat::MaterialManager::getAllUniversalMaterialByShaderName
		// 权限:		public 
		// 返回值:   std::list<UniversalMaterial *>
		// 限定符:	
		// 参数:		const std::string & shader_name
		// 功能:		
		//************************************
		std::list<UniversalMaterial *> getAllUniversalMaterialByShaderName(const std::string &shader_name);

	private:
		MaterialGroup *m_DefaultMaterial;
//		std::unordered_map<std::string, Material *> m_MaterialsInEngine;			//单独材质包(材质名, 材质)
		std::unordered_map<std::string, MaterialGroup *> m_MaterialInEngineGroup;	//组合材质包(包名, 包)
		std::unordered_map<std::string, Material *> m_MaterialsInSystem;
	};
}
