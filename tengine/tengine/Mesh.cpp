#include "Mesh.h"

#include <functional>



namespace tezcat
{
	Mesh::Mesh(const std::string &name, VertexData *storage_data) :
		m_Name(name),
		m_StorageData(storage_data),
		m_HasModel(false),
		m_Assembled(false)
	{
		m_Textures.resize(11, "");
	}

	Mesh::Mesh(const std::string &name) :
		m_Name(name),
		m_StorageData(nullptr),
		m_HasModel(false),
		m_Assembled(false)
	{
		m_Textures.resize(11, "");
	}

	Mesh::~Mesh()
	{

	}



	void Mesh::setMatrix(
		float x1, float x2, float x3, float x4,
		float y1, float y2, float y3, float y4,
		float z1, float z2, float z3, float z4,
		float w1, float w2, float w3, float w4)
	{
		//=========================
		//分离矩阵

		auto clamp = [](const float &value)
		{
			//return (value <= -0.000001f) ? -0.000001f : value;
			return value;
		};

		//位置坐标
		//列矩阵第四列前3个
		m_Position.x = clamp(x4);
		m_Position.y = clamp(y4);
		m_Position.z = clamp(z4);

		//分离缩放
		//缩放等于前3*3矩阵中每列的长度
		float length1 = glm::length(glm::vec3(clamp(x1), clamp(y1), clamp(z1)));
		float length2 = glm::length(glm::vec3(clamp(x2), clamp(y2), clamp(z2)));
		float length3 = glm::length(glm::vec3(clamp(x3), clamp(y3), clamp(z3)));


		m_Scale.x = length1;
		m_Scale.y = length2;
		m_Scale.z = length3;

// 		if (x1 >= 0)
// 			m_Scale.x = length1;
// 		else
// 			m_Scale.x = -length1;
// 
// 		if (y2 >= 0)
// 			m_Scale.y = length2;
// 		else
// 			m_Scale.y = -length2;
// 		if (z3 >= 0)
// 			m_Scale.z = length3;
// 		else
// 			m_Scale.z = -length3;


		//按Z轴旋转a弧度
		// cosa,   sina,   0,      0
		// -sina,  cosa,   0,      0
		// 0,      0,      1,      0
		// 1,      2,      3,      1
		//按X轴旋转a弧度
		// 1,      0,      0,      0
		// 0,      cosa,   sina,   0
		// 0,     -sina,   cosa,   0
		// 1,      2,      3,      1
		//按Y轴旋转a弧度
		// cosa,   0,      -sina,  0
		// 0,      1,      0,      0
		// sina,   0,      cosa,   0
		// 1,      2,      3,      1

		//弧度 = 角度乘以π后再除以180
		//角度 = 弧度除以π再乘以180

		glm::mat3 rotateMat3;
		rotateMat3[0][0] = x1 / length1;
		rotateMat3[0][1] = y1 / length1;
		rotateMat3[0][2] = z1 / length1;

		rotateMat3[1][0] = x2 / length2;
		rotateMat3[1][1] = y2 / length2;
		rotateMat3[1][2] = z2 / length2;

		rotateMat3[2][0] = x3 / length3;
		rotateMat3[2][1] = y3 / length3;
		rotateMat3[2][2] = z3 / length3;


		auto quat = glm::toQuat(glm::transpose(rotateMat3));
		m_Rotation = -glm::eulerAngles(quat);
	}

	void Mesh::foreachAllChildren(const std::function<void(Mesh *)> &function)
	{
		function(this);
		for (auto child : m_Children)
		{
			child->foreachAllChildren(function);
			function(child);
		}
	}


}