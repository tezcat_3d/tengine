#pragma once

#include <vector>
#include <unordered_map>
#include <cassert>
#include <string>
#include <list>
#include <functional>

#include "GLConfig.h"


struct aiNode;
struct aiScene;

namespace tezcat
{
	class Mesh
	{
	public:
		~Mesh();


		static Mesh *create(const std::string &name)
		{
			auto p = new std::aligned_storage<sizeof(Mesh), std::alignment_of<Mesh>::value>::type();
			return new (p) Mesh(name);
		}

		static Mesh *create(const std::string &name, VertexData *storage_data)
		{
			auto p = new std::aligned_storage<sizeof(Mesh), std::alignment_of<Mesh>::value>::type();
			return new (p) Mesh(name, storage_data);
		}

		//************************************
		// 函数名:  getStorageData
		// 返回值:  Data *
		// 功能:    
		//************************************
		VertexData * getStorageData() const { return m_StorageData; }

		//************************************
		// 函数名:  setStorageData
		// 返回值:  void
		// 参数名:  Data * val
		// 功能:    
		//************************************
		void setStorageData(VertexData * val) { m_StorageData = val; }

		//************************************
		// 函数名:  hasModel
		// 返回值:  bool
		// 功能:    
		//************************************
		bool hasModel() { return m_HasModel; }

		//************************************
		// 函数名:  setHasModel
		// 返回值:  void
		// 参数名:  bool has
		// 功能:    
		//************************************
		void setHasModel(bool has) { m_HasModel = has; }

		//************************************
		// 函数名:  getChildren
		// 返回值:  std::list<Mesh *> &
		// 功能:    
		//************************************
		std::list<Mesh *> &getChildren() { return m_Children; }

		//************************************
		// 函数名:  getName
		// 返回值:  std::string
		// 功能:    
		//************************************
		const std::string &getName() const { return m_Name; }

		//************************************
		// 函数名:  setName
		// 返回值:  void
		// 参数名:  std::string val
		// 功能:    
		//************************************
		void setName(const std::string &val) { m_Name = val; }

		//************************************
		// 函数名:  getPosition
		// 返回值:  glm::vec3
		// 功能:    
		//************************************
		const glm::vec3 &getPosition() const { return m_Position; }

		//************************************
		// 函数名:  getScale
		// 返回值:  glm::vec3
		// 功能:    
		//************************************
		const glm::vec3 &getScale() const { return m_Scale; }

		//************************************
		// 函数名:  getRotation
		// 返回值:  glm::quat
		// 功能:    
		//************************************
		const glm::vec3 &getRotation() const { return m_Rotation; }

		//************************************
		// 函数名:  addChild
		// 返回值:  void
		// 参数名:  Mesh * child
		// 功能:    
		//************************************
		void addChild(Mesh *child) { m_Children.push_back(child); }

		//************************************
		// 函数名:  getParent
		// 返回值:  Mesh *
		// 功能:    
		//************************************
		Mesh * getParent() const { return m_Parent; }

		//************************************
		// 函数名:  setParent
		// 返回值:  void
		// 参数名:  Mesh * val
		// 功能:    
		//************************************
		void setParent(Mesh * val) { m_Parent = val; }


		//************************************
		// 函数名:  setMatrix
		// 返回值:  void
		// 参数名:  float x1
		// 参数名:  float x2
		// 参数名:  float x3
		// 参数名:  float x4
		// 参数名:  float y1
		// 参数名:  float y2
		// 参数名:  float y3
		// 参数名:  float y4
		// 参数名:  float z1
		// 参数名:  float z2
		// 参数名:  float z3
		// 参数名:  float z4
		// 参数名:  float w1
		// 参数名:  float w2
		// 参数名:  float w3
		// 参数名:  float w4
		// 功能:    
		//************************************
		void setMatrix(float x1, float x2, float x3, float x4,
			float y1, float y2, float y3, float y4,
			float z1, float z2, float z3, float z4,
			float w1, float w2, float w3, float w4);

		//************************************
		// 函数名:  copyData2Mode
		// 返回值:  void
		// 参数名:  glm::vec3 & position
		// 参数名:  glm::vec3 & scale
		// 参数名:  glm::quat & rotation
		// 功能:    
		//************************************
		void copyData2Mode(glm::vec3 &position, glm::vec3 &scale, glm::vec3 &rotation)
		{
			position = m_Position;
			scale = m_Scale;
			rotation = m_Rotation;
		}

		//************************************
		// 函数名:  foreachAllChildren
		// 返回值:  void
		// 参数名:  const std::function<void
		// 参数名:  Mesh * 
		// 参数名:  > & function
		// 功能:    
		//************************************
		void foreachAllChildren(const std::function<void(Mesh *)> &function);

		//************************************
		// Method:    setTexture
		// FullName:  tezcat::Mesh::setTexture
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const unsigned int & index
		// Parameter: const std::string & name
		//************************************
		void setTexture(const unsigned int &index, const std::string &name)
		{
			m_Textures[index] = name;
		}

		//************************************
		// Method:    setMaterial
		// FullName:  tezcat::Mesh::setMaterial
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & matterial_name
		//************************************
		void setMaterial(const std::string &matterial_name)
		{
			m_MaterialName = matterial_name;
		}

		//************************************
		// Method:    getMaterial
		// FullName:  tezcat::Mesh::getMaterial
		// Access:    public 
		// Returns:   const std::string &
		// Qualifier: const
		//************************************
		const std::string &getMaterial() const { return m_MaterialName; }

		void setAssembled(const bool &ass) { m_Assembled = ass; }
		const bool &getAssembled() const { return m_Assembled; }

	private:
		Mesh(const std::string &name);
		Mesh(const std::string &name, VertexData *storage_data);

	private:
		friend class MeshManager;
		Mesh *m_Parent;
		std::list<Mesh *> m_Children;
		std::vector<std::string> m_Textures;
		bool m_HasModel;
		std::string m_MaterialName;
		bool m_Assembled;

		//缓存数据
	public:
		std::string m_Name;
		VertexData *m_StorageData;
		glm::mat4 m_MatrixData;
		glm::vec3 m_Position;
		glm::vec3 m_Scale;
		glm::vec3 m_Rotation;
		
	};
}