#include "MeshCommand.h"
#include "Transform.h"
#include "Mesh.h"
#include "Material.h"
#include "Camera.h"
#include "Program.h"

namespace tezcat
{

	MeshCommand::MeshCommand():
		m_VAOID(0),
		m_VBOID(0),
		m_EBOID(0),
		m_DrawMode(GL_TRIANGLES)
	{
		glGenVertexArrays(1, &m_VAOID);
	}

	MeshCommand::~MeshCommand()
	{
		glDeleteVertexArrays(1, &m_VAOID);
		glDeleteBuffers(1, &m_VBOID);
		if (m_DrawType == DrawType::Index)
		{
			glDeleteBuffers(1, &m_EBOID);
		}
	}

	void MeshCommand::sendData2GL(VertexData *mesh)
	{
		glBindVertexArray(m_VAOID);

		m_VertexSize = mesh->Vertices.size();
		glGenBuffers(1, &m_VBOID);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBOID);
		glBufferData(GL_ARRAY_BUFFER, m_VertexSize * sizeof(Vertex_PNUCTB), mesh->Vertices.data(), GL_STATIC_DRAW);
		m_DrawType = DrawType::Vertex;

		GL::bindVAO_PNUCTB();

		if (!mesh->Indices.empty())
		{
			m_IndexSize = mesh->Indices.size();
			glGenBuffers(1, &m_EBOID);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOID);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndexSize * sizeof(unsigned int), mesh->Indices.data(), GL_STATIC_DRAW);
			m_DrawType = DrawType::Index;
		}

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void MeshCommand::render()
	{
		glBindVertexArray(m_VAOID);
		switch (m_DrawType)
		{
		case DrawType::Vertex:
			glDrawArrays(m_DrawMode, 0, m_VertexSize);
			break;
		case DrawType::Index:
			glDrawElements(m_DrawMode, m_IndexSize, GL_UNSIGNED_INT, 0);
			break;
		}
	}

}