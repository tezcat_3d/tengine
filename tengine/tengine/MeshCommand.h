#pragma once

#include "RenderCommand.h"

namespace tezcat
{
	struct VertexData;
	class MeshCommand : public RenderCommand
	{
		enum class DrawType
		{
			Vertex,
			Index,
		};
	public:
		MeshCommand();
		~MeshCommand();

	public:
		virtual void sendData2GL(VertexData *mesh) override;
		virtual void render() override;

	private:
		GLuint m_VAOID;

		GLuint m_VBOID;
		GLuint m_EBOID;

		size_t m_VertexSize;
		size_t m_IndexSize;

		DrawType m_DrawType;
		GLenum m_DrawMode;
		GLenum m_IndexType;
	};
}