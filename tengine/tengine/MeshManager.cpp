#include "MeshManager.h"
#include "Mesh.h"
#include "EngineConfiguration.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "xsystem/XString.h"

namespace tezcat
{
	//
	//
	MeshManager::MeshManager()
	{
		m_ModelTypes = { "3ds", "3DS" , "obj", "fbx" };
	}

	MeshManager::~MeshManager()
	{
		this->clear();
	}

	void MeshManager::loadMeshFromFile(const std::string &file)
	{
		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile(
			file.c_str(),
			aiProcess_CalcTangentSpace |			//加入切线和双切线
			aiProcess_FlipUVs |						//翻转UV
			aiProcess_Triangulate |					//三角化网格
			aiProcess_JoinIdenticalVertices |		//识别相同顶点
			aiProcess_ImproveCacheLocality |		//更好的内存结构增加内存命中
			aiProcess_FindInvalidData |
			aiProcess_FindInstances |
			aiProcess_SortByPType);

		if (!scene)
		{
			m_MeshName.clear();
			return;
		}

		auto glMesh = Mesh::create("default");
		this->foreachChild(scene->mRootNode, glMesh, scene);
		m_MeshInSystem.emplace(std::make_pair(file, glMesh));
	}

	void MeshManager::preLoadMeshFromPackage(const std::string &name, XFilePackager::DataPackage &data_pack)
	{
		XFilePackager::UnPackInfo *model_pack = nullptr, *config_pack = nullptr;

		for (auto &data : data_pack)
		{
			auto ext = String::getNameByLastDot(data.file_name);
			if (ext == "model")
			{
				config_pack = &data;
			}
			else if (m_ModelTypes.find(String::getNameByLastDot(data.file_name)) != m_ModelTypes.end())
			{
				model_pack = &data;
			}
		}

		if (model_pack && config_pack)
		{
			Assimp::Importer importer;
			const aiScene* scene = importer.ReadFileFromMemory(
				model_pack->data,
				model_pack->length,
				aiProcess_CalcTangentSpace |			//加入切线和双切线
				aiProcess_FlipUVs |						//翻转UV
				aiProcess_Triangulate |					//三角化网格
				aiProcess_JoinIdenticalVertices |		//识别相同顶点
				aiProcess_ImproveCacheLocality |		//更好的内存结构增加内存命中
				aiProcess_FindInvalidData |
				aiProcess_FindInstances |
				aiProcess_SortByPType);

			if (!scene)
			{
				m_MeshName.clear();
				return;
			}

			auto cname = String::getNameByFirstDot(config_pack->file_name);
			XLightScriptSystem system;
			system.loadOneFileWithMemory(config_pack->data, cname);

			auto glMesh = Mesh::create("default");
			this->foreachChildWithConfig(scene->mRootNode, glMesh, scene, system.getFile(cname));
			m_MeshInEngine.emplace(std::make_pair(name, glMesh));
			m_MeshName.clear();
		}
	}

	Mesh *MeshManager::createMesh(const std::string &file)
	{
		if (m_MeshInSystem.find(file) == m_MeshInSystem.end())
		{
			this->loadMeshFromFile(file);
		}
		return m_MeshInSystem[file];
	}

	void MeshManager::foreachChild(aiNode *node, Mesh *root, const aiScene *scene)
	{
		root->setName(node->mName.C_Str());
		root->setMatrix(
			node->mTransformation.a1, node->mTransformation.a2, node->mTransformation.a3, node->mTransformation.a4,
			node->mTransformation.b1, node->mTransformation.b2, node->mTransformation.b3, node->mTransformation.b4,
			node->mTransformation.c1, node->mTransformation.c2, node->mTransformation.c3, node->mTransformation.c4,
			node->mTransformation.d1, node->mTransformation.d2, node->mTransformation.d3, node->mTransformation.d4);

		if (node->mNumMeshes != 0)
		{
			for (unsigned int meshIndex = 0; meshIndex < node->mNumMeshes; meshIndex++)
			{
				VertexData *d = new VertexData();
				auto &indices = d->Indices;
				auto &vertices = d->Vertices;

				const aiMesh* mesh = scene->mMeshes[node->mMeshes[meshIndex]];
				vertices.resize(mesh->mNumVertices);

				bool hasPos = mesh->HasPositions();
				bool hasUV0 = mesh->HasTextureCoords(0);
				bool hasNormal = mesh->HasNormals();
				bool hasColor0 = mesh->HasVertexColors(0);
				bool has2T = mesh->HasTangentsAndBitangents();
				bool hasFace = mesh->HasFaces();

				for (unsigned int vertexIndex = 0; vertexIndex < mesh->mNumVertices; vertexIndex++)
				{
					if (hasPos)
					{
						aiVector3D &pos = mesh->mVertices[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Position;
						value.x = pos.x;
						value.y = pos.y;
						value.z = pos.z;
					}

					if (hasUV0)
					{
						aiVector3D &UVW = mesh->mTextureCoords[0][vertexIndex];
						auto &value = vertices[vertexIndex].Layout_UV;
						value.x = UVW.x;
						value.y = UVW.y;
					}
					else
					{
						auto &value = vertices[vertexIndex].Layout_UV;
						value.x = 0;
						value.y = 0;
					}

					if (hasNormal)
					{
						aiVector3D &n = mesh->mNormals[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Normal;
						value.x = n.x;
						value.y = n.y;
						value.z = n.z;
					}

					if (hasColor0)
					{
						aiColor4D &n = *mesh->mColors[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Color;
						value.x = n.r;
						value.y = n.g;
						value.z = n.b;
						value.w = n.a;
					}
					else
					{
						auto &value = vertices[vertexIndex].Layout_Color;
						value.x = 1.0f;
						value.y = 1.0f;
						value.z = 1.0f;
						value.w = 1.0f;
					}

					if (has2T)
					{
						auto &value1 = vertices[vertexIndex].Layout_Tangent;
						auto &t = mesh->mTangents[vertexIndex];
						value1.x = t.x;
						value1.y = t.y;
						value1.z = t.z;

						auto &value2 = vertices[vertexIndex].Layout_BitTangent;
						auto &b = mesh->mBitangents[vertexIndex];
						value2.x = b.x;
						value2.y = b.y;
						value2.z = b.z;
					}
					else
					{
						vertices[vertexIndex].Layout_Tangent = glm::vec3(0.0f);
						vertices[vertexIndex].Layout_BitTangent = glm::vec3(0.0f);
					}
				}

				if (hasFace)
				{
					indices.reserve(3 * mesh->mNumFaces);
					for (unsigned int i = 0; i < mesh->mNumFaces; i++)
					{
						indices.emplace_back(mesh->mFaces[i].mIndices[0]);
						indices.emplace_back(mesh->mFaces[i].mIndices[1]);
						indices.emplace_back(mesh->mFaces[i].mIndices[2]);
					}
				}

				static std::function<void(aiMaterial *, Mesh *, aiTextureType)> loadMaterial =
					[this](aiMaterial *material, Mesh *mesh, aiTextureType type)
				{
					if (material->GetTextureCount(type) > 0)
					{
						aiString texture_path;
						if (material->GetTexture(type, 0, &texture_path) == AI_SUCCESS)
						{
							std::string true_path = texture_path.data;
// 							true_path = String::getFileName(true_path);
// 							true_path = m_MeshName + "." + true_path;
							mesh->setTexture(type - 1, true_path);
						}
					}
					else
					{
						mesh->setTexture(type - 1, "null");
					}
				};

				auto material = scene->mMaterials[mesh->mMaterialIndex];
				loadMaterial(material, root, aiTextureType_DIFFUSE);
				loadMaterial(material, root, aiTextureType_AMBIENT);
				loadMaterial(material, root, aiTextureType_EMISSIVE);
				loadMaterial(material, root, aiTextureType_HEIGHT);
				loadMaterial(material, root, aiTextureType_NORMALS);
				loadMaterial(material, root, aiTextureType_SHININESS);
				loadMaterial(material, root, aiTextureType_OPACITY);
				loadMaterial(material, root, aiTextureType_DISPLACEMENT);
				loadMaterial(material, root, aiTextureType_LIGHTMAP);
				loadMaterial(material, root, aiTextureType_REFLECTION);

				root->setMaterial(TG_DEFAULT_MATERIAL);
				root->setHasModel(true);
				root->setStorageData(d);
			}
		}

		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			auto child = node->mChildren[i];
			Mesh *mesh = Mesh::create(child->mName.C_Str());
			mesh->setParent(root);
			root->addChild(mesh);
			this->foreachChild(child, mesh, scene);
		}
	}


	void MeshManager::foreachChildWithConfig(aiNode *node, Mesh *root, const aiScene *scene, XLightScriptFile &config_file)
	{
		root->setName(node->mName.C_Str());
		root->setMatrix(
			node->mTransformation.a1, node->mTransformation.a2, node->mTransformation.a3, node->mTransformation.a4,
			node->mTransformation.b1, node->mTransformation.b2, node->mTransformation.b3, node->mTransformation.b4,
			node->mTransformation.c1, node->mTransformation.c2, node->mTransformation.c3, node->mTransformation.c4,
			node->mTransformation.d1, node->mTransformation.d2, node->mTransformation.d3, node->mTransformation.d4);

		if (node->mNumMeshes != 0)
		{
			auto assembled = config_file["init"]["assembled"]().asBool();
			root->setAssembled(assembled);
			if (assembled)
			{
				root->setMaterial(config_file["material"][root->getName()]().asSTDString());
			}
			else
			{
				root->setMaterial(config_file["material"]["all"]().asSTDString());
			}

			for (unsigned int meshIndex = 0; meshIndex < node->mNumMeshes; meshIndex++)
			{
				VertexData *d = new VertexData();
				auto &indices = d->Indices;
				auto &vertices = d->Vertices;

				const aiMesh* mesh = scene->mMeshes[node->mMeshes[meshIndex]];
				vertices.resize(mesh->mNumVertices);

				bool hasPos = mesh->HasPositions();
				bool hasUV0 = mesh->HasTextureCoords(0);
				bool hasNormal = mesh->HasNormals();
				bool hasColor0 = mesh->HasVertexColors(0);
				bool has2T = mesh->HasTangentsAndBitangents();
				bool hasFace = mesh->HasFaces();

				for (unsigned int vertexIndex = 0; vertexIndex < mesh->mNumVertices; vertexIndex++)
				{
					if (hasPos)
					{
						aiVector3D &pos = mesh->mVertices[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Position;
						value.x = pos.x;
						value.y = pos.y;
						value.z = pos.z;
					}

					if (hasUV0)
					{
						aiVector3D &UVW = mesh->mTextureCoords[0][vertexIndex];
						auto &value = vertices[vertexIndex].Layout_UV;
						value.x = UVW.x;
						value.y = UVW.y;
					}
					else
					{
						auto &value = vertices[vertexIndex].Layout_UV;
						value.x = 0;
						value.y = 0;
					}

					if (hasNormal)
					{
						aiVector3D &n = mesh->mNormals[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Normal;
						value.x = n.x;
						value.y = n.y;
						value.z = n.z;
					}

					if (hasColor0)
					{
						aiColor4D &n = *mesh->mColors[vertexIndex];
						auto &value = vertices[vertexIndex].Layout_Color;
						value.x = n.r;
						value.y = n.g;
						value.z = n.b;
						value.w = n.a;
					}
					else
					{
						auto &value = vertices[vertexIndex].Layout_Color;
						value.x = 1.0f;
						value.y = 1.0f;
						value.z = 1.0f;
						value.w = 1.0f;
					}

					if (has2T)
					{
						auto &value1 = vertices[vertexIndex].Layout_Tangent;
						auto &t = mesh->mTangents[vertexIndex];
						value1.x = t.x;
						value1.y = t.y;
						value1.z = t.z;

						auto &value2 = vertices[vertexIndex].Layout_BitTangent;
						auto &b = mesh->mBitangents[vertexIndex];
						value2.x = b.x;
						value2.y = b.y;
						value2.z = b.z;
					}
					else
					{
						vertices[vertexIndex].Layout_Tangent = glm::vec3(0.0f);
						vertices[vertexIndex].Layout_BitTangent = glm::vec3(0.0f);
					}
				}

				if (hasFace)
				{
					indices.reserve(3 * mesh->mNumFaces);
					for (unsigned int i = 0; i < mesh->mNumFaces; i++)
					{
						indices.emplace_back(mesh->mFaces[i].mIndices[0]);
						indices.emplace_back(mesh->mFaces[i].mIndices[1]);
						indices.emplace_back(mesh->mFaces[i].mIndices[2]);
					}
				}

				static std::function<void(aiMaterial *, Mesh *, aiTextureType)> loadMaterial =
					[this](aiMaterial *material, Mesh *mesh, aiTextureType type)
				{
					if (material->GetTextureCount(type) > 0)
					{
						aiString texture_path;
						if (material->GetTexture(type, 0, &texture_path) == AI_SUCCESS)
						{
							std::string true_path = texture_path.data;
							true_path = String::getFileName(true_path);
							true_path = m_MeshName + "." + true_path;
							mesh->setTexture(type - 1, true_path);
						}
					}
					else
					{
						mesh->setTexture(type - 1, "null");
					}
				};
				auto material = scene->mMaterials[mesh->mMaterialIndex];
				loadMaterial(material, root, aiTextureType_DIFFUSE);
				loadMaterial(material, root, aiTextureType_AMBIENT);
				loadMaterial(material, root, aiTextureType_EMISSIVE);
				loadMaterial(material, root, aiTextureType_HEIGHT);
				loadMaterial(material, root, aiTextureType_NORMALS);
				loadMaterial(material, root, aiTextureType_SHININESS);
				loadMaterial(material, root, aiTextureType_OPACITY);
				loadMaterial(material, root, aiTextureType_DISPLACEMENT);
				loadMaterial(material, root, aiTextureType_LIGHTMAP);
				loadMaterial(material, root, aiTextureType_REFLECTION);

				root->setHasModel(true);
				root->setStorageData(d);
			}
		}

		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			auto child = node->mChildren[i];
			Mesh *mesh = Mesh::create(child->mName.C_Str());
			mesh->setParent(root);
			root->addChild(mesh);
			this->foreachChildWithConfig(child, mesh, scene, config_file);
		}
	}

	void MeshManager::clear()
	{
		for (auto model : m_MeshInSystem)
		{
			delete model.second;
		}
		m_MeshInSystem.clear();

		for (auto model : m_MeshInEngine)
		{
			delete model.second;
		}
		m_MeshInEngine.clear();
	}

}
