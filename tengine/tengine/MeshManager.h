#pragma once

#include <unordered_map>
#include <string>
#include <list>
#include <unordered_set>

#include "xsystem/XFilePackager.h"
#include "xsystem/XLightScriptSystem.h"

struct aiNode;
struct aiScene;

namespace tezcat
{
	class Mesh;
	class MeshManager
	{
	public:
		MeshManager();
		~MeshManager();

		//************************************
		// 函数名:	preLoadMeshFromPackage
		// 全名:		tezcat::MeshManager::preLoadMeshFromPackage
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		const std::string & name
		// 参数:		XFilePackager::DataPackage & data_pack
		// 功能:		
		//************************************
		void preLoadMeshFromPackage(const std::string &name, XFilePackager::DataPackage &data_pack);

		//************************************
		// 函数名:  getMesh
		// 返回值:  GLMesh *
		// 参数名:  const std::string & file
		// 功能:    通过名称取得一个Mesh根节点,如果没有会尝试从文件加载
		//************************************
		Mesh *createMesh(const std::string &file);

		//************************************
		// 函数名:	createMeshFromPackage
		// 全名:		tezcat::MeshManager::createMeshFromPackage
		// 权限:		public 
		// 返回值:   tezcat::Mesh *
		// 限定符:	
		// 参数:		const std::string & name
		// 功能:		
		//************************************
		Mesh *createMeshFromPackage(const std::string &name)
		{
			return m_MeshInEngine[name];
		}

	private:
		//************************************
		// 函数名:  loadMeshFromFile
		// 返回值:  void
		// 参数名:  const std::string & file
		// 功能:	   从文件读取mesh
		//************************************
		void loadMeshFromFile(const std::string &file);

		//************************************
		// 函数名:	foreachChild
		// 全名:		tezcat::MeshManager::foreachChild
		// 权限:		private 
		// 返回值:   void
		// 限定符:	
		// 参数:		aiNode * node
		// 参数:		Mesh * root
		// 参数:		const aiScene * scene
		// 功能:		
		//************************************
		void foreachChild(aiNode *node, Mesh *root, const aiScene *scene);

		//************************************
		// 函数名:	foreachChildWithConfig
		// 全名:		tezcat::MeshManager::foreachChildWithConfig
		// 权限:		private 
		// 返回值:   void
		// 限定符:	
		// 参数:		aiNode * node
		// 参数:		Mesh * root
		// 参数:		const aiScene * scene
		// 参数:		LightScriptFile & config_file
		// 功能:		
		//************************************
		void foreachChildWithConfig(aiNode *node, Mesh *root, const aiScene *scene, XLightScriptFile &config_file);

		//************************************
		// 函数名:  clear
		// 返回值:  void
		// 功能:    
		//************************************
		void clear();

	private:
		std::unordered_map<std::string, Mesh *> m_MeshInSystem;
		std::unordered_map<std::string, Mesh *> m_MeshInEngine;
		std::unordered_set<std::string> m_ModelTypes;
		std::string m_MeshName;
	};
}