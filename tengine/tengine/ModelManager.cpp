#include "ModelManager.h"

namespace tezcat
{
	ModelManager::ModelManager()
	{

	}

	ModelManager::~ModelManager()
	{

	}
#if 0
	Entity * ModelManager::create(const std::string &name)
	{
		m_TempName = name;
		auto &file = Singleton<SimpleDataManager>::getInstance()->getModelFile(m_TempName);

		auto e = Singleton<TEntityFactory>::getInstance()->create();
		auto tag = new TEntityTag();
		tag->setIntTag(TEntityType::Node);
		e->addStatic(tag);
		e->addStatic(new GLTransform());
		//		e->addStatic(new GLMessager());

		this->loadMesh2Entity(nullptr, e, Singleton<MeshManager>::getInstance()->getMesh(file["model"]["path"]().asSTDString()));
		m_TempName.clear();
		return e;
	}

	void ModelManager::loadMesh2Entity(Entity *e_parent, Entity *e_child, Mesh *m_parent)
	{
		//取得需要更新的组件进行更新
		auto transform = e_child->getStatic<GLTransform>();
		transform->setName(m_parent->getName());
		//		auto messager = e_child->getStatic<GLMessager>();

		//如果有父类,加载父类数据
		if (e_parent != nullptr)
		{
			e_parent->addChild(e_child);
			transform->setParent(e_parent->getStatic<GLTransform>());
		}

		//加载矩阵数据
		transform->setPosition(m_parent->getPosition());
		transform->setScale(m_parent->getScale());
		transform->setRotation(m_parent->getRotation());

		//如果含有模型数据
		if (m_parent->hasModel())
		{
			auto material = new GLMaterial();
			e_child->addStatic(material);

			auto model = new GLMode();
			model->sendModel(m_parent->getStorageData());

			//缓存模型数据
			//messager->setModelData(m_parent->getStorageData());
			//通知gl绑定模型数据
			//messager->sendMesh();

			//取得模型配置文件
			auto &modeComfig = Singleton<SimpleDataManager>::getInstance()->getModelFile(m_TempName);
			auto &texture = modeComfig["texture"];
			for (auto &t : *texture.getClass())
			{
				auto tex = Singleton<GLTextureManager>::getInstance()->getTexture2D((*t.second)().asSTDString());
				material->addTexture(tex, static_cast<unsigned int>(FixedScriptTextureRegister.at(t.first)));
			}

			auto cmd = new GLRenderCommand();
			cmd->OnRender = [=](GLProgram *program)
			{
				auto mainCamera = Singleton<GLCameraManager>::getInstance()->getCurrentCamera();
				auto &m = transform->getSelfMatrix();
				auto mv = mainCamera->getViewMatrix() * m;
				auto mvp = mainCamera->getProjectionMatrix() * mv;

				glUniformMatrix4fv(program->getMatrixIndex(GET_FIXED_GLSLMATRIX_INDEX(FixedGLSLMatrixIndex::MATRIX_MODEL)), 1, GL_FALSE, &m[0][0]);
				glUniformMatrix4fv(program->getMatrixIndex(GET_FIXED_GLSLMATRIX_INDEX(FixedGLSLMatrixIndex::MATRIX_MODELVIEW)), 1, GL_FALSE, &mv[0][0]);
				glUniformMatrix4fv(program->getMatrixIndex(GET_FIXED_GLSLMATRIX_INDEX(FixedGLSLMatrixIndex::MATRIX_MODELVIEWPROJECTION)), 1, GL_FALSE, &mvp[0][0]);
				glUniformMatrix4fv(program->getMatrixIndex(GET_FIXED_GLSLMATRIX_INDEX(FixedGLSLMatrixIndex::MATRIX_MODELNORMAL)), 1, GL_FALSE, &glm::mat3(glm::inverseTranspose(mv))[0][0]);

				material->bind(program);
				model->render();
			};

			/*
			//加载纹理
			auto texCom = new GLTextrueComponent();
			auto texture = modeComfig["texture"];
			for (auto t : *texture.getClass())
			{
			GLTexture2D *t2d = nullptr;
			if (t.first == "color")
			{
			t2d = new GLTexture2D(GLTexture::TextureType::Color ,(*t.second)().asSTDString());
			}
			else if (t.first == "diffuse")
			{
			t2d = new GLTexture2D(GLTexture::TextureType::Diffuse, (*t.second)().asSTDString());
			}
			else if (t.first == "specular")
			{
			t2d = new GLTexture2D(GLTexture::TextureType::Specular, (*t.second)().asSTDString());
			}
			else if (t.first == "normal")
			{
			t2d = new GLTexture2D(GLTexture::TextureType::Noraml, (*t.second)().asSTDString());
			}
			else if (t.first == "environment")
			{
			t2d = new GLTexture2D(GLTexture::TextureType::Environment, (*t.second)().asSTDString());
			}
			texCom->addTexture(t2d);
			}
			e_child->addStatic(texCom);

			//配置各个通道中的数据索引
			for (auto &pair : *modeComfig["shader"].getClass())
			{
			auto &shaderName = (*pair.second)().asSTDString();

			//取得shader
			auto group = Singleton<ShaderEngine>::getInstance()->getProgram<false>(shaderName);
			//取得shader的配置文件
			auto &shaderConfig = Singleton<SimpleDataManager>::getInstance()->getShaderFile(shaderName);
			auto &textureConfig = shaderConfig["config"]["texture"];

			//改变Tag种类
			e_child->getStatic<TEntityTag>()->setIntTag(TEntityType::Model);

			GLSLTexture sltexture(
			group->file->findUniform(textureConfig["color"]().asSTDString())->getID(),
			group->file->findUniform(textureConfig["diffuse"]().asSTDString())->getID(),
			group->file->findUniform(textureConfig["specular"]().asSTDString())->getID(),
			group->file->findUniform(textureConfig["normal"]().asSTDString())->getID(),
			group->file->findUniform(textureConfig["environment"]().asSTDString())->getID());

			//读取shader数据
			auto &mode_config = shaderConfig["config"]["model"];

			GLSLModel slmode(
			group->file->findUniform(mode_config["m"]().asSTDString())->getID(),
			group->file->findUniform(mode_config["mv"]().asSTDString())->getID(),
			group->file->findUniform(mode_config["mvp"]().asSTDString())->getID(),
			group->file->findUniform(mode_config["normal"]().asSTDString())->getID());

			messager->addRenderCMD(group->list, [=]()
			{
			messager->useVAO();
			auto mainCamera = Singleton<GLCameraManager>::getInstance()->getCurrentCamera();
			auto &m = transform->getSelfMatrix();
			auto mv = mainCamera->getViewMatrix() * m;
			auto mvp = mainCamera->getProjectionMatrix() * mv;

			glUniformMatrix4fv(slmode.m, 1, GL_FALSE, &m[0][0]);
			glUniformMatrix4fv(slmode.mv, 1, GL_FALSE, &mv[0][0]);
			glUniformMatrix4fv(slmode.mvp, 1, GL_FALSE, &mvp[0][0]);
			glUniformMatrix4fv(slmode.normal, 1, GL_FALSE, &glm::mat3(glm::inverseTranspose(mv))[0][0]);

			texCom->bindTexture(sltexture);

			messager->send2Draw();
			});
			}
			*/
		}

		for (auto m_child : m_parent->getChildren())
		{
			Entity *e_child2 = Singleton<TEntityFactory>::getInstance()->create();
			auto tag = new TEntityTag();
			tag->setIntTag(TEntityType::Node);
			e_child2->addStatic(tag);
			e_child2->addStatic(new GLTransform());
			//			e_child2->addStatic(new GLMessager());
			//			e_child2->addStatic(new GLMaterial());

			this->loadMesh2Entity(e_child, e_child2, m_child);
		}
	}
#endif

}

