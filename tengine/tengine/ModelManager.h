#pragma once

#include <string>

namespace tezcat
{
	class Entity;
	class Mesh;
	class ModelManager
	{
	public:
		ModelManager();
		~ModelManager();

		//************************************
		// 函数名:  create
		// 返回值:  GLEntityX *
		// 参数名:  const std::string & name
		// 功能:    
		//************************************
		Entity *create(const std::string &name);


	private:
		//************************************
		// 函数名:  loadMesh2Entity
		// 返回值:  void
		// 参数名:  GLEntityX * e_parent
		// 参数名:  GLEntityX * e_child
		// 参数名:  Mesh * m_parent
		// 功能:    
		//************************************
		void loadMesh2Entity(Entity *e_parent, Entity *e_child, Mesh *m_parent);


	private:
		std::string m_TempName;
	};
}