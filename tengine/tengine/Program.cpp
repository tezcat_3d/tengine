#include "Program.h"

namespace tezcat
{

	unsigned int Program::m_CustomFixedID = 0;

	unsigned int Program::addFixedLocation(const std::string &fixed_name)
	{
		auto id = Program::FixedLocation::Count + m_CustomFixedID;
		String2FixedID.emplace(std::make_pair(fixed_name, static_cast<Program::FixedLocation>(id)));
		m_CustomFixedID += 1;
		return id;
	}

	std::unordered_map<std::string, Program::FixedLocation> Program::String2FixedID =
	{
		//mat4
		{ "Mat4_Model",				Program::FixedLocation::Mat4_Model },
		{ "Mat4_ModelView",			Program::FixedLocation::Mat4_ModelView },
		{ "Mat4_MVP",				Program::FixedLocation::Mat4_MVP },
		{ "Mat4_View",				Program::FixedLocation::Mat4_View },
		{ "Mat4_Projection",		Program::FixedLocation::Mat4_Projection },
		{ "Mat4_ViewProjection",	Program::FixedLocation::Mat4_ViewProjection },

		//mat3
		{ "Mat3_ModelNormal",		Program::FixedLocation::Mat3_ModelNormal },

		//vec3
		{ "Vec3_ViewPos",			Program::FixedLocation::Vec3_ViewPos },

		//tex
		{ "Texture_Ambient",		Program::FixedLocation::Texture_Ambient },
		{ "Texture_Diffuse",		Program::FixedLocation::Texture_Diffuse },
		{ "Texture_Normal",			Program::FixedLocation::Texture_Normal },
		{ "Texture_Spaculer",		Program::FixedLocation::Texture_Spaculer },
		{ "Texture_Reflection",		Program::FixedLocation::Texture_Reflection },
		{ "Texture_Lightmap",		Program::FixedLocation::Texture_Lightmap },
		{ "Texture_Skybox",			Program::FixedLocation::Texture_Skybox },

		//float
		{ "Float_DeltaTime",		Program::FixedLocation::Float_DeltaTime },

		{ "Uniform0",				Program::FixedLocation::Uniform0, },
		{ "Uniform1",				Program::FixedLocation::Uniform1, },
		{ "Uniform2",				Program::FixedLocation::Uniform2, },
		{ "Uniform3",				Program::FixedLocation::Uniform3, },
		{ "Uniform4",				Program::FixedLocation::Uniform4, },
		{ "Uniform5",				Program::FixedLocation::Uniform5, },
		{ "Uniform6",				Program::FixedLocation::Uniform6, },
		{ "Uniform7",				Program::FixedLocation::Uniform7, },
		{ "Uniform8",				Program::FixedLocation::Uniform8, },
		{ "Uniform9",				Program::FixedLocation::Uniform9, },
		{ "Uniform10",				Program::FixedLocation::Uniform10, },
		{ "Uniform11",				Program::FixedLocation::Uniform11, },
		{ "Uniform12",				Program::FixedLocation::Uniform12, },
		{ "Uniform13",				Program::FixedLocation::Uniform13, },
		{ "Uniform14",				Program::FixedLocation::Uniform14, },
		{ "Uniform15",				Program::FixedLocation::Uniform15, },

	};
}

