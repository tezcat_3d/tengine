#pragma once

//#include "GLHead.h"
#include <vector>
#include <unordered_map>
#include <string>

namespace tezcat
{
	class Program
	{
	public:
		enum FixedLocation : unsigned int
		{
			//�Զ���λ��
			//mat4
			Mat4_Model = 0,
			Mat4_ModelView,
			Mat4_MVP,
			Mat4_View,
			Mat4_Projection,
			Mat4_ViewProjection,

			//mat3
			Mat3_ModelNormal,


			//vec3
			Vec3_ViewPos,

			//tex
			Texture_Ambient,
			Texture_Diffuse,
			Texture_Normal,
			Texture_Spaculer,
			Texture_Reflection,
			Texture_Lightmap,
			Texture_Skybox,

			//float
			Float_DeltaTime,

			//Custom
			Uniform0,	Uniform1,	Uniform2,	Uniform3,	Uniform4,
			Uniform5,	Uniform6,	Uniform7,	Uniform8,	Uniform9,
			Uniform10,	Uniform11,	Uniform12,	Uniform13,	Uniform14,
			Uniform15,	Count
		};

	public:
		Program():m_ProgramIndex(0)
		{
			m_FixedLocation.resize(FixedLocation::Count, -1);
		}

		const std::string &getShaderName() const { return m_ShaderName; }
		void setShaderName(const std::string &val) { m_ShaderName = val; }

		const unsigned int &getProgramIndex() const { return m_ProgramIndex; }
		void setProgramIndex(const unsigned int &val) { m_ProgramIndex = val; }

		const unsigned int &convert2SLLocation(const std::string &name) const
		{
			return m_FixedLocation[String2FixedID[name]];
		}

		unsigned int getProgramID() { return m_ProgramID; }
		void setProgramID(const unsigned int &id) { m_ProgramID = id; }

		void setLocation(const Program::FixedLocation &index, const int &location)
		{
			m_FixedLocation[index] = location;
		}

		const int &getFixedLocation(const Program::FixedLocation &index) const { return m_FixedLocation[index]; }

	private:
		friend class ShaderManager;
		unsigned int m_ProgramIndex;
		unsigned int m_ProgramID;
		std::string m_ShaderName;
		std::vector<int> m_FixedLocation;
		static unsigned int m_CustomFixedID;

	public:
		static unsigned int addFixedLocation(const std::string &fixed_name);
		static std::unordered_map<std::string, Program::FixedLocation> String2FixedID;
		
	};
}