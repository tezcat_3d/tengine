#include "ProgramManager.h"
#include "Program.h"


namespace tezcat
{
	ProgramManager::ProgramManager()
	{
	}

	ProgramManager::~ProgramManager()
	{
		for (auto program : m_Programs)
		{
			glDeleteProgram(program.second->getProgramID());
		}
	}

	void ProgramManager::addProgram(Program *program)
	{
		program->setProgramIndex(m_Programs.size());
		m_Programs.emplace(program->getShaderName(), program);
	}
}

