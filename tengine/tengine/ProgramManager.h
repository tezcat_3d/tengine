#pragma once

#include "xsystem//XSingleton.h"
#include "GLHead.h"
#include "Program.h"
#include <unordered_map>
#include <string>

namespace tezcat
{
	class ProgramManager : public XSingleton<ProgramManager>
	{
	public:
		ProgramManager();
		~ProgramManager();

		//************************************
		// 函数名:	addProgram
		// 全名:		tezcat::ProgramManager::addProgram
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		Program * program
		// 功能:		
		//************************************
		void addProgram(Program *program);

		//************************************
		// 函数名:	use
		// 全名:		tezcat::ProgramManager::use
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		Program * program
		// 功能:		
		//************************************
		void use(Program *program)
		{
			glUseProgram(program->getProgramID());
		}

		//************************************
		// 函数名:  setValue1iv
		// 返回值:  void
		// 参数名:  const GLint & location
		// 参数名:  const GLint * value
		// 功能:    
		//************************************
		void setValue1iv(const GLint &location, const GLint *value)
		{
			if (location != -1)
			{
				glUniform1iv(location, 1, value);
			}
		}

		//************************************
		// 函数名:  setValue1fv
		// 返回值:  void
		// 参数名:  const GLint & location
		// 参数名:  const GLfloat * value
		// 功能:    
		//************************************
		void setValue1fv(const GLint &location, const GLfloat *value)
		{
			if (location != -1)
			{
				glUniform1fv(location, 1, value);
			}
		}

		//************************************
		// 函数名:  setValue3fv
		// 返回值:  void
		// 参数名:  const GLint & location
		// 参数名:  const GLfloat * value
		// 功能:    
		//************************************
		void setValue3fv(const GLint &location, const GLfloat *value)
		{
			if (location != -1)
			{
				glUniform3fv(location, 1, value);
			}
		}

		//************************************
		// 函数名:  setMatrix4fv
		// 返回值:  void
		// 参数名:  const FixedLocation & fixed
		// 参数名:  const GLfloat * matrix
		// 功能:    
		//************************************
		void setMatrix4fv(const GLint &location, const GLfloat *matrix)
		{
			if (location != -1)
			{
				glUniformMatrix4fv(location, 1, GL_FALSE, matrix);
			}
		}

		void setMatrix3fv(const GLint &location, const GLfloat *matrix)
		{
			if (location != -1)
			{
				glUniformMatrix3fv(location, 1, GL_FALSE, matrix);
			}
		}

		//************************************
		// 函数名:  useDefault
		// 返回值:  void
		// 功能:    
		//************************************
		void useDefault() { glUseProgram(0); }

		Program *getProgram(const std::string &name)
		{
			return m_Programs[name];
		}

	private:
		std::unordered_map<std::string, Program *> m_Programs;
	};
}