#pragma once

#include <vector>

#include "xsystem/XSingleton.h"
#include "RenderObject.h"
#include <list>

namespace tezcat
{
	class RenderCommandFactory : public XSingleton<RenderCommandFactory>
	{
		struct CMDWapper
		{
			std::list<RenderObject *> freeList;

			template<typename CMD>
			CMD *getCMD()
			{
				if (freeList.empty())
				{
					return new CMD();
				}
				else
				{
					auto cmd = freeList.front();
					freeList.pop_front();
					return static_cast<CMD *>(cmd);
				}
			}
		};
	public:
		RenderCommandFactory();
		~RenderCommandFactory();

		template<typename CMD>
		CMD *getCMD()
		{
#ifdef IOS
			return m_CMDs[CMD::RenderCommandGiveID()]->template getCMD<CMD>();
#else
			return m_CMDs[CMD::RenderCommandGiveID()]->getCMD<CMD>();
#endif
			
		}

		template<typename CMD>
		void registerCMD()
		{
			if (m_CMDs.size() < CMD::RenderCommandGiveID())
			{
				m_CMDs.resize(CMD::RenderCommandGiveID(), nullptr);
			}
			m_CMDs[CMD::RenderCommandGiveID()] = new CMDWapper();
		}

		template<typename CMD>
		void recycle(CMD *cmd)
		{
			m_CMDs[CMD::RenderCommandGiveID()]->freeList.push_back(cmd);
		}

	private:
		std::vector<CMDWapper *> m_CMDs;
	};
}

