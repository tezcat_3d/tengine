#include "RenderObject.h"
#include "RenderCommand.h"
#include "RenderSystem.h"
#include "Camera.h"
#include "Transform.h"
#include "UniversalMaterial.h"

namespace tezcat
{
	RenderObject::RenderObject():
		m_RenderCommand(nullptr),
		m_Camera(nullptr),
		m_Mesh(nullptr),
		m_Material(UniversalMaterial::DefaultMaterial),
		m_Transform(nullptr),
		m_ObjectType(ObjectType::Type_Null),
		m_Visible(true)
	{

	}

	RenderObject::~RenderObject()
	{

	}

	void RenderObject::uploadRenderCommand()
	{
		switch (m_ObjectType)
		{
		case RenderObject::ObjectType::Type_Null:
			break;
		case RenderObject::ObjectType::Type_Node:
			m_Transform->logic();
			break;
		case RenderObject::ObjectType::Type_Camera:
//			m_Transform->logic();
			m_Camera->render();
			break;
		case ObjectType::Type_Skybox:
			if (m_Visible)
			{
				RenderSystem::getInstance()->attachRenderObject(this);
			}
			break;
		case ObjectType::Type_Mesh:
			m_Transform->logic();
			if (m_Visible)
			{
				RenderSystem::getInstance()->attachRenderObject(this);
			}
			break;
		}
	}

}