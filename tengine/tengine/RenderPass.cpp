#include "RenderPass.h"
#include "ProgramManager.h"
#include "Transform.h"
#include "Material.h"
#include "RenderCommand.h"
#include "Camera.h"

namespace tezcat
{
	RenderPass::RenderPass():
		m_Enabled(true),
		m_MustBeClear(false),
		m_ID(-1)
	{
	
	}

	RenderPass::~RenderPass()
	{

	}

	void RenderPass::setProgram(Program *program)
	{
		m_Program = program;
	}

	void RenderPass::setProgram(const std::string &name)
	{
		m_Program = ProgramManager::getInstance()->getProgram(name);
	}

	SkyboxPass::SkyboxPass()
	{
		this->setBelong2Queue(RenderQueueType::Queue_Background);
	}

	//
	//
	void SkyboxPass::render(Camera * camera)
	{
//		m_Program->use();
		ProgramManager::getInstance()->use(m_Program);

		auto v = camera->getViewMatrix();
		v[3].x = 0;
		v[3].y = 0;
		v[3].z = 0;
		auto vp = camera->getProjectionMatrix() * v;

		ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_ViewProjection), &vp[0][0]);

		auto it = m_Renderables.begin();
		auto end = m_Renderables.end();
		while (it != end)
		{
			auto object = (*it);

			object->getMaterial()->uploadUniform(m_Program);
			object->getRenderCommand()->render();
			object->getMaterial()->unbindUniform();
			++it;
		}
	}


	//
	//
	void MeshPass::render(Camera *camera)
	{
		ProgramManager::getInstance()->use(m_Program);

		auto &v = camera->getViewMatrix();
		auto &p = camera->getProjectionMatrix();

		ProgramManager::getInstance()->setValue3fv(m_Program->getFixedLocation(Program::Vec3_ViewPos), &camera->getPosition()[0]);
		ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_View), &v[0][0]);
		ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_Projection), &p[0][0]);

		auto it = m_Renderables.begin();
		auto end = m_Renderables.end();
		while (it != end)
		{
			auto object = (*it);

			auto transform = object->getTransform();
			auto &m = transform->getWorldMatrix();
			auto mv = v * m;
			auto mvp = p * mv;
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_Model), &m[0][0]);
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_ModelView), &mv[0][0]);
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_MVP), &mvp[0][0]);
			ProgramManager::getInstance()->setMatrix3fv(m_Program->getFixedLocation(Program::Mat3_ModelNormal), &glm::mat3(glm::transpose(glm::inverse(m)))[0][0]);

			object->getMaterial()->uploadUniform(m_Program);
			object->getRenderCommand()->render();

			++it;
		}
	}

	PBRPass::PBRPass()
	{
		this->setBelong2Queue(RenderQueueType::Queue_Geometry);
	}

	void PBRPass::render(Camera *camera)
	{
		ProgramManager::getInstance()->use(m_Program);

		auto &v = camera->getViewMatrix();
		auto &p = camera->getProjectionMatrix();

		ProgramManager::getInstance()->setValue3fv(m_Program->getFixedLocation(Program::Vec3_ViewPos), &camera->getPosition()[0]);

		auto it = m_Renderables.begin();
		auto end = m_Renderables.end();
		while (it != end)
		{
			auto object = (*it);

			auto transform = object->getTransform();
			auto &m = transform->getWorldMatrix();
			auto mvp = p * v * m;
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_Model), &m[0][0]);
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_MVP), &mvp[0][0]);
			ProgramManager::getInstance()->setMatrix3fv(m_Program->getFixedLocation(Program::Mat3_ModelNormal), &glm::mat3(glm::transpose(glm::inverse(m)))[0][0]);

			object->getMaterial()->uploadUniform(m_Program);
			object->getRenderCommand()->render();

			++it;
		}
	}


	GlassPass::GlassPass()
	{
		this->setBelong2Queue(RenderQueueType::Queue_Alpha);
	}

	void GlassPass::render(Camera *camera)
	{
		ProgramManager::getInstance()->use(m_Program);

		auto &v = camera->getViewMatrix();
		auto &p = camera->getProjectionMatrix();

		ProgramManager::getInstance()->setValue3fv(m_Program->getFixedLocation(Program::Vec3_ViewPos), &camera->getPosition()[0]);

		auto it = m_Renderables.begin();
		auto end = m_Renderables.end();
		while (it != end)
		{
			auto object = (*it);

			auto transform = object->getTransform();
			auto &m = transform->getWorldMatrix();
			auto mvp = p * v * m;
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_Model), &m[0][0]);
			ProgramManager::getInstance()->setMatrix4fv(m_Program->getFixedLocation(Program::Mat4_MVP), &mvp[0][0]);
			ProgramManager::getInstance()->setMatrix3fv(m_Program->getFixedLocation(Program::Mat3_ModelNormal), &glm::mat3(glm::transpose(glm::inverse(m)))[0][0]);

			object->getMaterial()->uploadUniform(m_Program);
			object->getRenderCommand()->render();

			++it;
		}
	}

	ScreenPass::ScreenPass()
	{
		this->setBelong2Queue(RenderQueueType::Queue_Background);
	}

	void ScreenPass::render(Camera *camera)
	{
		ProgramManager::getInstance()->use(m_Program);

		auto it = m_Renderables.begin();
		auto end = m_Renderables.end();
		while (it != end)
		{
			auto object = (*it);

			object->getMaterial()->uploadUniform(m_Program);
			object->getRenderCommand()->render();
			++it;
		}
	}
}