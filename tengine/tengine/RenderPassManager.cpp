#include "RenderPassManager.h"
#include "Program.h"
#include "RenderPass.h"

namespace tezcat
{
	void RenderPassManager::createPass(Program *program, const std::string &name)
	{
		auto itID = m_ProgramWithID.find(program->getProgramID());
		auto itName = m_ProgramWithName.find(name);
		if ( (itID == m_ProgramWithID.end()) && (itName == m_ProgramWithName.end()) )
		{
			m_ProgramWithID.insert(std::make_pair(program->getProgramID(), program));
			m_ProgramWithName.insert(std::make_pair(name, program));

			RenderPass *pass = new RenderPass();
			pass->setProgram(program);
			m_Pass.insert(std::make_pair(name, pass));
		}
	}

	Program * RenderPassManager::getProgram(const std::string &name)
	{
		return m_ProgramWithName[name];
	}
}

