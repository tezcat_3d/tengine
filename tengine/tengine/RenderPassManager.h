#pragma once

#include <unordered_map>
#include <string>
#include "xsystem/XSingleton.h"

namespace tezcat
{
	class Program;
	class RenderPass;
	class RenderCommand;
	class RenderPassManager : public XSingleton<RenderPassManager>
	{
	public:

		void createPass(Program *program, const std::string &name);
		Program *getProgram(const std::string &name);


	private:
		std::unordered_map<unsigned int, Program *> m_ProgramWithID;
		std::unordered_map<std::string, Program *> m_ProgramWithName;

		std::unordered_map<std::string, RenderPass *> m_Pass;
	};
}