#include "RenderQueue.h"
#include "RenderPass.h"
#include "Material.h"
#include "RenderObject.h"


namespace tezcat
{
	RenderQueue::RenderQueue(RenderQueueType type):
		m_QueueType(type)
	{
	}

	RenderQueue::~RenderQueue()
	{

	}

	void RenderQueue::render(Camera *camera)
	{
//		OnBegin();
		auto it = m_RenderPass.begin();
		auto end = m_RenderPass.end();
		while (it != end)
		{
			auto pass = it->second;
			if (pass->getMustBeClear())
			{
				it = m_RenderPass.erase(it);
				end = m_RenderPass.end();
//				m_RenderPass.remove(pass->getXListNode());
			}
			else
			{
				pass->render(camera);
				pass->clear();
			}
			++it;
		}
//		OnEnd();
	}

	void RenderQueue::addRenderPass(RenderPass * pass)
	{
		m_RenderPass.emplace(pass->getPassID(), pass);
	}

	void RenderQueue::addRenderObject(RenderObject *object)
	{
		auto &id = object->getMaterial()->getPassID();
		m_RenderPass[id]->attachRenderObject(object);
	}

	void RenderQueue::clear()
	{

	}

}