#pragma once

//#include "xsystem/XList.h"
#include "QueuePassConfig.h"
#include <functional>
#include <vector>
//#include <list>
#include <map>

namespace tezcat
{
	class Camera;
	class RenderPass;
	class RenderObject;
	class RenderQueue
	{
	public:
		RenderQueue(RenderQueueType type);
		~RenderQueue();

		void render(Camera *camera);
		void addRenderPass(RenderPass *pass);
		void addRenderObject(RenderObject *object);
		
		void clear();

	private:
		std::function<void()> OnBegin;
		std::function<void()> OnEnd;

		RenderQueueType m_QueueType;

		size_t m_RangeMin;
		size_t m_RangeMax;

//		XList<RenderPass *> m_RenderPass;
//		std::list<RenderPass *> m_RenderPass;
		std::map < int, RenderPass *, std::less<int> > m_RenderPass;
	};
}