#include "RenderSystem.h"
#include "RenderQueue.h"
#include "RenderPass.h"
#include "RenderObject.h"
#include "EngineEvent.h"
#include "RenderCommand.h"
#include "QueuePassConfig.h"
#include "GLRenderState.h"

#include "Scene.h"
#include "Camera.h"


namespace tezcat
{
	RenderSystem::RenderSystem():
		m_Background(new RenderQueue(RenderQueueType::Queue_Background)),
		m_Geometry(new RenderQueue(RenderQueueType::Queue_Geometry)),
		m_Alpha(new RenderQueue(RenderQueueType::Queue_Alpha)),
		m_Overlay(new RenderQueue(RenderQueueType::Queue_Overlay))
	{
		XEventSystem::getInstance()->subscribe<ScenePush>(this, [this](XBaseEvent *event)
		{
			m_NowCameraList = &event->get<ScenePush>()->scene->getCameras();
		});
	}

	RenderSystem::~RenderSystem()
	{

	}

	void RenderSystem::render()
	{
		//fbo

		auto it = m_NowCameraList->begin();
		auto end = m_NowCameraList->end();
		while (it != end)
		{
			auto camera = (*it);
			glstate::onRender();
			glstate::onBackground();
			m_Background->render(camera);

			glstate::onGeometry();
			m_Geometry->render(camera);

			glstate::onAlphaBegin();
			m_Alpha->render(camera);
			glstate::onAlphaEnd();

			glstate::onOverlay();
			m_Overlay->render(camera);
			++it;
		}
	}

	void RenderSystem::attachRenderObject(RenderObject *object)
	{
		
		switch (object->getRenderCommand()->getQueueType())
		{
		case RenderQueueType::Queue_Background:
			m_Background->addRenderObject(object);
			break;
		case RenderQueueType::Queue_Geometry:
			m_Geometry->addRenderObject(object);
			break;
		case RenderQueueType::Queue_Alpha:
			m_Alpha->addRenderObject(object);
			break;
		case RenderQueueType::Queue_Overlay:
			m_Overlay->addRenderObject(object);
			break;
		default:
			throw std::logic_error("Object must have a type!");
			break;
		}
	}

	void RenderSystem::attachRenderPass(RenderPass *pass)
	{
		switch (pass->getBelong2Queue())
		{
		case RenderQueueType::Queue_Background:
			m_Background->addRenderPass(pass);
			break;
		case RenderQueueType::Queue_Geometry:
			m_Geometry->addRenderPass(pass);
			break;
		case RenderQueueType::Queue_Alpha:
			m_Alpha->addRenderPass(pass);
			break;
		case RenderQueueType::Queue_Overlay:
			m_Overlay->addRenderPass(pass);
			break;
		default:
			throw std::logic_error("Pass must have a type!");
			break;
		}
	}

	void RenderSystem::initSystem()
	{
		glstate::init();
	}

}

