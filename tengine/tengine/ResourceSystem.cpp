#include "ResourceSystem.h"

#include "Camera.h"

#include "ImageManager.h"
#include "Mesh.h"
#include "MeshManager.h"
#include "ShaderManager.h"
#include "GLTextureManager.h"
#include "RenderCommand.h"
#include "SkyboxCommand.h"
#include "CameraCommand.h"
#include "GlobalConfiguration.h"
#include "MaterialManager.h"
#include "xsystem/XString.h"
#include "xsystem/XLightScriptSystem.h"
#include "xsystem/XFilePackager.h"
#include "xsystem/XLogSystem.h"

#include "Transform.h"
#include "MeshCommand.h"

#include <functional>

namespace tezcat
{

	ResourceSystem::ResourceSystem():
		m_ImageManager(new ImageManager()),
		m_MeshManager(new MeshManager()),
		m_ShaderManager(new ShaderManager()),
		m_GLTextureManager(new GLTextureManager()),
		m_MaterialManager(new MaterialManager())
	{
		
	}

	ResourceSystem::~ResourceSystem()
	{
		delete m_MeshManager;
		delete m_ImageManager;
		delete m_ShaderManager;
		delete m_GLTextureManager;
		delete m_MaterialManager;
	}

	void ResourceSystem::loadConfiguration(const std::string &root_path, const std::string & relative_path)
	{
		m_ResRootPath = root_path;
		XLightScriptSystem system;
		if (system.loadScript(m_ResRootPath + relative_path))
		{
			auto &resFile = system.getFile("resource");

			//加载图片资源
			for (auto c : resFile["image"].getArray())
			{
				this->loadTextureFromPackage((*c)().asSTDString());
			}

			//加载材质资源,材质必须被第一个加载
			for (auto c : resFile["material"].getArray())
			{
				this->loadMaterialFromPackage((*c)().asSTDString());
			}

			//加载shader
			for (auto c : resFile["shader"].getArray())
			{
				this->loadShaderFromPackage((*c)().asSTDString());
			}

			//加载模型资源
			for (auto c : resFile["model"].getArray())
			{
				this->loadModelFromPackage((*c)().asSTDString());
			}
		}
		else
		{
			XLOG_INFO(ResourceSystem, "not found any res");
		}
	}

	void ResourceSystem::loadModelFromPackage(const std::string &pack_file)
	{
		XFilePackager packager;
		XFilePackager::DataPackage dataPackage;

		if (packager.unpack2Memory(pack_file, dataPackage))
		{
			auto name = String::getFileName(pack_file);
			m_MeshManager->preLoadMeshFromPackage(name, dataPackage);
		}
		else
		{
			XLOG_INFO(ResourceSystem, "model package <" + pack_file + "> not found");
		}
	}

	Entity *ResourceSystem::createModel(const std::string &name, const int &pass_id)
	{
		unsigned int model_count = 0;
		static std::function<void(Entity *, Entity *, Mesh *)> assmble =
			[&](Entity *parent, Entity *child, Mesh *root)
		{
			//取得需要更新的组件进行更新
			std::string name = root->getName();
			auto render_object = child->getRenderObject();
			auto transform = render_object->getTransform();
			transform->setName(name);
			child->setName(name);

			//如果有父类,加载父类数据
			if (parent != nullptr)
			{
				parent->addChild(child);
			}

			//加载矩阵数据
			transform->setPosition(root->getPosition());
			transform->setScale(root->getScale());
			transform->setRotation(root->getRotation());

			//如果含有模型数据
			if (root->hasModel())
			{
				//绑定并传送数据
				auto cmd = new MeshCommand();
				cmd->sendData2GL(root->getStorageData());
				cmd->setQueueType(RenderQueueType::Queue_Geometry);
				render_object->setMesh(root);
				render_object->setObjectType(RenderObject::ObjectType::Type_Mesh);
				render_object->setRenderCommand(cmd);
				auto um = m_MaterialManager->createDefaultMaterial(root->getMaterial() + std::to_string(model_count++));
				um->setPassID(pass_id);
				render_object->setMaterial(um);
			}
			else
			{
				render_object->setObjectType(RenderObject::ObjectType::Type_Node);
			}

			for (auto m_child : root->getChildren())
			{
				auto subChild = EntityManager::getInstance()->create();
				RenderObject *rb = new RenderObject();
				rb->setTransform(Transform::create());
				subChild->setRenderObject(rb);
				assmble(child, subChild, m_child);
			}
		};

		auto *entity = EntityManager::getInstance()->create();
		RenderObject *rb = new RenderObject();
		rb->setTransform(Transform::create());
		entity->setRenderObject(rb);
		auto *mesh = m_MeshManager->createMesh(name);
		assmble(nullptr, entity, mesh);
		
		return entity;
	}

	Entity * ResourceSystem::createModelFromPackage(const std::string &pack_name)
	{
		static std::function<void(Entity *, Entity *, Mesh *)> assmble =
			[&](Entity *parent, Entity *child, Mesh *root)
		{
			//取得需要更新的组件进行更新
			std::string name = root->getName();
			auto render_object = child->getRenderObject();
			auto transform = render_object->getTransform();
			transform->setName(name);
			child->setName(name);

			//如果有父类,加载父类数据
			if (parent != nullptr)
			{
				parent->addChild(child);
			}

			//加载矩阵数据
			transform->setPosition(root->getPosition());
			transform->setScale(root->getScale());
			transform->setRotation(root->getRotation());

			//如果含有模型数据
			if (root->hasModel())
			{
				//绑定并传送数据
				auto cmd = new MeshCommand();
				cmd->sendData2GL(root->getStorageData());
				render_object->setMesh(root);
				render_object->setObjectType(RenderObject::ObjectType::Type_Mesh);
				render_object->setRenderCommand(cmd);
				if (root->getAssembled())
				{
					auto mats = m_MaterialManager->createUniversalMaterialGroupFromPackage(String::getNameByFirstDot(root->getMaterial()));
					auto mat = mats->at(String::getNameByLastDot(root->getMaterial()));
					render_object->setMaterial(mat);
					cmd->setQueueType(static_cast<RenderQueueType>(mat->getQueueID()));
				}
				else
				{
					auto mat = m_MaterialManager->createUniversalMaterialFromPackage(root->getMaterial())->copy();
					render_object->setMaterial(mat);
					cmd->setQueueType(static_cast<RenderQueueType>(mat->getQueueID()));
				}
			}
			else
			{
				render_object->setObjectType(RenderObject::ObjectType::Type_Node);
			}

			for (auto m_child : root->getChildren())
			{
				auto subChild = EntityManager::getInstance()->create();
				RenderObject *rb = new RenderObject();
				rb->setTransform(Transform::create());
				subChild->setRenderObject(rb);
				assmble(child, subChild, m_child);
			}
		};

		auto *entity = EntityManager::getInstance()->create();
		RenderObject *rb = new RenderObject();
		rb->setTransform(Transform::create());
		entity->setRenderObject(rb);
		auto *mesh = m_MeshManager->createMeshFromPackage(pack_name);
		assmble(nullptr, entity, mesh);

		return entity;
	}


	Entity * ResourceSystem::createSkyBox()
	{
		auto *entity = EntityManager::getInstance()->create();

		SkyboxCommand *cmd = new SkyboxCommand();
		cmd->setQueueType(RenderQueueType::Queue_Background);
		cmd->sendData2GL(nullptr);

		RenderObject *sky_box = new RenderObject();
		sky_box->setObjectType(RenderObject::ObjectType::Type_Skybox);
		sky_box->setRenderCommand(cmd);
		sky_box->setTransform(Transform::create());
		entity->setRenderObject(sky_box);
		return entity;
	}

	Entity *ResourceSystem::createCamera(const std::string &name)
	{
		auto *entity = EntityManager::getInstance()->create();
		entity->setName(name);

		CameraCommond *cmd = new CameraCommond();
		cmd->setQueueType(RenderQueueType::Queue_Null);

		auto camera = Camera::create(
			60,
			static_cast<const float>(tezcat::GlobalConfiguration::ScreenWidth),
			static_cast<const float>(tezcat::GlobalConfiguration::ScreenHegiht),
			0.1f, 1000.0f);

		RenderObject *camera_object = new RenderObject();
		camera_object->setObjectType(RenderObject::ObjectType::Type_Camera);
		camera_object->setTransform(Transform::SwapTransform);
		camera_object->setRenderCommand(cmd);
		camera_object->setCamera(camera);
		camera->setTransformParent(Transform::SwapTransform);
		entity->setRenderObject(camera_object);
		return entity;
	}

	//=================================================================
	//
	//	
	//
	Program * ResourceSystem::createProgram(const std::string &name, const int &queue_id, const std::string &vs, const std::string &fs, const std::string &gs /*= ""*/, const std::string &tcs /*= ""*/, const std::string &tes /*= ""*/)
	{
		return m_ShaderManager->createProgram(name, queue_id, vs, fs, gs, tcs, tes);
	}

	Program * ResourceSystem::createProgramFromPackage(const std::string & pack_name)
	{
		return m_ShaderManager->createProgramFromPackage(pack_name);
	}


	void ResourceSystem::loadShaderFromPackage(const std::string &pack_name)
	{
		XFilePackager packager;
		XFilePackager::DataPackage dataPackage;

		if (packager.unpack2Memory(pack_name, dataPackage))
		{
			auto name = String::getFileName(pack_name);
			m_ShaderManager->preLoadShaderFormPackage(name, dataPackage);
		}
		else
		{
			XLOG_ERROR(ResourceSystem, "shader package <" + pack_name + "> not found");
		}
	}

	bool ResourceSystem::confirmShaderIsLoaded(const std::string &pack_name)
	{
		return m_ShaderManager->confirmShaderIsLoaded(pack_name);
	}

	//======================================================
	//
	//	Material
	//
	UniversalMaterial *ResourceSystem::createUniversalMaterialFromPackage(const std::string &pack_name)
	{
		return m_MaterialManager->createUniversalMaterialFromPackage(pack_name);
	}

	std::unordered_map<std::string, UniversalMaterial *> * ResourceSystem::createUniversalMaterialGroupFromPackage(const std::string &pack_name)
	{
		return m_MaterialManager->createUniversalMaterialGroupFromPackage(pack_name);
	}

	void ResourceSystem::loadMaterialFromPackage(const std::string &pack_name)
	{
		XFilePackager packager;
		XFilePackager::DataPackage dataPackage;
		if (packager.unpack2Memory(pack_name, dataPackage))
		{
			auto name = String::getFileName(pack_name);
			m_MaterialManager->preLoadUniversalMaterialFromPackage(name, dataPackage);
		}
		else
		{
			XLOG_ERROR(ResourceSystem, "material package <" + pack_name + "> not found");
		}
	}


	//======================================================
	//
	//	Texture
	//
	GLTexture2D *ResourceSystem::createTexture2D(const std::string &file)
	{
		return m_GLTextureManager->createTexture2D(file, m_ImageManager);
	}

	GLTextureCube * ResourceSystem::createCubeMap(
		const std::string &name,
		const std::string &positive_x, const std::string &negative_x,
		const std::string &positive_y, const std::string &negative_y,
		const std::string &positive_z, const std::string &negative_z)
	{
		return m_GLTextureManager->createCubeMap(
			name,
			positive_x, negative_x,
			positive_y, negative_y,
			positive_z, negative_z,
			m_ImageManager);
	}

	GLTextureCube * ResourceSystem::createCubeMapFromPackage(const std::string &cubemap_name)
	{
		return m_GLTextureManager->createCubeMapFromPackage(cubemap_name, m_ImageManager);
	}

	GLTexture2D * ResourceSystem::createTexture2DFromPackage(const std::string &name)
	{
		return m_GLTextureManager->createTexture2DFromPackage(name, m_ImageManager);
	}

	void ResourceSystem::loadTextureFromPackage(const std::string &pack_name)
	{
		XFilePackager packager;
		XFilePackager::DataPackage dataPackage;
		if (packager.unpack2Memory(pack_name, dataPackage))
		{
			auto name = String::getFileName(pack_name);
			if (String::getNameByLastDot(pack_name).compare("cubemap") == 0)
			{
				m_ImageManager->preLoadCubeMapFromPackage(name, dataPackage);
			}
			else
			{
				m_ImageManager->preLoadImagePackage(name, dataPackage);
			}
		}
		else
		{
			XLOG_ERROR(ResourceSystem, "material package <" + pack_name + "> not found");
		}
	}
}

