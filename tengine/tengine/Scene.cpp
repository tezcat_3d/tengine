#include "Scene.h"
#include "Entity.h"
#include "EntityManager.h"
#include "Camera.h"
#include "RenderObject.h"

namespace tezcat
{
	Scene::Scene(
		const std::string &name,
		const std::function<void(Scene *)> &onbuild,
		const std::function<void(Scene *)> &ondestroy,
		const std::function<void(Scene *)> &onpush,
		const std::function<void(Scene *)> &onpop):
		m_Name(name),
		OnBuild(onbuild),
		OnDestroy(ondestroy),
		OnPush(onpush),
		OnPop(onpop)
	{
		m_TheWorld = EntityManager::getInstance()->create();
		RenderObject *world = new RenderObject();
		auto transform = Transform::create();
		transform->setName("The_World");
		world->setTransform(transform);
		m_TheWorld->setRenderObject(world);
		m_TheWorld->setName("The_World");
	}

	Scene::Scene(const std::string &name):
		m_Name(name),
		OnBuild(nullptr),
		OnDestroy(nullptr),
		OnPush(nullptr),
		OnPop(nullptr)
	{

	}

	Scene::~Scene()
	{
		
	}

	void Scene::addChild(Entity *entity)
	{
		m_TheWorld->addChild(entity);

		auto render_object = entity->getRenderObject();
		switch (render_object->getObjectType())
		{
		case RenderObject::ObjectType::Type_Camera:
			render_object->getCamera()->setTransformParent();
			m_Cameras.push_back(render_object->getCamera());
			break;
		default:
			break;
		}
	}

	void Scene::logic()
	{
		//非递归
		//    1      2
		//  1 2 3
		//深度优先遍历整棵场景树
//		bool loop = true;
		m_CurrentIt = m_TheWorld->getChildren().begin();
		m_CurrentEnd = m_TheWorld->getChildren().end();

		while (true)
		{
			//有子节点
			while (m_CurrentIt != m_CurrentEnd)
			{
				auto child = (*m_CurrentIt);
				child->getRenderObject()->uploadRenderCommand();
				child->updataDynamicComponent();
				++m_CurrentIt;

				//如果有子节点
				if (child->hasChild())
				{
					//保存当前节点迭代信息
					m_EntityStack.emplace_back(std::make_pair(m_CurrentIt, m_CurrentEnd));
					//把迭代器改变为子节点初始迭代器
					m_CurrentIt = child->getChildren().begin();
					m_CurrentEnd = child->getChildren().end();
				}
			}

			//如果没有节点遍历了,关闭循环
			if (m_EntityStack.empty())
			{
//				loop = false;
				break;
			}
			//如果有剩余的节点,获得上层迭代信息
			else
			{
				auto &pair = m_EntityStack.back();
				m_CurrentIt = pair.first;
				m_CurrentEnd = pair.second;
				m_EntityStack.pop_back();
			}
		}
	}

	void Scene::onBuild()
	{
		if (OnBuild)
		{
			OnBuild(this);
		}
	}

	void Scene::onDestroy()
	{
		if (OnDestroy)
		{
			OnDestroy(this);
		}
	}

	void Scene::onPush()
	{
		if (OnPush)
		{
			OnPush(this);
		}
	}

	void Scene::onPop()
	{
		if (OnPop)
		{
			OnPop(this);
		}
	}
}