#include "SceneSystem.h"
#include "Entity.h"
#include "EngineEvent.h"


namespace tezcat
{
	SceneSystem::SceneSystem():
		m_CurrentScene(nullptr)
	{

	}

	SceneSystem::~SceneSystem()
	{

	}

	Scene *SceneSystem::createScene(
		const std::string &name,
		const std::function<void(Scene *)> &onbuild,
		const std::function<void(Scene *)> &ondestroy,
		const std::function<void(Scene *)> &onpush,
		const std::function<void(Scene *)> &onpop)
	{
		return new Scene(name, onbuild, ondestroy, onpush, onpop);
	}

	Scene *SceneSystem::replaceScene(
		const std::string &name,
		const std::function<void(Scene *)> &onbuild,
		const std::function<void(Scene *)> &ondestroy,
		const std::function<void(Scene *)> &onpush,
		const std::function<void(Scene *)> &onpop)
	{
		m_CurrentScene->onDestroy();
		delete m_CurrentScene;

		auto m_CurrentScene = this->createScene(name, onbuild, ondestroy, onpush, onpop);
		m_CurrentScene->onBuild();
		m_Scenes.back() = m_CurrentScene;

		auto event = XEventSystem::getInstance()->createEvent<SceneReplace>();
		event->scene = m_CurrentScene;
		XEventSystem::getInstance()->broadcast(event);

		return m_CurrentScene;
	}

	Scene *SceneSystem::popScene()
	{
		m_CurrentScene->onDestroy();
		delete m_CurrentScene;

		m_Scenes.pop_back();
		m_CurrentScene = m_Scenes.back();
		m_CurrentScene->onPop();
		return m_CurrentScene;
	}

	Scene *SceneSystem::pushScene(
		const std::string &name,
		const std::function<void(Scene *)> &onbuild,
		const std::function<void(Scene *)> &ondestroy,
		const std::function<void(Scene *)> &onpush,
		const std::function<void(Scene *)> &onpop)
	{
		if (m_CurrentScene)
		{
			m_CurrentScene->onPush();
		}

		m_CurrentScene = this->createScene(name, onbuild, ondestroy, onpush, onpop);
		m_CurrentScene->onBuild();

		auto event = XEventSystem::getInstance()->createEvent<ScenePush>();
		event->scene = m_CurrentScene;
		XEventSystem::getInstance()->broadcast(event);

		return m_CurrentScene;
	}

	void SceneSystem::logic()
	{
		m_CurrentScene->logic();
	}
}