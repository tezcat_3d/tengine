#include "ScreenCommand.h"

namespace tezcat
{
	ScreenCommand::ScreenCommand()
	{
	}

	ScreenCommand::~ScreenCommand()
	{
	}

	void ScreenCommand::sendData2GL(VertexData *mesh)
	{
		static float screen[] = 
		{
			//Positions   //UV
			-1.0f,  1.0f,  0.0f, 1.0f,
			-1.0f, -1.0f,  0.0f, 0.0f,
			1.0f, -1.0f,  1.0f, 0.0f,

			-1.0f,  1.0f,  0.0f, 1.0f,
			1.0f, -1.0f,  1.0f, 0.0f,
			1.0f,  1.0f,  1.0f, 1.0f
		};

		glGenVertexArrays(1, &m_VAOID);
		glBindVertexArray(m_VAOID);

		glGenBuffers(1, &m_VBOID);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBOID);
		glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), screen, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)(sizeof(float) * 2));

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void ScreenCommand::render()
	{
		glBindVertexArray(m_VAOID);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

}

