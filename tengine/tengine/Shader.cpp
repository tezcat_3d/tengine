#include "Shader.h"
#include "xsystem/XString.h"
#include "xsystem/XFileIO.h"
#include <iostream>

namespace tezcat
{
	ShaderFileGroup::ShaderFileGroup() :
		m_Used(false)
	{

	}

	void ShaderFileGroup::parse(std::string &shader, KeyWordFunction &function)
	{
		String::removeByFlag(shader, "\t\r");
		std::string keyWord;
		char temp;
		for (size_t index = 0; index < shader.size(); index++)
		{
			temp = shader[index];
			//如果读到空格,判断空格前的关键字
			if (temp == ' ' || temp == '(' || temp == ';' || temp == '/')
			{
				if (keyWord.empty())
				{
					continue;
				}
				//此时index处为空格
				auto it = function.find(keyWord);
				if (it != function.end())
				{
					function[keyWord](shader, index, this);
				}

				keyWord.clear();
			}
			else if (temp != '\n')
			{
				keyWord += temp;
			}
		}
	}

	void ShaderAssembler::assembleBase(const std::string &file_path)
	{
		auto shader = io::loadBinary2String(file_path);
		auto area = String::split2List(shader, '$');
		area.remove_if([](std::string &str)
		{
			return str.empty();
		});

		std::function<void()> handleVS;
		std::function<void()> handleFS;
		// 		std::function<void()> handleGS;
		// 		std::function<void()> handleTS;


		for (auto &str : area)
		{
			auto pos = str.find_first_of('\n');
			auto token = str.substr(0, pos);
			str.erase(0, pos + 1);
			String::removeByFlag(token, " ");
			if (token == "vs")
			{
				handleVS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subToken = content.substr(0, subPos);
						String::removeByFlag(subToken, " ");
						content.erase(0, subPos + 1);
						if (subToken == "vslayout")
						{
							m_VS.layout.fixed = content;
						}
						else if (subToken == "vsdeclare")
						{
							m_VS.declare.fixed = content;
						}
						else if (subToken == "vsout")
						{
							m_VS.out.fixed = content;
						}
						else if (subToken == "vsuniform")
						{
							m_VS.uniform.fixed = content;
						}
						else if (subToken == "vscustom")
						{
							m_VS.custom.fixed = content;
						}
						else if (subToken == "vsfunction")
						{
							m_VS.function.fixed = content;
						}
						else if (subToken == "vsmain")
						{
							m_VS.main.fixed = content;
						}
					}
				};
			}
			else if (token == "fs")
			{
				handleFS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subToken = content.substr(0, subPos);
						String::removeByFlag(subToken, " ");
						content.erase(0, subPos + 1);
						if (subToken == "fslayout")
						{
							m_FS.layout.fixed = content;
						}
						else if (subToken == "fsdeclare")
						{
							m_FS.declare.fixed = content;
						}
						else if (subToken == "fsin")
						{
							m_FS.in.fixed = content;
						}
						else if (subToken == "fsout")
						{
							m_FS.out.fixed = content;
						}
						else if (subToken == "fsuniform")
						{
							m_FS.uniform.fixed = content;
						}
						else if (subToken == "fscustom")
						{
							m_FS.custom.fixed = content;
						}
						else if (subToken == "fsfunction")
						{
							m_FS.function.fixed = content;
						}
						else if (subToken == "fsmain")
						{
							m_FS.main.fixed = content;
						}
					}
				};
			}
		}

		handleVS();
		handleFS();
	}

	void ShaderAssembler::assembleCustom(const std::string &file_path)
	{
		auto shader = io::loadBinary2String(file_path);
		auto area = String::split2List(shader, '$');
		area.remove_if([](std::string &str)
		{
			return str.empty();
		});

		std::function<void()> handleVS;
		std::function<void()> handleFS;
		// 		std::function<void()> handleGS;
		// 		std::function<void()> handleTS;

		auto addCustom = [](const std::string &controller1, Area &area, std::vector<std::string> &tokens, const std::string &content)
		{
			if (controller1 == "add")
			{
				auto &controller2 = tokens[2];
				String::removeByFlag(controller2, " ");
				if (controller2 == "before")
				{
					area.before = content;
				}
				else if (controller2 == "after")
				{
					area.after = content;
				}
			}
			else if (controller1 == "replace")
			{
				area.fixed = content;
			}
		};

		for (auto &str : area)
		{
			auto pos = str.find_first_of('\n');
			auto token = str.substr(0, pos);
			String::removeByFlag(token, " ");
			str.erase(0, pos + 1);
			if (token == "vs")
			{
				handleVS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subTokenWithController = content.substr(0, subPos);
						auto tokens = String::split2Vector(subTokenWithController, ' ');
						content.erase(0, subPos + 1);

						auto &subToken = tokens[0];
						String::removeByFlag(subToken, " ");
						auto &controller1 = tokens[1];
						String::removeByFlag(controller1, " ");


						if (subToken == "vslayout")
						{
							addCustom(controller1, m_VS.layout, tokens, content);
						}
						else if (subToken == "vsdeclare")
						{
							addCustom(controller1, m_VS.declare, tokens, content);
						}
						else if (subToken == "vsout")
						{
							addCustom(controller1, m_VS.out, tokens, content);
						}
						else if (subToken == "vsuniform")
						{
							addCustom(controller1, m_VS.uniform, tokens, content);
						}
						else if (subToken == "vscustom")
						{
							addCustom(controller1, m_VS.custom, tokens, content);
						}
						else if (subToken == "vsfunction")
						{
							addCustom(controller1, m_VS.function, tokens, content);
						}
						else if (subToken == "vsmain")
						{
							addCustom(controller1, m_VS.main, tokens, content);
						}
					}
				};
			}
			else if (token == "fs")
			{
				handleFS = [&]()
				{
					auto subArea = String::split2List(str, '@');
					subArea.remove_if([](std::string &str)
					{
						return str.empty();
					});

					for (auto &content : subArea)
					{
						auto subPos = content.find_first_of('\n');
						auto subTokenWithController = content.substr(0, subPos);
						auto tokens = String::split2Vector(subTokenWithController, ' ');
						content.erase(0, subPos + 1);

						auto &subToken = tokens[0];
						auto &controller1 = tokens[1];

						if (subToken == "fslayout")
						{
							addCustom(controller1, m_FS.layout, tokens, content);
						}
						else if (subToken == "fsdeclare")
						{
							addCustom(controller1, m_FS.declare, tokens, content);
						}
						else if (subToken == "fsin")
						{
							addCustom(controller1, m_FS.in, tokens, content);
						}
						else if (subToken == "fsout")
						{
							addCustom(controller1, m_FS.out, tokens, content);
						}
						else if (subToken == "fsuniform")
						{
							addCustom(controller1, m_FS.uniform, tokens, content);
						}
						else if (subToken == "fscustom")
						{
							addCustom(controller1, m_FS.custom, tokens, content);
						}
						else if (subToken == "fsfunction")
						{
							addCustom(controller1, m_FS.function, tokens, content);
						}
						else if (subToken == "fsmain")
						{
							addCustom(controller1, m_FS.main, tokens, content);
						}
					}
				};
			}
		}

		handleVS();
		handleFS();
	}

	void ShaderAssembler::display()
	{
		assembleVS();
		assembleFS();
	}

	void ShaderAssembler::assembleVS()
	{
		m_VS.assemble(m_VSResult);
		std::cout << m_VSResult;
	}

	void ShaderAssembler::assembleFS()
	{
		m_FS.assemble(m_FSResult);
		std::cout << m_FSResult;
	}

	void ShaderAssembler::removeSpcae(std::string &str)
	{
		String::removeByFlag(str, " ");
	}
}

