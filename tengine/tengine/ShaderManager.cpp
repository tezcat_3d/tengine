// ShaderEngine.h.cpp : 定义控制台应用程序的入口点。
//

#include "ShaderManager.h"
#include "ProgramManager.h"
#include "RenderPassManager.h"
#include "UniversalMaterial.h"

#include "GLConfig.h"
#include "xsystem/XFileIO.h"
#include "xsystem/XString.h"
#include "xsystem/XLightScriptSystem.h"
#include "xsystem/XLogSystem.h"
#include "EngineConfiguration.h"
#include "ResourceSystem.h"
#include "MaterialManager.h"

// #include "GLRenderEngine.h"
// #include "GLProgram.h"

namespace tezcat
{
#define TOKEN_ENTER '\n'

	ShaderManager::ShaderManager() :
		m_ProgramID(0), m_QueueID(-1)
	{
		m_Shaders.resize(static_cast<GLuint>(ShaderType::Count), ShaderInfo());

		//注册所有基础类型变量
		m_SLDataType.insert(std::make_pair("vec2", ShaderDataType::vec2));
		m_SLDataType.insert(std::make_pair("vec3", ShaderDataType::vec3));
		m_SLDataType.insert(std::make_pair("vec4", ShaderDataType::vec4));
		m_SLDataType.insert(std::make_pair("uvec2", ShaderDataType::uvec2));
		m_SLDataType.insert(std::make_pair("uvec3", ShaderDataType::uvec3));
		m_SLDataType.insert(std::make_pair("uvec4", ShaderDataType::uvec4));
		m_SLDataType.insert(std::make_pair("ivec2", ShaderDataType::ivec2));
		m_SLDataType.insert(std::make_pair("ivec3", ShaderDataType::ivec3));
		m_SLDataType.insert(std::make_pair("ivec4", ShaderDataType::ivec4));
		m_SLDataType.insert(std::make_pair("bvec2", ShaderDataType::bvec2));
		m_SLDataType.insert(std::make_pair("bvec3", ShaderDataType::bvec3));
		m_SLDataType.insert(std::make_pair("bvec4", ShaderDataType::bvec4));
		m_SLDataType.insert(std::make_pair("mat2", ShaderDataType::mat2));
		m_SLDataType.insert(std::make_pair("mat3", ShaderDataType::mat3));
		m_SLDataType.insert(std::make_pair("mat4", ShaderDataType::mat4));
		m_SLDataType.insert(std::make_pair("samplerCube", ShaderDataType::samplerCube));
		m_SLDataType.insert(std::make_pair("sampler2D", ShaderDataType::sampler2D));
		m_SLDataType.insert(std::make_pair("float", ShaderDataType::slfloat));
		m_SLDataType.insert(std::make_pair("int", ShaderDataType::slint));
		m_SLDataType.insert(std::make_pair("uint", ShaderDataType::sluint));
		m_SLDataType.insert(std::make_pair("bool", ShaderDataType::slbool));

		auto jumpChar = [](const char &flag, std::string &shader, size_t &index)
		{
			while (shader[index] == flag)
			{
				index += 1;
			}
			return shader[index];
		};

		//注册所有关键字处理方法
		//处理layout
		m_KeyWordFunction["layout"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderLayout layout;
			std::string name;
			char flag;
			size_t id;
			//			flag = shader[index];

			flag = jumpChar(' ', shader, index);

			if (flag == '(')
			{
				index += 1;
				//获取layout的属性
				//读到=标志位
				while (true)
				{
					flag = shader[index];
					if (flag == '=')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				if (name == "location")
				{
					layout.setIndexType(ShaderLayout::IndexType::location);
				}
				else if (name == "binding")
				{
					layout.setIndexType(ShaderLayout::IndexType::binding);
				}
				name.clear();

				//获取位置
				//跳过=
				index += 1;
				//读到位置标志位)
				while (true)
				{
					flag = shader[index];
					if (flag == ')')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				id = static_cast<size_t>(std::stoi(name));
				name.clear();

				//获取数据io
				index += 2;
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				if (name == "in")
				{
					layout.setDataIO(ShaderLayout::DataIO::in);
				}
				else if (name == "out")
				{
					layout.setDataIO(ShaderLayout::DataIO::out);
				}
				name.clear();

				//获取变量类型
				index += 1;
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}
					name += flag;
					index += 1;
				}
				String::removeByFlag(name, " ");
				layout.setDataType(m_SLDataType[name]);
				name.clear();

				//获取变量名称
				index += 1;
				while (true)
				{
					flag = shader[index];
					if (flag == ';')
					{
						index += 1;
						assert((shader[index] == TOKEN_ENTER) && "layout : Invalid shader format");
						break;
					}
					name += flag;
					index += 1;
				}

				String::removeByFlag(name, " ");
				layout.setName(name);
				name.clear();

				file->addLayout(id, layout);
			}
		};

		//处理uniform
		m_KeyWordFunction["uniform"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderUniform uniform;
			std::string name;
			char flag;
			flag = shader[index];

			while (flag == ' ')
			{
				index += 1;
				flag = shader[index];
			}

			//解析数据格式
			while (true)
			{
				flag = shader[index];
				if (flag == ' ')
				{
					break;
				}
				name += flag;
				index += 1;
			}
			String::removeByFlag(name, " ");
			auto slit = m_SLDataType.find(name);
			if (slit != m_SLDataType.end())
			{
				uniform.setDataType(slit->second);
			}
			else
			{
				uniform.setDataType(ShaderDataType::custom);
			}
			uniform.setDataTypeString(name);
			name.clear();

			//解析数据格式,获得变量名称
			index += 1;
			while (true)
			{
				flag = shader[index];
				if (flag == ' ')
				{
					while (true)
					{
						if (shader[index] == ';')
						{
							index += 1;
							assert((shader[index] == TOKEN_ENTER) && "uniform : Invalid shader format");
							break;
						}
						index += 1;
					}
					
					break;
				}
				else if (flag == '=')
				{
					while (true)
					{
						if (shader[index] == ';')
						{
							index += 1;
							assert((shader[index] == TOKEN_ENTER) && "uniform : Invalid shader format");
							break;
						}
						index += 1;
					}
					break;
				}				
				else if (flag == ';')
				{
					index += 1;
					assert((shader[index] == TOKEN_ENTER) && "uniform : Invalid shader format");
					break;
				}
				name += flag;
				index += 1;
			}
			String::removeByFlag(name, " ");
			file->addUniform(name, uniform);
		};

		//处理struct
		m_KeyWordFunction["struct"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			ShaderStruct slstruct;
			std::string name, struct_name;
			char flag;
			//			flag = shader[index];

			flag = jumpChar(' ', shader, index);

			//取得struct名称
			while (true)
			{
				flag = shader[index];
				if (flag == '{')
				{
					String::removeByFlag(name, " ");
					struct_name = name;
					break;
				}
				name += flag;
				index += 1;
			}
			name.clear();
			//跳过}
			index += 1;

			//取得内部数据
			bool loopStruct = true;
			while (loopStruct)
			{
				while (true)
				{
					flag = shader[index];
					if (flag == ' ')
					{
						break;
					}

					if (flag == '}')
					{
						loopStruct = false;
						break;
					}

					name += flag;
					index += 1;
				}

				if (loopStruct == false)
				{
					//跳过;
					index += 1;
					break;
				}

				//如果是自定义结构体
				String::removeByFlag(name, " ");
				if (m_SLDataType.find(name) == m_SLDataType.end())
				{
					std::string value;
					while (true)
					{
						flag = shader[index];
						if (flag == ';')
						{
							index += 1;
							assert((shader[index] == TOKEN_ENTER) && "struct : Invalid shader format");
							break;
						}
						value += flag;
						index += 1;
					}
					String::removeByFlag(value, " ");
					slstruct.m_Struct[value] = name;
					//跳过;
					index += 1;
				}
				//如果是内建
				else
				{
					std::string value;
					while (true)
					{
						flag = shader[index];
						if (flag == ';')
						{
							index += 1;
							assert((shader[index] == TOKEN_ENTER) && "struct : Invalid shader format");
							break;
						}
						value += flag;
						index += 1;
					}
					String::removeByFlag(value, " ");
					slstruct.m_Value[value] = name;
					//跳过;
					index += 1;
				}
				name.clear();
			}

			file->addStruct(struct_name, slstruct);
		};

		//处理#
		m_KeyWordFunction["#define"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			//发现宏
			while (true)
			{
				if (shader[index] == TOKEN_ENTER)
				{
					break;
				}
				index += 1;
			}
		};

		//发现版本号
		m_KeyWordFunction["#version"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			//发现宏
			while (true)
			{
				if (shader[index] == TOKEN_ENTER)
				{
					break;
				}
				index += 1;
			}
		};

		//处理/
		m_KeyWordFunction["/"] = [=](std::string &shader, size_t &index, ShaderFileGroup *file)
		{
			char nextChar = shader[index + 1];
			//发现 //
			if (nextChar == '/')
			{
				while (true)
				{
					nextChar = shader[index];
					if (nextChar == TOKEN_ENTER)
					{
						break;
					}
					index += 1;
				}
			}
			else if (nextChar == '*')
			{
				while (true)
				{
					nextChar = shader[index];
					if (nextChar == '*')
					{
						index += 1;
						nextChar = shader[index];
						if (nextChar == '/')
						{
							if (shader[index + 1] == TOKEN_ENTER)
							{
								index += 1;
							}
							break;
						}
					}
					index += 1;
				}
			}
		};
	}

	ShaderManager::~ShaderManager()
	{

	}

	Program *ShaderManager::createProgram(const std::string &name, const int &queue_id, const std::string &vs, const std::string &fs, const std::string &gs /*= ""*/, const std::string &tcs /*= ""*/, const std::string &tes /*= ""*/)
	{
		std::unordered_map<std::string, std::string> mapping;
		std::unordered_set<std::string> ignore_list;

		this->begin(name, queue_id);
		if (!vs.empty())
		{
			this->addShader(vs, ShaderType::Vertex);
		}
		if (!fs.empty())
		{
			this->addShader(vs, ShaderType::Fragment);
		}
		if (!gs.empty())
		{
			this->addShader(vs, ShaderType::Geometry);
		}
		if (!tcs.empty())
		{
			this->addShader(vs, ShaderType::Tess_Control);
		}
		if (!tes.empty())
		{
			this->addShader(vs, ShaderType::Tess_Evaluation);
		}

		auto program = this->end(mapping, ignore_list);
//		m_ProgramsInSystem.emplace(std::make_pair(name, program));
		return program;
	}

	Program *ShaderManager::createProgramFromPackage(const std::string &pack_name)
	{
//		return m_ProgramsInEngine[pack_name];
		return nullptr;
	}

	void ShaderManager::begin(const std::string &name, const int &queue_id)
	{
		m_QueueID = queue_id;
		m_ProgramName = name;
		m_ProgramID = glCreateProgram();
	}

	void ShaderManager::addShader(const std::string &file, ShaderType type)
	{
		GLuint id = 0;

		switch (type)
		{
		case ShaderType::Vertex:
			id = glCreateShader(GL_VERTEX_SHADER);
			break;
		case ShaderType::Fragment:
			id = glCreateShader(GL_FRAGMENT_SHADER);
			break;
		case ShaderType::Tess_Control:
			id = glCreateShader(GL_TESS_CONTROL_SHADER);
			break;
		case ShaderType::Tess_Evaluation:
			id = glCreateShader(GL_TESS_EVALUATION_SHADER);
			break;
		case ShaderType::Geometry:
			id = glCreateShader(GL_GEOMETRY_SHADER);
			break;
		}

		m_Shaders[static_cast<GLuint>(type)].id = id;

		std::string shaderHead, shader;
		auto componentShaderFile = io::loadBinary2String(file);

		m_Shaders[static_cast<GLuint>(type)].content = componentShaderFile;

		auto version = GL::getGLVersion();
		shaderHead = "#version " + std::to_string(version) + " core\n";

		shader = shaderHead + componentShaderFile;

		const char *sf = shader.c_str();

		glShaderSource(id, 1, &sf, NULL);
	}

	void ShaderManager::addShaderFromMemory(const char *data, ShaderType type)
	{
		GLuint id = 0;

		switch (type)
		{
		case ShaderType::Vertex:
			id = glCreateShader(GL_VERTEX_SHADER);
			break;
		case ShaderType::Fragment:
			id = glCreateShader(GL_FRAGMENT_SHADER);
			break;
		case ShaderType::Tess_Control:
			id = glCreateShader(GL_TESS_CONTROL_SHADER);
			break;
		case ShaderType::Tess_Evaluation:
			id = glCreateShader(GL_TESS_EVALUATION_SHADER);
			break;
		case ShaderType::Geometry:
			id = glCreateShader(GL_GEOMETRY_SHADER);
			break;
		}

		m_Shaders[static_cast<GLuint>(type)].id = id;

		std::string shaderHead, shader;
		std::string componentShaderFile(data);

//		std::cout << componentShaderFile << std::endl;

		m_Shaders[static_cast<GLuint>(type)].content = componentShaderFile;

		auto version = GL::getGLVersion();
#ifdef IOS
		shaderHead = "#version 300 es\nprecision mediump float";
		shader = shaderHead + componentShaderFile;
#else
		shaderHead = "#version " + std::to_string(version) + " core\n";
		shader = shaderHead + componentShaderFile;
#endif
		

		

		const char *sf = shader.c_str();

		glShaderSource(id, 1, &sf, NULL);
	}

	Program *ShaderManager::end(std::unordered_map<std::string, std::string> &script_uniform_map, std::unordered_set<std::string> &ignore_list)
	{
		//编译shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glCompileShader(shader.id);
			}
		}

		//检测shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				GLint comp;
				glGetShaderiv(shader.id, GL_COMPILE_STATUS, &comp);

				if (comp == GL_FALSE)
				{
					std::cout << "Shader Compilation FAILED" << std::endl;
					GLchar messages[256];
					glGetShaderInfoLog(shader.id, sizeof(messages), 0, &messages[0]);
					XLOG_ERROR(ShaderManager, "Shader Compilation FAILED <" + std::string(messages, strlen(messages) - 1) + ">");
					std::cout << messages;
				}
			}
		}

		//连接shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glAttachShader(m_ProgramID, shader.id);
			}
		}

		//连接检测
		GLint linkStatus;

		//连接program
		glLinkProgram(m_ProgramID);
		glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &linkStatus);
		if (linkStatus == GL_FALSE)
		{
			std::cout << "Shader Linking FAILED" << std::endl;
			GLchar messages[256];
			glGetProgramInfoLog(m_ProgramID, sizeof(messages), 0, messages);
			XLOG_ERROR(ShaderManager, "Shader Linking FAILED <" + std::string(messages, strlen(messages) - 1) + ">");
			std::cout << messages;
		}

		//测试
		GLint validateStatus;
		glValidateProgram(m_ProgramID);
		glGetProgramiv(m_ProgramID, GL_VALIDATE_STATUS, &validateStatus);
		if (validateStatus == GL_FALSE)
		{
			std::cout << "Shader Validation FAILED" << std::endl;
			GLchar messages[256];
			glGetProgramInfoLog(m_ProgramID, sizeof(messages), 0, messages);
			XLOG_WARNING(ShaderManager, "Shader Validation FAILED <" + std::string(messages, strlen(messages) - 1) + ">");
			std::cout << messages;
		}

		//删除shader
		for (auto &shader : m_Shaders)
		{
			if (shader.id != 0)
			{
				glDeleteShader(shader.id);
			}
		}

		//格式化并清除数据
		ShaderFileGroup *file = new ShaderFileGroup();
		for (size_t index = 0; index < m_Shaders.size(); index++)
		{
			auto &shader = m_Shaders[index];
			if (shader.id != 0)
			{
				file->m_Used = true;
				file->parse(shader.content, m_KeyWordFunction);
				shader.id = 0;
				shader.content.clear();
			}
		}

		m_Groups.emplace(std::make_pair(m_ProgramName, file));

		//================================================================================
		//获得所有变量名称
		GLint id = -1;

		//遍历所有结构体获得名称
		std::function<void(const std::string &, const std::string &, std::list<UniversalMaterial *> &)> loadAllStruct2Base =
			[&](const std::string &data_name, const std::string &data_type, std::list<UniversalMaterial *> &ums)
		{
			std::string temp;
			//找到结构体信息
			auto it = file->m_StructInfo.find(data_type);
			//遍历结构体内部内置变量
			for (auto d : it->second.m_Value)
			{
				//主名字+结构体内变量名
				temp = data_name + "." + d.first;
				id = glGetUniformLocation(m_ProgramID, temp.c_str());
				//				file->m_UniformWithID[temp] = id;
				file->m_Uniforms[temp].setDataType(m_SLDataType[data_type]);
				file->m_Uniforms[temp].setDataTypeString(data_type);
				file->m_Uniforms[temp].setID(id);

				//读取忽略列表
				if (ignore_list.find(temp) != ignore_list.end())
				{
					continue;;
				}

				for (auto um : ums)
				{
					um->setProgramID(m_ProgramID);
					switch (m_SLDataType[data_type])
					{
					case ShaderDataType::slfloat:
						um->cacheFloat1(temp, id);
						break;
					case ShaderDataType::slint:
						um->cacheInt1(temp, id);
						break;
					case ShaderDataType::slbool:
						um->cacheBool1(temp, id);
						break;
					case ShaderDataType::sluint:
						um->cacheUInt1(temp, id);
						break;
					case ShaderDataType::vec2:
						um->cacheFloat2(temp, id);
						break;
					case ShaderDataType::vec3:
						um->cacheFloat3(temp, id);
						break;
					case ShaderDataType::vec4:
						um->cacheFloat4(temp, id);
						break;
					case ShaderDataType::uvec2:
						um->cacheUInt2(temp, id);
						break;
					case ShaderDataType::uvec3:
						um->cacheUInt3(temp, id);
						break;
					case ShaderDataType::uvec4:
						um->cacheUInt4(temp, id);
						break;
					case ShaderDataType::ivec2:
						um->cacheInt2(temp, id);
						break;
					case ShaderDataType::ivec3:
						um->cacheInt3(temp, id);
						break;
					case ShaderDataType::ivec4:
						um->cacheInt4(temp, id);
						break;
					case ShaderDataType::bvec2:
						um->cacheBool2(temp, id);
						break;
					case ShaderDataType::bvec3:
						um->cacheBool3(temp, id);
						break;
					case ShaderDataType::bvec4:
						um->cacheBool4(temp, id);
						break;
					case ShaderDataType::sampler2D:
						um->cacheTexture2D(temp, id);
						break;
					case ShaderDataType::samplerCube:
						um->cacheCubeMap(temp, id);
						break;
					}
				}
			}

			//遍历所有自定义结构体
			for (auto d : it->second.m_Struct)
			{
				//主名字+结构体变量名
				temp = data_name + "." + d.first;
				loadAllStruct2Base(temp, d.second, ums);
			}
		};

		//获得所有与此shader相关的材质并填充位置数据
		auto mats = ResourceSystem::getInstance()->getMaterialManager()->getAllUniversalMaterialByShaderName(m_ProgramName);
		//如果没有找到合适的shader,创建一个
		if (mats.empty())
		{
			auto um = new UniversalMaterial();
			um->setShaderName(m_ProgramName);
			um->setName(TG_DEFAULT_MATERIAL + std::string(".") + m_ProgramName);
			mats.emplace_back(um);
			ResourceSystem::getInstance()->getMaterialManager()->addUniversalMaterial(um);
		}

		for (auto &mat : mats)
		{
			mat->setProgramID(m_ProgramID);
		}

		//注册所有变量
		for (auto uniform : file->m_UniformInfo)
		{
			if (uniform.second.getDataType() == ShaderDataType::custom)
			{
				loadAllStruct2Base(uniform.first, uniform.second.getDataTypeString(), mats);
			}
			else
			{
				id = glGetUniformLocation(m_ProgramID, uniform.first.c_str());
				//				file->m_UniformWithID[uniform.first] = id;
				file->m_Uniforms[uniform.first].setDataType(uniform.second.getDataType());
				file->m_Uniforms[uniform.first].setDataTypeString(uniform.second.getDataTypeString());
				file->m_Uniforms[uniform.first].setID(id);

				//读取忽略列表
				if (ignore_list.find(uniform.first) != ignore_list.end())
				{
					continue;;
				}

				for (auto um : mats)
				{
					switch (uniform.second.getDataType())
					{
					case ShaderDataType::slfloat:
						um->cacheFloat1(uniform.first, id);
						break;
					case ShaderDataType::slint:
						um->cacheInt1(uniform.first, id);
						break;
					case ShaderDataType::slbool:
						um->cacheBool1(uniform.first, id);
						break;
					case ShaderDataType::sluint:
						um->cacheUInt1(uniform.first, id);
						break;
					case ShaderDataType::vec2:
						um->cacheFloat2(uniform.first, id);
						break;
					case ShaderDataType::vec3:
						um->cacheFloat3(uniform.first, id);
						break;
					case ShaderDataType::vec4:
						um->cacheFloat4(uniform.first, id);
						break;
					case ShaderDataType::uvec2:
						um->cacheUInt2(uniform.first, id);
						break;
					case ShaderDataType::uvec3:
						um->cacheUInt3(uniform.first, id);
						break;
					case ShaderDataType::uvec4:
						um->cacheUInt4(uniform.first, id);
						break;
					case ShaderDataType::ivec2:
						um->cacheInt2(uniform.first, id);
						break;
					case ShaderDataType::ivec3:
						um->cacheInt3(uniform.first, id);
						break;
					case ShaderDataType::ivec4:
						um->cacheInt4(uniform.first, id);
						break;
					case ShaderDataType::bvec2:
						um->cacheBool2(uniform.first, id);
						break;
					case ShaderDataType::bvec3:
						um->cacheBool3(uniform.first, id);
						break;
					case ShaderDataType::bvec4:
						um->cacheBool4(uniform.first, id);
						break;
					case ShaderDataType::sampler2D:
						um->cacheTexture2D(uniform.first, id);
						break;
					case ShaderDataType::samplerCube:
						um->cacheCubeMap(uniform.first, id);
						break;
					}
				}
			}
		}

		//为每一个program创建一个渲染队列,并添加到渲染引擎中
		Program *program = new Program();
		program->setProgramID(m_ProgramID);
		program->setShaderName(m_ProgramName);

		for (auto &pair : script_uniform_map)
		{
			auto it = file->m_Uniforms.find(pair.first);
			if (it != file->m_Uniforms.end())
			{
				auto location = it->second.getID();
				auto fixedit = Program::String2FixedID.find(pair.second);
				if (fixedit != Program::String2FixedID.end())
				{
					program->setLocation(fixedit->second, location);
				}
				else
				{
					std::cout << pair.first << "-->" << pair.second << " not found" << std::endl;
				}
			}
			else
			{
				std::cout << pair.first << " not found" << std::endl;
			}
		}

		return program;
	}

	void ShaderManager::preLoadShaderFormPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package)
	{
		if (data_package.empty())
		{
			return;
		}
		XLightScriptSystem system;
		XLightScriptFile *filePtr = nullptr;
		for (auto &data : data_package)
		{
			auto ext = String::getNameByLastDot(data.file_name);
			if (ext == TG_VERTEX_FILE)
			{
				this->addShaderFromMemory(data.data, ShaderType::Vertex);
			}
			else if (ext == TG_FRAGMENT_FILE)
			{
				this->addShaderFromMemory(data.data, ShaderType::Fragment);
			}
			else if (ext == TG_GEOMETRY_FILE)
			{
				this->addShaderFromMemory(data.data, ShaderType::Geometry);
			}
			else if (ext == TG_TESS_CONTROL_FILE)
			{
				this->addShaderFromMemory(data.data, ShaderType::Tess_Control);
			}
			else if (ext == TG_TESS_EVALUATION_FILE)
			{
				this->addShaderFromMemory(data.data, ShaderType::Tess_Evaluation);
			}
			else
			{
				auto &file = system.loadOneFileWithMemory(data.data, pack_name);
				filePtr = &file;
				this->begin(pack_name, file["init"]["queue"]().asInt());
			}
		}
		std::unordered_map<std::string, std::string> mappings;
		std::unordered_set<std::string> ignore_list;

		mappings.reserve((*filePtr)["FixedLocation"].getClass().size());
		for (auto &pair : (*filePtr)["FixedLocation"].getClass())
		{
			mappings.emplace(std::make_pair((*pair.second)().asSTDString(), pair.first));
		}

		ignore_list.reserve((*filePtr)["FixedIgnore"].getArray().size());
		for (auto ign : (*filePtr)["FixedIgnore"].getArray())
		{
			ignore_list.emplace((*ign)().asSTDString());
		}
		
		ProgramManager::getInstance()->addProgram(this->end(mappings, ignore_list));
//		m_ProgramsInEngine.emplace(std::make_pair(pack_name, this->end(mappings, ignore_list)));
	}

	bool ShaderManager::confirmShaderIsLoaded(const std::string &pack_name)
	{
//		return m_ProgramsInEngine.find(pack_name) != m_ProgramsInEngine.end();
		return true;
	}

}

