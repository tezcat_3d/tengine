#pragma once

#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <vector>
#include <array>
#include <string>
#include "Shader.h"
#include "Program.h"
#include "xsystem/XFilePackager.h"


namespace tezcat
{
	class ShaderManager
	{
		struct ShaderInfo
		{
			GLuint id;
			std::string content;
		};

	public:
		ShaderManager();
		~ShaderManager();

		Program *createProgram(
			const std::string &name,
			const int &queue_id,
			const std::string &vs,
			const std::string &fs,
			const std::string &gs = "",
			const std::string &tcs = "",
			const std::string &tes = "");

		//************************************
		// 函数名:	createProgramFromPackage
		// 全名:		tezcat::ShaderManager::createProgramFromPackage
		// 权限:		public 
		// 返回值:   tezcat::Program *
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		Program *createProgramFromPackage(const std::string &pack_name);

		//************************************
		// 函数名:	preLoadShaderFormPackage
		// 全名:		tezcat::ShaderManager::preLoadShaderFormPackage
		// 权限:		public 
		// 返回值:   void
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 参数:		XFilePackager::DataPackage & data_package
		// 功能:		
		//************************************
		void preLoadShaderFormPackage(const std::string &pack_name, XFilePackager::DataPackage &data_package);

		//************************************
		// 函数名:	confirmShaderIsLoaded
		// 全名:		tezcat::ShaderManager::confirmShaderIsLoaded
		// 权限:		public 
		// 返回值:   bool
		// 限定符:	
		// 参数:		const std::string & pack_name
		// 功能:		
		//************************************
		bool confirmShaderIsLoaded(const std::string &pack_name);

	private:
		//************************************
		// 函数名:  begin
		// 返回值:  void
		// 参数名:  const std::string & name
		// 参数名:  const int & queue_id
		// 功能:    
		//************************************
		void begin(const std::string &name, const int &queue_id);

		//************************************
		// 函数名:  addShader
		// 返回值:  void
		// 参数名:  const std::string & file
		// 参数名:  ShaderType type
		// 功能:    
		//************************************
		void addShader(const std::string &file, ShaderType type);

		//************************************
		// 函数名:	addShaderWithMemory
		// 全名:		tezcat::ShaderManager::addShaderWithMemory
		// 权限:		public
		// 返回值:   void
		// 限定符:	
		// 参数:		const char * data
		// 参数:		ShaderType type
		// 功能:		
		//************************************
		void addShaderFromMemory(const char *data, ShaderType type);

		//************************************
		// 函数名:  end
		// 返回值:  void
		// 功能:    
		//************************************
		Program *end(std::unordered_map<std::string, std::string> &script_uniform_map, std::unordered_set<std::string> &ignore_list);

	private:
		int m_QueueID;
		GLuint m_ProgramID;
		std::string m_ProgramName;
		std::vector<ShaderInfo> m_Shaders;

		std::unordered_map<std::string, ShaderFileGroup *> m_Groups;
		std::unordered_map<std::string, ShaderDataType> m_SLDataType;

// 		std::unordered_map<std::string, Program *> m_ProgramsInSystem;
// 		std::unordered_map<std::string, Program *> m_ProgramsInEngine;

		ShaderAssembler m_Assembler;
		KeyWordFunction m_KeyWordFunction;
	};
}