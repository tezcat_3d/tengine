#include "SkyboxCommand.h"

namespace tezcat
{

	SkyboxCommand::SkyboxCommand()
	{

	}

	SkyboxCommand::~SkyboxCommand()
	{

	}

	void SkyboxCommand::sendData2GL(VertexData *data)
	{
		float p = 1.0f;
		float points[] = {
			-p,  p, -p,
			-p, -p, -p,
			p, -p, -p,
			p, -p, -p,
			p,  p, -p,
			-p,  p, -p,

			-p, -p,  p,
			-p, -p, -p,
			-p,  p, -p,
			-p,  p, -p,
			-p,  p,  p,
			-p, -p,  p,

			p, -p, -p,
			p, -p,  p,
			p,  p,  p,
			p,  p,  p,
			p,  p, -p,
			p, -p, -p,

			-p, -p,  p,
			-p,  p,  p,
			p,  p,  p,
			p,  p,  p,
			p, -p,  p,
			-p, -p,  p,

			-p,  p, -p,
			p,  p, -p,
			p,  p,  p,
			p,  p,  p,
			-p,  p,  p,
			-p,  p, -p,

			-p, -p, -p,
			-p, -p,  p,
			p, -p, -p,
			p, -p, -p,
			-p, -p,  p,
			p, -p,  p
		};


		glGenVertexArrays(1, &m_VAOID);
		glBindVertexArray(m_VAOID);

		m_VertexSize = sizeof(points) / sizeof(float) / 3;
		glGenBuffers(1, &m_VBOID);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBOID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);

		glEnableVertexAttribArray(GLAttributeID(AttributeID::Position));
		glVertexAttribPointer(GLAttributeID(AttributeID::Position), 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	}

	void SkyboxCommand::render()
	{
		glBindVertexArray(m_VAOID);
		glDrawArrays(GL_TRIANGLES, 0, m_VertexSize);
	}

}

