#include "SkyboxMaterial.h"
#include "ProgramManager.h"
#include "GLTexture.h"
#include "ResourceSystem.h"

namespace tezcat
{
	SkyboxMaterial::SkyboxMaterial():
		m_CubeMap(nullptr), m_Location(0)
	{

	}

	void SkyboxMaterial::uploadUniform(Program *program)
	{
		m_CubeMap->bind(0, m_Location);
		ProgramManager::getInstance()->setValue1iv(program->getFixedLocation(Program::FixedLocation::Texture_Skybox), &m_Location);
	}

	void SkyboxMaterial::unbindUniform()
	{
		m_CubeMap->unbind();
	}

	void SkyboxMaterial::setImage(
		const std::string &positive_x, const std::string &negative_x,
		const std::string &positive_y, const std::string &negative_y,
		const std::string &positive_z, const std::string &negative_z)
	{
		m_CubeMap = ResourceSystem::getInstance()->createCubeMap(
			m_Name,
			positive_x, negative_x,
			positive_y, negative_y,
			positive_z, negative_z);
	}
}

