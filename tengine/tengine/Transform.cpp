#include "Transform.h"

namespace tezcat
{
	Transform *Transform::SwapTransform = Transform::create();


	Transform::Transform() :
		m_Name("error_transform"),
		m_UpdataWho(UpdataWho::UpdataAll),
		m_WorldMatrix(1.0f),
		m_Scale(1.0f),
		m_Position(0.0f),
		m_Rotation(0.0f)
	{}

	Transform::~Transform()
	{

	}


	void Transform::logic()
	{
// 		switch (m_UpdataWho)
// 		{
// 		case UpdataWho::Position:
// 			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
// 
// 			m_LocationMatrix = m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_LocationMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::Rotation:
// 			m_RotationMatrix = glm::toMat4(
// 				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
// 				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
// 				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));
// 
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::Scale:
// 			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
// 
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::Position | UpdataWho::Rotation:
// 			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
// 			m_RotationMatrix = glm::toMat4(
// 				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
// 				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
// 				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));
// 
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::Position | UpdataWho::Scale:
// 			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
// 			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::Rotation | UpdataWho::Scale:
// 			m_RotationMatrix = glm::toMat4(
// 				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
// 				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
// 				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));
// 
// 			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		case UpdataWho::UpdataAll:
// 			m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);
// 
// 			m_RotationMatrix = glm::toMat4(
// 				glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
// 				glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
// 				glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));
// 
// 			m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
// 			m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 			m_UpdataWho = UpdataWho::NoUpdata;
// 			break;
// 
// 		default:
// 			if (m_Parent->m_UpdataWho == UpdataWho::Parent)
// 			{
// 				m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
// 				m_UpdataWho = UpdataWho::NoUpdata;
// 			}
// 			break;
// 		}

		m_TranslationMatrix = glm::translate(IdentifyMat4, m_Position);

		m_RotationMatrix = glm::toMat4(
			glm::rotate(IdentifyQuat, m_Rotation.x, AxisX) *
			glm::rotate(IdentifyQuat, m_Rotation.y, AxisY) *
			glm::rotate(IdentifyQuat, m_Rotation.z, AxisZ));

		m_ScaleMatrix = glm::scale(IdentifyMat4, m_Scale);
		m_WorldMatrix = m_Parent->getWorldMatrix() * m_TranslationMatrix * m_RotationMatrix * m_ScaleMatrix;
	}

	void Transform::setMatrix(
		const float &col1_row1, const float &col1_row2, const float &col1_row3,
		const float &col2_row1, const float &col2_row2, const float &col2_row3,
		const float &col3_row1, const float &col3_row2, const float &col3_row3,
		const float &col4_row1, const float &col4_row2, const float &col4_row3)
	{
		m_Position.x = col4_row1;
		m_Position.y = col4_row2;
		m_Position.z = col4_row3;

		float length1 = glm::length(glm::vec3(col1_row1, col1_row2, col1_row3));
		float length2 = glm::length(glm::vec3(col2_row1, col2_row2, col2_row3));
		float length3 = glm::length(glm::vec3(col3_row1, col3_row2, col3_row3));

		m_Scale.x = length1;
		m_Scale.y = length2;
		m_Scale.z = length3;

		glm::mat3 rotateMat3;
		rotateMat3[0][0] = col1_row1 / length1; rotateMat3[0][1] = col1_row2 / length1; rotateMat3[0][2] = col1_row3 / length1;
		rotateMat3[1][0] = col2_row1 / length2; rotateMat3[1][1] = col2_row2 / length2; rotateMat3[1][2] = col2_row3 / length2;
		rotateMat3[2][0] = col3_row1 / length3; rotateMat3[2][1] = col3_row2 / length3; rotateMat3[2][2] = col3_row3 / length3;

		auto quat = glm::toQuat(glm::transpose(rotateMat3));
		m_Rotation = -glm::eulerAngles(quat);
	}
}

