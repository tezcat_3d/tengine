#include "UniversalMaterial.h"
#include "ResourceSystem.h"

namespace tezcat
{
	UniversalMaterial *UniversalMaterial::DefaultMaterial = new UniversalMaterial(0, 0, "DefaultMaterial", "No_Shader");

	UniversalMaterial::UniversalMaterial():
		m_TextrueCount(0)
	{

	}

	UniversalMaterial::UniversalMaterial(const unsigned int &pass_id, const int &queue_id, const std::string &name, const std::string &shader_name)
	{
		m_PassID = pass_id;
		m_QueueID = queue_id;
		m_ShaderName = shader_name;
		m_Name = name;
	}

	UniversalMaterial::~UniversalMaterial()
	{

	}

	void UniversalMaterial::uploadUniform(Program *program)
	{
		for (auto &uniform : m_Proerty)
		{
			uniform.second->upload();
		}
	}

	UniversalMaterial * UniversalMaterial::copy()
	{
		UniversalMaterial *um = new UniversalMaterial();
		um->m_Name = this->m_Name;
		um->m_PassID = this->m_PassID;
		um->m_Proerty = this->m_Proerty;
		um->m_QueueID = this->m_QueueID;
		um->m_ShaderName = this->m_ShaderName;
		um->m_TextrueCount = this->m_TextrueCount;
		return um;
	}

	//============================================================================================
	//
	//	Value 1
	//
	void UniversalMaterial::setInt1Fast(const std::string &name, const int &value)
	{
		static_cast<UniformValue1 *>(m_Proerty[name])->setInt(value);
	}

	void UniversalMaterial::setUInt1Fast(const std::string &name, const unsigned int &value)
	{
		static_cast<UniformValue1 *>(m_Proerty[name])->setUInt(value);
	}

	void UniversalMaterial::setFloat1Fast(const std::string &name, const float &value)
	{
		static_cast<UniformValue1 *>(m_Proerty[name])->setFloat(value);
	}

	void UniversalMaterial::setBool1Fast(const std::string &name, const bool &value)
	{
		static_cast<UniformValue1 *>(m_Proerty[name])->setBool(value);
	}

	void UniversalMaterial::setInt1(const std::string &name, const int &value)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue1 *>(m_Proerty[name])->setInt(value);
		}
	}

	void UniversalMaterial::setUInt1(const std::string &name, const unsigned int &value)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue1 *>(m_Proerty[name])->setUInt(value);
		}
	}

	void UniversalMaterial::setFloat1(const std::string &name, const float &value)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue1 *>(m_Proerty[name])->setFloat(value);
		}
	}

	void UniversalMaterial::setBool1(const std::string &name, const bool &value)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue1 *>(m_Proerty[name])->setBool(value);
		}
	}

	void UniversalMaterial::cacheBool1(const std::string &name, const bool &value, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheBool1(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(UniformBaseValue::ValueType::BOOL);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheInt1(const std::string &name, const int &value, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheInt1(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(UniformBaseValue::ValueType::INT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheFloat1(const std::string &name, const float &value, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheFloat1(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(UniformBaseValue::ValueType::FLOAT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheUInt1(const std::string &name, const unsigned int &value, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(value);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheUInt1(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue1(UniformBaseValue::ValueType::UINT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	//============================================================================================
	//
	//	Value 2
	//
	void UniversalMaterial::setInt2Fast(const std::string &name, const int &value1, const int &value2)
	{
		static_cast<UniformValue2 *>(m_Proerty[name])->setInt(value1, value2);
	}

	void UniversalMaterial::setUInt2Fast(const std::string &name, const unsigned int &value1, const unsigned int &value2)
	{
		static_cast<UniformValue2 *>(m_Proerty[name])->setUInt(value1, value2);
	}

	void UniversalMaterial::setFloat2Fast(const std::string &name, const float &value1, const float &value2)
	{
		static_cast<UniformValue2 *>(m_Proerty[name])->setFloat(value1, value2);
	}

	void UniversalMaterial::setBool2Fast(const std::string &name, const bool &value1, const bool &value2)
	{
		static_cast<UniformValue2 *>(m_Proerty[name])->setBool(value1, value2);
	}

	void UniversalMaterial::setInt2(const std::string &name, const int &value1, const int &value2)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue2 *>(m_Proerty[name])->setInt(value1, value2);
		}
	}

	void UniversalMaterial::setUInt2(const std::string &name, const unsigned int &value1, const unsigned int &value2)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue2 *>(m_Proerty[name])->setUInt(value1, value2);
		}
	}

	void UniversalMaterial::setFloat2(const std::string &name, const float &value1, const float &value2)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue2 *>(m_Proerty[name])->setFloat(value1, value2);
		}
	}

	void UniversalMaterial::setBool2(const std::string &name, const bool &value1, const bool &value2)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue2 *>(m_Proerty[name])->setBool(value1, value2);
		}
	}

	void UniversalMaterial::cacheInt2(const std::string &name, const int &value1, const int &value2, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheInt2(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(UniformBaseValue::ValueType::INT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheUInt2(const std::string &name, const unsigned int &value1, const unsigned int &value2, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheUInt2(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(UniformBaseValue::ValueType::UINT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheFloat2(const std::string &name, const float &value1, const float &value2, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheFloat2(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(UniformBaseValue::ValueType::FLOAT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheBool2(const std::string &name, const bool &value1, const bool &value2, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(value1, value2);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheBool2(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue2(UniformBaseValue::ValueType::BOOL);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	//============================================================================================
	//
	//	Value 3
	//
	void UniversalMaterial::setInt3Fast(const std::string &name, const int &value1, const int &value2, const int &value3)
	{
		static_cast<UniformValue3 *>(m_Proerty[name])->setInt(value1, value2, value3);
	}

	void UniversalMaterial::setUInt3Fast(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3)
	{
		static_cast<UniformValue3 *>(m_Proerty[name])->setUInt(value1, value2, value3);
	}

	void UniversalMaterial::setFloat3Fast(const std::string &name, const float &value1, const float &value2, const float &value3)
	{
		static_cast<UniformValue3 *>(m_Proerty[name])->setFloat(value1, value2, value3);
	}

	void UniversalMaterial::setBool3Fast(const std::string &name, const bool &value1, const bool &value2, const bool &value3)
	{
		static_cast<UniformValue3 *>(m_Proerty[name])->setBool(value1, value2, value3);
	}

	void UniversalMaterial::setInt3(const std::string &name, const int &value1, const int &value2, const int &value3)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue3 *>(m_Proerty[name])->setInt(value1, value2, value3);
		}
	}

	void UniversalMaterial::setUInt3(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue3 *>(m_Proerty[name])->setUInt(value1, value2, value3);
		}
	}

	void UniversalMaterial::setFloat3(const std::string &name, const float &value1, const float &value2, const float &value3)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue3 *>(m_Proerty[name])->setFloat(value1, value2, value3);
		}
	}

	void UniversalMaterial::setBool3(const std::string &name, const bool &value1, const bool &value2, const bool &value3)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue3 *>(m_Proerty[name])->setBool(value1, value2, value3);
		}
	}

	void UniversalMaterial::cacheBool3(const std::string &name, const bool &value1, const bool &value2, const bool &value3, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheBool3(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(UniformBaseValue::ValueType::BOOL);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheInt3(const std::string &name, const int &value1, const int &value2, const int &value3, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheInt3(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(UniformBaseValue::ValueType::INT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheFloat3(const std::string &name, const float &value1, const float &value2, const float &value3, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheFloat3(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(UniformBaseValue::ValueType::FLOAT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheUInt3(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(value1, value2, value3);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheUInt3(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue3(UniformBaseValue::ValueType::UINT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	//============================================================================================
	//
	//	Value 3
	//
	void UniversalMaterial::setInt4Fast(const std::string &name, const int &value1, const int &value2, const int &value3, const int &value4)
	{
		static_cast<UniformValue4 *>(m_Proerty[name])->setInt(value1, value2, value3, value4);
	}

	void UniversalMaterial::setUInt4Fast(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3, const unsigned int &value4)
	{
		static_cast<UniformValue4 *>(m_Proerty[name])->setUInt(value1, value2, value3, value4);
	}

	void UniversalMaterial::setFloat4Fast(const std::string &name, const float &value1, const float &value2, const float &value3, const float &value4)
	{
		static_cast<UniformValue4 *>(m_Proerty[name])->setFloat(value1, value2, value3, value4);
	}

	void UniversalMaterial::setBool4Fast(const std::string &name, const bool &value1, const bool &value2, const bool &value3, const bool &value4)
	{
		static_cast<UniformValue4 *>(m_Proerty[name])->setBool(value1, value2, value3, value4);
	}

	void UniversalMaterial::setInt4(const std::string &name, const int &value1, const int &value2, const int &value3, const int &value4)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue4 *>(m_Proerty[name])->setInt(value1, value2, value3, value4);
		}
	}

	void UniversalMaterial::setUInt4(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3, const unsigned int &value4)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue4 *>(m_Proerty[name])->setUInt(value1, value2, value3, value4);
		}
	}

	void UniversalMaterial::setFloat4(const std::string &name, const float &value1, const float &value2, const float &value3, const float &value4)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue4 *>(m_Proerty[name])->setFloat(value1, value2, value3, value4);
		}
	}

	void UniversalMaterial::setBool4(const std::string &name, const bool &value1, const bool &value2, const bool &value3, const bool &value4)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValue4 *>(m_Proerty[name])->setBool(value1, value2, value3, value4);
		}
	}

	void UniversalMaterial::cacheInt4(const std::string &name, const int &value1, const int &value2, const int &value3, const int &value4, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheInt4(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(UniformBaseValue::ValueType::INT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheFloat4(const std::string &name, const float &value1, const float &value2, const float &value3, const float &value4, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheFloat4(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(UniformBaseValue::ValueType::FLOAT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheUInt4(const std::string &name, const unsigned int &value1, const unsigned int &value2, const unsigned int &value3, const unsigned int &value4, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheUInt4(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(UniformBaseValue::ValueType::UINT);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheBool4(const std::string &name, const bool &value1, const bool &value2, const bool &value3, const bool &value4, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(value1, value2, value3, value4);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheBool4(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValue4(UniformBaseValue::ValueType::BOOL);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	//============================================================================================
	//
	//	Texture
	//
	void UniversalMaterial::genTexture2DBuffer(const std::string &name, const unsigned int &width, const unsigned int &height)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(UniformBaseValue::ValueCount::C2);
			um->setLocation(m_TextrueCount++);
			um->genTexture2DBuffer(width, height);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValueWithTexture *>(m_Proerty[name])->genTexture2DBuffer(width, height);
		}
	}

	void UniversalMaterial::updataTexture2D(const std::string &name,
		const unsigned int &width, const unsigned int &height,
		const unsigned int &x_offset, const unsigned int &y_offset,
		unsigned char *data)
	{
		static_cast<UniformValueWithTexture *>(m_Proerty[name])->updataTexture2D(width, height, x_offset, y_offset, data);
	}

	void UniversalMaterial::setTexture2D(const std::string &name, const std::string &pack_dot_name)
	{
		static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(pack_dot_name), m_TextrueCount++);
	}

	void UniversalMaterial::setTextureCube(const std::string &name, const std::string &positive_x, const std::string &negative_x, const std::string &positive_y, const std::string &negative_y, const std::string &positive_z, const std::string &negative_z)
	{
		
	}

	void UniversalMaterial::setTextureCube(const std::string &name, const std::string &pack_name)
	{
		static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(pack_name), m_TextrueCount++);
	}

	void UniversalMaterial::cacheTexture2D(const std::string &name, const std::string &pack_name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(pack_name), m_TextrueCount++, UniformBaseValue::ValueCount::C2);
			um->setTextureName(name);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheTexture2D(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(UniformBaseValue::ValueCount::C2);
			um->setTextureName(name);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::setTexture2DName(const std::string &name, const std::string &tex_name)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(UniformBaseValue::ValueCount::C2);
			um->setTextureName(tex_name);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTextureName(tex_name);
		}
	}

	void UniversalMaterial::cacheTexture2D(const std::string &name, const std::string pack_name)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(pack_name), m_TextrueCount++, UniformBaseValue::ValueCount::C3);
			um->setTextureName(name);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTexture(ResourceSystem::getInstance()->createTexture2DFromPackage(pack_name), m_TextrueCount++);
		}
	}


	void UniversalMaterial::setCubeMapName(const std::string &name, const std::string &tex_name)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(UniformBaseValue::ValueCount::C3);
			um->setTextureName(tex_name);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTextureName(tex_name);
		}
	}

	void UniversalMaterial::cacheCubeMap(const std::string &name, const std::string &pack_name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(pack_name), m_TextrueCount++, UniformBaseValue::ValueCount::C3);
			um->setTextureName(name);
			um->setLocation(location);
			m_Proerty[name] = um;
		}
	}

	void UniversalMaterial::cacheCubeMap(const std::string &name, const int &location)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(UniformBaseValue::ValueCount::C3);
			um->setLocation(location);
			um->setTextureName(name);
			m_Proerty[name] = um;
		}
		else
		{
			m_Proerty[name]->setLocation(location);
		}
	}

	void UniversalMaterial::cacheCubeMap(const std::string &name, const std::string pack_name)
	{
		if (m_Proerty.find(name) == m_Proerty.end())
		{
			auto um = new UniformValueWithTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(pack_name), m_TextrueCount++, UniformBaseValue::ValueCount::C3);
			um->setTextureName(name);
			m_Proerty[name] = um;
		}
		else
		{
			static_cast<UniformValueWithTexture *>(m_Proerty[name])->setTexture(ResourceSystem::getInstance()->createCubeMapFromPackage(pack_name), m_TextrueCount++);
		}
	}
}

