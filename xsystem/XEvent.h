#pragma once

#include <vector>
#include <unordered_map>
#include <functional>
#include <iostream>
#include "XIDCreator.h"
#include "XSingleton.h"

namespace tezcat
{
	class XBaseEvent
	{
	public:
		XBaseEvent() = default;
		virtual ~XBaseEvent() = default;

		template<typename Event>
		Event *get() { return static_cast<Event *>(this); }

		virtual void clear() = 0;
		virtual void doSomething() {}
	};

	template<typename Name>
	class XEvent : public XBaseEvent
	{
	public:
		XEvent() {}
		virtual ~XEvent() {}

	private:
		friend class XEventSystem;
		static size_t getKind()
		{
			static size_t kind = XIDCreator<XBaseEvent>::give();
			return kind;
		}
	};

	class XEventSystem : public XSingleton<XEventSystem>
	{
		struct Wapper
		{
			std::unordered_map<void *, std::function<void(XBaseEvent *)>> eventListener;
		};

	public:
		XEventSystem() { }
		~XEventSystem()
		{
			for (auto e : m_CacheEvent)
			{
				delete e;
			}
		}

		template<typename Event>
		void registerEvent()
		{
			auto kind = Event::getKind();
			if (m_CacheEvent.capacity() <= kind)
			{
				m_CacheEvent.resize(kind + 1);
				m_Listeners.resize(kind + 1);
			}
			m_CacheEvent[kind] = new Event;
			m_Listeners[kind] = new Wapper();
		}

		template<typename Event>
		Event *createEvent()
		{
			return static_cast<Event *>(m_CacheEvent[Event::getKind()]);
		}

		template<typename Event>
		void broadcast(Event *event)
		{
			auto maps = m_Listeners[Event::getKind()];
			for (auto &data : maps->eventListener)
			{
				data.second(event);
			}

			event->clear();
		}

		template<typename Event>
		void subscribe(void *ptr, const std::function<void(XBaseEvent *)> &function)
		{
			m_Listeners[Event::getKind()]->eventListener.emplace(std::make_pair(ptr, function));
		}

		template<typename Event>
		void unSubscribe(void *ptr)
		{
			m_Listeners[Event::getKind()]->eventListener.erase(ptr);
		}

		void debug()
		{
			for (auto &index : m_Listeners)
			{
				std::cout << index->eventListener.size() << std::endl;
			}
		}

	private:

	private:
//		std::vector<std::unordered_map<void *, std::function<void(XBaseEvent *)>>> m_Listeners;
		std::vector<Wapper *> m_Listeners;
		std::vector<XBaseEvent *> m_CacheEvent;
	};
}