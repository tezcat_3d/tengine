#pragma once

#include "XMemory.h"
#include <list>
#include <type_traits>

namespace tezcat
{
	template<typename Component>
	class XFactory : public BaseIndexPool
	{
	public:
		static XFactory *getInstance()
		{
			static XFactory instance;
			return &instance;
		}

		virtual ~XFactory() {}

		virtual void recycle(const std::size_t &id) override
		{
			Component *ptr = static_cast<Component*>(this->get(id));
			ptr->~Component();
			memset(ptr, 0x00, sizeof(Component));
			m_FreeList.push_back(id);
		}

		template<typename... Args>
		Component *create(Args && ... args)
		{
			Component *com = nullptr;
			if (m_FreeList.empty())
			{
				this->expand(m_NowComponentID);
				com = ::new(this->get(m_NowComponentID)) Component(std::forward<Args>(args)...);
				com->setComponentID(m_NowComponentID++);
			}
			else
			{
				auto cid = m_FreeList.front();
				com = ::new(this->get(cid)) Component(std::forward<Args>(args)...);
				com->setComponentID(cid);
				m_FreeList.pop_front();
			}
			return com;
		}

		Component *create()
		{
			Component *com = nullptr;
			if (m_FreeList.empty())
			{
				this->expand(m_NowComponentID);
				com = ::new(this->get(m_NowComponentID)) Component();
				com->setComponentID(m_NowComponentID++);
			}
			else
			{
				auto cid = m_FreeList.front();
				com = ::new(this->get(cid)) Component();
				com->setComponentID(cid);
				m_FreeList.pop_front();
			}
			return com;
		}

	private:
		XFactory() : BaseIndexPool(sizeof(Component)), m_NowComponentID(0) {}

	private:
		std::size_t m_NowComponentID;
		std::list<std::size_t> m_FreeList;
	};


	//
	template<typename Entity, typename Memory = std::list<Entity *>>
	class XFactory2
	{
	public:
// 		static XFactory2 *getInstance()
// 		{
// 			static XFactory2 instance;
// 			return &instance;
// 		}

		Entity *create()
		{
			if (m_Memory.empty())
			{
				auto *temp = new std::aligned_storage<sizeof(Entity), std::alignment_of<Entity>::value>::type();
				return new(temp) Entity();
			}
			else
			{
				auto *entity = m_Memory.back();
				m_Memory.pop_back();
				return entity;
			}
			
		}

		template<typename... Args>
		Entity *create(Args &&...args)
		{
			if (m_Memory.empty())
			{
				auto *temp = new std::aligned_storage<sizeof(Entity), std::alignment_of<Entity>::value>::type(std::forward<Args>(args)...);
				return temp;
//				return ::new(temp) Entity(std::forward<Args>(args)...);
			}
			else
			{
				auto *entity = m_Memory.back();
				m_Memory.pop_back();
				return ::new(entity) Entity(std::forward<Args>(args)...);
			}
		}

		void recycle(Entity *entity)
		{
			m_Memory.push_back(entity);
		}

		void giveMemoryBack2System()
		{
			for (auto *entity : m_Memory)
			{
				delete entity;
			}
		}

	private:
		Memory m_Memory;
	};

	template<typename Entity, typename Memory = std::list<Entity *>>
	class XFactory3
	{
	public:
		Entity *create()
		{
			if (m_Memory.empty())
			{
				return new Entity();
			}
			else
			{
				auto *entity = m_Memory.back();
				m_Memory.pop_back();
				return entity;
			}

		}

		template<typename... Args>
		Entity *create(Args &&...args)
		{
			if (m_Memory.empty())
			{
				return new Entity(std::forward<Args>(args)...);
			}
			else
			{
				auto *entity = m_Memory.back();
				m_Memory.pop_back();
				return new(entity)Entity(std::forward<Args>(args)...);
			}
		}

		void recycle(Entity *entity)
		{
			m_Memory.push_back(entity);
		}

		void giveMemoryBack2System()
		{
			for (auto *entity : m_Memory)
			{
				delete entity;
			}
		}

	private:
		Memory m_Memory;
	};
}