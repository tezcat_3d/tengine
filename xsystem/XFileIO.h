﻿#pragma once

#include <fstream>
#include <iostream>
#include <string>


namespace tezcat
{
	namespace io
	{
		const std::string ErrorString = "$error_string";

		//************************************
		// Method:    loadBinary
		// FullName:  tezcat::IO::loadBinary
		// Access:    public static 
		// Returns:   char *
		// Qualifier:
		// Parameter: const std::string & file
		// Note:      别忘记释放内存
		//************************************
		static inline char *loadBinary(const std::string &file, unsigned int &length)
		{
			std::ifstream in(file, std::ifstream::binary);
			if (in.fail())
			{
				in.close();
				return nullptr;
			}
			else
			{
				in.seekg(0, in.end);
				length = (size_t)in.tellg();
				in.seekg(0, in.beg);
				char *dataBuffer = new char[length] {};
				in.read(dataBuffer, length);
				in.close();
				return dataBuffer;
			}
		}

		//***************************************
		// Method:    loadBinary2String
		// FullName:  tezcat::io::loadBinary
		// Access:    public static 
		// Returns:   std::string
		// Qualifier:
		// Parameter: const std::string & file
		// Note:	  
		//***************************************
		static inline std::string loadBinary2String(const std::string &file)
		{
			unsigned int length;
			auto data = loadBinary(file, length);
			if (data != nullptr)
			{
				std::string temp(data, length);
				delete[] data;
				return temp;
			}
			else
			{
				return ErrorString;
			}
		}

		static inline bool createFile(const std::string &file_name, const std::string &content)
		{
			std::ofstream out(file_name, std::ofstream::binary);

			if (out.fail())
			{
				out.close();
				return false;
			}
			else
			{
				out << content;
				out.flush();
				out.close();
				return true;
			}
		}

		static inline bool createFile(const std::string &file_name, const char *data, const std::streamsize &data_size)
		{
			std::ofstream out(file_name, std::ofstream::binary);

			if (out.fail())
			{
				out.close();
				return false;
			}
			else
			{
				out.write(data, data_size);
				out.flush();
				out.close();
				return true;
			}
		}

		static inline std::string getPathByIndexAndFlag(std::string path, const size_t &index, const std::string &flag)
		{
			std::string result;
			for (size_t i = 0; i < index; i++)
			{
				auto pos = path.find_first_of(flag);
				auto temp = path.substr(0, pos + 1);
				path = path.substr(pos, path.size() - 1);
				result += temp;
			}

			return result;
		}

		static inline std::string getPathByLastFlag(std::string path, const std::string &flag)
		{
			return path.substr(0, path.find_last_of(flag) + 1);
		}
	}
}