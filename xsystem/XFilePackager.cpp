// Packager.cpp : 定义控制台应用程序的入口点。
//

#include "XFilePackager.h"
#include "XString.h"
#include <iostream>
#include <random>


namespace tezcat
{
	XFilePackager::XFilePackager():
		m_FilesLength(0)
	{
	}

	XFilePackager::~XFilePackager()
	{
	}

	void XFilePackager::pack(const std::string &file_name)
	{
		std::ofstream outp(file_name, std::ofstream::binary);
		unsigned int pack_head = 5;
		unsigned int offset = pack_head;
		char *package_data = new char[pack_head + m_FilesLength]{};
		package_data[0] = 'X';
		package_data[1] = 'T';
		package_data[2] = 'Z';
		package_data[3] = 'K';
		package_data[4] = 'T';
		for (auto &file : m_Files)
		{
			memmove(package_data + offset, file.data, static_cast<size_t>(file.length));
			offset += static_cast<size_t>(file.length);
			delete file.data;
		}

		this->code(package_data, offset);
		outp.write(package_data, offset);

		outp.flush();
		outp.close();
		delete package_data;
		m_Files.clear();
		m_FilesLength = 0;
	}

	bool XFilePackager::unpack2File(const std::string &path)
	{
		unsigned int file_length;
		auto data = this->openFile(path, file_length);
		if (data == nullptr)
		{
			return false;
		}

		this->code(data, file_length);

		unsigned int index = 0;
		index += 5;

		while (index < file_length)
		{
			char head = data[index];
			index += 1;
			unsigned int length = 0;

			for (auto i = 0; i < 4; i++)
			{
				((char *)(&length))[i] = data[index];
				index += 1;
			}

			unsigned int fileNameLength = data[index];
			index += 1;

			std::string fileName(&data[index], fileNameLength);
//			fileName.assign(&data[index], fileNameLength);
			index += fileNameLength;

			std::ofstream outp(fileName, std::ofstream::binary);
			outp.write(&data[index], length);

			outp.flush();
			outp.close();
			index += length;
		}

		delete data;

		return true;
	}

	bool XFilePackager::unpack2Memory(const std::string &path, std::vector<UnPackInfo> &datas)
	{
		unsigned int file_length;
		auto data = this->openFile(path, file_length);
		if (data == nullptr)
		{
			return false;
		}

		this->code(data, file_length);

		unsigned int index = 0;
		index += 5;

		while (index < file_length)
		{
			char head = data[index];
			index += 1;
			unsigned int length = 0;

			for (auto i = 0; i < 4; i++)
			{
				((char *)(&length))[i] = data[index];
				index += 1;
			}

			unsigned int fileNameLength = data[index];
			index += 1;

			std::string fileName(&data[index], fileNameLength);
//			fileName.assign(&data[index], fileNameLength);
			index += fileNameLength;

			UnPackInfo info;
			info.file_name = fileName;
			info.length = length;
			info.data = new char[length + 1] {};
			memmove(info.data, &data[index], length);
			info.data[length] = '\0';
			datas.emplace_back(info);
			index += length;
		}

		delete data;

		return true;
	}

	void XFilePackager::loadFile(const std::string &file_path)
	{
		auto tempPath = file_path;
		String::replaceAllByFlag(tempPath, "\\", "/");

		unsigned int file_length = 0;
		auto data = this->openFile(tempPath, file_length);
		if (data == nullptr)
		{
			return;
		}

		Info info;
		size_t fileNameLength;
		std::string fileName;
		auto pos = tempPath.find_last_of('/');
		if (pos == std::string::npos)
		{
			fileNameLength = tempPath.length();
			fileName = tempPath;
		}
		else
		{
			fileName = tempPath.substr(pos + 1, tempPath.length() - pos + 1);
			fileNameLength = fileName.length();
		}

		std::random_device rd;
		std::mt19937 gen;
		gen.seed(rd());
		std::uniform_int_distribution<int> dis(-126, 126);

		auto size = sizeof(char) + sizeof(unsigned int) + sizeof(char) + fileNameLength + file_length;
		info.data = new char[size] {};
		info.data[0] = static_cast<char>(dis(gen));

		info.data[1] = ((char *)(&file_length))[0];
		info.data[2] = ((char *)(&file_length))[1];
		info.data[3] = ((char *)(&file_length))[2];
		info.data[4] = ((char *)(&file_length))[3];

		info.data[5] = (char)fileNameLength;
		for (size_t i = 0; i < fileNameLength; i++)
		{
			info.data[6 + i] = fileName[i];
		}

		auto offset = sizeof(char) + sizeof(unsigned int) + sizeof(char) + fileNameLength;
		memmove(&(info.data[offset]), data, file_length);
		delete data;
		info.length = size;
		m_Files.emplace_back(info);
		m_FilesLength += size;
	}

	void XFilePackager::compare(const std::string &file1, const std::string &file2)
	{
		unsigned int file_length1, file_length2;

		auto data1 = this->openFile(file1, file_length1);
		auto data2 = this->openFile(file2, file_length2);

		if (file_length1 != file_length2)
		{
			std::cout << "length error" << std::endl;
		}

		for (size_t i = 0; i < file_length1; i++)
		{
			if (data1[i] != data2[i])
			{
				std::cout << "data error" << std::endl;
			}
		}
	}

	char *XFilePackager::openFile(const std::string &file_name, unsigned int &file_length)
	{
		std::ifstream in(file_name, std::ifstream::binary);
		if (in.fail())
		{
			std::cout << "can not found file " << file_name << std::endl;
			in.close();
			return nullptr;
		}
		else
		{
			in.seekg(0, in.end);
			file_length = (size_t)in.tellg();
			in.seekg(0, in.beg);

			char *dataBuffer = new char[file_length] {};

			in.read(dataBuffer, file_length);
			in.close();
			return dataBuffer;
		}
	}

	void XFilePackager::code(char *data, unsigned int &data_length)
	{
		std::string key("TezCat-X");
		size_t keyLength = key.length();
		for (size_t i = 0; i < data_length; i++)
		{
			data[i] ^= key.at(i % keyLength);
		}
	}
}