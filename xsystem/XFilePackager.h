#pragma once

#include <list>
#include <vector>
#include <string>
#include <fstream>

namespace tezcat
{
	class XFilePackager
	{
		struct Info
		{
			char *data;
			std::streamsize length;
		};

	public:
		struct UnPackInfo
		{
			std::string file_name;
			unsigned int length;
			char *data;
		};

		typedef std::vector<UnPackInfo> DataPackage;

	public:
		XFilePackager();
		~XFilePackager();

		//************************************
		// Method:    pack
		// FullName:  tezcat::XFilePackager::pack
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & file_name
		//************************************
		void pack(const std::string &file_name);

		//************************************
		// Method:    unpack
		// FullName:  tezcat::XFilePackager::unpack
		// Access:    public 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & path
		//************************************
		bool unpack2File(const std::string &path);

		//************************************
		// Method:    unpack2Memory
		// FullName:  tezcat::XFilePackager::unpack2Memory
		// Access:    public 
		// Returns:   bool
		// Qualifier:
		// Parameter: const std::string & path
		// Parameter: std::vector<UnPackInfo> & datas
		//************************************
		bool unpack2Memory(const std::string &path, std::vector<UnPackInfo> &datas);

		//************************************
		// Method:    loadFile
		// FullName:  tezcat::XFilePackager::loadFile
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & file_path
		//************************************
		void loadFile(const std::string &file_path);

		//************************************
		// Method:    compare
		// FullName:  tezcat::XFilePackager::compare
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: const std::string & file1
		// Parameter: const std::string & file2
		//************************************
		void compare(const std::string &file1, const std::string &file2);

	private:

		//************************************
		// Method:    openFile
		// FullName:  tezcat::XFilePackager::openFile
		// Access:    private 
		// Returns:   char *
		// Qualifier:
		// Parameter: const std::string & file_name
		// Parameter: unsigned int & file_length
		//************************************
		char *openFile(const std::string &file_name, unsigned int &file_length);

		//************************************
		// Method:    code
		// FullName:  tezcat::XFilePackager::code
		// Access:    private 
		// Returns:   void
		// Qualifier:
		// Parameter: char * data
		// Parameter: unsigned int & data_length
		//************************************
		void code(char *data, unsigned int &data_length);

	private:
		std::list<Info> m_Files;
		unsigned int m_FilesLength;
	};
}

