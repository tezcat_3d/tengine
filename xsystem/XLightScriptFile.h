#pragma once

#include <string>
#include <unordered_map>
#include <list>
#include "XLightScriptValue.h"

namespace tezcat
{
	class XLightScriptFile
	{
	public:
		XLightScriptFile();
		~XLightScriptFile();

		void setName(const std::string &name)
		{
			m_Name = name;
		}

		void addValue(const std::string &name, XLightScriptValue *value)
		{
			m_ValueBlock.insert(std::make_pair(name, value));
		}

		XLightScriptValue &operator[](const std::string &name)
		{
			return *m_ValueBlock[name];
		}

		std::unordered_map<std::string, XLightScriptValue *> &getValueBlock() { return m_ValueBlock; }

	private:
		std::string m_Name;
		std::unordered_map<std::string, XLightScriptValue *> m_ValueBlock;
	};
}