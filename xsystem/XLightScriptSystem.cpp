#include "XLightScriptSystem.h"
#include "XLightScriptFile.h"


#include "XFileIO.h"
#include "XString.h"
#include <cassert>

namespace tezcat
{
	XLightScriptSystem::~XLightScriptSystem()
	{
		for (auto file : m_Files)
		{
			delete file.second;
		}
	}

	XLightScriptSystem::XLightScriptSystem() :
		m_CurrentValue(nullptr),
		m_NowType(XLightScriptValue::Type::Var)
	{
		m_ParseFunction.resize(static_cast<size_t>(XLightScriptValue::Type::Count), nullptr);

		m_ParseFunction[static_cast<size_t>(XLightScriptValue::Type::Class)] = [=](char &flag, std::string &name, std::string &data, size_t &index)
		{
			//发现等于有3种情况
			//class={
			//array=[
			//value=
			char subFlag;
			if (flag == '=')
			{
				subFlag = data[index + 1];
				//发现类,改变当前状态,此时temp为类名
				if (subFlag == '{')
				{
					//跳过=和{
					index += 1;
					std::cout << "push value<class>: " + name << std::endl;
					auto old = m_CurrentValue;
					this->pushValue(XLightScriptValue::Type::Class);
					if (m_ValueStack.size() == 1)
					{
						m_CurrentFile->addValue(name, m_CurrentValue);
					}
					else
					{
						old->addValue(name, m_CurrentValue);
					}
				}
				//发现数组,改变当前状态,此时temp为数组名
				else if (subFlag == '[')
				{
					//跳过=和[
					index += 1;
					std::cout << "push value<array>: " + name << std::endl;
					auto old = m_CurrentValue;
					this->pushValue(XLightScriptValue::Type::Array);
					if (m_ValueStack.size() == 1)
					{
						m_CurrentFile->addValue(name, m_CurrentValue);
					}
					else
					{
						old->addValue(name, m_CurrentValue);
					}
				}
				//发现值,根据当前栈中类型的情况,决定解析方案.
				else
				{
					//发现string,此时name为string名
					if (subFlag == '"')
					{
						//跳过=
						index += 1;
						char sc;
						std::string stemp;
						for (; index < data.size(); index++)
						{
							sc = data[index];
							if (sc == ';')
							{
								std::cout << "add value<string>: " + name + " : " + stemp << std::endl;
								if (m_CurrentValue == nullptr)
								{
									m_CurrentFile->addValue(name, new XLightScriptValue(XLightScriptValue::Type::Var, stemp));
								}
								else
								{
									m_CurrentValue->addValue(name, new XLightScriptValue(XLightScriptValue::Type::Var, stemp));
								}
								break;
							}
							else if (sc != '"')
							{
								stemp += sc;
							}
						}
					}
					//发现实数,此时name为实数名
					else
					{
						//跳过=
						index += 1;
						char rc;
						std::string rtemp;
						for (; index < data.size(); index++)
						{
							rc = data[index];
							if (rc == ';')
							{
								std::cout << "add value<real>: " + name + " : " + rtemp << std::endl;
								if (m_CurrentValue == nullptr)
								{
									m_CurrentFile->addValue(name, new XLightScriptValue(XLightScriptValue::Type::Var, rtemp));
								}
								else
								{
									m_CurrentValue->addValue(name, new XLightScriptValue(XLightScriptValue::Type::Var, rtemp));
								}
								break;
							}
							else
							{
								rtemp += rc;
							}
						}
					}
				}

				name.clear();
			}
			//类结束
			else if (flag == '}')
			{
				std::cout << "pop value<class>" << std::endl;
				//跳过;
				index += 1;
				this->popValue();
			}
			//数组结束
			else if (flag == ']')
			{
				std::cout << "pop value<array>" << std::endl;
				//跳过;
				index += 1;
				this->popValue();
			}
			//加入
			else if (flag != ';')
			{
				if (flag != '=')
				{
					name += flag;
				}
			}
		};


		m_ParseFunction[static_cast<size_t>(XLightScriptValue::Type::Array)] = [=](char &flag, std::string& name, std::string &data, size_t &index)
		{
			//数组个体没有名称,也没有=控制符,仅寻找2级控制符
			//name变量永远为空
			//发现类,改变当前状态
			if (flag == '{')
			{
				std::cout << "push value<class>: " + name << std::endl;
				auto old = m_CurrentValue;
				this->pushValue(XLightScriptValue::Type::Class);
				if (m_ValueStack.size() == 1)
				{
					m_CurrentFile->addValue(name, m_CurrentValue);
				}
				else
				{
					old->addValue(name, m_CurrentValue);
				}
				name.clear();
			}
			//发现数组,改变当前状态
			else if (flag == '[')
			{
				std::cout << "push value<array>: " + name << std::endl;
				auto old = m_CurrentValue;
				this->pushValue(XLightScriptValue::Type::Array);
				if (m_ValueStack.size() == 1)
				{
					m_CurrentFile->addValue(name, m_CurrentValue);
				}
				else
				{
					old->addValue(name, m_CurrentValue);
				}
				name.clear();
			}
			//类结束
			else if (flag == '}')
			{
				std::cout << "pop value<class>" << std::endl;
				//跳过;
				index += 1;
				this->popValue();
			}
			//数组结束
			else if (flag == ']')
			{
				std::cout << "pop value<array>" << std::endl;
				//跳过;
				index += 1;
				this->popValue();
			}
			//加入数组不能跳过
			else if (flag != ';' && flag != '=')
			{
				name += flag;
			}
			//不能跳过
			else if (flag == ';' && flag != '=')
			{
				if (name.find('"') != std::string::npos)
				{
					String::removeByFlag(name, "\"");
					std::cout << "add value<string>: " + name << std::endl;
					m_CurrentValue->addValue("", new XLightScriptValue(XLightScriptValue::Type::Var, name));
				}
				else
				{
					std::cout << "add value<real>: " + name << std::endl;
					m_CurrentValue->addValue("", new XLightScriptValue(XLightScriptValue::Type::Var, name));
				}

				name.clear();
			}
		};

		m_ParseFunction[static_cast<size_t>(XLightScriptValue::Type::Var)] = m_ParseFunction[static_cast<size_t>(XLightScriptValue::Type::Class)];
	}

	bool XLightScriptSystem::loadScript(std::string file, const std::string &limit)
	{
		String::replaceAllByFlag(file, "\\", "/");
		auto findPath = file.find_last_of('/');
		auto path = file.substr(0, findPath + 1);

		if (!limit.empty())
		{
			auto exPos = file.find_first_of('.');
			auto ex = file.substr(exPos);
			assert(ex == limit);
		}

		std::string name;
		if (findPath != std::string::npos)
		{
			name = file.substr(findPath + 1, file.find_first_of('.') - findPath - 1);
		}
		else
		{
			name = file.substr(0, file.find_first_of('.'));
		}

		auto data = io::loadBinary2String(file);
		if (data == io::ErrorString)
		{
			return false;
		}

		//移除多余字符
		String::removeByFlag(data, "\n\r\t ");
		m_CurrentFile = new XLightScriptFile();
		m_CurrentFile->setName(name);
		m_Files[name] = m_CurrentFile;


		this->progressing(m_CurrentFile, data);

		return true;
	}

	XLightScriptFile & XLightScriptSystem::loadOneFileWithMemory(const char *data, const std::string &name)
	{
		m_CurrentFile = new XLightScriptFile();
		m_CurrentFile->setName(name);
		m_Files[name] = m_CurrentFile;
		std::string fileData(data);
		String::removeByFlag(fileData, "\n\r\t ");

		auto temp = m_CurrentFile;
		this->progressing(m_CurrentFile, fileData);
		return *temp;
	}

	void XLightScriptSystem::loadScriptByPath(const std::string file_name, const std::string &path)
	{
		std::string name = file_name.substr(0, file_name.find_first_of('.'));

		auto data = io::loadBinary2String(path + file_name);

		//移除多余字符
		String::removeByFlag(data, "\n\r\t ");
		m_CurrentFile = new XLightScriptFile();
		m_CurrentFile->setName(name);
		m_Files[name] = m_CurrentFile;


		this->progressing(m_CurrentFile, data);
	}

	void XLightScriptSystem::loadList(std::string list_file_path)
	{
		String::replaceAllByFlag(list_file_path, "\\", "/");
		auto findPath = list_file_path.find_last_of('/');
		auto exPos = list_file_path.find_first_of('.');
		auto path = list_file_path.substr(0, findPath + 1);
		auto ex = list_file_path.substr(exPos);

		assert(ex == ".ttlist");

		std::string name;
		if (findPath != std::string::npos)
		{
			name = list_file_path.substr(findPath + 1, exPos - findPath - 1);
		}
		else
		{
			name = list_file_path.substr(0, exPos);
		}
		auto data = io::loadBinary2String(list_file_path);
		String::removeByFlag(data, "\n\r\t ");
		auto tempFile = new XLightScriptFile();
		m_CurrentFile = tempFile;
		this->progressing(m_CurrentFile, data);

		for (auto file : tempFile->getValueBlock())
		{
			for (auto value : file.second->getArray())
			{
				this->loadScriptByPath((*value)().asSTDString(), path);
			}
		}

		delete tempFile;
	}

	void XLightScriptSystem::progressing(XLightScriptFile *file, std::string &data)
	{
		std::string temp;
		for (size_t index = 0; index < data.size(); index++)
		{
			m_ParseFunction[static_cast<size_t>(m_NowType)](data[index], temp, data, index);
		}

		std::cout << "Stack size :" << m_ValueStack.size() << std::endl;

		m_NowType = XLightScriptValue::Type::Var;
		m_CurrentValue = nullptr;
		m_CurrentFile = nullptr;
	}


	void XLightScriptSystem::pushValue(XLightScriptValue::Type type)
	{
		m_CurrentValue = new XLightScriptValue(type);
		m_ValueStack.push_back(m_CurrentValue);
		m_NowType = type;
	}

	void XLightScriptSystem::popValue()
	{
		if (m_ValueStack.empty())
		{
			return;
		}

		m_ValueStack.pop_back();
		if (m_ValueStack.empty())
		{
			m_CurrentValue = nullptr;
			m_NowType = XLightScriptValue::Type::Var;
		}
		else
		{
			m_CurrentValue = m_ValueStack.back();
			m_NowType = m_CurrentValue->getType();
		}
	}

}