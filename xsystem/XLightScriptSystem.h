#pragma once


#include <string>
#include <unordered_map>
#include <functional>
#include <vector>
#include "XLightScriptValue.h"
#include "XLightScriptFile.h"

namespace tezcat
{
	class XLightScriptSystem
	{
	public:
		XLightScriptSystem();
		~XLightScriptSystem();

		//************************************
		// 函数名:  loadScript
		// 返回值:  void
		// 参数名:  std::string file_path
		// 参数名:  const std::string & limit
		// 功能:    
		//************************************
		bool loadScript(std::string file_path, const std::string &limit = "");

		XLightScriptFile &loadOneFileWithMemory(const char *data, const std::string &name);

		//************************************
		// 函数名:  loadList
		// 返回值:  void
		// 参数名:  std::string list_file_path
		// 功能:    
		//************************************
		void loadList(std::string list_file_path);

		//************************************
		// 函数名:  getFile
		// 返回值:  LightScriptFile &
		// 参数名:  const std::string & name
		// 功能:    
		//************************************
		XLightScriptFile &getFile(const std::string &name) { return *m_Files[name]; }

		//************************************
		// 函数名:  getAllFile
		// 返回值:  std::unordered_map<std::string, LightScriptFile *> &
		// 功能:    
		//************************************
		std::unordered_map<std::string, XLightScriptFile *> &getAllFile() { return m_Files; }

	private:
		void progressing(XLightScriptFile *file, std::string &data);
		void pushValue(XLightScriptValue::Type type);
		void popValue();

		void loadScriptByPath(const std::string file_name, const std::string &path);


	private:
		std::unordered_map<std::string, XLightScriptFile *> m_Files;

		std::vector<XLightScriptValue *> m_ValueStack;

		XLightScriptValue *m_CurrentValue;
		XLightScriptFile *m_CurrentFile;

		std::vector<std::function<void(char&, std::string&, std::string &, size_t &)>> m_ParseFunction;

		XLightScriptValue::Type m_NowType;
	};
}

