#include "XLightScriptValue.h"

namespace tezcat
{

	XLightScriptValue::XLightScriptValue(Type type):
		m_Type(type)
	{

	}

	XLightScriptValue::XLightScriptValue(Type type, const std::string &value):
		m_Type(type), m_Var(value)
	{

	}

	XLightScriptValue::~XLightScriptValue()
	{
		for (auto data : m_Array)
		{
			delete data;
		}

		for (auto &data : m_Class)
		{
			delete data.second;
		}
	}

	void XLightScriptValue::addValue(const std::string &name, XLightScriptValue *value)
	{
		switch (m_Type)
		{
		case Type::Class:
			m_Class.insert(std::make_pair(name, value));
			break;
		case Type::Array:
			m_Array.push_back(value);
			break;
		default:
			break;
		}
	}

}

