#pragma once

#include <unordered_map>
#include "XValue.h"

namespace tezcat
{
	class XLightScriptValue
	{
	public:
		enum class Type
		{
			Class,
			Array,
			Var,
			Count
		};

	public:
		XLightScriptValue(Type type);
		XLightScriptValue(Type type, const std::string &value);
		~XLightScriptValue();

		void addValue(const std::string &name, XLightScriptValue *value);

		XValue &operator()()
		{
			return m_Var;
		}

		XLightScriptValue &operator[](const std::string &name)
		{
			return *m_Class.at(name);
		}

		XLightScriptValue &operator[](const size_t &index)
		{
			return *m_Array.at(index);
		}

		XLightScriptValue::Type &getType() { return m_Type; }

		std::unordered_map<std::string, XLightScriptValue *> &getClass() { return m_Class; }

		std::vector<XLightScriptValue *> &getArray() { return m_Array; }

		bool hasValueInClass(const std::string &name)
		{
			return m_Class.find(name) != m_Class.end();
		}

	private:
		XValue m_Var;
		std::unordered_map<std::string, XLightScriptValue *> m_Class;
		std::vector<XLightScriptValue *> m_Array;

		Type m_Type;

	};
}

