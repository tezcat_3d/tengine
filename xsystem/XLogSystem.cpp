// XLogSystem.cpp : 定义控制台应用程序的入口点。
//

#include "XLogSystem.h"
#include <string>
#include <iostream>

namespace tezcat
{
	XLogSystem::XLogSystem()
	{
		m_Out.open("TEngineLog.txt");
	}

	XLogSystem::~XLogSystem()
	{
		m_Out.close();
	}

	void XLogSystem::info(const std::string &belone, const std::string &info)
	{
		m_Out << "Info\t" << belone << "\t:[" << info << "]" << std::endl;
		m_Out.flush();
	}

	void XLogSystem::error(const std::string &belone, const std::string &error_info)
	{
		m_Out << "Error\t" << belone << "\t:[" << error_info << "]" << std::endl;
		m_Out.flush();
	}

	void XLogSystem::warning(const std::string &belone, const std::string &warning_info)
	{
		m_Out << "Warning\t" << belone << "\t:[" << warning_info << "]" << std::endl;
		m_Out.flush();
	}
}