#pragma once

#include <fstream>
#include "XSingleton.h"


namespace tezcat
{
	class XLogSystem : public XSingleton<XLogSystem>
	{
	public:
		XLogSystem();
		~XLogSystem();

		void info(const std::string &belone, const std::string &info);
		void error(const std::string &belone, const std::string &error_info);
		void warning(const std::string &belone, const std::string &warning_info);

	private:
		std::ofstream m_Out;
	};

#define XLOG_INFO(belone, content) tezcat::XLogSystem::getInstance()->info(#belone, content);
#define XLOG_ERROR(belone, content) tezcat::XLogSystem::getInstance()->error(#belone, content);
#define XLOG_WARNING(belone, content) tezcat::XLogSystem::getInstance()->warning(#belone, content);
}

