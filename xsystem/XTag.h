#pragma once

#include <string>
#include <unordered_set>
#include "XComponent.h"

namespace tezcat
{
	template <typename TagType>
	class XTag : public XComponent<XTag<TagType>>
	{
	public:
		XTag() {};
		~XTag() {};

		void addStringTag(const std::string &tag)
		{
			m_StringTags.insert(tag);
		}

		void removeStringTag(const std::string &tag)
		{
			m_StringTags.erase(tag);
		}

		bool hasStringTag(const std::string &tag)
		{
			return m_StringTags.find(tag) != m_StringTags.end();
		}

		void setIntTag(const TagType &tag)
		{
			m_IntTag = tag;
		}

		TagType &getIntTag() { return m_IntTag; }

		bool equal2IntTag(const TagType &tag)
		{
			return m_IntTag == tag;
		}

	private:
		std::unordered_set<std::string> m_StringTags;
		TagType m_IntTag;
	};
}