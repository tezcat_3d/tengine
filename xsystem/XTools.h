#pragma once

#include <sstream>
#include <random>
#include <functional>

namespace tezcat
{
	namespace tools
	{
		struct random
		{
			static int Int(int min, int max)
			{
				gen.seed(rd());
				std::uniform_int_distribution<int> dis(min, max);
				return dis(gen);
			}

			static size_t Size_t(size_t min, size_t max)
			{
				gen.seed(rd());
				std::uniform_int_distribution<size_t> dis(min, max);
				return dis(gen);
			}

			static float Float(float min, float max)
			{
				gen.seed(rd());
				std::uniform_real_distribution<float> dis(min, max);
				return dis(gen);
			}

		private:
			static std::random_device rd;
			static std::mt19937 gen;
		};

// 		inline static int random(int min, int max)
// 		{
// 			std::random_device rd;
// 			std::mt19937 gen(rd());
// 			std::uniform_int_distribution<int> dis(min, max);
// 			return dis(gen);
// 		}
// 
// 		inline static size_t random_uint(size_t min, size_t max)
// 		{
// 			static std::random_device rd;
// 			static std::mt19937 gen(rd());
// 			static std::uniform_int_distribution<size_t> dis(min, max);
// 			return dis(gen);
// 		}
// 
// 		inline static float random(float min, float max)
// 		{
// 			std::random_device rd;
// 			std::mt19937 gen(rd());
// 			std::uniform_real_distribution<float> dis(min, max);
// 			return dis(gen);
// 		}

		//�ж�����
		inline static bool isODD(const int &value)
		{
			return (value & 1) != 0;
		}

		//�ж�ż��
		inline static bool isEven(const int &value)
		{
			return (value & 1) == 0;
		}
	}
}

