#include "XValue.h"
#include "XString.h"

#include <sstream>
#include <stdexcept>
#include <stdio.h>



namespace tezcat
{
	tezcat::XValue XValue::NullValue;

	XValue::XValue():
		m_Type(ValueType::Null), m_IfMove(false)
	{

	}

	XValue::XValue(const bool &v):
		m_Type(ValueType::Bool), m_IfMove(false)
	{
		m_Values.b = v;
	}

	XValue::XValue(const int &v) :
		m_Type(ValueType::Int), m_IfMove(false)
	{
		m_Values.i = v;
	}

	XValue::XValue(const unsigned int &v) :
		m_Type(ValueType::UInt), m_IfMove(false)
	{
		m_Values.ui = v;
	}

	XValue::XValue(const float &v) :
		m_Type(ValueType::Float), m_IfMove(false)
	{
		m_Values.f = v;
	}

	XValue::XValue(const double &v) :
		m_Type(ValueType::Double), m_IfMove(false)
	{
		m_Values.d = v;
	}

	XValue::XValue(const std::string &v) :
		m_Type(ValueType::String), m_IfMove(false)
	{
		m_Values.str = new std::string();
		*m_Values.str = v;
	}

	XValue::XValue(void *ptr) :
		m_Type(ValueType::ObjPtr), m_IfMove(false)
	{
		m_Values.ptr = ptr;
	}

	XValue::XValue(XValue &&other) :
		m_IfMove(false)
	{
		*this = std::move(other);
		std::cout << "tezcat::TValue Use Move Builder!!!" << std::endl;
	}

	XValue::XValue(const XValue &other)
	{
//		memset(&m_Values, 0, sizeof(m_Values));
		*this = other;
	}



	XValue::~XValue()
	{
//		std::cout << "tezcat::TValue Delete!!" << std::endl;
		this->clear();
	}

	bool XValue::asBool()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return m_Values.b;
			break;
		case ValueType::Int:
			return m_Values.i > 0;
			break;
		case ValueType::UInt:
			return m_Values.ui != 0;
			break;
		case ValueType::Float:
			return m_Values.f >= std::numeric_limits<float>::epsilon();
			break;
		case ValueType::Double:
			return m_Values.d >= std::numeric_limits<double>::epsilon();
			break;
		case ValueType::String:
			return !(!m_Values.str->compare("0") || !m_Values.str->compare("false"));
			break;
		default:
			throw std::logic_error("tezcat::TValue can not convert to bool");
			break;
		}

		return false;
	}

	int XValue::asInt()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return m_Values.b ? 1 : 0;
			break;
		case ValueType::Int:
			return m_Values.i;
			break;
		case ValueType::UInt:
			return static_cast<int>(m_Values.ui);
			break;
		case ValueType::Float:
			return static_cast<int>(m_Values.f);
			break;
		case ValueType::Double:
			return static_cast<int>(m_Values.d);
			break;
		case ValueType::String:
			return String::toNumber<int>(*m_Values.str);
			break;
		default:
			throw std::logic_error("tezcat::TValue can not convert to int");
			break;
		}

		return std::numeric_limits<int>::min();
	}

	unsigned int XValue::asUInt()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return m_Values.b ? 1 : 0;
			break;
		case ValueType::Int:
			return static_cast<unsigned int>(m_Values.i);
			break;
		case ValueType::UInt:
			return m_Values.ui;
			break;
		case ValueType::Float:
			return static_cast<unsigned int>(m_Values.f);
			break;
		case ValueType::Double:
			return static_cast<unsigned int>(m_Values.d);
			break;
		case ValueType::String:
			return String::toNumber<unsigned int>(*m_Values.str);
			break;
		default:
			throw std::logic_error("tezcat::TValue can not convert to unsigned int");
			break;
		}

		return std::numeric_limits<unsigned int>::min();
	}

	float XValue::asFloat()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return m_Values.b ? 1.0f : 0.0f;
			break;
		case ValueType::Int:
			return static_cast<float>(m_Values.i);
			break;
		case ValueType::UInt:
			return static_cast<float>(m_Values.ui);
			break;
		case ValueType::Float:
			return m_Values.f;
			break;
		case ValueType::Double:
			return static_cast<float>(m_Values.d);
			break;
		case ValueType::String:
//			return std::stof(*m_Values.str);
			return String::toNumber<float>(*m_Values.str);
			break;
		default:
			throw std::logic_error("tezcat::TValue can not convert to float");
			break;
		}

		return std::numeric_limits<float>::min();
	}

	double XValue::asDouble()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return m_Values.b ? 1.0 : 0.0;
			break;
		case ValueType::Int:
			return static_cast<double>(m_Values.i);
			break;
		case ValueType::UInt:
			return static_cast<double>(m_Values.ui);
			break;
		case ValueType::Float:
			return static_cast<double>(m_Values.f);
			break;
		case ValueType::Double:
			return m_Values.d;
			break;
		case ValueType::String:
			return String::toNumber<double>(*m_Values.str);
			break;
		default:
			throw std::logic_error("tezcat::TValue can not convert to double");
			break;
		}

		return std::numeric_limits<double>::min();
	}

	std::string XValue::asSTDString()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			return String::toString(m_Values.b);
			break;
		case ValueType::Int:
			return String::toString(m_Values.i);
			break;
		case ValueType::UInt:
			return String::toString(m_Values.ui);
			break;
		case ValueType::Float:
			return String::toString(m_Values.f);
			break;
		case ValueType::Double:
			return String::toString(m_Values.d);
			break;
		case ValueType::String:
			return *m_Values.str;
			break;
		case ValueType::ObjPtr:
			return std::string("$xvalue.error");
			break;
		default:
			return std::string("$xvalue.error");
			break;
		}
	}

	bool XValue::toBool()
	{
		*this = this->asBool();
		return m_Values.b;
	}

	int XValue::toInt()
	{
		*this = this->asInt();
		return m_Values.i;
	}

	unsigned int XValue::toUInt()
	{
		*this = this->asUInt();
		return m_Values.ui;
	}

	float XValue::toFloat()
	{
		*this = this->asFloat();
		return m_Values.f;
	}

	double XValue::toDouble()
	{
		*this = this->asDouble();
		return m_Values.d;
	}

	std::string XValue::toSTDString()
	{
		*this = this->asSTDString();
		return *m_Values.str;
	}

	void XValue::clear()
	{
		switch (m_Type)
		{
		case ValueType::Bool:
			m_Values.b = false;
			break;
		case ValueType::Int:
			m_Values.i = 0;
			break;
		case ValueType::UInt:
			m_Values.ui = 0;
			break;
		case ValueType::Float:
			m_Values.f = 0.0f;
			break;
		case ValueType::Double:
			m_Values.d = 0.0;
			break;
		case ValueType::String:
			if (m_Values.str && m_IfMove == false)
			{
				delete m_Values.str;
				m_Values.str = nullptr;
			}
			break;
		case ValueType::ObjPtr:
			if (m_Values.ptr)
			{
				m_Values.ptr = nullptr;
			}
			break;
		}

		m_Type = ValueType::Null;
		memset(&m_Values, 0, sizeof(m_Values));
	}

	void XValue::reset(ValueType type)
	{
		if (m_Type == type)
		{
			return;
		}

		this->clear();

		switch (type)
		{
		case ValueType::Null:break;
		case ValueType::Bool:break;
		case ValueType::Int:break;
		case ValueType::UInt:break;
		case ValueType::Float:break;
		case ValueType::Double:break;
		case ValueType::String:
			m_Values.str = new std::string();
			break;
		case ValueType::ObjPtr:
			m_Values.ptr = nullptr;
			break;
		}

		m_Type = type;
	}

	XValue & XValue::operator = (const bool &b)
	{
		this->reset(ValueType::Bool);
		m_Values.b = b;
		return *this;
	}

	XValue & XValue::operator = (const int &i)
	{
		this->reset(ValueType::Int);
		m_Values.i = i;
		return *this;
	}

	XValue & XValue::operator=(const unsigned int &ui)
	{
		this->reset(ValueType::UInt);
		m_Values.ui = ui;
		return *this;
	}

	XValue & XValue::operator = (const float &f)
	{
		this->reset(ValueType::Float);
		m_Values.f = f;
		return *this;
	}

	XValue & XValue::operator = (const double &d)
	{
		this->reset(ValueType::Double);
		m_Values.d = d;
		return *this;
	}

	XValue & XValue::operator = (const std::string &s)
	{
		this->reset(ValueType::String);
		*m_Values.str = s;
		return *this;
	}

	XValue & XValue::operator = (const char *cc)
	{
		this->reset(ValueType::String);
		*m_Values.str = cc;
		return *this;
	}

	XValue & XValue::operator = (void *ptr)
	{
		this->reset(ValueType::ObjPtr);
		m_Values.ptr = ptr;
		return *this;
	}

	XValue & XValue::operator = (XValue &&other)
	{
		std::cout << "tezcat::TValue Use Move Operator!!!" << std::endl;
		if (this != &other)
		{
			other.m_IfMove = true;
			this->clear();
			m_Type = other.m_Type;
			m_Values = other.m_Values;
		}

		return *this;
	}

	XValue & XValue::operator = (const XValue &other)
	{
		this->reset(other.m_Type);
		switch (m_Type)
		{
		case ValueType::Bool:
			m_Values.b = other.m_Values.b;
			break;
		case ValueType::Int:
			m_Values.i = other.m_Values.i;
			break;
		case ValueType::Float:
			m_Values.f = other.m_Values.f;
			break;
		case ValueType::Double:
			m_Values.d = other.m_Values.d;
			break;
		case ValueType::String:
			*m_Values.str = *other.m_Values.str;
			break;
		case ValueType::ObjPtr:
			m_Values.ptr = other.m_Values.ptr;
			break;
		}

		return *this;
	}

	bool XValue::operator == (const XValue &other) const
	{
		return this == &other;
	}

	bool XValue::operator != (const XValue &other) const
	{
		return this != &other;
	}


}