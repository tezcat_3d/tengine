﻿#pragma once


#ifdef WIN32
#include <unordered_map>
#else
#include <map>
#include <unordered_map>
#endif // WIN32

#include <string>
#include <iostream>
#include <vector>

namespace tezcat
{
	class XValue
	{
		enum class ValueType : unsigned char
		{
			Null,
			Bool,
			Int,
			UInt,
			Float,
			Double,
			String,
			ObjPtr,
		};
	public:
		XValue();
		explicit XValue(const bool &v);
		explicit XValue(const int &v);
		explicit XValue(const unsigned int &v);
		explicit XValue(const float &v);
		explicit XValue(const double &v);
		explicit XValue(const std::string &v);
		explicit XValue(XValue &&other);
		explicit XValue(void *ptr);
		XValue(const XValue &other);
		~XValue();

		//*************************
		//
		// 转化数据格式,并返回结果
		// 不改变内部数据格式,只返回一个副本
		// 如需改变内部格式,请使用toXX函数
		//
		//*************************
	public:
		bool asBool();
		int asInt();
		unsigned int asUInt();
		float asFloat();
		double asDouble();
		std::string asSTDString();
		template<class T>
		typename std::enable_if<std::is_class<T>::value, T>::type *asObject()
		{
			switch (m_Type)
			{
			case ValueType::Null:
				throw std::logic_error("tezcat::TValue Null can not convert to object");
				break;
			case ValueType::Bool:
				throw std::logic_error("tezcat::TValue Bool can not convert to object");
				break;
			case ValueType::Int:
				throw std::logic_error("tezcat::TValue Int can not convert to object");
				break;
			case ValueType::UInt:
				throw std::logic_error("tezcat::TValue UInt can not convert to object");
				break;
			case ValueType::Float:
				throw std::logic_error("tezcat::TValue Float can not convert to object");
				break;
			case ValueType::Double:
				throw std::logic_error("tezcat::TValue Double can not convert to object");
				break;
			case ValueType::String:
				throw std::logic_error("tezcat::TValue String can not convert to object");
				break;
			case ValueType::ObjPtr:
				return static_cast<T *>(m_Values.ptr);
				break;
			default:
				throw std::logic_error("tezcat::TValue Unknown can not convert to object");
				break;
			}

			return nullptr;
		}

		//*************************
		//
		// 转化数据格式,并返回结果
		// 改变内部数据格式,返回一个副本
		//
		//*************************
	public:
		bool toBool();
		int toInt();
		unsigned int toUInt();
		float toFloat();
		double toDouble();
		std::string toSTDString();

	public:
		bool isNull() const { return m_Type == ValueType::Null; }
		ValueType getType() const { return m_Type; }

	public:
		XValue &operator = (XValue &&other);
		XValue &operator = (const XValue &other);
		XValue &operator = (const bool &b);
		XValue &operator = (const int &i);
		XValue &operator = (const unsigned int &ui);
		XValue &operator = (const float &f);
		XValue &operator = (const double &d);
		XValue &operator = (const std::string &s);
		XValue &operator = (const char *cc);
		XValue &operator = (void *ptr);

	public:
		// +
		float operator + (float f)
		{
			return this->asFloat() + f;
		}

		int operator + (int i)
		{
			return this->asInt() + i;
		}

		unsigned int operator + (unsigned int ui)
		{
			return this->asUInt() + ui;
		}

		double operator + (double d)
		{
			return this->asDouble() + d;
		}

		// -
		float operator - (float f)
		{
			return this->asFloat() - f;
		}

		int operator - (int i)
		{
			return this->asInt() - i;
		}

		unsigned int operator - (unsigned int ui)
		{
			return this->asUInt() - ui;
		}

		double operator - (double d)
		{
			return this->asDouble() - d;
		}

		// *
		float operator * (float f)
		{
			return this->asFloat() * f;
		}

		int operator * (int i)
		{
			return this->asInt() * i;
		}

		unsigned int operator * (unsigned int ui)
		{
			return this->asUInt() * ui;
		}

		double operator * (double d)
		{
			return this->asDouble() * d;
		}

		// /
		float operator / (float f)
		{
			return this->asFloat() / f;
		}

		int operator / (int i)
		{
			return this->asInt() / i;
		}

		unsigned int operator / (unsigned int ui)
		{
			return this->asUInt() / ui;
		}

		double operator / (double d)
		{
			return this->asDouble() * d;
		}

		// +=
		void operator += (float f)
		{
			*this = this->asFloat() + f;
		}

		void operator += (int i)
		{
			*this = this->asInt() + i;
		}

		void operator += (unsigned int ui)
		{
			*this = this->asUInt() + ui;
		}

		void operator += (double d)
		{
			*this = this->asDouble() + d;
		}

		// -=
		void operator -= (float f)
		{
			*this = this->asFloat() - f;
		}

		void operator -= (int i)
		{
			*this = this->asInt() - i;
		}

		void operator -= (unsigned int ui)
		{
			*this = this->asUInt() - ui;
		}

		void operator -= (double d)
		{
			*this = this->asDouble() - d;
		}

		// *=
		void operator *= (float f)
		{
			*this = this->asFloat() * f;
		}

		void operator *= (int i)
		{
			*this = this->asInt() * i;
		}

		void operator *= (unsigned int ui)
		{
			*this = this->asUInt() * ui;
		}

		void operator *= (double d)
		{
			*this = this->asDouble() * d;
		}

		// /=
		void operator /= (float f)
		{
			*this = this->asFloat() / f;
		}

		void operator /= (int i)
		{
			*this = this->asInt() / i;
		}

		void operator /= (unsigned int ui)
		{
			*this = this->asUInt() / ui;
		}

		void operator /= (double d)
		{
			*this = this->asDouble() / d;
		}

	public:
		bool operator == (const XValue &other) const;
		bool operator != (const XValue &other) const;

	private:
		void clear();
		void reset(ValueType type);

	private:
		union __Values
		{
			bool b;
			int i;
			unsigned int ui;
			float f;
			double d;
			void *ptr;
			std::string *str;
		}m_Values;

		ValueType m_Type;

		bool m_IfMove;
	public:
		static XValue NullValue;
	};

	typedef std::unordered_map<std::string, XValue> PropertyMap;
	typedef std::vector<XValue> PropertyArray;
}