#pragma once

#include "XSingleton.h"
#include "XEntity.h"

#include <vector>
#include <functional>
#include <typeinfo>
#include <typeindex>

namespace tezcat
{
	struct XClassInfo
	{
		std::type_info info;
		std::type_index index;
	};

	template<typename MyRTTI_Base>
	class XClassSeqarator
	{
	public:
		XClassSeqarator::XClassSeqarator()
		{
			m_Functions.resize(10, nullptr);
		}

		XClassSeqarator::~XClassSeqarator()
		{
		}

		template<typename MyRTTI>
		void registerClass(const std::function<void(MyRTTI_Base)> &function)
		{
			auto index = MyRTTI::getClassIDStatic();
			if (index > m_Functions.size() - 1)
			{
				m_Functions.resize(index + 5, nullptr);
			}
			m_Functions[index] = function;
		}

		void separate(MyRTTI_Base rtti)
		{
			m_Functions[rtti->getClassID()](rtti);
		}

	private:
		std::vector<std::function<void(MyRTTI_Base)>> m_Functions;
	};

//	typedef XClassSeqarator<XBaseEntity *> XEntitySeqarator;
	typedef XClassSeqarator<XBaseComponent *> XComponentSeqarator;
}

