#pragma once
#include <unordered_map>
#include <string>
#include "XString.h"

namespace tezcat
{
	template<class T>
	class XDirectory
	{
	public:
		XDirectory() : m_Separator('/'), m_Name("default") {}
		XDirectory(const std::string &name) : m_Name(name), m_Separator('/') {}
		~XDirectory() {}

		void addChild(XDirectory<T> *child)
		{
			m_Children.insert(std::make_pair(child->m_Name, child));
		}

		XDirectory<T> *addChild(const std::string &name)
		{
			XDirectory *dir = new XDirectory(name);
			m_Children.insert(std::make_pair(name, dir));
			return dir;
		}

		T getData(const std::string &name)
		{
			auto list = String::split2List(name, m_Separator);
			return m_Data[name];
		}

		XDirectory<T> *getChild(const std::string &name) { return m_Children[name]; }

		void setDefaultSeparator(const char &separator) { m_Separator = separator; }

		void setName(const std::string &name) { m_Name = name; }

	private:
		std::string m_Name;
		char m_Separator;
		std::unordered_map<std::string, XDirectory<T> *> m_Children;
		std::unordered_map<std::string, T> m_Data;
	};
}

