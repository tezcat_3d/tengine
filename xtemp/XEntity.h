#pragma once

#include <vector>
#include <list>
#include <cstdint>
#include <bitset>
#include <tuple>
#include <unordered_map>
#include <functional>
#include <cassert>

//#include "XRTTI.h"
#include "XIDCreator.h"
#include "XComponentWapper.h"

namespace tezcat
{
	template<typename T>
	class XEntityClassIDSeed {};

	template<size_t ComponentCount>
	class XEntity
	{
	public:
		XEntity(XEntity *parent = nullptr) :
			m_GlobalID(XIDCreator<XEntity, size_t>::give()),
			OnUpdataDynamicComponent(nullptr),
			OnUpdataStaticComponent(nullptr)
		{
		}

		~XEntity()
		{

		}

		//
		//静态组件
		//************************************
		// 函数名:  hasStatic
		// 返回值:  bool
		// 功能:    
		//************************************
		template<typename XCom>
		bool hasStatic()
		{
			return m_StaticComponent.template hasComponent<XCom>();
		}

		//************************************
		// 函数名:  getStatic
		// 返回值:  XCom *
		// 功能:    
		//************************************
		template<typename XCom>
		XCom *getStatic()
		{
			return m_StaticComponent.getComponent<XCom>();
		}

		//************************************
		// 函数名:  addStatic
		// 返回值:  void
		// 参数名:  XCom * com
		// 功能:    
		//************************************
		template<typename XCom>
		void addStatic(XCom *com)
		{
			m_StaticComponent.addComponent<XCom>(com);
		}

		//************************************
		// 函数名:  removeStatic
		// 返回值:  void
		// 功能:    
		//************************************
		template<typename XCom>
		void removeStatic()
		{
			m_StaticComponent.removeComponent<XCom>();
		}

		//
		//动态组件
		//************************************
		// 函数名:  hasDynamic
		// 返回值:  bool
		// 功能:    
		//************************************
		template<typename XCom>
		bool hasDynamic()
		{
			return m_DynamicComponent.hasComponent<XCom>();
		}

		//************************************
		// 函数名:  getDynamic
		// 返回值:  XCom *
		// 功能:    
		//************************************
		template<typename XCom>
		XCom *getDynamic()
		{
			return m_DynamicComponent.getComponent<XCom>();
		}

		//************************************
		// 函数名:  addDynamic
		// 返回值:  void
		// 参数名:  XCom * com
		// 功能:    
		//************************************
		template<typename XCom>
		void addDynamic(XCom *com)
		{
			m_DynamicComponent.addComponent<XCom>(com);
		}

		//************************************
		// 函数名:  removeDynamic
		// 返回值:  void
		// 功能:    
		//************************************
		template<typename XCom>
		void removeDynamic()
		{
			m_DynamicComponent.removeComponent<XCom>();
		}


		//************************************
		// 函数名:  updataDynamicComponent
		// 返回值:  void
		// 功能:    
		//************************************
		void updataDynamicComponent()
		{
			auto it = m_DynamicComponent.getLogic()->resetIterator();
			while (it->hasNext())
			{
				if (OnUpdataDynamicComponent)
				{
					OnUpdataDynamicComponent(it->value());
				}
				it->next();
			}
		}

		//************************************
		// 函数名:  updataStaticComponent
		// 返回值:  void
		// 功能:    
		//************************************
		void updataStaticComponent()
		{
			auto it = m_StaticComponent.getLogic()->resetIterator();
			while (it->hasNext())
			{
				if (OnUpdataStaticComponent)
				{
					OnUpdataStaticComponent(it->value());
				}
				it->next();
			}
		}

		//************************************
		// 函数名:  addChild
		// 返回值:  void
		// 参数名:  XEntity * entity
		// 功能:    
		//************************************
		void addChild(XEntity *entity)
		{
			assert(entity->m_Parent == nullptr);
			entity->m_Parent = this;
			m_Children.push_back(entity->m_XListNode);
		}

		//************************************
		// 函数名:  removeChild
		// 返回值:  void
		// 参数名:  XEntity * entity
		// 功能:    
		//************************************
		void removeChild(XEntity *entity)
		{
			assert(entity->m_Parent == this);
			entity->m_Parent = nullptr;
			m_Children.remove(entity->m_XListNode);
		}

		//************************************
		// 函数名:  removeSelf
		// 返回值:  void
		// 功能:    
		//************************************
		void removeSelf()
		{
			m_XListNode->remove();
		}

		//************************************
		// 函数名:  hasChild
		// 返回值:  bool
		// 功能:    
		//************************************
		bool hasChild() const { return m_Children.count() != 0; }

		//************************************
		// 函数名:  getGlobalID
		// 返回值:  size_t &
		// 功能:    
		//************************************
		size_t &getGlobalID() { return m_GlobalID; }

		//************************************
		// 函数名:  getGroupID
		// 返回值:  size_t &
		// 功能:    
		//************************************
		size_t &getGroupID() { return m_GroupID; }

		//************************************
		// 函数名:  getIDInGroup
		// 返回值:  size_t &
		// 功能:    
		//************************************
		size_t &getIDInGroup() { return m_IDInGroup; }

		//************************************
		// 函数名:  getChildren
		// 返回值:  XList<XEntity *> &
		// 功能:    
		//************************************
		typename List &getChildren() { return m_Children; }

		//************************************
		// 函数名:  getXListNode
		// 返回值:  typename XList<XEntity *>::Node *
		// 功能:    
		//************************************
		typename ListNode *getXListNode() const { return m_XListNode; }

		//************************************
		// 函数名:  getParent
		// 返回值:  typename XEntity<ComponentCount> *
		// 功能:    
		//************************************
		typename XEntity *getParent() { return m_Parent; }

	public:
		std::function<void(XBaseComponent *)> OnUpdataDynamicComponent;
		std::function<void(XBaseComponent *)> OnUpdataStaticComponent;

	private:
		XComponentWapper<ComponentCount> m_StaticComponent;		//固定组件
		XComponentWapper<16> m_DynamicComponent;				//动态组件
	};
}