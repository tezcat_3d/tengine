#pragma once

#include <vector>

namespace tezcat
{
	struct XBaseGroup
	{
	protected:
		typedef size_t Kind;
		static Kind m_Kind;
	};

	template<typename CustomGroup>
	struct XGroup : XBaseGroup
	{
		static Kind getKind()
		{
			static Kind kind = m_Kind++;
			std::cout << std::string(typeid(CustomGroup).name()) + " id:" << kind << std::endl;
			return kind;
		}
	};
}