#pragma once

#include <utility>
#include <type_traits>


namespace tezcat
{
	template<typename T>
	class XList
	{
	public:

		struct Node
		{
			~Node()
			{
				m_Parent->remove(this);
				m_Parent = nullptr;
				next = nullptr;
				prev = nullptr;
			}

			void remove()
			{
				m_Parent->remove(this);
			}

			bool isMyParent(XList<T> *parent)
			{
				return m_Parent == parent;
			}

			Node *copy()
			{
				Node *node = new Node();
			}

			auto get() { return value.get(); }

		private:
			explicit Node(XList<T> *parent):
 				next(nullptr), prev(nullptr), m_Parent(parent), m_RefrenceCount(1) {}
			explicit Node(const T &v, XList<T> *parent = nullptr) :
				next(nullptr), prev(nullptr), m_Parent(parent), value(v), m_RefrenceCount(1) {}
			explicit Node(const std::reference_wrapper<T> &wrapper, XList<T> *parent = nullptr) :
				next(nullptr), prev(nullptr), m_Parent(parent), value(wrapper), m_RefrenceCount(1) {}

		private:
			friend class XList<T>;
			Node *next;
			Node *prev;
			XList<T> *m_Parent;
			unsigned int m_RefrenceCount;
			std::reference_wrapper<T> value;
		};

		struct Iterator
		{
			Iterator(Node *node):m_Node(node)
			{
			}

			Iterator() :m_Node(nullptr)
			{
			}

			bool hasNext()
			{
				return m_Node != nullptr;
			}

			void next()
			{
				m_Node = m_Node->next;
			}
			
			Node *node() { return m_Node; }

			auto value() { return m_Node->get(); }

		private:
			friend class XList<T>;
			Node *m_Node;
		};

		typedef Node Wapper;

	public:
		XList() : m_Count(0), m_Iterator(new Iterator()), m_Head(new Node(this)), m_Tail(new Node(this))
		{
// 			m_Head->value = 0;
// 			m_Tail->value = 0;

			m_Head->next = m_Tail;
			m_Tail->prev = m_Head;
		}
		~XList()
		{
			delete m_Iterator;
 			delete m_Head;
 			delete m_Tail;
		}

		//************************************
		// 函数名:  createNode
		// 返回值:  Node *
		// 功能:    
		//************************************
		static Node *createNode(const T &value)
		{
			return new Node(value);
		}

		//************************************
		// 函数名:  push_back
		// 返回值:  Node *
		// 参数名:  const T & value
		// 功能:    
		//************************************
		Node *push_back(const T &value)
		{
			Node *node = new Node(this);
			node->value = value;
			node->prev = m_Tail->prev;
			m_Tail->prev = node;

			m_Count += 1;

			return node;
		}

		//************************************
		// 函数名:  push_back
		// 返回值:  void
		// 参数名:  Node * node
		// 功能:    
		//************************************
		void push_back(Node *node)
		{
			node->m_Parent = this;
			//取得当前尾
			auto prev = m_Tail->prev;
			//当前尾的下一个是新尾
			prev->next = node;
			//新尾的上一个是当前尾
			node->prev = prev;
			//标记尾的前一个是新尾
			m_Tail->prev = node;

			m_Count += 1;
		}

		//************************************
		// 函数名:  push_front
		// 返回值:  Node *
		// 参数名:  const T & value
		// 功能:    
		//************************************
		Node *push_front(const T &value)
		{
			Node *node = new Node(this);
			node->value = value;
			node->next = m_Head->next;
			m_Head->next = node;

			m_Count += 1;

			return node;
		}

		//************************************
		// 函数名:  push_front
		// 返回值:  void
		// 参数名:  Node * node
		// 功能:    
		//************************************
		void push_front(Node *node)
		{
			//防止洗白白~
			node->next = nullptr;
			node->prev = nullptr;

			node->m_Parent = this;
			//取得当前头
			auto next = m_Head->next;
			//当前头的上一个是新头
			next->prev = node;
			//新头的下一个是当前头
			node->next = next;
			//标记头的下一个是新头
			m_Head->next = node;

			m_Count += 1;
		}

		//************************************
		// 函数名:  remove
		// 返回值:  void
		// 参数名:  Node * node
		// 功能:    
		//************************************
		void remove(Node *node)
		{
			if (m_Count == 0)
			{
				return;
			}

			auto prev = node->prev;
			auto next = node->next;

			prev->next = next;
			next->prev = prev;

			//防止洗白白~
			node->next = nullptr;
			node->prev = nullptr;
			node->m_Parent = nullptr;

			m_Count -= 1;
		}

		void find2Remove(Node *node)
		{
			Node *temp = m_Head->next;
			while (temp != node)
			{
				temp = temp->next;
			}
		}

		//************************************
		// 函数名:  frontNode
		// 返回值:  Node *
		// 功能:    获得头结点
		//************************************
		Node *frontNode() { return m_Head->next; }

		//************************************
		// 函数名:  backNode
		// 返回值:  Node *
		// 功能:    获得尾结点
		//************************************
		Node *backNode() { return m_Tail->prev; }

		//************************************
		// 函数名:  popFrontNode
		// 返回值:  Node *
		// 功能:    弹出头并返回
		//************************************
		Node *popFrontNode()
		{
			if (m_Count == 0)
			{
				return nullptr;
			}

			//获得当前头
			auto result = m_Head->next;
			//当前头的下一个是新的头
			m_Head->next = result->next;
			//新头的前一个为空
			m_Head->next->prev = nullptr;
			m_Count -= 1;
			//返回被弹出的头
			return result;
		}

		T &popFrontValue()
		{
			if (m_Count == 0)
			{
				return m_Head->value;
			}

			//获得当前头
			auto result = m_Head->next;
			//当前头的下一个是新的头
			m_Head->next = result->next;
			//新头的前一个为空
			m_Head->next->prev = nullptr;
			m_Count -= 1;
			//返回被弹出的头
			return result->value;
		}

		//************************************
		// 函数名:  popBackNode
		// 返回值:  Node *
		// 功能:    弹出尾并返回
		//************************************
		Node *popBackNode()
		{
			if (m_Count == 0)
			{
				return nullptr;
			}

			//获得当前尾
			auto result = m_Tail->prev;
			//当前尾的上一个是新的尾
			m_Tail->prev = result->prev;
			//新尾的下一个为空
			m_Tail->prev->next = nullptr;
			m_Count -= 1;
			//返回被弹出的尾
			return result;
		}

		T &popBackValue()
		{
			if (m_Count == 0)
			{
				return m_Tail->value;
			}

			//获得当前尾
			auto result = m_Tail->prev;
			//当前尾的上一个是新的尾
			m_Tail->prev = result->prev;
			//新尾的下一个为空
			m_Tail->prev->next = nullptr;
			m_Count -= 1;
			//返回被弹出的尾
			return result->value;
		}

		//************************************
		// 函数名:  front
		// 返回值:  T &
		// 功能:    获得头值
		//************************************
		T &front() { return m_Head->next->value; }

		//************************************
		// 函数名:  back
		// 返回值:  T &
		// 功能:    获得尾值
		//************************************
		T &back() { return m_Tail->prev->value; }

		//************************************
		// 函数名:  clear
		// 返回值:  void
		// 功能:    清空链表但是不删除数据
		//************************************
		void clear()
		{
			m_Head->next = m_Tail;
			m_Tail->prev = m_Head;
			m_Count = 0;
		}

		//************************************
		// 函数名:  clearAndDelete
		// 返回值:  void
		// 功能:    清空链表并删除数据
		//************************************
		void clearAndDelete()
		{
			auto current = m_Head->next;
			while (current != nullptr)
			{
				auto temp = current->next;
				delete current;
				current = temp;
			}

			this->clear();
		}

		//************************************
		// 函数名:  count
		// 返回值:  const size_t &
		// 功能:    
		//************************************
		const size_t &count() const { return m_Count; }

		bool empty() const { return m_Count == 0; }

		Iterator *resetIterator()
		{
//			m_Iterator->m_Node = (m_Count == 0) ? nullptr : m_Head->next;

			
			if (m_Count == 0)
			{
				m_Iterator->m_Node = nullptr;
			}
			else
			{
				m_Iterator->m_Node = m_Head->next;
			}
			
			return m_Iterator;
		}

		Iterator *iterator() { return m_Iterator; }


	private:
		//Head为标记位
		//Head的next才是第一个元素
		Node *m_Head;
		//Tail为标记位
		//Tail的prev才是最后一个元素
		Node *m_Tail;
		//默认迭代器
		Iterator *m_Iterator;
		//数量
		size_t m_Count;
	};

#define XLIST_CREATE_NODE(Type)\
private:\
	XList<Type>::Node *m_XListNode;\
public:\
	XList<Type>::Node *getXListNode() { return m_XListNode; }
}

