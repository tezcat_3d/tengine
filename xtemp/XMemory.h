#pragma once


#include <iostream>
#include <vector>
#include <new>
#include <algorithm>

namespace tezcat
{
	//==============================
	//
	//	根据索引的内存管理
	//
	//==============================

	class XMemory
	{

	};


	class BaseMemory
	{
	public:
		BaseMemory();
		~BaseMemory();

	private:

	};


	//
	//
	class BaseIndexPool
	{
	public:
		BaseIndexPool(std::size_t type_size) : m_TypeSize(type_size) {}
		BaseIndexPool(std::size_t type_size, std::size_t chunk_size) :
			m_TypeSize(type_size), m_ChunkSize(chunk_size), m_Capacity(0), m_Size(0) {}
		virtual ~BaseIndexPool() {}

		void init(std::size_t chunk_size)
		{
			m_ChunkSize = chunk_size;
			m_Capacity = 0;
			m_Size = 0;
		}

		//************************************
		// Method:    expand
		// FullName:  tezcat::BaseIndexPool::expand
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: std::size_t index
		// 扩展容量
		//************************************
		inline void expand(const std::size_t &index)
		{
			if (index + 1 < m_Capacity)
			{
				return;
			}

// 			//多几倍
// 			auto rate = double(index + 1) / double(m_Capacity);
// 			std::cout << rate << std::endl;
// 			//判断小数是否为0
// 			int temp = (int)rate;
// 			std::cout << temp << std::endl;
// 			auto dot = rate - double(temp);
// 			std::cout << dot << std::endl;
// 			if (dot > 0)
// 			{
// 				temp += 1;
// 			}
// 
// 			while (temp--)
// 			{
				char *chunk = new char[m_TypeSize * m_ChunkSize];
				memset(chunk, 0x00, m_TypeSize * m_ChunkSize);
				m_Blocks.push_back(chunk);
				m_Capacity += m_ChunkSize;
				m_Size += 1;
//			}
		}

		//************************************
		// Method:    get
		// FullName:  tezcat::BaseIndexPool::get
		// Access:    public 
		// Returns:   void *
		// Qualifier:
		// Parameter: std::size_t index
		// 根据Index寻找内存
		//************************************
		inline void *get(const std::size_t &index)
		{
			//	index / m_ChunkSize组划分
			//	(index % m_ChunkSize) * m_TypeSize数据偏移
			return m_Blocks[index / m_ChunkSize] + (index % m_ChunkSize) * m_TypeSize;
		}

		//************************************
		// Method:    get
		// FullName:  tezcat::BaseIndexPool::get
		// Access:    public 
		// Returns:   const void *
		// Qualifier: const
		// Parameter: std::size_t index
		// 根据Index寻找内存
		//************************************
		inline const void *get(const std::size_t &index) const
		{
			return m_Blocks[index / m_ChunkSize] + (index % m_ChunkSize) * m_TypeSize;
		}

		//************************************
		// Method:    getTypeSize
		// FullName:  tezcat::BaseIndexPool::getTypeSize
		// Access:    public 
		// Returns:   std::size_t
		// Qualifier: const
		// 类型大小
		//************************************
		std::size_t getTypeSize() const { return m_TypeSize; }

		//************************************
		// Method:    getChunkSize
		// FullName:  tezcat::BaseIndexPool::getChunkSize
		// Access:    public 
		// Returns:   std::size_t
		// Qualifier: const
		// 每一个内存块的大小
		//************************************
		std::size_t getChunkSize() const { return m_ChunkSize; }

		//************************************
		// Method:    getCapacity
		// FullName:  tezcat::BaseIndexPool::getCapacity
		// Access:    public 
		// Returns:   std::size_t
		// Qualifier: const
		// 总容量,即可以存储多少个此类型的对象
		//************************************
		std::size_t getCapacity() const { return m_Capacity; }

		//************************************
		// Method:    getSize
		// FullName:  tezcat::BaseIndexPool::getSize
		// Access:    public 
		// Returns:   std::size_t
		// Qualifier: const
		// 有多少个内存块
		//************************************
		std::size_t getSize() const { return m_Size; }

		//************************************
		// Method:    recycle
		// FullName:  tezcat::BaseIndexPool::recycle
		// Access:    virtual public 
		// Returns:   void
		// Qualifier:
		// Parameter: std::size_t index
		//************************************
		virtual void recycle(const std::size_t &index) = 0;

		//************************************
		// Method:    recycleMemory
		// FullName:  tezcat::BaseIndexPool::recycleMemory
		// Access:    public 
		// Returns:   void
		// Qualifier:
		//************************************
		void recycleMemory()
		{
			for (auto *p : m_Blocks)
			{
				delete p;
			}
		}

	private:
		std::vector<char *> m_Blocks;
		std::size_t m_TypeSize;
		std::size_t m_ChunkSize;
		std::size_t m_Capacity;
		std::size_t m_Size;
	};


	template<typename Type, std::size_t ChunkSize = 8192>
	class TypeIndexPool : public BaseIndexPool
	{
	public:
		TypeIndexPool() : BaseIndexPool(sizeof(Type), ChunkSize) {}
		virtual ~TypeIndexPool() {}

		virtual void recycle(const std::size_t &index) override
		{
			Type *ptr = static_cast<Type*>(this->get(index));
			ptr->~Type();
			memset(ptr, 0x00, sizeof(Type));
		}
	};
}