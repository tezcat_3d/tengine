#pragma once

#include "XIDCreator.h"

namespace tezcat
{
	class XRTTI
	{
	public:
		XRTTI() : m_GlobalID(XIDCreator<XRTTI, size_t>::give()) {};
		virtual ~XRTTI() {}

		const size_t &getGlobalID() const { return m_GlobalID; }
		virtual const size_t &getClassID() = 0;

	protected:
		size_t m_GlobalID;		//ȫ��ID
	};
}